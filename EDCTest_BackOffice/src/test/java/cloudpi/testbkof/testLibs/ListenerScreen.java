package cloudpi.testbkof.testLibs;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import cloudpi.testbkof.testLibs.BaseTest;

public class ListenerScreen extends TestListenerAdapter {
	
//	WebDriver driver = BaseTest.driver ;
	
	private static String fileSeperator = System.getProperty("file.separator");
	
	private  String screenShotPath = "";
	
	@Override
	public void onTestFailure(ITestResult result) {
		System.out.println("***** Error " + result.getName() + " test has failed *****");
		
		WebDriver driver = BaseTest.driver;
//		System.out.println(" BaseTest.driver  --> " + BaseTest.driver);
		
		String testClassName = getTestClassName(result.getInstanceName()).trim();

		String testMethodName = result.getName().toString().trim();

		if (driver != null) {

			  SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HHmmss");
			  String ntime = sdf.format(new Date());  
			  String ndate = ntime.split("-")[0];
			  String nhour = ntime.split("-")[1];
			  screenShotPath = "testReport"+fileSeperator+ ndate + fileSeperator+ "FailureScreen" 
			           + fileSeperator + testClassName+ fileSeperator;
			   String screenShotName = "S"+nhour+"_"+testMethodName + ".png";
			   
//				screenShotPath = ".." + fileSeperator + "Screenshots"
//						+ fileSeperator + "Results" + fileSeperator + testClassName
//						+ fileSeperator;  // Original path 
				
				takeScreenShot(driver,screenShotPath,screenShotName);
				
				System.out.println("Screenshot can be found : in " + screenShotPath);
		}
	}

	public  void  takeScreenShot(WebDriver driver,String screenShotPath,String screenShotName ) {
		
		try {
			File file = new File(screenShotPath);
			if (!file.exists()) {
//				System.out.println("File created " + file);
				file.mkdir();
			}

			File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			File targetFile = new File(screenShotPath, screenShotName);
			FileUtils.copyFile(screenshotFile, targetFile);

		} catch (Exception e) {
			System.out.println("An exception occured while taking screenshot " + e.getCause());
		}
	}

	public String getTestClassName(String testName) {
		String[] reqTestClassname = testName.split("\\.");
		int i = reqTestClassname.length - 1;
		System.out.println("Required Test Class  Name : " + reqTestClassname[i]);
		return reqTestClassname[i];
	}

}