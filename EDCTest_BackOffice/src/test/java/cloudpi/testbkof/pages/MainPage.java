package cloudpi.testbkof.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import cloudpi.testbkof.pages.MainPage;

import org.apache.log4j.Logger;

public  class MainPage extends cloudpi.testbkof.pages.BasePage  {
	
	public  static  WebElement element = null;
	
	public MainPage(WebDriver driver) {
		super(driver);
	}

	static Logger log = Logger.getLogger(MainPage.class.getName());
	
	public  String getPageTitle(){
		return driver.getTitle();
	}
}
