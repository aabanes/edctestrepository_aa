package cloudpi.testbkof.pages;

import java.io.IOException;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import cloudpi.testbkof.pages.RepositoryElements;

public class EDC_BackOffice  extends cloudpi.testbkof.pages.MainPage{
	private RepositoryElements repoBKElements = null;
	
	public EDC_BackOffice(WebDriver driver) {
		super(driver);
		
		try {
			repoBKElements = new RepositoryElements("../EDCTest_BackOffice/config/BackOffice_Repository.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static WebElement element = null;
	
	public WebElement BKOF_Login(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Login"));
		return element;
	}
	
	public WebElement BKOF_Password(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Password"));
		return element;
	}
	
	public WebElement BKOF_LoginBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_LoginBtn"));
		return element;
	}
	
	//Change Password page
	public WebElement BKOF_ChangePassword_OldPassword(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_ChangePassword_OldPassword"));
		return element;
	}
	
	public WebElement BKOF_ChangePassword_NewPassword(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_ChangePassword_NewPassword"));
		return element;
	}
	
	public WebElement BKOF_ChangePassword_CnfmPassword(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_ChangePassword_CnfmPassword"));
		return element;
	}

	public WebElement BKOF_ChangePassword_SubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_ChangePassword_SubmitBtn"));
		return element;
	}
	
	
	
	public WebElement BKOF_Lookup(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Lookup"));
		return element;
	}
	
	public WebElement BKOF_MyProfileLink(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MyProfileLink"));
		return element;
	}
	
	public WebElement BKOF_Gross_Exposure(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Gross_Exposure"));
		return element;
	}
	
	public WebElement BKOF_Total_Exposure(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Total_Exposure"));
		return element;
	}
	
	public WebElement BKOF_Header(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Header"));
		return element;
	}

	public WebElement BKOF_Menu_Identity(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Menu_Identity"));
		return element;
	}
	
	public WebElement BKOF_MainMenu(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Commericial(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Commericial"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Risk(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Risk"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Claims(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Claims"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Administration(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Administration"));
		return element;
	}
	
	public WebElement BKOF_ExitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_ExitBtn"));
		return element;
	}
	
	//Task Menu
	public WebElement BKOF_Risk_TaskMenu_All(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_TaskMenu_All"));
		return element;
	}
	
	public WebElement BKOF_Risk_TaskMenu_Applications(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_TaskMenu_Applications"));
		return element;
	}
	
	public WebElement BKOF_Risk_TaskMenu_Monitoring(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_TaskMenu_Monitoring"));
		return element;
	}
	
	public WebElement BKOF_Risk_TaskMenu_ResponseApprovals(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_TaskMenu_ResponseApprovals"));
		return element;
	}
	
	public WebElement BKOF_Risk_TaskMenu_ReviewApprovals(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_TaskMenu_ReviewApprovals"));
		return element;
	}
	
	public WebElement BKOF_Risk_TaskMenu_Identifications(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_TaskMenu_Identifications"));
		return element;
	}
	
	public WebElement BKOF_Risk_TaskMenu_Assessment(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_TaskMenu_Assessment"));
		return element;
	}
	
	public WebElement BKOF_Risk_TaskMenu_Submission(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_TaskMenu_Submission"));
		return element;
	}
	
	public WebElement BKOF_Risk_TaskMenu_Presubmission(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_TaskMenu_Presubmission"));
		return element;
	}
	
	public WebElement BKOF_Risk_TaskMenu_Quote(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_TaskMenu_Quote"));
		return element;
	}
	
	public WebElement BKOF_Risk_TaskMenu_Policy(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_TaskMenu_Policy"));
		return element;
	}
	
	public WebElement BKOF_Risk_TaskMenu_Other(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_TaskMenu_Other"));
		return element;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//Menu - Commercial Menu
	public WebElement BKOF_MainMenu_Commericial_Workflow(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Commericial_Workflow"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Commericial_Underwriting(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Commericial_Underwriting"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Commericial_Portfolio(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Commericial_Portfolio"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Commericial_View(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Commericial_View"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Commericial_Analytics(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Commericial_Analytics"));
		return element;
	}
	
	
	//My Contacts - User Access tab
	public WebElement BKOF_Contact_UserAccess_BeforeSubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_UserAccess_BeforeSubmitBtn"));
		return element;
	}
	
	public WebElement BKOF_Contact_UserAccess_Active(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_UserAccess_Active"));
		return element;
	}
	
	public WebElement BKOF_Contact_UserAccess_AddProfileBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_UserAccess_AddProfileBtn"));
		return element;
	}
	
	public WebElement BKOF_Contact_UserAccess_AddProfile_ProfileDropDown(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_UserAccess_AddProfile_ProfileDropDown"));
		return element;
	}
	
	public WebElement BKOF_Contact_UserAccess_AddProfile_SubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_UserAccess_AddProfile_SubmitBtn"));
		return element;
	}
	
	public WebElement BKOF_Contact_UserAccess_SubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_UserAccess_SubmitBtn"));
		return element;
	}
	
	public WebElement BKOF_Contact_UserAccessTab_ProfileTable(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_UserAccessTab_ProfileTable"));
		return element;
	}
	
	
	// My Profile Page //
	public WebElement BKOF_MyProfile_IDUsrAccessTab(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MyProfile_IDUsrAccessTab"));
		return element;
	}
	
	public WebElement BKOF_MyProfile_AuthoritiesTab(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MyProfile_AuthoritiesTab"));
		return element;
	}
	
	public WebElement BKOF_MyProfile_CntryGradeTab(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MyProfile_CntryGradeTab"));
		return element;
	}
	
	public WebElement BKOF_MyProfile_IDUsrAccessTab_DateFormat(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MyProfile_IDUsrAccessTab_DateFormat"));
		return element;
	}
	
	public WebElement BKOF_MyProfile_IDUsrAccessTab_SubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MyProfile_IDUsrAccessTab_SubmitBtn"));
		return element;
	}
	
	public WebElement BKOF_MyProfile_CloseBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MyProfile_CloseBtn"));
		return element;
	}
	
	// My Profile Page - ID Card and User Access tab //
	public WebElement BKOF_MyProfile_IDUsrAccessTab_Avaliability(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MyProfile_IDUsrAccessTab_Avaliability"));
		return element;
	}
	
	public WebElement BKOF_MyProfile_IDUsrAccessTab_OldPassText(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MyProfile_IDUsrAccessTab_OldPassText"));
		return element;
	}
	
	
	public WebElement BKOF_MyProfile_IDUsrAccessTab_NewPassText(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MyProfile_IDUsrAccessTab_NewPassText"));
		return element;
	}
	
	
	public WebElement BKOF_MyProfile_IDUsrAccessTab_CnfmNewPassText(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MyProfile_IDUsrAccessTab_CnfmNewPassText"));
		return element;
	}
	
	
	// Commercial Menu //
	public WebElement BKOF_MainMenu_Commerical_Workflow(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Commerical_Workflow"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Commerical_UnderWriting(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Commerical_UnderWriting"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Commerical_Portfolio(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Commerical_Portfolio"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Commerical_View(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Commerical_View"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Commerical_Analytics(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Commerical_Analytics"));
		return element;
	}
	
	// Commericial -> Workflow menu
	public WebElement BKOF_MainMenu_Commerical_Workflow_MyTasks(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Commerical_Workflow_MyTasks"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Commerical_Workflow_MyWorkBasket(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Commerical_Workflow_MyWorkBasket"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Commerical_Workflow_TeamTasks(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Commerical_Workflow_TeamTasks"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Commerical_Workflow_UnassignedTasks(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Commerical_Workflow_UnassignedTasks"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Commerical_Workflow_AllTasks(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Commerical_Workflow_AllTasks"));
		return element;
	}
	
	
	
	
	public WebElement BKOF_Menu_Group(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Menu_Group"));
		return element;
	}
	
	public WebElement BKOF_Menu_Reports(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Menu_Reports"));
		return element;
	}
	
	public WebElement BKOF_Menu_Events(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Menu_Events"));
		return element;
	}
	
	public WebElement BKOF_Menu_Comments(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Menu_Comments"));
		return element;
	}
	
	public WebElement BKOF_Menu_Limits(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Menu_Limits"));
		return element;
	}
	
	public WebElement BKOF_Menu_Assessment(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Menu_Assessment"));
		return element;
	}
	
	public WebElement BKOF_Menu_Tasks(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Menu_Tasks"));
		return element;
	}
	
	public WebElement BKOF_Menu_Declarations(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Menu_Declarations"));
		return element;
	}
	
	public WebElement BKOF_Task_New_Assignee(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Task_New_Assignee"));
		return element;
	}
	
	public WebElement BKOF_Task_New_Assignee_SubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Task_New_Assignee_SubmitBtn"));
		return element;
	}
	
	public WebElement BKOF_Demand_ReqAmt(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Demand_ReqAmt"));
		return element;
	}
	
	public WebElement BKOF_Demand_Notes(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Demand_Notes"));
		return element;
	}
	
	public WebElement BKOF_Task_AutoComplete(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Task_AutoComplete"));
		return element;
	}
	
	public WebElement BKOF_Task_DeclinePreSubmission(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Task_DeclinePreSubmission"));
		return element;
	}
	
	public WebElement BKOF_Task_CreateSubmission(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Task_CreateSubmission"));
		return element;
	}
	
	public WebElement BKOF_Task_DeclinePreSubmissionLabel(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Task_DeclinePreSubmissionLabel"));
		return element;
	}
	
	public WebElement BKOF_Task_CreateSubmissionLabel(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Task_CreateSubmissionLabel"));
		return element;
	}
	
	public WebElement BKOF_Task_SubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Task_SubmitBtn"));
		return element;
	}

	public WebElement BKOF_Task_DeclinePreSubmissionPopup_ReasonSelect(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Task_DeclinePreSubmissionPopup_ReasonSelect"));
		return element;
	}
	
	public WebElement BKOF_Task_DeclinePreSubmissionPopup_Comment(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Task_DeclinePreSubmissionPopup_Comment"));
		return element;
	}
	
	public WebElement BKOF_Task_DeclinePreSubmission_SubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Task_DeclinePreSubmission_SubmitBtn"));
		return element;
	}
	public WebElement BKOF_Task_CreateSubmission_SubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Task_CreateSubmission_SubmitBtn"));
		return element;
	}
	public WebElement BKOF_Submission_Cancel_CnfmSubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_Cancel_CnfmSubmitBtn"));
		return element;
	}
	
	public WebElement BKOF_Submission_SaveBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_SaveBtn"));
		return element;
	}
	
	
	public WebElement BKOF_Task_ViewSubmissionBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Task_ViewSubmissionBtn"));
		return element;
	}
	
	
	//Submission page
	public WebElement BKOF_Submission_Status(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_Status"));
		return element;
	}
	
	public WebElement BKOF_Submission_GeneralInfo_PolicyAllocation_Insurer(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_PolicyAllocation_Insurer"));
		return element;
	}
	
	public WebElement BKOF_Submission_GeneralInfo_PolicyAllocation_Office(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_PolicyAllocation_Office"));
		return element;
	}

	public WebElement BKOF_Submission_GeneralInfo_PolicyAllocation_CommOwner(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_PolicyAllocation_CommOwner"));
		return element;
	}
	
	public WebElement BKOF_Submission_GeneralInfo_Broker_FilterPerCountry(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_Broker_FilterPerCountry"));
		return element;
	}
	
	public WebElement BKOF_Submission_GeneralInfo_Broker_CompanyName(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_Broker_CompanyName"));
		return element;
	}
	
	public WebElement BKOF_Submission_GeneralInfo_Broker_NationalID(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_Broker_NationalID"));
		return element;
	}
	
	public WebElement BKOF_Submission_GeneralInfo_Broker_Address(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_Broker_Address"));
		return element;
	}
	
	public WebElement BKOF_Submission_GeneralInfo_Broker_ContactName(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_Broker_ContactName"));
		return element;
	}
	
	
	public WebElement BKOF_Submission_GeneralInfo_Broker_NewUsrBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_Broker_NewUsrBtn"));
		return element;
	}
	
	
	public WebElement BKOF_Submission_GeneralInfo_Broker_PhoneNum(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_Broker_PhoneNum"));
		return element;
	}
	
	public WebElement BKOF_Submission_GeneralInfo_Broker_FaxNum(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_Broker_FaxNum"));
		return element;
	}
	
	public WebElement BKOF_Submission_GeneralInfo_Broker_Email(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_Broker_Email"));
		return element;
	}
	
	//Submission Product fields
	public WebElement BKOF_Submission_GeneralInfo_Product_StartDateField(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_Product_StartDateField"));
		return element;
	}
	
	
	public WebElement BKOF_Submission_GeneralInfo_Product_StartDatePickerBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_Product_StartDatePickerBtn"));
		return element;
	}
	
	
	public WebElement BKOF_Submission_GeneralInfo_Product_StartDatePickerNum(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_Product_StartDatePickerNum"));
		return element;
	}
	
	public WebElement BKOF_Submission_GeneralInfo_Product_DatePicker_Days(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_Product_DatePicker_Days"));
		return element;
	}
	
	
	public WebElement BKOF_Submission_GeneralInfo_Product_DatePicker_Month(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_Product_DatePicker_Month"));
		return element;
	}
	
	
	public WebElement BKOF_Submission_GeneralInfo_Product_DatePicker_Year(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_Product_DatePicker_Year"));
		return element;
	}
	
	public WebElement BKOF_Submission_GeneralInfo_Product_RenewalDuration(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_Product_RenewalDuration"));
		return element;
	}
	
	
	public WebElement BKOF_Submission_GeneralInfo_Product_RenewalDurationField(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_Product_RenewalDurationField"));
		return element;
	}
	
	
	public WebElement BKOF_Submission_GeneralInfo_Product_RenewalDate(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_Product_RenewalDate"));
		return element;
	}
	
	
	public WebElement BKOF_Submission_GeneralInfo_Product_RenewalDateField(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_Product_RenewalDateField"));
		return element;
	}
	
	public WebElement BKOF_Submission_GeneralInfo_Product_RenewalDatePickerBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_GeneralInfo_Product_RenewalDatePickerBtn"));
		return element;
	}
	
	
	
	public WebElement BKOF_Submission_CancelBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_CancelBtn"));
		return element;
	}
	
	public WebElement BKOF_Submission_CompanyName(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_CompanyName"));
		return element;
	}
	
	public WebElement BKOF_Submission_ApplicantTab_ContactDropdown(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_ApplicantTab_ContactDropdown"));
		return element;
	}
	
	public WebElement BKOF_Submission_ApplicantTab_PhoneNum(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_ApplicantTab_PhoneNum"));
		return element;
	}
	
	public WebElement BKOF_Submission_ApplicantTab_FaxNum(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_ApplicantTab_FaxNum"));
		return element;
	}
	
	public WebElement BKOF_Submission_ApplicantTab_Email(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_ApplicantTab_Email"));
		return element;
	}
	
	//Submission -> Applicant Tab -> New User
	public WebElement BKOF_Submission_ApplicantTab_NewBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_ApplicantTab_NewBtn"));
		return element;
	}
	
	public WebElement BKOF_Submission_ApplicantTab_NewUsrPopup_Title(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_ApplicantTab_NewUsrPopup_Title"));
		return element;
	}
	
	public WebElement BKOF_Submission_ApplicantTab_NewUsrPopup_LastName(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_ApplicantTab_NewUsrPopup_LastName"));
		return element;
	}
	

	public WebElement BKOF_Submission_ApplicantTab_NewUsrPopup_FirstName(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_ApplicantTab_NewUsrPopup_FirstName"));
		return element;
	}
	

	public WebElement BKOF_Submission_ApplicantTab_NewUsrPopup_Phone(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_ApplicantTab_NewUsrPopup_Phone"));
		return element;
	}
	

	public WebElement BKOF_Submission_ApplicantTab_NewUsrPopup_Fax(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_ApplicantTab_NewUsrPopup_Fax"));
		return element;
	}
	

	public WebElement BKOF_Submission_ApplicantTab_NewUsrPopup_Position(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_ApplicantTab_NewUsrPopup_Position"));
		return element;
	}
	

	public WebElement BKOF_Submission_ApplicantTab_NewUsrPopup_Department(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_ApplicantTab_NewUsrPopup_Department"));
		return element;
	}
	

	public WebElement BKOF_Submission_ApplicantTab_NewUsrPopup_Language(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_ApplicantTab_NewUsrPopup_Language"));
		return element;
	}
	

	public WebElement BKOF_Submission_ApplicantTab_NewUsrPopup_DateFormat(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_ApplicantTab_NewUsrPopup_DateFormat"));
		return element;
	}
	

	public WebElement BKOF_Submission_ApplicantTab_NewUsrPopup_Email(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_ApplicantTab_NewUsrPopup_Email"));
		return element;
	}
	

	public WebElement BKOF_Submission_ApplicantTab_NewUsrPopup_SubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Submission_ApplicantTab_NewUsrPopup_SubmitBtn"));
		return element;
	}
	
	
	
	// Commercial -> Underwriting Menu
	public WebElement BKOF_MainMenu_Commerical_UnderWriting_PreSubmissionList(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Commerical_UnderWriting_PreSubmissionList"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Commerical_UnderWriting_SubmissionList(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Commerical_UnderWriting_SubmissionList"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Commerical_UnderWriting_QuotesList(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Commerical_UnderWriting_QuotesList"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Commerical_UnderWriting_NewSubmission(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Commerical_UnderWriting_NewSubmission"));
		return element;
	}
	
	// Commercial -> Underwriting -> PreSubmission page.
	public WebElement BKOF_Commercial_UnderWriting_PreSubmission_Filter_PreSubID(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Commercial_UnderWriting_PreSubmission_Filter_PreSubID"));
		return element;
	}
	
	public WebElement BKOF_Commercial_UnderWriting_PreSubmission_Filter_SubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Commercial_UnderWriting_PreSubmission_Filter_SubmitBtn"));
		return element;
	}
	
	
	
	
	
	public WebElement BKOF_Decision_Area(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Decision_Area"));
		return element;
	}
	
	public WebElement BKOF_Decision_Accept(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Decision_Accept"));
		return element;
	}
	
	public WebElement BKOF_Decision_PartialAccept(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Decision_PartialAccept"));
		return element;
	}
	
	public WebElement BKOF_Decision_PartialAccept_Amt(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Decision_PartialAccept_Amt"));
		return element;
	}
	
	public WebElement BKOF_Decision_Reject(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Decision_Reject"));
		return element;
	}
	
	public WebElement BKOF_Decision_SubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Decision_SubmitBtn"));
		return element;
	}
	
	public WebElement BKOF_Decision_CancelReqBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Decision_CancelReqBtn"));
		return element;
	}
	
	public WebElement BKOF_Decision_CfrmPopup_CancelBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Decision_CfrmPopup_CancelBtn"));
		return element;
	}
	
	public WebElement BKOF_Decision_CfrmPopup_CfrmSubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Decision_CfrmPopup_CfrmSubmitBtn"));
		return element;
	}
	
	public WebElement BKOF_Task_Table(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Task_Table"));
		return element;
	}
	
	public WebElement BKOF_SearchWindow(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_SearchWindow"));
		return element;
	}
	
	public WebElement BKOF_SearchWindow_InternalSearchLink(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_SearchWindow_InternalSearchLink"));
		return element;
	}
	
	public WebElement BKOF_SearchWindow_ExternalSearchLink(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_SearchWindow_ExternalSearchLink"));
		return element;
	}
	
	public WebElement BKOF_SearchWindow_DirectSearchLink(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_SearchWindow_DirectSearchLink"));
		return element;
	}
	
	public WebElement BKOF_SearchWindow_DirectSearch_Ref(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_SearchWindow_DirectSearch_Ref"));
		return element;
	}
	
	public WebElement BKOF_SearchWindow_SubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_SearchWindow_SubmitBtn"));
		return element;
	}
	
	public WebElement BKOF_Menu_Header(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Menu_Header"));
		return element;
	}
	
	public WebElement BKOF_SearchIcon(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_SearchIcon"));
		return element;
	}
	
	
	//Menu - Administration
	//## BKOF_Administration ##
	public WebElement BKOF_MainMenu_Administration_PplOrg(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Administration_Ppl&Org"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Administration_ProdMgmt(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Administration_ProdMgmt"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Administration_Settings(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Administration_Settings"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Administration_RefData(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Administration_RefData"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Administration_Analytics(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Administration_Analytics"));
		return element;
	}

	// ## BKOF_Administration_People&Organization_Menu ##
	public WebElement BKOF_MainMenu_Administration_PplOrg_InsuredList(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Administration_Ppl&Org_InsuredList"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Administration_PplOrg_InsurerList(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Administration_Ppl&Org_InsurerList"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Administration_PplOrg_BrokerList(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Administration_Ppl&Org_BrokerList"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Administration_PplOrg_AssigneeList(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Administration_Ppl&Org_AssigneeList"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Administration_PplOrg_OperatingComp(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Administration_Ppl&Org_OperatingComp"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Administration_PplOrg_UserProfile(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Administration_Ppl&Org_UserProfile"));
		return element;
	}
	
	
	public WebElement BKOF_PplOrg_InsuredList_FilterBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Ppl&Org_InsuredList_FilterBtn"));
		return element;
	}
	
	public WebElement BKOF_PplOrg_InsuredList_FilterSearch_Email(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Ppl&Org_InsuredList_FilterSearch_Email"));
		return element;
	}
	
	public WebElement BKOF_PplOrg_InsuredList_FilterSearch_Name(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Ppl&Org_InsuredList_FilterSearch_Name"));
		return element;
	}
	
	public WebElement BKOF_PplOrg_InsuredList_FilterSearch_SubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Ppl&Org_InsuredList_FilterSearch_SubmitBtn"));
		return element;
	}
	
	public WebElement BKOF_PplOrg_InsuredList_FilterSearch_Results(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Ppl&Org_InsuredList_FilterSearch_Results"));
		return element;
	}
	
	// Administration - People & Organization - User Profile
	public WebElement BKOF_PplOrg_UserProfile_Menu(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_PplOrg_UserProfile_Menu"));
		return element;
	}
	
	public WebElement BKOF_PplOrg_UserProfile_ProfileList(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_PplOrg_UserProfile_ProfileList"));
		return element;
	}
	
	public WebElement BKOF_PplOrg_UserProfile_Menu_FrontOfficeTab(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_PplOrg_UserProfile_Menu_FrontOfficeTab"));
		return element;
	}
	
	public WebElement BKOF_PplOrg_UserProfile_Menu_BackOfficeTab(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_PplOrg_UserProfile_Menu_BackOfficeTab"));
		return element;
	}
	
	public WebElement BKOF_PplOrg_UserProfile_NewProfileBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_PplOrg_UserProfile_NewProfileBtn"));
		return element;
	}
	
	public WebElement BKOF_PplOrg_UserProfile_NewProfilePopup_ProfileName(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_PplOrg_UserProfile_NewProfilePopup_ProfileName"));
		return element;
	}
	
	public WebElement BKOF_PplOrg_UserProfile_NewProfilePopup_SubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_PplOrg_UserProfile_NewProfilePopup_SubmitBtn"));
		return element;
	}
	
	public WebElement BKOF_PPlOrg_UserProfile_PermissionsTab(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_PPlOrg_UserProfile_PermissionsTab"));
		return element;
	}
	
	public WebElement BKOF_PPlOrg_UserProfile_EventsTab(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_PPlOrg_UserProfile_EventsTab"));
		return element;
	}
	
	public WebElement BKOF_PplOrg_UserProfile_EventsList(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_PplOrg_UserProfile_EventsList"));
		return element;
	}
	
	//Administration - Workbasket
	public WebElement BKOF_MainMenu_Administration_Settings_WrkBsktSet(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Administration_Settings_WrkBsktSet"));
		return element;
	}
	
	
	public WebElement BKOF_MainMenu_Administration_Settings_WrkRlsSet(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Administration_Settings_WrkRlsSet"));
		return element;
	}
	
	public WebElement BKOF_MainMenu_Administration_Settings_WkflAudTrl(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_MainMenu_Administration_Settings_WkflAudTrl"));
		return element;
	}
	
	public WebElement BKOF_Administration_WorkBasket_Header(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Administration_WorkBasket_Header"));
		return element;
	}
	
	public WebElement BKOF_Administration_WorkBasket_WorkBasketList(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Administration_WorkBasket_WorkBasketList"));
		return element;
	}
	
	public WebElement BKOF_Administration_WorkBasket_WorkBasketDetailContent(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Administration_WorkBasket_WorkBasketDetailContent"));
		return element;
	}
	
	public WebElement BKOF_Administration_WorkBasket_AddBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Administration_WorkBasket_AddBtn"));
		return element;
	}
	
	public WebElement BKOF_Administration_WorkBasket_AddWkBst_Code(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Administration_WorkBasket_AddWkBst_Code"));
		return element;
	}
	public WebElement BKOF_Administration_WorkBasket_AddWkBst_Description(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Administration_WorkBasket_AddWkBst_Description"));
		return element;
	}
	
	public WebElement BKOF_Administration_WorkBasket_AddWkBst_SelectStrategy(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Administration_WorkBasket_AddWkBst_SelectStrategy"));
		return element;
	}
	
	public WebElement BKOF_Administration_WorkBasket_AddWkBst_AuthorityLevel(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Administration_WorkBasket_AddWkBst_AuthorityLevel"));
		return element;
	}
	
	public WebElement BKOF_Administration_WorkBasket_AddWkBst_UserTable(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Administration_WorkBasket_AddWkBst_UserTable"));
		return element;
	}
	
	public WebElement BKOF_Administration_WorkBasket_AddWkBst_AddBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Administration_WorkBasket_AddWkBst_AddBtn"));
		return element;
	}
	
	public WebElement BKOF_Administration_WorkBasket_AddWkBst_SubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Administration_WorkBasket_AddWkBst_SubmitBtn"));
		return element;
	}
	
	
	
	// Risk -> Portfolio
	public WebElement BKOF_Risk_Portfolio(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio"));
		return element;
	}
	public WebElement BKOF_Risk_Portfolio_Buyers(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_Buyers"));
		return element;
	}
	public WebElement BKOF_Risk_Portfolio_TempBuyersHistory(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_TempBuyersHistory"));
		return element;
	}
	public WebElement BKOF_Risk_Portfolio_Limits(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_Limits"));
		return element;
	}
	public WebElement BKOF_Risk_Portfolio_PolicyList(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_PolicyList"));
		return element;
	}
	public WebElement BKOF_Risk_Portfolio_InsuredList(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_InsuredList"));
		return element;
	}
	public WebElement BKOF_Risk_Portfolio_Groups(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_Groups"));
		return element;
	}
	
	public WebElement BKOF_Risk_Portfolio_InsuredList_SearchResults(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_InsuredList_SearchResults"));
		return element;
	}
	
	public WebElement BKOF_Risk_Portfolio_InsuredList_CreateInsurerBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_InsuredList_CreateInsurerBtn"));
		return element;
	}
	
	public WebElement BKOF_Risk_Portfolio_InsuredList_CreateInsurer_header(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_InsuredList_CreateInsurer_header"));
		return element;
	}
	
	public WebElement BKOF_Risk_Portfolio_InsuredList_CreateInsurer_SubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_InsuredList_CreateInsurer_SubmitBtn"));
		return element;
	}
	
	public WebElement BKOF_Risk_Portfolio_InsuredList_CreateInsurer_ManualCreateLink(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_InsuredList_CreateInsurer_ManualCreateLink"));
		return element;
	}
	
	public WebElement BKOF_Risk_Portfolio_InsuredList_CreateInsurer_CompanyName(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_InsuredList_CreateInsurer_ManualCreateLink"));
		return element;
	}
	
	public WebElement BKOF_Risk_Portfolio_InsuredList_CreateInsurer_ActivityCode(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_InsuredList_CreateInsurer_ActivityCode"));
		return element;
	}
	
	
	public WebElement BKOF_Risk_Portfolio_InsuredList_CreateInsurer_Address(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_InsuredList_CreateInsurer_Address"));
		return element;
	}
	
	
	public WebElement BKOF_Risk_Portfolio_InsuredList_CreateInsurer_zipCode(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_InsuredList_CreateInsurer_zipCode"));
		return element;
	}
	
	
	public WebElement BKOF_Risk_Portfolio_InsuredList_CreateInsurer_city(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_InsuredList_CreateInsurer_city"));
		return element;
	}
	
	public WebElement BKOF_Risk_Portfolio_InsuredList_CreateInsurer_phoneNum(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_InsuredList_CreateInsurer_phoneNum"));
		return element;
	}
	

	
	
	
	public WebElement BKOF_Contact_UserTable(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_UserTable"));
		return element;
	}
	
	public WebElement BKOF_Contact_AddUserBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_AddUserBtn"));
		return element;
	}
	
	
	public WebElement BKOF_Menu_Contact(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Menu_Contact"));
		return element;
	}
	
	public WebElement BKOF_Contact_IDCardTab(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_IDCardTab"));
		return element;
	}
	
	public WebElement BKOF_Contact_UserAccessTab(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_UserAccessTab"));
		return element;
	}
	
	public WebElement BKOF_Contact_UserActive(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_UserActive"));
		return element;
	}
	
	
	
	
	
	
	
	
	
	public WebElement BKOF_Contact_IDCard_Content(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_IDCard_Content"));
		return element;
	}
	public WebElement BKOF_Contact_IDCard_Title(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_IDCard_Title"));
		return element;
	}
	public WebElement BKOF_Contact_IDCard_FirstName(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_IDCard_FirstName"));
		return element;
	}
	public WebElement BKOF_Contact_IDCard_LastName(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_IDCard_LastName"));
		return element;
	}
	public WebElement BKOF_Contact_IDCard_JobTitle(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_IDCard_JobTitle"));
		return element;
	}
	public WebElement BKOF_Contact_IDCard_Department(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_IDCard_Department"));
		return element;
	}
	public WebElement BKOF_Contact_IDCard_PhoneNum(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_IDCard_PhoneNum"));
		return element;
	}
	public WebElement BKOF_Contact_IDCard_FaxNum(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_IDCard_FaxNum"));
		return element;
	}
	public WebElement BKOF_Contact_IDCard_Email(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_IDCard_Email"));
		return element;
	}
	public WebElement BKOF_Contact_IDCard_Language(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_IDCard_Language"));
		return element;
	}
	
	public WebElement BKOF_Contact_IDCard_DateFormat(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_IDCard_DateFormat"));
		return element;
	}
	
	public WebElement BKOF_Contact_IDCard_SubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_IDCard_SubmitBtn"));
		return element;
	}
	
	public WebElement BKOF_Contact_IDCard_ProvideUserAccessBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_IDCard_ProvideUserAccessBtn"));
		return element;
	}
	
	public WebElement BKOF_Contact_Message(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_Message"));
		return element;
	}
	
	public WebElement BKOF_Contact_UserAccess_NewPassword(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_UserAccess_NewPassword"));
		return element;
	}
	
	public WebElement BKOF_Contact_UserAccess_Login(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Contact_UserAccess_Login"));
		return element;
	}
	
	public WebElement BKOF_Menu_Policies(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Menu_Policies"));
		return element;
	}
	
	//Submission menu
	public WebElement BKOF_Menu_Submission_Events(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Menu_Submission_Events"));
		return element;
	}
	
	
	//Preferences
	public WebElement BKOF_Menu_Preferences(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Menu_Preferences"));
		return element;
	}
	
	public WebElement BKOF_Preferences_PreferencesTab(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Preferences_PreferencesTab"));
		return element;
	}
	
	public WebElement BKOF_Preferences_PreferencesWithProfileTab(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Preferences_PreferencesWithProfileTab"));
		return element;
	}
	
	public WebElement BKOF_Preferences_ReportsTab(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Preferences_ReportsTab"));
		return element;
	}
	
	public WebElement BKOF_Preferences_MailSettingsTab(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Preferences_MailSettingsTab"));
		return element;
	}
	
	// BKOF Preferences - Preferences With Profile tab //
	public WebElement BKOF_Preferences_PreferencesWithProfileTab_SelectHomepage(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Preferences_PreferencesWithProfileTab_SelectHomepage"));
		return element;
	}
	
	public WebElement BKOF_Preferences_PreferencesWithProfileTab_SelectPortfolio(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Preferences_PreferencesWithProfileTab_SelectPortfolio"));
		return element;
	}
	
	public WebElement BKOF_Preferences_PreferencesWithProfileTab_ApplyToAllProfiles(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Preferences_PreferencesWithProfileTab_ApplyToAllProfiles"));
		return element;
	}
	
	public WebElement BKOF_Preferences_PreferencesWithProfileTab_SubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Preferences_PreferencesWithProfileTab_SubmitBtn"));
		return element;
	}
	
	//Events
	public WebElement BKOF_Events_EntityyMgmt(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Events_EntityyMgmt"));
		return element;
	}
	
	
	//Perimeter
	
	public WebElement BKOF_Menu_Perimeter(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Menu_Perimeter"));
		return element;
	}
	
	
	public WebElement BKOF_Perimeter_PortalAccess_AccessToTPChbx(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Perimeter_PortalAccess_AccessToTPChbx"));
		return element;
	}
	
	
	public WebElement BKOF_Perimeter_EDCTP_EDCTP20Chbx(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Perimeter_EDCTP_EDCTP20Chbx"));
		return element;
	}
	
	
	public WebElement BKOF_Perimeter_SubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Perimeter_SubmitBtn"));
		return element;
	}
	
	//Policy Page
	public WebElement BKOF_Policy_PolicyTitle(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Policy_PolicyTitle"));
		return element;
	}
	
	public WebElement BKOF_PolicyPage_Contact_Email(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_PolicyPage_Contact_Email"));
		return element;
	}
	
	public WebElement BKOF_PolicyPage_Contact_EmailSubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_PolicyPage_Contact_EmailSubmitBtn"));
		return element;
	}
	
	
	
	//Risk -> Portfolio -> Policy List
	public WebElement BKOF_Risk_Portfolio_PolicyList_Header(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_PolicyList_Header"));
		return element;
	}
	
	public WebElement BKOF_Risk_Portfolio_PolicyList_CreatePolicyFromTemplateBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_PolicyList_CreatePolicyFromTemplateBtn"));
		return element;
	}
	
	public WebElement BKOF_Risk_Portfolio_PolicyList_CreatePolicFromTeplatePopupTitle(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_PolicyList_CreatePolicyFromTeplatePopupTitle"));
		return element;
	}
	
	// Risk -> Portfolio -> Policy List - Create Policy From Template Popup
	public WebElement BKOF_Risk_Portfolio_PolicyList_CreatePolicyFromTeplatePopup_ModelSelect(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_PolicyList_CreatePolicyFromTeplatePopup_NewInsuredRdoBtn"));
		return element;
	}
	
	public WebElement BKOF_Risk_Portfolio_PolicyList_CreatePolicyFromTeplatePopup_NewInsuredRdoBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_PolicyList_CreatePolicyFromTeplatePopup_NewInsuredRdoBtn"));
		return element;
	}
	
	public WebElement BKOF_Risk_Portfolio_PolicyList_CreatePolicyFromTeplatePopup_InsuredDropDown(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_PolicyList_CreatePolicyFromTeplatePopup_InsuredDropDown"));
		return element;
	}
	
	public WebElement BKOF_Risk_Portfolio_PolicyList_CreatePolicyFromTeplatePopup_DefaultCurrDropDown(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_PolicyList_CreatePolicyFromTeplatePopup_DefaultCurrDropDown"));
		return element;
	}
	
	public WebElement BKOF_Risk_Portfolio_PolicyList_CreatePolicyFromTeplatePopup_SubmitBtn(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Risk_Portfolio_PolicyList_CreatePolicyFromTeplatePopup_SubmitBtn"));
		return element;
	}
	
	//Identity
	
	public WebElement BKOF_Identity_NameTable(){
		element = driver.findElement(repoBKElements.byLocator("BKOF_Identity_NameTable"));
		return element;
	}
	
	
	public void BKOF_login(String username, String password) {
		try {
			driver.navigate().to("https://cis-qac.edc.ca/grams/do");
			BKOF_Login().sendKeys(username);
			BKOF_Password().sendKeys(password);
			BKOF_LoginBtn().click();
		} catch (NoSuchElementException e) {
			Reporter.log(e.getMessage());
		}
	}
}