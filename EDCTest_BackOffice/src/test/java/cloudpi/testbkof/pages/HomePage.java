package cloudpi.testbkof.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import cloudpi.testbkof.pages.HomePage;

import org.apache.log4j.Logger;

public  class HomePage extends cloudpi.testbkof.pages.BasePage  {
	
	public  static  WebElement element = null;
	
	public HomePage(WebDriver driver) {
		super(driver);
	}

	static Logger log = Logger.getLogger(HomePage.class.getName());
	
	public  String getPageTitle(){
		return driver.getTitle();
	}
}
