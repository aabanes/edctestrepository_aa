package cloudpi.testTPI.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;

//import org.openqa.selenium.support.FindBy;
// 
//import org.openqa.selenium.support.How;
// 
//import org.openqa.selenium.support.PageFactory;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.log4j.Logger;


/**
 * @author dtao
 *
 */
public class TPI_HomePage extends cloudpi.testTPI.pages.BasePage {

	// private WebDriver driver;
	public static WebElement element = null;

	private WebDriverWait drWait = null;

	static Logger log = Logger.getLogger(TPI_HomePage.class.getName());

	public TPI_HomePage(WebDriver driver) {
		super(driver);
	}

	public String getPageTitle() {
		return driver.getTitle();
	}

	/*
	 * 
	 * Exception handling approach should be depend on the Application test case
	 * scenario.
	 * 
	 * The methods could be only state the type of Exception
	 * 
	 * catch the type of the Exception and then log into files and finally throw
	 * it
	 * 
	 * or in the test case using the Assert or testNg Assertion rising it in the
	 * finally
	 * 
	 * Sometime only catch it, not throws , it really depends on the element or
	 * test cases itself
	 * 
	 */

	/*
	 * explicit waits for a Expected condition happened
	 * 
	 */
//	public WebElement registry() {
//
//		WebDriverWait drWait = new WebDriverWait(driver, 10);
//
//		drWait.withTimeout(30, TimeUnit.SECONDS).pollingEvery(50, TimeUnit.MILLISECONDS);
//
//		drWait.ignoring(NoSuchElementException.class);
//
//		WebElement element = drWait
//				.until(ExpectedConditions.elementToBeClickable(By.xpath(".//li[@id='lists-and-registry']/span")));
//
//		return element;
//	}

	public WebElement logOutButton() {
		
		By bySi = repoElements.byLocator("Home_Logout");
		element = driver.findElement(bySi);
		return element;
	}

	public void logoutAccount() {
		logOutButton().click();
		log.info("  Account Logout OK  " );
	}

	public WebElement home_PortfolioTab() {
		element = driver.findElement(repoElements.byLocator("Home_Portfolio"));
		return element;
	}

	public void gotoPortfolio() {
		home_PortfolioTab().click();
	}
	public WebElement home_ReportingTab() {
		element = driver.findElement(repoElements.byLocator("Home_Reporting"));
		return element;
	}
	public void gotoReporting() {
		home_ReportingTab().click();
	}
	public WebElement home_ViewTab() {
		element = driver.findElement(repoElements.byLocator("Home_View"));
		return element;
	}

	public WebElement home_ManageTab() {
		element = driver.findElement(repoElements.byLocator("Home_Manage"));
		return element;
	}

	public WebElement home_ContactTab() {
		element = driver.findElement(repoElements.byLocator("Home_Contact"));
		return element;
	}

	public WebElement home_HelpTab() {
		element = driver.findElement(repoElements.byLocator("Home_Help"));
		return element;
	}

	public WebElement search_LookText() {
		element = driver.findElement(repoElements.byLocator("Home_LookText"));
		return element;
	}
	public void searchInput(String item) {
		element = search_LookText();
		element.click();
		element.clear();
		element.sendKeys(item);

	}
	public WebElement searchButton() {
		element = driver.findElement(repoElements.byLocator("Home_SearchButton"));
		return element;
	}


	// public void main(String[] args) {
	//
	// driver = new FirefoxDriver();
	// //Put a Implicit wait, this means that any search for elements on the
	// page could take the time the implicit wait is set for before throwing
	// exception
	//
	// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//
	//
	// // TODO Auto-generated method stub
	//
	// }

}
