package cloudpi.testTPI.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import cloudpi.testTPI.pages.TPI_RepositoryElements;

import java.io.IOException;

public abstract class BasePage {

	protected  WebDriver driver;
	protected static TPI_RepositoryElements repoElements = null;
	public  static  int need = 0;
	private static Logger log = Logger.getLogger(BasePage.class.getName());

	public BasePage(WebDriver driver)  {
		 this.driver = driver;
		if (need < 1 ) {
				//repoElements = new RepositoryElements("./config/RepositoryTPI.properties");
//				System.out.println(" Repository load done ");
				log.info(" Repository load done ");
				need++ ;
		}
	}
	public  abstract  String getPageTitle();
	

}
