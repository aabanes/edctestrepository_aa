
package cloudpi.testTPI.pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cloudpi.testTPI.pages.TPI_HomePage;
import cloudpi.testTPI.pages.TPI_RepositoryElements;
import cloudpi.testTPI.testLibs.AppConstant;

public class TPI_PortfolioDetailPage extends cloudpi.testTPI.pages.TPI_HomePage {

	public static WebElement element = null;

	private WebDriverWait drWait = null;

	static Logger log = Logger.getLogger(TPI_PortfolioDetailPage.class.getName());

	private String portDetailPgRepoFile = AppConstant.TPIpropertyPath +"TPI_PortDetailPageRepository.properties";
	private   TPI_RepositoryElements repoElementsPortfolioDetail = null;
	
	public TPI_PortfolioDetailPage(WebDriver driver) throws IOException {
		super(driver);
		repoElementsPortfolioDetail = new TPI_RepositoryElements(portDetailPgRepoFile);
	}

	public WebElement detailNameId() {

		By bySi = repoElementsPortfolioDetail.byLocator("DetailNameID");
		element = driver.findElement(bySi);
		return element;
	}
	public WebElement serviceButton() {
		By bySi = repoElementsPortfolioDetail.byLocator("ServiceButton");
		element = driver.findElement(bySi);
		return element;
	}

	/**
	 * Navigate to tab
	 * 
	 */
	public WebElement detail_IdentityTab() {
		element = driver.findElement(repoElementsPortfolioDetail.byLocator("LinkLB_Identity"));
		return element;
	}

	public WebElement detail_CoverageTab() {
		element = driver.findElement(repoElementsPortfolioDetail.byLocator("LinkLB_Coverage"));
		return element;
	}

	public WebElement detail_TotalTab() {
		element = driver.findElement(repoElementsPortfolioDetail.byLocator("LinkLB_Total"));
		return element;
	}

	public WebElement detail_CommentsTab() {
		element = driver.findElement(repoElementsPortfolioDetail.byLocator("LinkLB_Comments"));
		return element;
	}
	
	public WebElement coverageSelect(){
		element = driver.findElement(repoElementsPortfolioDetail.byLocator("CoveragePolicyHSelectDpBox"));
		return element;
	}
	
	public WebElement DCLCreditApplyIcon(){
		element = driver.findElement(repoElementsPortfolioDetail.byLocator("DCLCreditApplyIcon"));
		return element;
	}
	
	public WebElement CoverageTabSelect(){
		element = driver.findElement(repoElementsPortfolioDetail.byLocator("CoverageTabSelect"));
		return element;
	}
	
	public WebElement CoveragePolicyAmt(){
		element = driver.findElement(repoElementsPortfolioDetail.byLocator("CoveragePolicyAmt"));
		return element;
	}
	
	public String getCID(){
		String cid = "";
		element = driver.findElement(repoElementsPortfolioDetail.byLocator("Pof_DetailCID"));
		cid = element.getText();
		return cid;
	}


	/*
	 * Identity 
	 * 
	 * 
	 */


}
