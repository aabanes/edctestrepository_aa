package cloudpi.testTPI.pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PortfolioPage extends cloudpi.testTPI.pages.TPI_HomePage {

	public static WebElement element = null;

	private WebDriverWait drWait = null;

	static Logger log = Logger.getLogger(PortfolioPage.class.getName());

	public PortfolioPage(WebDriver driver) {
		super(driver);
	}
	
	/*
	 * The NoSuchElementException or NullPointerException Exception could be
	 * happen during finding
	 * 
	 * navigationToSuopport element, state the 2 exception in the methods ,
	 * catch those Exception
	 * 
	 * then write the log, finally , throw the Exception if it happened
	 * 
	 * 
	 */

	/**
	 * Navigate to tab
	 * 
	 */
	public WebElement portFolio_BuyerTab() {
		element = driver.findElement(repoElements.byLocator("PortFolio_Buyer"));
		return element;
	}

	public WebElement portFolio_PendQuotaTab() {
		element = driver.findElement(repoElements.byLocator("PortFolio_PendQuota"));
		return element;
	}

	public WebElement portFolio_PendDeciTab() {
		element = driver.findElement(repoElements.byLocator("PortFolio_PendDeci"));
		return element;
	}

	public WebElement portFolio_FutureLmtTab() {
		element = driver.findElement(repoElements.byLocator("PortFolio_FutureLmt"));
		return element;
	}

	public WebElement portFolio_TempLmtTab() {
		element = driver.findElement(repoElements.byLocator("PortFolio_TempLmt"));
		return element;
	}

	public WebElement portFolio_AggregteTab() {
		element = driver.findElement(repoElements.byLocator("PortFolio_Aggregte"));
		return element;
	}
/*
 * Portfolio Buyer Search and Filter
 * 
 * 
 */
	public WebElement buyerSearchSelect() {

		element = driver.findElement(repoElements.byLocator("BuyerSearchSelect"));
		log.info("user Email text box element found");
		return element;
	}
	public WebElement buyerSearchName() {

		element = driver.findElement(repoElements.byLocator("BuyerSearchName"));
		log.info("user Email text box element found");
		return element;
	}
	
	
	public WebElement plyFilterSelect() {
		element = driver.findElement(repoElements.byLocator("SelectPlyFilter"));
		return element;
	}

	public void searchInput(CharSequence[] item) {
		element = search_LookText();
		element.click();
		element.clear();
		element.sendKeys(item);

	}

	public WebElement countryFilterSelect() {
		element = driver.findElement(repoElements.byLocator("SelectCountryFilter"));
		return element;
	}
	
	public WebElement filterSubmitButton() {
		element = driver.findElement(repoElements.byLocator("FilterSubmitButton"));
		return element;
	}

	public WebElement filterResult() {
		element = driver.findElement(repoElements.byLocator("FilterResult"));
		return element;
	}
	public String  filterLinesNumber() {
			      String linesNumber = filterResult().getText();
		          int st = linesNumber.indexOf("line");
		          String number = linesNumber.substring(0, st-1).trim();
		return number;
	}
/*
 *  
 * 
 * 	## Search Result table Header
 */
	public WebElement tableHdCompanyRefer() {
		element = driver.findElement(repoElements.byLocator("tableHdCompanyRefer"));
		return element;
	}

	public WebElement tableHdBuyer() {
		element = driver.findElement(repoElements.byLocator("tableHdBuyer"));
		return element;
	}

	public WebElement tableHdCountry() {
		element = driver.findElement(repoElements.byLocator("tableHdCountry"));
		return element;
	}

	public WebElement tableHdCurrency() {
		element = driver.findElement(repoElements.byLocator("tableHdCurrency"));
		return element;
	}

	public WebElement tableHdEDC_Credit() {
		element = driver.findElement(repoElements.byLocator("tableHdEDC_Credit"));
		return element;
	}

	public WebElement tableHdDecison_Date() {
		element = driver.findElement(repoElements.byLocator("tableHdDecison_Date"));
		return element;
	}
	
	public WebElement tableHdType() {
		element = driver.findElement(repoElements.byLocator("tableHdType"));
		return element;
	}

	// The Table Cell tr[1]/tb[1]
	
	public WebElement tableCell11() {

		By bySi = repoElements.byLocator("tableCell11");
		element = driver.findElement(bySi);
		return element;
	}
	
	public WebElement Pof_DetailButton() {

		By bySi = repoElements.byLocator("Home_Logout");
		element = driver.findElement(bySi);
		return element;
	}
	public WebElement Pof_AddButton() {

		By bySi = repoElements.byLocator("Pof_AddButton");
		element = driver.findElement(bySi);
		return element;
	}
	public WebElement Pof_ExportButton() {

		By bySi = repoElements.byLocator("Pof_ExportButton");
		element = driver.findElement(bySi);
		return element;
	}
	
	public void filluserNameText(String uName) throws Exception {
		try {
			WebElement elm = userNameText();
			elm.click();
			elm.clear();
			elm.sendKeys(uName);
			log.info(" Inputed user Name  as : " + uName);
		} catch (Exception ex) {
			log.error(" fail in  Inputed  user Name as : " + uName);
			throw ex;
		}
	}
	public WebElement userNameText() {
		element = driver.findElement(repoElements.byLocator("login_Password"));
		return element;
	}
	public void fillPassword(String upassword) throws Exception {
		try {
			WebElement elm = passwordText();
			elm.click();
			elm.clear();
			elm.sendKeys(upassword);
			log.info(" Inputed  password  as : " + upassword);
		} catch (Exception ex) {
			log.error(" fail in  Inputed  password  as : " + upassword);
			throw ex;
		}
	}

	public WebElement passwordText() {
		element = driver.findElement(repoElements.byLocator("login_Password"));
		return element;
	}
	
	/*
	 * 
	 * Exception handling approach should be depend on the Application test case
	 * scenario.
	 * 
	 * The methods could be only state the type of Exception
	 * 
	 * catch the type of the Exception and then log into files and finally throw
	 * it
	 * 
	 * or in the test case using the Assert or testNg Assertion rising it in the
	 * finally
	 * 
	 * Sometime only catch it, not throws , it really depends on the element or
	 * test cases itself
	 * 
	 */

	/*
	 * explicit waits for a Expected condition happened
	 * 
	 */
//	public WebElement registry() {
//
//		WebDriverWait drWait = new WebDriverWait(driver, 10);
//
//		drWait.withTimeout(30, TimeUnit.SECONDS).pollingEvery(50, TimeUnit.MILLISECONDS);
//
//		drWait.ignoring(NoSuchElementException.class);
//
//		WebElement element = drWait
//				.until(ExpectedConditions.elementToBeClickable(By.xpath(".//li[@id='lists-and-registry']/span")));
//
//		return element;
//	}	
	// public void main(String[] args) {
	//
	// driver = new FirefoxDriver();
	// //Put a Implicit wait, this means that any search for elements on the
	// page could take the time the implicit wait is set for before throwing
	// exception
	//
	// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//
	// //Launch the Online Store Website
	//
	// // TODO Auto-generated method stub
	//
	// }



}
