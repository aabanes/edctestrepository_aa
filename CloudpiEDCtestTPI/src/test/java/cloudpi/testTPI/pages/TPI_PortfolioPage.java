package cloudpi.testTPI.pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cloudpi.testTPI.pages.TPI_HomePage;
import cloudpi.testTPI.pages.TPI_RepositoryElements;
import cloudpi.testTPI.testLibs.AppConstant;

public class TPI_PortfolioPage extends cloudpi.testTPI.pages.TPI_HomePage {

	public static WebElement element = null;

	private WebDriverWait drWait = null;

	static Logger log = Logger.getLogger(TPI_PortfolioPage.class.getName());

	private String portfolioPgRepoFile = AppConstant.TPIpropertyPath +"TPI_ViewPortfolioPageRepository.properties";
	private   TPI_RepositoryElements repoElementsPortfolio = null;
	public TPI_PortfolioPage(WebDriver driver) throws IOException {
		super(driver);
		repoElementsPortfolio= new TPI_RepositoryElements(portfolioPgRepoFile);
	}

	/*
	 * The NoSuchElementException or NullPointerException Exception could be
	 * happen during finding
	 * 
	 * navigationToSuopport element, state the 2 exception in the methods ,
	 * catch those Exception
	 * 
	 * then write the log, finally , throw the Exception if it happened
	 * 
	 * 
	 */

	/**
	 * Navigate to tab
	 * 
	 */
	
	public WebElement viewPortFolio() {
		element = driver.findElement(repoElementsPortfolio.byLocator("viewPortFolio"));
		return element;
	}
	public WebElement portFolio_BuyerTab() {
		element = driver.findElement(repoElementsPortfolio.byLocator("PortFolio_Buyer"));
		return element;
	}

	public WebElement portFolio_PendQuotaTab() {
		element = driver.findElement(repoElementsPortfolio.byLocator("PortFolio_PendQuota"));
		return element;
	}

	public WebElement portFolio_PendDeciTab() {
		element = driver.findElement(repoElementsPortfolio.byLocator("PortFolio_PendDeci"));
		return element;
	}

	public WebElement portFolio_FutureLmtTab() {
		element = driver.findElement(repoElementsPortfolio.byLocator("PortFolio_FutureLmt"));
		return element;
	}

	public WebElement portFolio_TempLmtTab() {
		element = driver.findElement(repoElementsPortfolio.byLocator("PortFolio_TempLmt"));
		return element;
	}

	public WebElement portFolio_AggregteTab() {
		element = driver.findElement(repoElementsPortfolio.byLocator("PortFolio_Aggregte"));
		return element;
	}
/*
 * Portfolio Buyer Search and Filter
 * 
 * 
 */
	public WebElement buyerSearchSelect() {

		element = driver.findElement(repoElementsPortfolio.byLocator("buyerSearchSelect"));
		log.info("user Email text box element found");
		return element;
	}
	public WebElement buyerSearchName() {

		element = driver.findElement(repoElementsPortfolio.byLocator("BuyerSearchName"));
		log.info("user Email text box element found");
		return element;
	}
	
	
	public WebElement plyFilterSelect() {
		element = driver.findElement(repoElementsPortfolio.byLocator("SelectPlyFilter"));
		return element;
	}

	public void searchInput(CharSequence[] item) {
		element = search_LookText();
		element.click();
		element.clear();
		element.sendKeys(item);

	}

	public WebElement countryFilterSelect() {
		element = driver.findElement(repoElementsPortfolio.byLocator("SelectCountryFilter"));
		return element;
	}
	
	public WebElement filterSubmitButton() {
		element = driver.findElement(repoElementsPortfolio.byLocator("FilterSubmitButton"));
		return element;
	}

	public WebElement filterResult() {
		element = driver.findElement(repoElementsPortfolio.byLocator("FilterResult"));
		return element;
	}
	public String  filterLinesNumber() {
			      String linesNumber = filterResult().getText();
		          int st = linesNumber.indexOf("line");
		          String number = linesNumber.substring(0, st-1).trim();
		return number;
	}
/*
 *  
 * 
 * 	## Search Result table Header
 */
	public WebElement tableHdCompanyRefer() {
		element = driver.findElement(repoElementsPortfolio.byLocator("tableHdCompanyRefer"));
		return element;
	}

	public WebElement tableHdBuyer() {
		element = driver.findElement(repoElementsPortfolio.byLocator("tableHdBuyer"));
		return element;
	}

	public WebElement tableHdCountry() {
		element = driver.findElement(repoElementsPortfolio.byLocator("tableHdCountry"));
		return element;
	}

	public WebElement tableHdCurrency() {
		element = driver.findElement(repoElementsPortfolio.byLocator("tableHdCurrency"));
		return element;
	}

	public WebElement tableHdEDC_Credit() {
		element = driver.findElement(repoElementsPortfolio.byLocator("tableHdEDC_Credit"));
		return element;
	}

	public WebElement tableHdDecison_Date() {
		element = driver.findElement(repoElementsPortfolio.byLocator("tableHdDecison_Date"));
		return element;
	}
	
	public WebElement tableHdType() {
		element = driver.findElement(repoElementsPortfolio.byLocator("tableHdType"));
		return element;
	}

	// The Table Cell tr[1]/tb[1]
	
	public WebElement tabletr1td1() {

		By bySi = repoElementsPortfolio.byLocator("tabletr1td1");
		element = driver.findElement(bySi);
		return element;
	}
	public WebElement buyerNameOnlyOne() {

		By bySi = repoElementsPortfolio.byLocator("buyerNameOnlyOne");
		element = driver.findElement(bySi);
		return element;
	}
	
	public WebElement buyerDetailBtn() {

		By bySi = repoElementsPortfolio.byLocator("buyerDetailBtn");
		element = driver.findElement(bySi);
		return element;
	}
	public WebElement Pof_AddButton() {

		By bySi = repoElementsPortfolio.byLocator("Pof_AddButton");
		element = driver.findElement(bySi);
		return element;
	}
	public WebElement Pof_ExportButton() {

		By bySi = repoElementsPortfolio.byLocator("Pof_ExportButton");
		element = driver.findElement(bySi);
		return element;
	}
	
	public void filluserNameText(String uName) throws Exception {
		try {
			WebElement elm = userNameText();
			elm.click();
			elm.clear();
			elm.sendKeys(uName);
			log.info(" Inputed user Name  as : " + uName);
		} catch (Exception ex) {
			log.error(" fail in  Inputed  user Name as : " + uName);
			throw ex;
		}
	}
	public WebElement userNameText() {
		element = driver.findElement(repoElementsPortfolio.byLocator("login_Password"));
		return element;
	}
	public void fillPassword(String upassword) throws Exception {
		try {
			WebElement elm = passwordText();
			elm.click();
			elm.clear();
			elm.sendKeys(upassword);
			log.info(" Inputed  password  as : " + upassword);
		} catch (Exception ex) {
			log.error(" fail in  Inputed  password  as : " + upassword);
			throw ex;
		}
	}

	public WebElement passwordText() {
		element = driver.findElement(repoElementsPortfolio.byLocator("login_Password"));
		return element;
	}
	
	/*
	 * 
	 * Exception handling approach should be depend on the Application test case
	 * scenario.
	 * 
	 * The methods could be only state the type of Exception
	 * 
	 * catch the type of the Exception and then log into files and finally throw
	 * it
	 * 
	 * or in the test case using the Assert or testNg Assertion rising it in the
	 * finally
	 * 
	 * Sometime only catch it, not throws , it really depends on the element or
	 * test cases itself
	 * 
	 */

	/*
	 * explicit waits for a Expected condition happened
	 * 
	 */
//	public WebElement registry() {
//
//		WebDriverWait drWait = new WebDriverWait(driver, 10);
//
//		drWait.withTimeout(30, TimeUnit.SECONDS).pollingEvery(50, TimeUnit.MILLISECONDS);
//
//		drWait.ignoring(NoSuchElementException.class);
//
//		WebElement element = drWait
//				.until(ExpectedConditions.elementToBeClickable(By.xpath(".//li[@id='lists-and-registry']/span")));
//
//		return element;
//	}	
	// public void main(String[] args) {
	//
	// driver = new FirefoxDriver();
	// //Put a Implicit wait, this means that any search for elements on the
	// page could take the time the implicit wait is set for before throwing
	// exception
	//
	// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//
	// //Launch the Online Store Website
	//
	// // TODO Auto-generated method stub
	//
	// }



}
