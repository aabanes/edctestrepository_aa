package cloudpi.testTPI.pages;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
 
import org.openqa.selenium.WebElement;
 
//import org.openqa.selenium.support.FindBy;
// 
//import org.openqa.selenium.support.How;
// 
//import org.openqa.selenium.support.PageFactory;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cloudpi.testTPI.pages.TPI_RepositoryElements;

import org.apache.log4j.Logger;

public class LoginPage extends cloudpi.testTPI.pages.BasePage {

//	private WebDriver driver;
	public  static  WebElement element = null;
	
	private WebDriverWait drWait = null;
	
	static Logger log = Logger.getLogger(LoginPage.class.getName());
	
	public LoginPage(WebDriver driver) {
		super(driver);
		try {
			repoElements = new TPI_RepositoryElements("../CloudpiEDCtestTPI/config/RepositoryTPI.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public  void  pageOpen(){
		driver.navigate().to("https://cis-qac.edc.ca/herakles/do");
	}
	public  String getPageTitle(){
		return driver.getTitle();
	}
	
//	public   void signAccount(String userName, String passWord) throws NoSuchElementException,NullPointerException {
//       	try{
//		    signIn().click();
//       		fillEmailTextbox(userName);fillPassword(passWord);
//       		signInButton().click();
//    		log.info("SignIn Account OK with name : " + userName );
//       	}catch (NullPointerException ex1){
//       		log.error("Signin Failed by Exception --" + ex1.toString());
//
//       		throw ( new  NullPointerException() );}
//   	    catch (NoSuchElementException ex2){
//   		log.error("Signin Failed by no element found  --" + ex2.toString());
//
//   		throw ( new NoSuchElementException("not found element ") );
//   		}	
//	}
	
	public  void signAccount(String userName, String passWord)throws Exception  {
		driver.get("https://cis-qac.edc.ca/herakles/do");
		    filluserNameText(userName);
       		fillPassword(passWord);
       		logInButton().click();
   			log.info("SignIn Account OK with name : " + userName );
	}
	
	public void signInWithTempPass(String oldPass, String newPass, String cnfmPass)
	{
		driver.findElement(repoElements.byLocator("expiredPass_oldPass")).sendKeys(oldPass);
		driver.findElement(repoElements.byLocator("expiredPass_newPass")).sendKeys(newPass);
		driver.findElement(repoElements.byLocator("expiredPass_cnfmPass")).sendKeys(cnfmPass);
		driver.findElement(repoElements.byLocator("expiredPass_SubmitBtn")).click();
	}
	
	public   WebElement userNameText() {

		element = driver.findElement(repoElements.byLocator("Login_Name"));
		log.info("user Email text box element found");
		return element;
	}

	public   void filluserNameText( String uName) throws Exception{
		try{
		WebElement elm = userNameText();
		elm.click();
		elm.clear(); 
		elm.sendKeys(uName);
		log.info(" Inputed user Name  as : " + uName);
     	}catch(Exception ex){
		log.error(" fail in  Inputed  user Name as : " + uName );
		throw ex;
    	}
	}

	public   WebElement passwordText() {
		element = driver.findElement(repoElements.byLocator("login_Password"));
		return element;
	}
	
	public   void fillPassword(String upassword) throws Exception  {
		try{
		WebElement elm = passwordText();
		elm.click();
		elm.clear(); 
		elm.sendKeys(upassword);
		log.info(" Inputed  password  as : " + upassword );
     	}catch(Exception ex){
		log.error(" fail in  Inputed  password  as : " + upassword );
		throw ex;
    	}
	}
	public   WebElement logInButton() {
		
		By bySi = repoElements.byLocator("login_Button");
        element = driver.findElement(bySi);
		return element;
	}
	
	public   WebElement forgotPassword() {
		By bySi = repoElements.byLocator("ForgotPassword");
        element = driver.findElement(bySi);
		return element;
	}
	public  void  clickforgotPassword() {
			forgotPassword().click();
		}
	/*
	 * The second part of Element is on the forgot Password page
	 * 
	 * We define it in LogIn page as well 
	 * 
	 */
	
	public   WebElement eMailAddressText() {

		element = driver.findElement(repoElements.byLocator("EmailAddress"));
		log.info("user Email text box element found");
		return element;
	}

	public   void enterEMailAddress( String eMailId) throws Exception{
		try{
		WebElement elm = eMailAddressText();
		elm.click();
		elm.clear(); 
		elm.sendKeys(eMailId);
		log.info(" Inputed eMailId  as : " + eMailId);
     	}catch(Exception ex){
		log.error(" fail in  Inputed eMailId as : " + eMailId );
		throw ex;
    	}
	}
	public   WebElement submitButton() {
		By bySi = repoElements.byLocator("ForgotPage_SubmitButton");
        element = driver.findElement(bySi);
		return element;
	}
	public  void  clickSubmitButton() {
		submitButton().click();
		}
	
	public   WebElement backLogInPage() {
		By bySi = repoElements.byLocator("BackLogIn");
        element = driver.findElement(bySi);
		return element;
	}
	public  void  goBackLogInPage() {
		backLogInPage().click();
		}
	
	public   WebElement yourEmailEnter() {

        element = driver.findElement(repoElements.byLocator("EnterYouEmail"));
		return element;
	}
	
	public WebElement logoutBtn(){
        element = driver.findElement(repoElements.byLocator("LogoutBtn"));
		return element;
	}
	
}
