package cloudpi.testTPI.pages;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;

public class TPI_RepositoryElements {

	private FileInputStream stream;
	private String repositoryFile;
	private Properties propertyFile = new Properties();

	public TPI_RepositoryElements(String fileName) throws IOException {
		this.repositoryFile = fileName;
		stream = new FileInputStream(repositoryFile);
		propertyFile.load(stream);
	}

	public By byLocator(String locatorName) {
		String locatorProperty = propertyFile.getProperty(locatorName);
//		System.out.println(locatorProperty);
		String locatorType = locatorProperty.split(":")[0];
		String locatorValue = locatorProperty.split(":")[1];
		
		By locator = null;
		if (locatorType.toLowerCase().equals("id")){
			locator = By.id(locatorValue);
		}else if (locatorType.toLowerCase().equals("name")){
			locator = By.name(locatorValue);
		}else if (locatorType.toLowerCase().equals("cssselector")){
			locator = By.cssSelector(locatorValue);
		}else if (locatorType.toLowerCase().equals("linktext")){
			locator = By.linkText(locatorValue);			
		}else if (locatorType.toLowerCase().equals("partiallinktext")){
			locator = By.partialLinkText(locatorValue);
		}else if (locatorType.toLowerCase().equals("tagName")){
			locator = By.tagName(locatorValue);
		}else if (locatorType.toLowerCase().equals("xpath")){
			locator = By.xpath(locatorValue);
		}
		return locator;
	}

//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//	}

}
