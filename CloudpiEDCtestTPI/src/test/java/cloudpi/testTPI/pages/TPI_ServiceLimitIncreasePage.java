package cloudpi.testTPI.pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cloudpi.testTPI.testLibs.AppConstant;

public class TPI_ServiceLimitIncreasePage extends cloudpi.testTPI.pages.TPI_HomePage {

	public static WebElement element = null;

	private WebDriverWait drWait = null;

	static Logger log = Logger.getLogger(TPI_ServiceLimitIncreasePage.class.getName());

	protected TPI_RepositoryElements repoElementsCLIncrease = null;

	private String buyerLimiteIncreasePgRepoFile = AppConstant.TPIpropertyPath
			+ "TPI_BuyerCLimiteIncreasePageRepository.properties";

	public TPI_ServiceLimitIncreasePage(WebDriver driver) throws IOException {
		super(driver);
		repoElementsCLIncrease = new TPI_RepositoryElements(buyerLimiteIncreasePgRepoFile);
	}

	
	public WebElement choicePolicyZone() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("choicePolicyZone"));
		return element;
	}

	public WebElement policyHSelectApplyDropList() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("policyHSelectApplyDropList"));
		return element;
	}

	public WebElement creditApplyIncreaseIcon() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("creditApplyIncreaseIcon"));
		return element;
	}

	public WebElement screenTitleIncrease() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("screenTitleIncrease"));
		return element;
	}

	//
	// ** Application Form
	public WebElement CreditApprovalRequest() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("CreditApprovalRequest"));
		return element;
	}
	
	public WebElement ExisitingCreditApproval() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("InsureCreditLimitText"));
		return element;
	}

	public WebElement insurerCreditLimitText() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("insurerCreditLimitText"));
		return element;
	}

	public WebElement requestCreditApprovalText() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("requestCreditApprovalText"));
		return element;
	}

	public WebElement requestCurrency() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("RequestCurrency"));
		return element;
	}

	public WebElement maxPayTermText() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("MaxPayTermText"));
		return element;
	}

	public WebElement coveragePeriod() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("coveragePeriod"));
		return element;
	}

	public WebElement creditRateSelectDropList() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("CreditRateSelectDropList"));
		return element;
	}

	public WebElement insureRatio() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("insureRatio"));
		return element;
	}
	public WebElement inCreaseComments() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("inCreaseComments"));
		return element;
	}
	
	
	public WebElement creditSubmitButton() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("creditSubmitButton"));
		return element;
	}

	public WebElement identityCreditApproved() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("identityCreditApproved"));
		return element;
	}

	public WebElement serviceWindowsCloseX() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("serviceWindowsCloseX"));
		return element;
	}

	public WebElement detailNameId() {
		By bySi = repoElementsCLIncrease.byLocator("DetailNameID");
		element = driver.findElement(bySi);
		return element;
	}

	public WebElement serviceButton() {
		By bySi = repoElementsCLIncrease.byLocator("ServiceButton");
		element = driver.findElement(bySi);
		return element;
	}

	/**
	 * Navigate to tab
	 * 
	 */
	public WebElement detail_IdentityTab() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("LinkLB_Identity"));
		return element;
	}

	public WebElement detail_CoverageTab() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("LinkLB_Coverage"));
		return element;
	}

	public WebElement detail_TotalTab() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("LinkLB_Total"));
		return element;
	}

	public WebElement detail_CommentsTab() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("LinkLB_Comments"));
		return element;
	}

	public WebElement DCLTile() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("DCLTile"));
		return element;
	}

	public WebElement DCLCreditAmount() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("DCLCreditAmount"));
		return element;
	}

	public WebElement DCLPaymentTerms() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("DCLPaymentTerms"));
		return element;
	}

	public WebElement DCLExpiryDate() {
		element = driver.findElement(repoElementsCLIncrease.byLocator("DCLExpiryDate"));
		return element;
	}

	/*
	 * Identity
	 * 
	 * 
	 */

}
