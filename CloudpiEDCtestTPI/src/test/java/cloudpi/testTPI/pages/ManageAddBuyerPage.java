package cloudpi.testTPI.pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class ManageAddBuyerPage  extends cloudpi.testTPI.pages.TPI_HomePage {

	public static WebElement element = null;

	private WebDriverWait drWait = null;

	static Logger log = Logger.getLogger(ManageAddBuyerPage.class.getName());

	public ManageAddBuyerPage(WebDriver driver) {
		super(driver);
		try {
			repoElements = new TPI_RepositoryElements("../CloudpiEDCtestTPI/config/RepositoryTPI.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public WebElement manage_AddBuyer() {
		By bySi = repoElements.byLocator("Manage_AddBuyer");
		element = driver.findElement(bySi);
		return element;
	}
	public void openAddBuyer() throws InterruptedException {
		home_ManageTab().click();
		Thread.sleep(2600);
		manage_AddBuyer().click();
	}
	
	public WebElement buyerNewIdArea() {
		element = driver.findElement(repoElements.byLocator("BuyerNewIdArea"));
		return element;
	}
//	public Select countrySelectDropBox() {
//		By bySi = repoElements.byLocator("CountrySelectDropBox");
//		element = driver.findElement(bySi);
//		Select selectbox = new Select(element);
//		return selectbox;
//	}
	public WebElement countrySelectDropBox() {
		By bySi = repoElements.byLocator("CountrySelectDropBox");
		element = driver.findElement(bySi);
		return element;
	}
	public WebElement stateSelectDropBox() {
		By bySi = repoElements.byLocator("StateSelectDropBox");
		element = driver.findElement(bySi);
		return element;
	}
	public WebElement companyNameText() {
		element = driver.findElement(repoElements.byLocator("CompanyNameText"));
		return element;
	}
	public WebElement buyerSubmitButton() {
		element = driver.findElement(repoElements.byLocator("BuyerSubmitButton"));
		return element;
	}
	
	public WebElement ABC999SelectButton() {
		element = driver.findElement(repoElements.byLocator("ABC999SelectButton"));
		return element;
	}
	public WebElement ABCSelectButton() {
		element = driver.findElement(repoElements.byLocator("ABCSelectButton"));
		return element;
	}
	
	public WebElement choicePolicyZone() {
		element = driver.findElement(repoElements.byLocator("choicePolicyZone"));
		return element;
	}
	
	public WebElement policyHSelectApplyDropList() {
		element = driver.findElement(repoElements.byLocator("PolicyHSelectApplyDropList"));
		return element;
	}
	public WebElement DCLCreditApplyIcon() {
		element = driver.findElement(repoElements.byLocator("DCLCreditApplyIcon"));
		return element;
	}
	public WebElement screenTitleDeclareCreditLimit() {
		element = driver.findElement(repoElements.byLocator("DeclareDiscretionaryCreditLimit"));
		return element;
	}
	
	public WebElement TPI_AddBuyer_DUNS() {
		element = driver.findElement(repoElements.byLocator("TPI_AddBuyer_DUNS"));
		return element;
	}
	
	public WebElement TPI_AddBuyer_SearchResults() {
		element = driver.findElement(repoElements.byLocator("TPI_AddBuyer_SearchResults"));
		return element;
	}
	
	public WebElement Manage_Menulink() {
		element = driver.findElement(repoElements.byLocator("Manage_Menulink"));
		return element;
	}
	
	//
	// ** Application Form 
	
	public WebElement insureCreditLimitText() {
		element = driver.findElement(repoElements.byLocator("InsureCreditLimitText"));
		return element;
	}
	public WebElement requestDCLText() {
		element = driver.findElement(repoElements.byLocator("RequestDCLText"));
		return element;
	}
	
	public WebElement requestCurrency() {
		element = driver.findElement(repoElements.byLocator("RequestCurrency"));
		return element;
	}
	public WebElement maxPayTermText() {
		element = driver.findElement(repoElements.byLocator("MaxPayTermText"));
		return element;
	}
	
	public WebElement durationSelectDropList() {
		element = driver.findElement(repoElements.byLocator("DurationPeriodSelectDropList"));
		return element;
	}
	
	public WebElement creditRateSelectDropList() {
		element = driver.findElement(repoElements.byLocator("CreditRateSelectDropList"));
		return element;
	}
	
	public WebElement insureRatio() {
		element = driver.findElement(repoElements.byLocator("InsureRatio"));
		return element;
	}
	
	public WebElement creditSubmitButton() {
		element = driver.findElement(repoElements.byLocator("CreditSubmitButton"));
		return element;
	}
	
	public WebElement creditApproved() {
		element = driver.findElement(repoElements.byLocator("CreditApproved"));
		return element;
	}
	public WebElement DCWindowsCloseButton() {
		element = driver.findElement(repoElements.byLocator("DCWindowsCloseButton"));
		return element;
	}
	public WebElement detailNameId() {
		By bySi = repoElements.byLocator("DetailNameID");
		element = driver.findElement(bySi);
		return element;
	}
	public WebElement serviceButton() {
		By bySi = repoElements.byLocator("ServiceButton");
		element = driver.findElement(bySi);
		return element;
	}

	/**
	 * Navigate to tab
	 * 
	 */
	public WebElement detail_IdentityTab() {
		element = driver.findElement(repoElements.byLocator("LinkLB_Identity"));
		return element;
	}

	public WebElement detail_CoverageTab() {
		element = driver.findElement(repoElements.byLocator("LinkLB_Coverage"));
		return element;
	}

	public WebElement detail_TotalTab() {
		element = driver.findElement(repoElements.byLocator("LinkLB_Total"));
		return element;
	}

	public WebElement detail_CommentsTab() {
		element = driver.findElement(repoElements.byLocator("LinkLB_Comments"));
		return element;
	}
	public WebElement DCLTile() {
		element = driver.findElement(repoElements.byLocator("DCLTile"));
		return element;
	}
	public WebElement DCLCreditAmount() {
		element = driver.findElement(repoElements.byLocator("DCLCreditAmount"));
		return element;
	}
	public WebElement DCLPaymentTerms() {
		element = driver.findElement(repoElements.byLocator("DCLPaymentTerms"));
		return element;
	}
	public WebElement DCLExpiryDate() {
		element = driver.findElement(repoElements.byLocator("DCLExpiryDate"));
		return element;
	}

	/*
	 * Identity 
	 * 
	 * 
	 */

}
