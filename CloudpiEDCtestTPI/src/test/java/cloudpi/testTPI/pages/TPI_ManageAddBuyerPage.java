package cloudpi.testTPI.pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cloudpi.testTPI.pages.TPI_RepositoryElements;
import cloudpi.testTPI.testLibs.AppConstant;

public class TPI_ManageAddBuyerPage  extends cloudpi.testTPI.pages.TPI_HomePage {

	public static WebElement element = null;

	private WebDriverWait drWait = null;

	static Logger log = Logger.getLogger(TPI_ManageAddBuyerPage.class.getName());
	
	protected  TPI_RepositoryElements repoElementsManageAddBuyer = null;

	private String manageAddBuyerPgRepoFile = AppConstant.TPIpropertyPath +"TPI_ManageAddBuyerPageRepository.properties";
	
	public TPI_ManageAddBuyerPage(WebDriver driver) throws IOException {
		super(driver);
		repoElementsManageAddBuyer = new TPI_RepositoryElements(manageAddBuyerPgRepoFile);
	}
	public WebElement manage_AddBuyer() {
		By bySi = repoElementsManageAddBuyer.byLocator("Manage_AddBuyer");
		element = driver.findElement(bySi);
		return element;
	}
	public void openAddBuyer() throws InterruptedException {
		home_ManageTab().click();
		Thread.sleep(2600);
		manage_AddBuyer().click();
	}
	
	public WebElement buyerNewIdArea() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("BuyerNewIdArea"));
		return element;
	}
//	public Select countrySelectDropBox() {
//		By bySi = repoElementsManageAddBuyer.byLocator("CountrySelectDropBox");
//		element = driver.findElement(bySi);
//		Select selectbox = new Select(element);
//		return selectbox;
//	}
	public WebElement countrySelectDropBox() {
		By bySi = repoElementsManageAddBuyer.byLocator("CountrySelectDropBox");
		element = driver.findElement(bySi);
		return element;
	}
	public WebElement stateSelectDropBox() {
		By bySi = repoElementsManageAddBuyer.byLocator("StateSelectDropBox");
		element = driver.findElement(bySi);
		return element;
	}
	public WebElement companyNameText() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("CompanyNameText"));
		return element;
	}
	
	public WebElement DUNSNumberTextBox() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("DUNSNumberTextBox"));
		return element;
	}
	public WebElement buyerSubmitButton() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("BuyerSubmitButton"));
		return element;
	}
	
	public WebElement ABC999SelectButton() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("ABC999SelectButton"));
		return element;
	}
	
	public WebElement DUNSSelectBtn() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("DUNSSelectBtn"));
		return element;
	}
	public WebElement ABCSelectButton() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("ABCSelectButton"));
		return element;
	}
	
	public WebElement choicePolicyZone() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("choicePolicyZone"));
		return element;
	}
	
	public WebElement policyHSelectApplyDropList() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("PolicyHSelectApplyDropList"));
		return element;
	}
	public WebElement DCLCreditApplyIcon() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("DCLCreditApplyIcon"));
		return element;
	}
	public WebElement screenTitleDeclareCreditLimit() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("DeclareDiscretionaryCreditLimit"));
		return element;
	}
	
	public WebElement SelectBtnOneResult() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("SelectBtnOneResult"));
		return element;
	}
	
	//
	// ** Application Form 
	
	public WebElement insureCreditLimitText() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("InsureCreditLimitText"));
		return element;
	}
	public WebElement requestDCLText() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("RequestDCLText"));
		return element;
	}
	
	public WebElement requestCurrency() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("RequestCurrency"));
		return element;
	}
	public WebElement maxPayTermText() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("MaxPayTermText"));
		return element;
	}
	
	public WebElement durationSelectDropList() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("DurationPeriodSelectDropList"));
		return element;
	}
	
	public WebElement creditRateSelectDropList() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("CreditRateSelectDropList"));
		return element;
	}
	
	public WebElement insureRatio() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("InsureRatio"));
		return element;
	}
	
	public WebElement creditSubmitButton() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("CreditSubmitButton"));
		return element;
	}
	
	public WebElement creditApproved() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("CreditApproved"));
		return element;
	}
	public WebElement DCWindowsCloseButton() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("DCWindowsCloseButton"));
		return element;
	}
	public WebElement detailNameId() {
		By bySi = repoElementsManageAddBuyer.byLocator("DetailNameID");
		element = driver.findElement(bySi);
		return element;
	}
	public WebElement serviceButton() {
		By bySi = repoElementsManageAddBuyer.byLocator("ServiceButton");
		element = driver.findElement(bySi);
		return element;
	}

	/**
	 * Navigate to tab
	 * 
	 */
	public WebElement detail_IdentityTab() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("LinkLB_Identity"));
		return element;
	}

	public WebElement detail_CoverageTab() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("LinkLB_Coverage"));
		return element;
	}

	public WebElement detail_TotalTab() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("LinkLB_Total"));
		return element;
	}

	public WebElement detail_CommentsTab() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("LinkLB_Comments"));
		return element;
	}
	public WebElement DCLTile() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("DCLTile"));
		return element;
	}
	public WebElement DCLCreditAmount() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("DCLCreditAmount"));
		return element;
	}
	public WebElement DCLPaymentTerms() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("DCLPaymentTerms"));
		return element;
	}
	public WebElement DCLExpiryDate() {
		element = driver.findElement(repoElementsManageAddBuyer.byLocator("DCLExpiryDate"));
		return element;
	}


	/*
	 * Identity 
	 * 
	 * 
	 */

}
