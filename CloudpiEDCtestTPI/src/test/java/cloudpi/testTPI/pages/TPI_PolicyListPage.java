package cloudpi.testTPI.pages;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import cloudpi.testTPI.testLibs.AppConstant;

public class TPI_PolicyListPage extends cloudpi.testTPI.pages.TPI_HomePage {

	public static WebElement element = null;

	static Logger log = Logger.getLogger(TPI_PolicyListPage.class.getName());
	
	protected  TPI_RepositoryElements repoElementsPolicyView= null;

	private String policyViewPgRepoFile = AppConstant.TPIpropertyPath +"TPI_PolicyViewListRepository.properties";
	
	public TPI_PolicyListPage(WebDriver driver) throws IOException {
		super(driver);
		repoElementsPolicyView = new TPI_RepositoryElements(policyViewPgRepoFile);
	}
	
	// ### Policy List menu item ###
	public WebElement PolicyListMenu_View() {
		element = driver.findElement(repoElementsPolicyView.byLocator("PolicyListMenu_View"));
		return element;
	}
	
	public WebElement PolicyListMenu_PolicyList() {
		element = driver.findElement(repoElementsPolicyView.byLocator("PolicyListMenu_PolicyList"));
		return element;
	}
	
	public WebElement PolicyList_PolicyTypeSearch() {
		element = driver.findElement(repoElementsPolicyView.byLocator("PolicyList_PolicyTypeSearch"));
		return element;
	}
	
	public WebElement PolicyList_PolicySearchCriteria() {
		element = driver.findElement(repoElementsPolicyView.byLocator("PolicyList_PolicySearchCriteria"));
		return element;
	}
	
	public WebElement PolicyList_PolicySearchBtn() {
		element = driver.findElement(repoElementsPolicyView.byLocator("PolicyList_PolicySearchBtn"));
		return element;
	}
	
	
	public WebElement PolicyList_SearchResults() {
		element = driver.findElement(repoElementsPolicyView.byLocator("PolicyList_SearchResults"));
		return element;
	}
	
	public WebElement PolicyList_AddBtn() {
		element = driver.findElement(repoElementsPolicyView.byLocator("PolicyList_AddBtn"));
		return element;
	}
	
	
	// ### Elements possibly duplicated in another file ###
	public WebElement PolicyList_AddNewPolicy_Province() {
		element = driver.findElement(repoElementsPolicyView.byLocator("PolicyList_AddNewPolicy_Province"));
		return element;
	}
	
	public WebElement PolicyList_AddNewPolicy_CompanyName() {
		element = driver.findElement(repoElementsPolicyView.byLocator("PolicyList_AddNewPolicy_CompanyName"));
		return element;
	}
	
	public WebElement PolicyList_AddNewPolicy_ZipCode() {
		element = driver.findElement(repoElementsPolicyView.byLocator("PolicyList_AddNewPolicy_ZipCode"));
		return element;
	}
	
	public WebElement PolicyList_AddNewPolicy_City() {
		element = driver.findElement(repoElementsPolicyView.byLocator("PolicyList_AddNewPolicy_City"));
		return element;
	}
	
	public WebElement PolicyList_AddNewPolicy_DunsNum() {
		element = driver.findElement(repoElementsPolicyView.byLocator("PolicyList_AddNewPolicy_DunsNum"));
		return element;
	}
	
	public WebElement PolicyList_AddNewPolicy_Result() {
		element = driver.findElement(repoElementsPolicyView.byLocator("PolicyList_AddNewPolicy_Result"));
		return element;
	}
	
	public WebElement PolicyList_AddNewPolicy_SubmitBtn() {
		element = driver.findElement(repoElementsPolicyView.byLocator("PolicyList_AddNewPolicy_SubmitBtn"));
		return element;
	}
	
	
	public WebElement PolicyList_RegisterPolicy_InsPolNum() {
		element = driver.findElement(repoElementsPolicyView.byLocator("PolicyList_RegisterPolicy_InsPolNum"));
		return element;
	}
	
	public WebElement PolicyList_RegisterPolicy_PolCurr() {
		element = driver.findElement(repoElementsPolicyView.byLocator("PolicyList_RegisterPolicy_PolCurr"));
		return element;
	}
	
	public WebElement PolicyList_RegisterPolicy_SocialResponsibility() {
		element = driver.findElement(repoElementsPolicyView.byLocator("PolicyList_RegisterPolicy_SocialResponsibility"));
		return element;
	}
	
	public WebElement PolicyList_RegisterPolicy_CreditIns() {
		element = driver.findElement(repoElementsPolicyView.byLocator("PolicyList_RegisterPolicy_CreditIns"));
		return element;
	}
	
	public WebElement PolicyList_RegisterPolicy_SubmitBtn() {
		element = driver.findElement(repoElementsPolicyView.byLocator("PolicyList_RegisterPolicy_SubmitBtn"));
		return element;
	}
}
