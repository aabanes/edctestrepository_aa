package cloudpi.testTPI.pages;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import cloudpi.testTPI.testLibs.AppConstant;

public class TPI_CreditApprovalRequestPage extends cloudpi.testTPI.pages.TPI_HomePage {

	public static WebElement element = null;

	static Logger log = Logger.getLogger(TPI_ServiceLimitIncreasePage.class.getName());

	protected TPI_RepositoryElements repoElementsCLRequest = null;

	private String buyerLimiteReqPgRepoFile = AppConstant.TPIpropertyPath
			+ "TPI_CreditLimitRequestRepository.properties";

	public TPI_CreditApprovalRequestPage(WebDriver driver) throws IOException {
		super(driver);
		repoElementsCLRequest = new TPI_RepositoryElements(buyerLimiteReqPgRepoFile);
	}

	
	public WebElement CreditLimitInc_CreditApprovalRequestBtn() {
		element = driver.findElement(repoElementsCLRequest.byLocator("CreditLimitInc_CreditApprovalRequestBtn"));
		return element;
	}
	
	public WebElement CreditLimitInc_InsCreditLimit() {
		element = driver.findElement(repoElementsCLRequest.byLocator("CreditLimitInc_InsCreditLimit"));
		return element;
	}
	
	public WebElement CreditLimitInc_ReqCreditApprv() {
		element = driver.findElement(repoElementsCLRequest.byLocator("CreditLimitInc_ReqCreditApprv"));
		return element;
	}
	
	public WebElement CreditLimitInc_ReqCreditApprvCurr() {
		element = driver.findElement(repoElementsCLRequest.byLocator("CreditLimitInc_ReqCreditApprvCurr"));
		return element;
	}
	
	public WebElement CreditLimitInc_MaxPaymentTerms() {
		element = driver.findElement(repoElementsCLRequest.byLocator("CreditLimitInc_MaxPaymentTerms"));
		return element;
	}
	
	public WebElement CreditLimitInc_CoveragePeriod() {
		element = driver.findElement(repoElementsCLRequest.byLocator("CreditLimitInc_CoveragePeriod"));
		return element;
	}
	
	public WebElement CreditLimitInc_BuyerCreditRating() {
		element = driver.findElement(repoElementsCLRequest.byLocator("CreditLimitInc_BuyerCreditRating"));
		return element;
	}
	
	public WebElement CreditLimitInc_CoInsRatio() {
		element = driver.findElement(repoElementsCLRequest.byLocator("CreditLimitInc_CoInsRatio"));
		return element;
	}
	
	public WebElement CreditLimitInc_OverduesPending() {
		element = driver.findElement(repoElementsCLRequest.byLocator("CreditLimitInc_OverduesPending"));
		return element;
	}

	public WebElement CreditLimitInc_NewClientIns() {
		element = driver.findElement(repoElementsCLRequest.byLocator("CreditLimitInc_NewClientIns"));
		return element;
	}
	
	public WebElement CreditLimitInc_Comments() {
		element = driver.findElement(repoElementsCLRequest.byLocator("CreditLimitInc_Comments"));
		return element;
	}
	
	public WebElement CreditLimitInc_SubmitBtn() {
		element = driver.findElement(repoElementsCLRequest.byLocator("CreditLimitInc_SubmitBtn"));
		return element;
	}
	
	public WebElement CreditLimitAcceptQuote() {
		element = driver.findElement(repoElementsCLRequest.byLocator("CreditLimitAcceptQuote"));
		return element;
	}
	
	public WebElement CreditLimitDeclineQuote() {
		element = driver.findElement(repoElementsCLRequest.byLocator("CreditLimitDeclineQuote"));
		return element;
	}
	
	public WebElement CreditLimitCancelFuture() {
		element = driver.findElement(repoElementsCLRequest.byLocator("CreditLimitCancelFuture"));
		return element;
	}
	
	public WebElement CreditLimitCancellation() {
		element = driver.findElement(repoElementsCLRequest.byLocator("CreditLimitCancellation"));
		return element;
	}
	
	public WebElement CreditLimitEffectiveDate() {
		element = driver.findElement(repoElementsCLRequest.byLocator("CreditLimitEffectiveDate"));
		return element;
	}
	
	public WebElement CreditLimitSubmitBtn() {
		element = driver.findElement(repoElementsCLRequest.byLocator("CreditLimitSubmitBtn"));
		return element;
	}
	
	public WebElement CreditLimitPopupClose() {
		element = driver.findElement(repoElementsCLRequest.byLocator("CreditLimitPopupClose"));
		return element;
	}
}