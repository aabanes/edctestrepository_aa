
package cloudpi.testTPI.pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PortfolioDetailPage extends cloudpi.testTPI.pages.TPI_HomePage {

	public static WebElement element = null;

	private WebDriverWait drWait = null;

	static Logger log = Logger.getLogger(PortfolioDetailPage.class.getName());

	public PortfolioDetailPage(WebDriver driver) {
		super(driver);
	}

	public WebElement detailNameId() {

		By bySi = repoElements.byLocator("DetailNameID");
		element = driver.findElement(bySi);
		return element;
	}
	public WebElement serviceButton() {
		By bySi = repoElements.byLocator("ServiceButton");
		element = driver.findElement(bySi);
		return element;
	}


	/**
	 * Navigate to tab
	 * 
	 */
	public WebElement detail_IdentityTab() {
		element = driver.findElement(repoElements.byLocator("LinkLB_Identity"));
		return element;
	}

	public WebElement detail_CoverageTab() {
		element = driver.findElement(repoElements.byLocator("LinkLB_Coverage"));
		return element;
	}

	public WebElement detail_TotalTab() {
		element = driver.findElement(repoElements.byLocator("LinkLB_Total"));
		return element;
	}

	public WebElement detail_CommentsTab() {
		element = driver.findElement(repoElements.byLocator("LinkLB_Comments"));
		return element;
	}


	/*
	 * Identity 
	 * 
	 * 
	 */


}
