package cloudpi.testTPI.testLibs;

import java.util.concurrent.TimeUnit;

//import comTestLibs.SearCons;


import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.ITestContext;
import org.testng.annotations.*;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import cloudpi.testTPI.testLibs.AppConstant;

import org.testng.asserts.Assertion;
import org.testng.Assert;

//import appcom.searspages.RepositoryElements;

public class BaseTest {

	public static WebDriver driver = null;
	public static String baseBrowser;
	public static String baseUrl;

	private Assertion hardAssert = new Assertion();
	private SoftAssert softAssert = new SoftAssert();
	
	
	public void launchBrowser(String runBrowser) {

		baseBrowser = runBrowser.toLowerCase();

		if (baseBrowser.equals("firefox")) {
			driver = new FirefoxDriver();
		} else if (baseBrowser.equals("ie")) {
			driver = new InternetExplorerDriver();
		} else if (baseBrowser.equals("Chrome")) {
			driver = new ChromeDriver();
		} else if (baseBrowser.equals("Safari")) {
			driver = new ChromeDriver();
		} else {
			driver = new FirefoxDriver();
		}
	}

	@BeforeClass(enabled = true )
//	@Parameters({ "runBrowser", "runUrl" })
//	public void setUpBrowser(String runBrowser, String runUrl ) {
	public void setUpBrowser() {

//			baseBrowser = "ie";
//			baseBrowser = "chrome";
	     	baseBrowser = "firefox";	
		baseUrl = "https://cis-qac.edc.ca/herakles/do";

//		    baseBrowser = runBrowser;   baseUrl = runUrl;

		launchBrowser(baseBrowser, baseUrl);
		Reporter.log("Application Lauched successfully | ");

		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
	}
	public void launchBrowser(String runBrowser, String runUrl) {

		String baseBrowser1 = runBrowser.toLowerCase().trim();
		String baseUrl1 = runUrl;

		if (baseBrowser1.equals("firefox")) {
//			FirefoxProfile ffprofile= new FirefoxProfile();
//			ffprofile.setPreference("\\edcfs2\\share\\griebr\\firefox.exe -P Test -no-remote");
			System.setProperty("webdriver.firefox.bin", "./TestLibrary/firefox/firefox.exe");
			driver = new FirefoxDriver();
		} else if (baseBrowser1.equals("ie")) {
			System.setProperty("webdriver.ie.driver","./TestLibrary/IEDriverServer_Win32_2.47.0/IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		} else if (baseBrowser1.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver","./TestLibrary/chromedriver_win32/chromedriver.exe");
			driver = new ChromeDriver();
		} else if (baseBrowser1.equals("Safari")) {
			driver = new ChromeDriver();
		} else {
			driver = new FirefoxDriver();
		}
		driver.get(baseUrl1);
	}

	public void closeBrowser() {

		driver.close();
		driver.quit();
	}

	public void freshPage() {

		driver.get(baseUrl);

	}

	// @Test(priority = 1, enabled = true )
	// public void f() {
	// }

	// @BeforeMethod(priority = 1, enabled = true )
	// public void beforeMethod() {
	// }
	//
	// @Parameters({ "first-name" })
	// @AfterMethod
	// public void afterMethod() {
	// }
	//
	// @BeforeClass
	// public void beforeClass() {
	//
	// }
	//
	// @AfterClass
	// public void afterClass() {
	// }
	//
//	 @BeforeTest
//	 public void beforeTest() {
//	 }
	//
	// @AfterTest
	// public void afterTest() {
	// }

	@BeforeSuite(alwaysRun = true)
	public void setUpSuite(ITestContext context) {
		 String runPath = System.getProperty("user.dir");
		 AppConstant.projectPath = runPath ;
		PropertyConfigurator.configure("./config/Log4j.properties");
		baseBrowser = context.getCurrentXmlTest().getParameter("runBrowser");
//		 System.out.println("baseBrowser 000000 --> "+baseBrowser );
		baseUrl = context.getCurrentXmlTest().getParameter("runUrl");
	}

	@AfterSuite
	public void teardownSuite() {
	}

	/*
	 * 
	/*
	 * Below are the verify method using TestNg Assertion / SoftAssert to verify
	 * the test cases
	 */

	public boolean verify(boolean actual, boolean expected) {

		boolean act = actual;
		boolean exp = expected;
		try {
			hardAssert.assertEquals(act, exp);
		} catch (Exception e) {
			Assert.fail();
		}
		return true;
	}

	public boolean verify(boolean actual, boolean expected, String message) {

		boolean act = actual;
		boolean exp = expected;
		String mes = message;
		try {
			hardAssert.assertEquals(act, exp, mes);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verify(byte[] actual, byte[] expected, String message) {

		// boolean act = actual ; boolean exp = expected; String mes = message;
		try {
			hardAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verify(byte actual, byte expected, String message) {

		try {
			hardAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verify(java.util.Collection actual,
			java.util.Collection expected, String message) {

		try {
			hardAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verify(double actual, double expected, double delta,
			String message) {

		try {
			hardAssert.assertEquals(actual, expected, delta, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verify(float actual, float expected, float delta,
			String message) {

		try {
			hardAssert.assertEquals(actual, expected, delta, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verify(int actual, int expected, String message) {

		try {
			hardAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verify(long actual, long expected, String message) {

		try {
			hardAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verify(java.lang.Object[] actual,
			java.lang.Object[] expected, String message) {

		try {
			hardAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verify(java.lang.Object actual, java.lang.Object expected,
			String message) {

		try {
			hardAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			System.out.println(e.toString());
			hardAssert.fail();
		}
		return true;
	}

	public boolean verify(String actual, String expected, String message) {

		try {
			hardAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			System.out.println("verify fail in ----> "+ e.toString());
			hardAssert.fail();
		}
		return true;
	}

	public boolean verifyTrue(boolean condition, String message) {

		try {
			hardAssert.assertTrue(condition, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verifyNoTrue(boolean condition, String message) {

		try {
			hardAssert.assertFalse(condition, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public void verifyFail(String message) {

		try {
			hardAssert.fail(message);
		} catch (Exception e) {
			hardAssert.fail();
		}
	}

	public boolean verifySame(java.lang.Object actual,
			java.lang.Object expected, String message) {

		try {
			hardAssert.assertSame(actual, expected, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verifyNotSame(java.lang.Object actual,
			java.lang.Object expected, String message) {

		try {
			hardAssert.assertNotSame(actual, expected, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verifyNull(java.lang.Object object, String message) {

		try {
			hardAssert.assertNull(object, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verifyNotNull(java.lang.Object object, String message) {

		try {
			hardAssert.assertNotNull(object, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	/*
	 * Below are Soft Assertion in order to continue run test case rest of steps
	 * even the
	 * 
	 * Assert exception happened
	 */

	public boolean softVerify(boolean actual, boolean expected) {

		boolean act = actual;
		boolean exp = expected;
		try {
			softAssert.assertEquals(act, exp);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(boolean actual, boolean expected, String message) {

		boolean act = actual;
		boolean exp = expected;
		String mes = message;
		try {
			softAssert.assertEquals(act, exp, mes);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(byte[] actual, byte[] expected, String message) {

		// boolean act = actual ; boolean exp = expected; String mes = message;
		try {
			softAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(byte actual, byte expected, String message) {

		try {
			softAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(java.util.Collection actual,
			java.util.Collection expected, String message) {

		try {
			softAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(double actual, double expected, double delta,
			String message) {

		try {
			softAssert.assertEquals(actual, expected, delta, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(float actual, float expected, float delta,
			String message) {

		try {
			softAssert.assertEquals(actual, expected, delta, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(int actual, int expected, String message) {

		try {
			System.out.println("start soft verify ----");
//			Assert.assertEquals(actual, expected, message);
			if  (actual == expected) {
				System.out.println("Test is pass for thid step ");
			}else{
				System.out.println(message) ;
				softAssert.fail();
//				Assert.fail();
			}
		} catch (Exception e) {
			System.out.println("trigger Excrption soft verify ----");
//			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(long actual, long expected, String message) {

		try {
			softAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(java.lang.Object[] actual,
			java.lang.Object[] expected, String message) {

		try {
			softAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(java.lang.Object actual,
			java.lang.Object expected, String message) {

		try {
			softAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(String actual, String expected, String message) {

		try {
			softAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerifyTrue(boolean condition, String message) {

		try {
			softAssert.assertTrue(condition, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerifyNotTrue(boolean condition, String message) {

		try {
			softAssert.assertFalse(condition, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerifyFail(String message) {

		try {
			softAssert.fail(message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerifySame(java.lang.Object actual,
			java.lang.Object expected, String message) {

		try {
			softAssert.assertSame(actual, expected, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean SoftVerifyNotSame(java.lang.Object actual,
			java.lang.Object expected, String message) {

		try {
			softAssert.assertNotSame(actual, expected, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean SoftVerifyNull(java.lang.Object object, String message) {

		try {
			softAssert.assertNull(object, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean SfotVerifyNotNull(java.lang.Object object, String message) {

		try {
			softAssert.assertNotNull(object, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}
	
	public void softVerifyAll() {
		  softAssert.assertAll();
		}
	public void sleep(double seconds) throws InterruptedException {
		
		double mSeconds = seconds * 1000;
		Double dSeconds =new Double(mSeconds);
		int sleepMiSeconds = dSeconds.intValue();
		Thread.sleep(sleepMiSeconds);
	}
	

	  	
}
