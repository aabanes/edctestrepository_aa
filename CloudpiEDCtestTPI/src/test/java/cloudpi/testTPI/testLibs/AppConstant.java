package cloudpi.testTPI.testLibs;


public class AppConstant {
	
	//This is the list of System Variables
	
	public static final String BaseURL = "https://cis-qac.edc.ca/herakles/do";
	public static final String FF = "Firefox";
	public static final String IE = "InternetExplore";
	public static final String CR = "C";
	public static String projectPath = "F:\\Cloudpipe";
	public static int count = 0;
	public static int testcasesNumbers = 0;
	public static final String SignedPageTitleTPI = "EDC Trade Partnership Insurance - Brian Grieve";
	public static String exportingPageNameEDC = "Getting Started - Export Development Canada (EDC)";
	public static String solutionPageNameEDC = "Our Solutions - Export Development Canada (EDC)";
	public static String countryPageNameEDC = "Country Info - Export Development Canada (EDC)";
	public static String knowledgePageNameEDC = "Knowledge Centre - Export Development Canada (EDC)";
	public static String eventsPageNameEDC = "Upcoming Events";
	public static String aboutUsPageNameEDC = "About us - Export Development Canada (EDC)";
	public static String TPIpropertyPath = "../CloudpiEDCtestTPI/config/";		
}
