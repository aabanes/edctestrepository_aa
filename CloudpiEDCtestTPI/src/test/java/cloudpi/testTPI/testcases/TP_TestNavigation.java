package cloudpi.testTPI.testcases;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import cloudpi.testbkof.pages.EDC_BackOffice;
import cloudpi.testTP.pages.RepositoryElements;
import cloudpi.testTP.pages.TP_ApplyForApprovalPage;
import cloudpi.testTP.pages.TP_LandingPage;
import cloudpi.testTP.pages.TP_Menu;
import cloudpi.testTP.pages.TP_PortfolioPage;

public class TP_TestNavigation extends cloudpi.testTP.testLibs.BaseTest {

	private String username = "B.Grieve61";
	private String password = "1Test12345";

	private TP_LandingPage tpLand = null;
	private TP_Menu tpMenu = null;
	private TP_PortfolioPage tpPortfolio = null;
	private TP_ApplyForApprovalPage tpApproval = null;
	private String policyNum = "";
	private RepositoryElements repEle = null;
	private By newBy = null;
	private By newBy2 = null;
	private EDC_BackOffice bkOffice = null;

	@Test(priority = 1, enabled = false)
	@Parameters({ "username", "password" })
	public void TPNavigation() {

		tpLand = new TP_LandingPage(driver);
		tpMenu = new TP_Menu(driver);
		tpPortfolio = new TP_PortfolioPage(driver);

		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		// Starts on Landing Page. Populate Email and Password fields with
		// username and password values
		// then clicks login

		Reporter.log("Beginning test case TPNavigation");
		tpLand.TP_Landing_Email().sendKeys(username);
		tpLand.TP_Landing_Password().sendKeys(password);
		tpLand.TP_Landing_LoginBtn().click();

		if (verify(tpMenu.TP_MenuNav().isDisplayed(), true)) {
			Reporter.log("Successfully logged in");
		} else {
			Reporter.log("Unsuccessfully logged in - Menu not found");
		}

		// Clicks on the Apply For Approval button
		// tpMenu.TP_Menu_ApplyForApprovalButton().click();
		Reporter.log("@ Apply For A Approval page");

		//Clicks on the View My Portfolio button
		tpMenu.TP_Menu_ViewPortfolioButton().click();
		Reporter.log("@ View Portfolio page");
		tpMenu.TP_Menu_ManagePolicyButton().click();
		Reporter.log("@ Manage Policy page");
		tpMenu.TP_Menu_ManageUsersButton().click();
		Reporter.log("@ Manage Users page");
		tpMenu.TP_Menu_ShowAcount().click();
		tpMenu.TP_Menu_SignOut().click();
	}

	@Test(priority = 1, enabled = false)
	@Parameters({ "username", "password" })
	public void TP_ApplyForApproval_Decline() {

		tpLand = new TP_LandingPage(driver);
		tpMenu = new TP_Menu(driver);
		tpApproval = new TP_ApplyForApprovalPage(driver);

		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		String date = new SimpleDateFormat("dd/MM/yyyy").format(new Date());

		Reporter.log("Beginning test case TP_ApplyForApproval_Decline");
		tpLand.TP_Landing_Email().sendKeys(username);
		tpLand.TP_Landing_Password().sendKeys(password);
		tpLand.TP_Landing_LoginBtn().click();

		if (verify(tpMenu.TP_MenuNav().isDisplayed(), true)) {
			Reporter.log("Successfully logged in");
		} else {
			Reporter.log("Unsuccessfully logged in - Menu not found");
			Assert.fail("Unsuccessfully logged in - Menu not found");
		}

		tpMenu.TP_Menu_ApplyForApprovalButton().click();

		// Checking to see if we're at step 1 and, if so, populate fields.
		if (verify(tpApproval.TP_ApplyForApproval_Step1Active().isDisplayed(), true)) {
			Reporter.log("@ Apply For A Approval - Step 1");

			// Ideally this would be from a file and not hardcoded
			tpApproval.TP_ApplyForApproval_stp1_nameTextfield().sendKeys("Weyerhaeuser Company");
			tpApproval.TP_ApplyForApproval_stp1_addressTextfield().sendKeys("33663 Weyerhaeuser Way S PO BOX 9777");
			tpApproval.TP_ApplyForApproval_stp1_cityTextField().sendKeys("Federal Way");

			Select country = new Select(tpApproval.TP_ApplyForApproval_stp1_countryDropDown());
			country.selectByVisibleText("UNITED STATES");

			Select state = new Select(tpApproval.TP_ApplyForApproval_stp1_stateDropDown());
			state.selectByVisibleText("WASHINGTON");

			tpApproval.TP_ApplyForApproval_stp1_postalCodeTextfield().sendKeys("98003");
			tpApproval.TP_ApplyForApproval_stp1_dnsNumberTextfield().sendKeys("1306992");

			tpApproval.TP_ApplyForApproval_stp1_SearchButton().click();
			tpApproval.TP_ApplyForApproval_stp1_SearchPane().click();

		} else {
			Assert.fail("Not at correct step!");
		}

		// Checking to see if we're at step 2 and, if so, populate fields.
		if (verify(tpApproval.TP_ApplyForApproval_Step2Active().isDisplayed(), true)) {
			Reporter.log("@ Apply For A Approval - Step 2");

			// Ideally this would be from a file and not hardcoded
			tpApproval.TP_ApplyForApproval_stp2_creditLimitTextField().sendKeys("5000");
			tpApproval.TP_ApplyForApproval_stp2_coveragePeriod90().click();
			tpApproval.TP_ApplyForApproval_stp2_paymentTerms90().click();
			tpApproval.TP_ApplyForApproval_stp2_nextButton().click();
		} else {
			Assert.fail("Not at correct step!");
		}

		// Checking to see if we're at step 3 and, if so, populate fields.
		if (verify(tpApproval.TP_ApplyForApproval_Step3Active().isDisplayed(), true)) {
			Reporter.log("@ Apply For A Approval - Step 3");
			tpApproval.TP_ApplyForApproval_stp3_goodsExportedYes().click();
			tpApproval.TP_ApplyForApproval_stp3_negativeInfoNo().click();
			tpApproval.TP_ApplyForApproval_stp3_sellingCustNo().click();
			tpApproval.TP_ApplyForApproval_stp3_agreement().click();
			tpApproval.TP_ApplyForApproval_stp3_submitButton().click();
		} else {
			Assert.fail("Not at correct step!");
		}

		// Checking to see if we're at step 4 and, if so, populate fields.
		if (verify(tpApproval.TP_ApplyForApproval_Step4Active().isDisplayed(), true)) {
			Reporter.log("@ Apply For A Approval - Step 4");
			try {
				sleep(10);
			} catch (InterruptedException e) {
				Reporter.getCurrentTestResult().setStatus(ITestResult.FAILURE);
				Reporter.log(e.getMessage());
				e.printStackTrace();
			}
		} else {
			Assert.fail("Not at correct step!");
		}

		// Outstanding Order field. Verify values are as expected. Again, should
		// be from a file.
		if (verify(tpApproval.TP_ApplyForApproval_osOffer_header().getText().equals("Outstanding Offer"), true)) {
			Reporter.log("@ On Outstanding Offers page - Validating Offer");
			validateItems(tpApproval.TP_ApplyForApproval_osOffer_eDCApprvReq_dateOfRequest().getText().equals(date),
					tpApproval.TP_ApplyForApproval_osOffer_eDCApprvReq_dateOfRequest().getText());
			validateItems(
					tpApproval.TP_ApplyForApproval_osOffer_eDCApprvReq_customer().getText()
							.equals("BRIDGESTONE AMERICAS TIRE OPERATIONS, LLC 535 MARRIOT DRNASHVILLE, TN, 37214 UNITED STATES"),
					tpApproval.TP_ApplyForApproval_osOffer_eDCApprvReq_customer().getText());
			validateItems(
					tpApproval.TP_ApplyForApproval_osOffer_eDCApprvReq_requestedCreditLimit().getText()
							.equals("USD 5,000.00"),
					tpApproval.TP_ApplyForApproval_osOffer_eDCApprvReq_requestedCreditLimit().getText());
			validateItems(
					tpApproval.TP_ApplyForApproval_osOffer_eDCApprvReq_coveragePeriod().getText().equals("90 days"),
					tpApproval.TP_ApplyForApproval_osOffer_eDCApprvReq_coveragePeriod().getText());
			validateItems(tpApproval.TP_ApplyForApproval_osOffer_eDCApprvReq_negInfo().getText().equals("No"),
					tpApproval.TP_ApplyForApproval_osOffer_eDCApprvReq_negInfo().getText());

			validateItems(
					tpApproval.TP_ApplyForApproval_osOffer_eDCApprvOffer_decision().getText().equals("Full Approval"),
					tpApproval.TP_ApplyForApproval_osOffer_eDCApprvOffer_decision().getText());
			validateItems(isNumeric(tpApproval.TP_ApplyForApproval_osOffer_eDCApprvOffer_refNum().getText()),
					tpApproval.TP_ApplyForApproval_osOffer_eDCApprvOffer_refNum().getText());
			validateItems(
					tpApproval.TP_ApplyForApproval_osOffer_eDCApprvOffer_creditLimit().getText().equals("USD 5,000.00"),
					tpApproval.TP_ApplyForApproval_osOffer_eDCApprvOffer_creditLimit().getText());
			// Cannot validate Coverage Date range as business rules behind
			// range is unknown
			validateItems(tpApproval.TP_ApplyForApproval_osOffer_eDCApprvOffer_insPercentage().getText().equals("90%"),
					tpApproval.TP_ApplyForApproval_osOffer_eDCApprvOffer_insPercentage().getText());
			validateItems(
					tpApproval.TP_ApplyForApproval_osOffer_eDCApprvOffer_premiumAmt().getText().equals("USD 27.68"),
					tpApproval.TP_ApplyForApproval_osOffer_eDCApprvOffer_premiumAmt().getText());
			// Cannot validate Offer Expiry Date as business rules behind date
			// is unknown

			validateItems(
					tpApproval.TP_ApplyForApproval_osOffer_policyProfile_company().getText()
							.equals("CP ENERGY MARKETING (US) INC 505 2 ST SW 8TH FLCALGARY, AB, T2P 1N8 CANADA"),
					tpApproval.TP_ApplyForApproval_osOffer_policyProfile_company().getText());
			// Cannot validate Policy Num as business rules behind Policy Num is
			// unknown
			validateItems(
					tpApproval.TP_ApplyForApproval_osOffer_policyProfile_mainContactName().getText()
							.equals("Elaine Elias"),
					tpApproval.TP_ApplyForApproval_osOffer_policyProfile_mainContactName().getText());
			validateItems(tpApproval.TP_ApplyForApproval_osOffer_policyProfile_policyLang().getText().equals("English"),
					tpApproval.TP_ApplyForApproval_osOffer_policyProfile_policyLang().getText());
			validateItems(tpApproval.TP_ApplyForApproval_osOffer_policyProfile_policyCurr().getText().equals("USD"),
					tpApproval.TP_ApplyForApproval_osOffer_policyProfile_policyCurr().getText());

			Calendar c = Calendar.getInstance();
			c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			String newDate = format.format(c.getTime());
			validateItems(
					tpApproval.TP_ApplyForApproval_osOffer_policyProfile_policyEffectiveDate().getText()
							.equals(newDate),
					tpApproval.TP_ApplyForApproval_osOffer_policyProfile_policyEffectiveDate().getText());

			tpApproval.TP_ApplyForApproval_osOffer_agreement().click();
			tpApproval.TP_ApplyForApproval_osOffer_acceptPayButton().click();
		} else {
			Assert.fail("Not at Outstanding Offer page!");
		}

		if (verify(tpApproval.TP_ApplyForApproval_osOffer_CCheader().getText().equals("Outstanding Offer Payment"),
				true)) {
			driver.switchTo().frame("frameMoneris");
			tpApproval.TP_ApplyForApproval_osOffer_cancelTransButton().click();

			if (verify(tpApproval.TP_ApplyForApproval_osOffer_cancelMsg().getText()
					.equals("Payment has been cancelled. Outstanding Offer not modified."), true)) {
				Reporter.log("Cancellation message is present");
			} else {
				Assert.fail("Cancellation message not present");
			}
			tpApproval.TP_ApplyForApproval_osOffer_showMeOffer().click();
		} else {
			Assert.fail("Not at Outstanding Offer Payment page!");
		}

		driver.switchTo().defaultContent();

		/*
		 * tpApproval.TP_ApplyForApproval_osOffer_declineOffer().click(); Select
		 * declineReason = new
		 * Select(tpApproval.TP_ApplyForApproval_osOffer_declineOfferInPopup());
		 * declineReason.selectByVisibleText("No longer selling to the customer"
		 * ); tpApproval.TP_ApplyForApproval_osOffer_declineOfferCnfm().click();
		 * 
		 * if(verify(tpApproval.TP_ApplyForApproval_osOffer_declineMsg().getText
		 * ().equals("You have declined this offer on " + date), true)) {
		 * Reporter.log("Decline message present"); } else { Reporter.log(
		 * "Decline message not correct. Message is as follows:");
		 * Reporter.log(tpApproval.TP_ApplyForApproval_osOffer_declineMsg().
		 * getText()); }
		 */
	}

	@Test(priority = 1, enabled = true)
	@Parameters({ "username", "password" })
	public void TP_Test() {
		tpLand = new TP_LandingPage(driver);
		tpMenu = new TP_Menu(driver);
		tpPortfolio = new TP_PortfolioPage(driver);
		bkOffice = new EDC_BackOffice(driver);

		try {
			repEle = new RepositoryElements("./config/Repository.properties");
			newBy = repEle.byLocator("TP_Portfolio_table");
			newBy2 = repEle.byLocator("BKOF_Task_Table");
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		tpLand.TP_Landing_Email().sendKeys(username);
		tpLand.TP_Landing_Password().sendKeys(password);
		tpLand.TP_Landing_LoginBtn().click();

		tpMenu.TP_Menu_ViewPortfolioButton().click();
		tpPortfolio.TP_PortfolioNav_UnderReviewLabel().click();
		try {
			sleep(5);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<WebElement> resultsPaneEle = driver.findElements(newBy);
		for (int i = 0; i < resultsPaneEle.size(); i++) {
			if (resultsPaneEle.get(i).findElement(By.tagName("h3")).getText().contains("WEYERHAEUSER COMPANY")) {
				String res = resultsPaneEle.get(i).findElement(By.tagName("h3")).getText();
				policyNum = res.substring(res.indexOf("(") + 1, res.lastIndexOf(")"));
				break;
			}
		}
		
		if(policyNum == "" || policyNum == null)
		{
			Assert.fail("Policy # is empty -- for some reason");
		}
		
		driver.navigate().to("https://cis-qac.edc.ca/grams/do");
		bkOffice.BKOF_Login().sendKeys("B.Grieve");
		bkOffice.BKOF_Password().sendKeys("1Test123");
		bkOffice.BKOF_LoginBtn().click();
		
		bkOffice.BKOF_Lookup().sendKeys(policyNum);
		bkOffice.BKOF_Lookup().sendKeys(Keys.RETURN);
		
		try {
			sleep(10);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		//System.out.println(bkOffice.BKOF_Header().getText());
		System.out.println(bkOffice.BKOF_Gross_Exposure().getText().replace("K", "").trim());
		System.out.println(bkOffice.BKOF_Total_Exposure().getText().replace("K", "").trim());
		
		bkOffice.BKOF_Menu_Tasks().click();
		try {
			sleep(5);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<WebElement> resultsPaneEle2 = driver.findElements(newBy2);
		for (int i = 0; i < resultsPaneEle2.size(); i++) {
			if (resultsPaneEle2.get(i).findElement(By.tagName("td")).getText().contains("Application")) {
				resultsPaneEle2.get(i).findElement(By.tagName("td")).click();
			}
			if (bkOffice.BKOF_Demand_ReqAmt().getText()=="5 KUSD" && bkOffice.BKOF_Demand_Notes().isDisplayed())
			{
				break;
			}
			try {
				sleep(3);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println(bkOffice.BKOF_Task_New_Assignee().getText());
		
			if (bkOffice.BKOF_Task_New_Assignee().getText().equals("B.Grieve") == false)
			{
				bkOffice.BKOF_Task_New_Assignee().clear();
				bkOffice.BKOF_Task_New_Assignee().sendKeys("B.Grieve");
				bkOffice.BKOF_Task_AutoComplete().click();
				bkOffice.BKOF_Task_New_Assignee_SubmitBtn().click();
			}
			else
			{
				Assert.fail("New Assignee field missing!");
			}
		
		bkOffice.BKOF_Decision_Accept().click();
		bkOffice.BKOF_Decision_SubmitBtn().click();
		bkOffice.BKOF_Decision_CfrmPopup_CancelBtn().click();
		//bkOffice.BKOF_Decision_CfrmPopup_CfrmSubmitBtn().click();
	}

	private boolean isNumeric(String num) {
		Reporter.log("Validating Reference Number (if contains only numeric characters)");
		try {
			double d = Double.parseDouble(num);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	private void validateItems(boolean result, String text) {
		if (result == false) {
			Reporter.getCurrentTestResult().setStatus(ITestResult.FAILURE);
			Reporter.log("Element that failed validation: " + text);
		} else {
			Reporter.getCurrentTestResult().setStatus(ITestResult.SUCCESS);
			Reporter.log("Element that passed validation: " + text);
		}
	}
}
