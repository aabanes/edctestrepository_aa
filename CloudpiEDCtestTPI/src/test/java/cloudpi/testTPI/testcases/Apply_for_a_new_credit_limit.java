package cloudpi.testTPI.testcases;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.Test;

import cloudpi.testTPI.pages.LoginPage;
import cloudpi.testTPI.pages.TPI_PortfolioDetailPage;
import cloudpi.testTPI.pages.TPI_PortfolioPage;
import cloudpi.testTPI.pages.TPI_RepositoryElements;
import cloudpi.testTPI.pages.TPI_CreditApprovalRequestPage;
import cloudpi.testTPI.utility.ExcelUtils;
import cloudpi.testbkof.pages.EDC_BackOffice;

public class Apply_for_a_new_credit_limit extends cloudpi.testTPI.testLibs.BaseTest {
	private LoginPage tpiLogin = null;
	private TPI_PortfolioPage tpiPortfolio = null;
	private TPI_PortfolioDetailPage tpiPortfolioDetail = null;
	private TPI_CreditApprovalRequestPage tpiLimitReq = null;
	private EDC_BackOffice bkOffice = null;
	private String bkOfUsername, bkOfPassword, compName, tpiUsername, tpiPassword, dunsNum, preGrossExposure,
			preTotalExposure, insCreditLimit, reqCreditAppr, reqCreditApprCurr, maxPayTerms, coveragePeriod,
			buyerCredRating, coInsRatio, claimsPending, overdues, newClientIns, comments;
	private ExcelUtils excel = null;
	private TPI_RepositoryElements repEle = null;
	private By newBy, newBy2;
	private Wait<WebDriver> wait;

	@Test(priority = 1, enabled = true)
	public void apply_for_new_credit_limit() {

		bkOffice = new EDC_BackOffice(driver);
		tpiLogin = new LoginPage(driver);
		try {
			tpiPortfolio = new TPI_PortfolioPage(driver);
			tpiPortfolioDetail = new TPI_PortfolioDetailPage(driver);
			tpiLimitReq = new TPI_CreditApprovalRequestPage(driver);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		wait = new WebDriverWait(driver, 60);
		excel = new ExcelUtils();
		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(5, 0);
			bkOfPassword = excel.getCellData(5, 1);
			tpiUsername = excel.getCellData(2, 0);
			tpiPassword = excel.getCellData(2, 1);

			excel.getExcelFile("./testData/TestData.xlsx", "Sheet2");
			compName = excel.getCellData(4, 2);

			excel.getExcelFile("./testData/TestData.xlsx", "Sheet5");
			insCreditLimit = excel.getCellData(0, 1);
			reqCreditAppr = excel.getCellData(1, 1);
			reqCreditApprCurr = excel.getCellData(2, 1);
			maxPayTerms = excel.getCellData(3, 1);
			coveragePeriod = excel.getCellData(4, 1);
			buyerCredRating = excel.getCellData(5, 1);
			coInsRatio = excel.getCellData(6, 1);
			claimsPending = excel.getCellData(7, 1);
			overdues = excel.getCellData(8, 1);
			newClientIns = excel.getCellData(9, 1);
			comments = excel.getCellData(10, 1);

			System.out.println(insCreditLimit + " " + reqCreditAppr + " " + reqCreditApprCurr + " " + maxPayTerms + " "
					+ coveragePeriod + " " + buyerCredRating + " " + coInsRatio + " " + claimsPending + " " + overdues
					+ " " + newClientIns + " " + comments);

			repEle = new TPI_RepositoryElements("./config/TPI_PortDetailPageRepository.properties");
			newBy = repEle.byLocator("tabletr1td1");

			repEle = new TPI_RepositoryElements("../EDCTest_BackOffice/config/BackOffice_Repository.properties");
			newBy2 = repEle.byLocator("BKOF_Risk_Tasks_TasksTable");

		} catch (Exception e) {
			e.printStackTrace();
		}

		Reporter.log(
				"Begin Test: Apply for a new credit limit - buyer is Canadian company that exists in DNB, and it has buyer rating (1)");
		Reporter.log(
				"Precondition: Canadian buyer that exists in DNB \n Take note of the total exposure on the buyer before starting test");

		// Getting DUNS #
		try {
			tpiLogin.signAccount(tpiUsername, tpiPassword);
		} catch (Exception e) {
			e.printStackTrace();
		}

		tpiPortfolio.gotoPortfolio();
		tpiPortfolio.buyerSearchName().sendKeys(compName);
		tpiPortfolio.filterSubmitButton().click();

		dunsNum = tpiPortfolio.tabletr1td1().getText();
		dunsNum = dunsNum.substring(dunsNum.indexOf("(") + 1, dunsNum.indexOf(")"));
		tpiLogin.logoutBtn().click();

		bkOffice.BKOF_login(bkOfUsername, bkOfPassword);
		bkOffice.BKOF_Lookup().sendKeys(dunsNum);
		bkOffice.BKOF_Lookup().sendKeys(Keys.ENTER);

		waitForPageToLoad(bkOffice.BKOF_Gross_Exposure());
		preGrossExposure = bkOffice.BKOF_Gross_Exposure().getText();
		preTotalExposure = bkOffice.BKOF_Total_Exposure().getText();

		System.out.println("Gross Exposure Text: " + preGrossExposure);
		System.out.println("Total Exposure Text: " + preTotalExposure);
		System.out.println("Gross Exposure (as Int): " + preGrossExposure.replace("K", ""));
		System.out.println("Total Exposure (as Int): " + preTotalExposure.replace("K", ""));

		driver.get("https://cis-qac.edc.ca/herakles/do?");
		try {
			Reporter.log("Step 1 - Login to front portal");
			tpiLogin.signAccount(tpiUsername, tpiPassword);
		} catch (Exception e) {
			e.printStackTrace();
		}

		tpiPortfolio.gotoPortfolio();
		tpiPortfolio.buyerSearchName().sendKeys(compName);
		tpiPortfolio.filterSubmitButton().click();
		tpiPortfolio.tabletr1td1().click();
		tpiPortfolio.buyerDetailBtn().click();
		tpiPortfolioDetail.serviceButton().click();

		Select selPolicyHolder = new Select(tpiPortfolioDetail.coverageSelect());
		if (selPolicyHolder.getFirstSelectedOption().getText().equals("")) {
			Reporter.log(
					"Step 2 - Naviage to buyer as per data setup, click services, ensure that the PH field defaults to blank");
		} else {
			Reporter.log("Step 2 Result - Value exists in Policy Holder dropdown. Value: "
					+ selPolicyHolder.getFirstSelectedOption().getText());
		}

		List<WebElement> policyHolderOptions = selPolicyHolder.getOptions();
		for (WebElement ele : policyHolderOptions) {
			if (ele.getText().contains(compName.toUpperCase())) {
				selPolicyHolder.selectByVisibleText(ele.getText());
				break;
			}
		}

		tpiLimitReq.CreditLimitInc_CreditApprovalRequestBtn().click();
		tpiLimitReq.CreditLimitInc_InsCreditLimit().sendKeys(insCreditLimit);
		tpiLimitReq.CreditLimitInc_ReqCreditApprv().sendKeys(reqCreditAppr);

		Select selReqCreditCurr = new Select(tpiLimitReq.CreditLimitInc_ReqCreditApprvCurr());
		selReqCreditCurr.selectByVisibleText(reqCreditApprCurr);

		tpiLimitReq.CreditLimitInc_MaxPaymentTerms().sendKeys(maxPayTerms);

		Select selCoveragePeriod = new Select(tpiLimitReq.CreditLimitInc_CoveragePeriod());
		selCoveragePeriod.selectByVisibleText(coveragePeriod);

		Select selCoverageRating = new Select(tpiLimitReq.CreditLimitInc_BuyerCreditRating());
		selCoverageRating.selectByVisibleText(buyerCredRating);

		tpiLimitReq.CreditLimitInc_CoInsRatio().sendKeys(coInsRatio);
		tpiLimitReq.CreditLimitInc_Comments().sendKeys(comments);
		tpiLimitReq.CreditLimitInc_SubmitBtn().click();
		tpiLogin.logoutBtn().click();

		bkOffice.BKOF_login(bkOfUsername, bkOfPassword);
		bkOffice.BKOF_Lookup().sendKeys(dunsNum);
		bkOffice.BKOF_Lookup().sendKeys(Keys.ENTER);
		bkOffice.BKOF_Menu_Tasks().click();

		waitForPageToLoad(driver.findElement(newBy));
		List<WebElement> taskTable = driver.findElements(newBy2);
		List<WebElement> tasks;
		for (int i = 0; i < taskTable.size(); i++) {
			tasks = taskTable.get(i).findElements(By.tagName("tr"));
			if (tasks.get(i).getText().contains("0 h")) {
				tasks.get(i).click();
			}
		}

		if (bkOffice.BKOF_Task_New_Assignee().getText().equals(bkOfUsername) == false) {
			bkOffice.BKOF_Task_New_Assignee().clear();
			bkOffice.BKOF_Task_New_Assignee().sendKeys(bkOfUsername);
			bkOffice.BKOF_Task_AutoComplete().click();
			bkOffice.BKOF_Task_New_Assignee_SubmitBtn().click();
		} else {
			Assert.fail("New Assignee field missing!");
		}

		waitForPageToLoad(driver.findElement(newBy));
		taskTable = driver.findElements(newBy2);
		for (int i = 0; i < taskTable.size(); i++) {
			tasks = taskTable.get(i).findElements(By.tagName("tr"));
			if (tasks.get(i).getText().contains(bkOfUsername)) {
				tasks.get(i).click();
			}
		}
		
		bkOffice.BKOF_Decision_Accept().click();
		bkOffice.BKOF_Decision_SubmitBtn().click();
		bkOffice.BKOF_Decision_CfrmPopup_CfrmSubmitBtn().click();
		
		waitForPageToLoad(driver.findElement(newBy));
		taskTable = driver.findElements(newBy2);
		for (int i = 0; i < taskTable.size(); i++) {
			tasks = taskTable.get(i).findElements(By.tagName("tr"));
			if (tasks.get(i).getText().contains("0 h")) {
				tasks.get(i).click();
			}
		}

		if (bkOffice.BKOF_Task_New_Assignee().getText().equals(bkOfUsername) == false) {
			bkOffice.BKOF_Task_New_Assignee().clear();
			bkOffice.BKOF_Task_New_Assignee().sendKeys(bkOfUsername);
			bkOffice.BKOF_Task_AutoComplete().click();
			bkOffice.BKOF_Task_New_Assignee_SubmitBtn().click();
		} else {
			Assert.fail("New Assignee field missing!");
		}

		waitForPageToLoad(driver.findElement(newBy));
		taskTable = driver.findElements(newBy2);
		for (int i = 0; i < taskTable.size(); i++) {
			tasks = taskTable.get(i).findElements(By.tagName("tr"));
			if (tasks.get(i).getText().contains(bkOfUsername)) {
				tasks.get(i).click();
			}
		}
		
		bkOffice.BKOF_Decision_Accept().click();
		bkOffice.BKOF_Decision_SubmitBtn().click();
		bkOffice.BKOF_Decision_CfrmPopup_CfrmSubmitBtn().click();
	}

	private void waitForPageToLoad(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
	}
}