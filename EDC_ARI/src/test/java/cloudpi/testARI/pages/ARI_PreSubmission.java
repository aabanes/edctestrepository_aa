package cloudpi.testARI.pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ARI_PreSubmission extends cloudpi.testARI.pages.HomePage {

	private static WebElement element = null;
	private RepositoryElements repoElementsARIPreSubmission = null;

	public ARI_PreSubmission(WebDriver driver) {
		super(driver);

		try {
			repoElementsARIPreSubmission = new RepositoryElements("./config/ARI_PreSubmission_Repository.properties");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public WebElement ARI_PreSubmission_ApplyForSubmissionBtn() {
		element = driver.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_ApplyForSubmissionBtn"));
		return element;
	}

	// PreSubmission Application //
	public WebElement ARI_PreSubmission_AppSubmission_ContinueBtn() {
		element = driver.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_ContinueBtn"));
		return element;
	}


	// Presubmission Application menu items //
	public WebElement ARI_PreSubmission_AppSubmission_FinancialDataTab() {
		element = driver.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialDataTab"));
		return element;
	}
	
	public WebElement ARI_PreSubmission_AppSubmission_CreditManagementTab() {
		element = driver.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CreditManagementTab"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CoverageRequestedTab() {
		element = driver.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CoverageRequestedTab"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CountryCoverageTab() {
		element = driver.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CountryCoverageTab"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_ListOfBuyersTab() {
		element = driver.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_ListOfBuyersTab"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_PolicyAdministrationTab() {
		element = driver.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_PolicyAdministrationTab"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_AcceptanceTab() {
		element = driver.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_AcceptanceTab"));
		return element;
	}

	// Financial Data Tab //
	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesUS1() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesUS1"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesUS2() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesUS2"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesUS3() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesUS3"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesUS4() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesUS4"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesCAD1() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesCAD1"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesCAD2() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesCAD2"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesCAD3() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesCAD3"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesCAD4() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesCAD4"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesOther1() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesOther1"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesOther2() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesOther2"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesOther3() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesOther3"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesOther4() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesOther4"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsUS1() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsUS1"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsUS2() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsUS2"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsUS3() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsUS3"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsUS4() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsUS4"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsCAD1() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsCAD1"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsCAD2() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsCAD2"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsCAD3() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsCAD3"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsCAD4() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsCAD4"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsOther1() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsOther1"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsOther2() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsOther2"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsOther3() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsOther3"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsOther4() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsOther4"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_LossesLastYr() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_LossesLastYr"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_LossesIncurred() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_LossesIncurred"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_LargestSingleLoss() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_LargestSingleLoss"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_FinancialData_ReasonOfLossList() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_ReasonOfLossList"));
		return element;
	}

	// Credit Management Tab //
	public WebElement ARI_PreSubmission_AppSubmission_CreditManagement_PercentageOfReceivables() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CreditManagement_PercentageOfReceivables"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CreditManagement_CreditDecisions() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CreditManagement_CreditDecisions"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CreditManagement_SourceInfo() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CreditManagement_SourceInfo"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CreditManagement_FrequencyReview() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CreditManagement_FrequencyReview"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CreditManagement_SelectDelinquency() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CreditManagement_SelectDelinquency"));
		return element;
	}

	// Coverage Requested Tab //
	public WebElement ARI_PreSubmission_AppSubmission_CoverageRequested_PrimaryProduct() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CoverageRequested_PrimaryProduct"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CoverageRequested_OverallBusiness() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CoverageRequested_OverallBusiness"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CoverageRequested_TypeOfBusiness() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CoverageRequested_TypeOfBusiness"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CoverageRequested_ReadilySoldRdoBtnYes() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CoverageRequested_ReadilySoldRdoBtnYes"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CoverageRequested_ReadilySoldRdoBtnNo() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CoverageRequested_ReadilySoldRdoBtnNo"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CoverageRequested_PercentagedShipped() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CoverageRequested_PercentagedShipped"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CountryCoverage_WithinAddBtn() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CountryCoverage_WithinAddBtn"));
		return element;
	}
	
	public WebElement ARI_PreSubmission_AppSubmission_CoverageRequested_AffiliateRdoBtnYes() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CoverageRequested_AffiliateRdoBtnYes"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CoverageRequested_AffiliateRdoBtnNo() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CoverageRequested_AffiliateRdoBtnNo"));
		return element;
	}

	// Country Coverage //
	public WebElement ARI_PreSubmission_AppSubmission_CountryCoverage_UltimateDestinationRdBtnYes() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CountryCoverage_UltimateDestinationRdBtnYes"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CountryCoverage_UltimateDestinationRdBtnNo() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CountryCoverage_UltimateDestinationRdBtnNo"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CountryCoverage_OutsideAddBtn() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CountryCoverage_OutsideAddBtn"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CountryCoverage_OutsideCan_Country() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CountryCoverage_OutsideCan_Country"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CountryCoverage_OutsideCan_AnticipatedInsSales() {
		element = driver.findElement(repoElementsARIPreSubmission
				.byLocator("ARI_PreSubmission_AppSubmission_CountryCoverage_OutsideCan_AnticipatedInsSales"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CountryCoverage_OutsideCan_AnticipatedInsSalesCurr() {
		element = driver.findElement(repoElementsARIPreSubmission
				.byLocator("ARI_PreSubmission_AppSubmission_CountryCoverage_OutsideCan_AnticipatedInsSalesCurr"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CountryCoverage_OutsideCan_PaymentTerms() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CountryCoverage_OutsideCan_PaymentTerms"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CountryCoverage_WithinCan_Country() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CountryCoverage_WithinCan_Country"));
		return element;
	}
	
	public WebElement ARI_PreSubmission_AppSubmission_CountryCoverage_WithinCan_Province() {
		element = driver.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CountryCoverage_WithinCan_Province"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CountryCoverage_WithinCan_AnticipatedInsSales() {
		element = driver.findElement(repoElementsARIPreSubmission
				.byLocator("ARI_PreSubmission_AppSubmission_CountryCoverage_WithinCan_AnticipatedInsSales"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CountryCoverage_WithinCan_AnticipatedInsSalesCurr() {
		element = driver.findElement(repoElementsARIPreSubmission
				.byLocator("ARI_PreSubmission_AppSubmission_CountryCoverage_WithinCan_AnticipatedInsSalesCurr"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_CountryCoverage_WithinCan_PaymentTerms() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_CountryCoverage_WithinCan_PaymentTerms"));
		return element;
	}

	// Policy Administration //
	public WebElement ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerRdBtnYes() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerRdBtnYes"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerRdBtnNo() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerRdBtnNo"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerName() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerName"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerAddress() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerAddress"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerCity() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerCity"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerProvince() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerProvince"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerCountry() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerCountry"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerFirstName() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerFirstName"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerLastName() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerLastName"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerWorkPhone() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerWorkPhone"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerEmail() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerEmail"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_PolicyAdministration_PolicyBeneficiaryRdBtnYes() {
		element = driver.findElement(repoElementsARIPreSubmission
				.byLocator("ARI_PreSubmission_AppSubmission_PolicyAdministration_PolicyBeneficiaryRdBtnYes"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_PolicyAdministration_PolicyBeneficiaryRdBtnNo() {
		element = driver.findElement(repoElementsARIPreSubmission
				.byLocator("ARI_PreSubmission_AppSubmission_PolicyAdministration_PolicyBeneficiaryRdBtnNo"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_PolicyAdministration_PolicyInsuredRdBtnYes() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_PolicyAdministration_PolicyInsuredRdBtnYes"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_PolicyAdministration_PolicyInsuredRdBtnNo() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_PolicyAdministration_PolicyInsuredRdBtnNo"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_PolicyAdministration_StartDate() {
		element = driver
				.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_PolicyAdministration_StartDate"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_PolicyAdministration_PolicyLanguage() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_PolicyAdministration_PolicyLanguage"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_PolicyAdministration_PolicyCurrency() {
		element = driver.findElement(
				repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_PolicyAdministration_PolicyCurrency"));
		return element;
	}

	// Acceptance tab //
	public WebElement ARI_PreSubmission_AppSubmission_Acceptance_Comment() {
		element = driver.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_Acceptance_Comment"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_Acceptance_IAgreeChkbx() {
		element = driver.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_Acceptance_IAgreeChkbx"));
		return element;
	}

	// Save Draft / Submit buttons //
	public WebElement ARI_PreSubmission_AppSubmission_Acceptance_DraftBtn() {
		element = driver.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_Acceptance_DraftBtn"));
		return element;
	}

	public WebElement ARI_PreSubmission_AppSubmission_Acceptance_SubmitBtn() {
		element = driver.findElement(repoElementsARIPreSubmission.byLocator("ARI_PreSubmission_AppSubmission_Acceptance_SubmitBtn"));
		return element;
	}
}
