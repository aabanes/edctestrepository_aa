package cloudpi.testARI.pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ARI_Portfolio extends cloudpi.testARI.pages.HomePage{
	
	private static WebElement element = null;
	private RepositoryElements repoElementsARIPortfolio = null;
		
	public ARI_Portfolio(WebDriver driver) {
		super(driver);
		
		try {
			repoElementsARIPortfolio = new RepositoryElements("./config/ARI_Portfolio_Repository.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//### Portfolio Page Repository ###
	//## Portfolio Tabs ##
	public WebElement ARI_Portfolio_PortfolioTab(){
		element = driver.findElement(repoElementsARIPortfolio.byLocator("ARI_Portfolio_PortfolioTab"));
		return element;
	}
	
	public WebElement ARI_Portfolio_PendingTab(){
		element = driver.findElement(repoElementsARIPortfolio.byLocator("ARI_Portfolio_PendingTab"));
		return element;
	}
	
	public WebElement ARI_Portfolio_TemporaryLimitsTab(){
		element = driver.findElement(repoElementsARIPortfolio.byLocator("ARI_Portfolio_TemporaryLimitsTab"));
		return element;
	}


	//### Portfolio Search ###
	public WebElement ARI_Portfolio_Search_TypeSearch(){
		element = driver.findElement(repoElementsARIPortfolio.byLocator("ARI_Portfolio_Search_TypeSearch"));
		return element;
	}
	
	public WebElement ARI_Portfolio_Search_TypeSearchTextfield(){
		element = driver.findElement(repoElementsARIPortfolio.byLocator("ARI_Portfolio_Search_TypeSearchTextfield"));
		return element;
	}

	//### Portfolio Filter ###
	public WebElement ARI_Portfolio_Filter_SelectCountry(){
		element = driver.findElement(repoElementsARIPortfolio.byLocator("ARI_Portfolio_Filter_SelectCountry"));
		return element;
	}

	
	public WebElement ARI_Portfolio_Filter_SelectBuyerTypeTempOnly(){
		element = driver.findElement(repoElementsARIPortfolio.byLocator("ARI_Portfolio_Filter_SelectBuyerTypeTempOnly"));
		return element;
	}

	
	public WebElement ARI_Portfolio_Filter_SelectBuyerType(){
		element = driver.findElement(repoElementsARIPortfolio.byLocator("ARI_Portfolio_Filter_SelectBuyerType"));
		return element;
	}

	
	public WebElement ARI_Portfolio_Filter_SelectType(){
		element = driver.findElement(repoElementsARIPortfolio.byLocator("ARI_Portfolio_Filter_SelectType"));
		return element;
	}

	
	public WebElement ARI_Portfolio_Filter_SelectPeriod(){
		element = driver.findElement(repoElementsARIPortfolio.byLocator("ARI_Portfolio_Filter_SelectPeriod"));
		return element;
	}

	
	public WebElement ARI_Portfolio_Filter_SubmitBtn(){
		element = driver.findElement(repoElementsARIPortfolio.byLocator("ARI_Portfolio_Filter_SubmitBtn"));
		return element;
	}


	//### Portfolio Results Table ###
	public WebElement ARI_Portfolio_ResultsTable(){
		element = driver.findElement(repoElementsARIPortfolio.byLocator("ARI_Portfolio_ResultsTable"));
		return element;
	}


	//### Portfolio Buttons ###
	public WebElement ARI_Portfolio_DetailsBtn(){
		element = driver.findElement(repoElementsARIPortfolio.byLocator("ARI_Portfolio_DetailsBtn"));
		return element;
	}
	
	public WebElement ARI_Portfolio_AddBtn(){
		element = driver.findElement(repoElementsARIPortfolio.byLocator("ARI_Portfolio_AddBtn"));
		return element;
	}
	
	public WebElement ARI_Portfolio_ExportBtn(){
		element = driver.findElement(repoElementsARIPortfolio.byLocator("ARI_Portfolio_ExportBtn"));
		return element;
	}
}
