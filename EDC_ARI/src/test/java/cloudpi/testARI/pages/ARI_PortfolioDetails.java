package cloudpi.testARI.pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ARI_PortfolioDetails extends cloudpi.testARI.pages.HomePage{
	
	private static WebElement element = null;
	private RepositoryElements repoElementsARIPortfolioDetails;
		
	public ARI_PortfolioDetails(WebDriver driver) {
		super(driver);
		
		try {
			repoElementsARIPortfolioDetails = new RepositoryElements("./config/ARI_PortfolioDetails_Repository.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//### Policy Detail Tabs ###
	public WebElement ARI_PolicyDetails_PolicyTab(){
		element = driver.findElement(repoElementsARIPortfolioDetails.byLocator("ARI_PolicyDetails_PolicyTab"));
		return element;
	}
	
	public WebElement ARI_PolicyDetails_MidTermAssessments(){
		element = driver.findElement(repoElementsARIPortfolioDetails.byLocator("ARI_PolicyDetails_MidTermAssessments"));
		return element;
	}
	

	//### Policy Terms ###
	public WebElement ARI_PolicyDetail_Policy(){
		element = driver.findElement(repoElementsARIPortfolioDetails.byLocator("ARI_PolicyDetail_Policy"));
		return element;
	}
	
	public WebElement ARI_PolicyDetail_PolicyNum(){
		element = driver.findElement(repoElementsARIPortfolioDetails.byLocator("ARI_PolicyDetail_PolicyNum"));
		return element;
	}
	
	public WebElement ARI_PolicyDetail_InsType(){
		element = driver.findElement(repoElementsARIPortfolioDetails.byLocator("ARI_PolicyDetail_InsType"));
		return element;
	}
	
	public WebElement ARI_PolicyDetail_ProductName(){
		element = driver.findElement(repoElementsARIPortfolioDetails.byLocator("ARI_PolicyDetail_ProductName"));
		return element;
	}
	
	public WebElement ARI_PolicyDetail_FileCreation(){
		element = driver.findElement(repoElementsARIPortfolioDetails.byLocator("ARI_PolicyDetail_FileCreation"));
		return element;
	}
	
	public WebElement ARI_PolicyDetail_LastRenewal(){
		element = driver.findElement(repoElementsARIPortfolioDetails.byLocator("ARI_PolicyDetail_LastRenewal"));
		return element;
	}
	
	public WebElement ARI_PolicyDetail_NextRenewal(){
		element = driver.findElement(repoElementsARIPortfolioDetails.byLocator("ARI_PolicyDetail_NextRenewal"));
		return element;
	}
	
	public WebElement ARI_PolicyDetail_Currency(){
		element = driver.findElement(repoElementsARIPortfolioDetails.byLocator("ARI_PolicyDetail_Currency"));
		return element;
	}
	
	public WebElement ARI_PolicyDetail_CountriesCovered(){
		element = driver.findElement(repoElementsARIPortfolioDetails.byLocator("ARI_PolicyDetail_CountriesCovered"));
		return element;
	}
	
	public WebElement ARI_PolicyDetail_Insurer(){
		element = driver.findElement(repoElementsARIPortfolioDetails.byLocator("ARI_PolicyDetail_Insurer"));
		return element;
	}
	
	public WebElement ARI_PolicyDetail_CommericialOwner(){
		element = driver.findElement(repoElementsARIPortfolioDetails.byLocator("ARI_PolicyDetail_CommericialOwner"));
		return element;
	}
	
	public WebElement ARI_PolicyDetail_RiskOwner(){
		element = driver.findElement(repoElementsARIPortfolioDetails.byLocator("ARI_PolicyDetail_RiskOwner"));
		return element;
	}


	//### Mid Term Amendments ###
	public WebElement ARI_PolicyDetails_MidTermAmendments_addBtn(){
		element = driver.findElement(repoElementsARIPortfolioDetails.byLocator("ARI_PolicyDetails_MidTermAmendments_addBtn"));
		return element;
	}
	
	public WebElement ARI_PolicyDetails_MidTermAmendments_exportBtn(){
		element = driver.findElement(repoElementsARIPortfolioDetails.byLocator("ARI_PolicyDetails_MidTermAmendments_exportBtn"));
		return element;
	}
	
	public WebElement ARI_PolicyDetails_MidTermAmendments_resultsTable(){
		element = driver.findElement(repoElementsARIPortfolioDetails.byLocator("ARI_PolicyDetails_MidTermAmendments_resultsTable"));
		return element;
	}
}
