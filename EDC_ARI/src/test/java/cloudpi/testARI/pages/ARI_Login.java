package cloudpi.testARI.pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ARI_Login extends cloudpi.testARI.pages.HomePage{
	
	private static WebElement element = null;
	private RepositoryElements repoElementsLogin = null;
		
	public ARI_Login(WebDriver driver) {
		super(driver);
		
		try {
			repoElementsLogin = new RepositoryElements("./config/ARI_Login_Repository.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public  void  pageOpen(){
		driver.navigate().to("https://cis-qac2.edc.ca/herakles/do");
	}
	
	public WebElement ARI_loginTextField(){
		element = driver.findElement(repoElementsLogin.byLocator("ARI_loginTextField"));
		return element;
	}
	
	public WebElement ARI_passwordTextField(){
		element = driver.findElement(repoElementsLogin.byLocator("ARI_passwordTextField"));
		return element;
	}
	
	public WebElement ARI_submitBtn(){
		element = driver.findElement(repoElementsLogin.byLocator("ARI_submitBtn"));
		return element;
	}
	
	public void ARI_SignIn(String username, String password) {
		driver.navigate().to("https://cis-qac2.edc.ca/herakles/do");
		ARI_loginTextField().sendKeys(username);
		ARI_passwordTextField().sendKeys(password);
		ARI_submitBtn().click();
	}
}
