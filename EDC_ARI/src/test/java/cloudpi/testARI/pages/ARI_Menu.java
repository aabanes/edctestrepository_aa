package cloudpi.testARI.pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ARI_Menu extends cloudpi.testARI.pages.HomePage {

	private static WebElement element = null;
	private RepositoryElements repoElementsARIMenu = null;

	public ARI_Menu(WebDriver driver) {
		super(driver);

		try {
			repoElementsARIMenu = new RepositoryElements("./config/ARI_Menu_Repository.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// #### ARI Menu (includes submenus)####
	public WebElement ARI_Menu_PolicyHolder_Exit(){
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_PolicyHolder_Exit"));
		return element;
	}
	
	// ### ARI_Portfolio ####
	public WebElement ARI_Menu_Portfolio() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_Portfolio"));
		return element;
	}

	// ### ARI_Menu_View ###
	public WebElement ARI_Menu_View() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_View"));
		return element;
	}
	
	// ## ARI_Menu_PolicyHolder_View_Submenu ##
		
	public WebElement ARI_Menu_PolicyHolder_View_Portfolio(){
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_PolicyHolder_View_Portfolio"));
		return element;
	}
	
	public WebElement ARI_Menu_PolicyHolder_View_PreSubmission() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_PolicyHolder_View_PreSubmission"));
		return element;
	}

	// ## ARI_Menu_View_Submenu ##
	public WebElement ARI_Menu_View_Portfolio() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_View_Portfolio"));
		return element;
	}

	public WebElement ARI_Menu_View_PolicyDetail() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_View_PolicyDetail"));
		return element;
	}

	public WebElement ARI_Menu_View_ApplicationHistory() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_View_ApplicationHistory"));
		return element;
	}

	public WebElement ARI_Menu_View_TemporaryBuyersHistory() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_View_TemporaryBuyersHistory"));
		return element;
	}

	public WebElement ARI_Menu_View_ExpiredLimitsHistory() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_View_ExpiredLimitsHistory"));
		return element;
	}

	public WebElement ARI_Menu_View_ClaimsHistory() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_View_ClaimsHistory"));
		return element;
	}

	public WebElement ARI_Menu_View_NotifiableEventsHistory() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_View_NotifiableEventsHistory"));
		return element;
	}

	public WebElement ARI_Menu_View_TurnoverDeclarationHistory() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_View_TurnoverDeclarationHistory"));
		return element;
	}

	public WebElement ARI_Menu_View_EmailArchive() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_View_EmailArchive"));
		return element;
	}

	public WebElement ARI_Menu_View_UserGuide() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_View_UserGuide"));
		return element;
	}

	public WebElement ARI_Menu_View_PreSubmission() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_View_PreSubmission"));
		return element;
	}

	// ### ARI_Menu_Action ###
	public WebElement ARI_Menu_Action() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_Action"));
		return element;
	}
	
	// ### ARI_Menu_PolicyHolder_Action ###
	public WebElement ARI_Menu_PolicyHolder_Action() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_PolicyHolder_Action"));
		return element;
	}
		
	// ## ARI_Menu_PolicyHolder_Action Submenu ##
	public WebElement ARI_Menu_PolicyHolder_Action_ProfileManagement() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_PolicyHolder_Action_ProfileManagement"));
		return element;
	}
	
	public WebElement ARI_Menu_PolicyHolder_Action_AddABuyer() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_Action_AddABuyer"));
		return element;
	}
	
	public WebElement ARI_Menu_PolicyHolder_Action_PreSubmission() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_PolicyHolder_Action_PreSubmission"));
		return element;
	}
	// ## ARI_Menu_Action_Submenu ##
	public WebElement ARI_Menu_Action_ProfileManagement() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_Action_ProfileManagement"));
		return element;
	}
	
	public WebElement ARI_Menu_Action_UserManagement() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_Action_UserManagement"));
		return element;
	}
	
	public WebElement ARI_Menu_Action_AddABuyer() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_Action_AddABuyer"));
		return element;
	}
	
	public WebElement ARI_Menu_Action_ClaimDeclaration() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_Action_ClaimDeclaration"));
		return element;
	}
	
	public WebElement ARI_Menu_Action_TurnoverDeclaration() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_Action_TurnoverDeclaration"));
		return element;
	}
	
	public WebElement ARI_Menu_Action_MidTermAmendmentReq () {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_Action_MidTermAmendmentReq "));
		return element;
	}
	
	public WebElement ARI_Menu_Action_PreSubmission() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_Action_PreSubmission"));
		return element;
	}

	// ### ARI_Menu_Contact ###
	public WebElement ARI_Menu_Contact() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_Contact"));
		return element;
	}

	// ### ARI_Menu_Help ###
	public WebElement ARI_Menu_Help() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_Help"));
		return element;
	}

	// ## ARI_Menu_Help_Submenu ##	
	public WebElement ARI_Menu_Help_UserGuide() {
		element = driver.findElement(repoElementsARIMenu.byLocator("ARI_Menu_Help_UserGuide"));
		return element;
	}

}
