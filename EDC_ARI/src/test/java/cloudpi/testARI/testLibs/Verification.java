package cloudpi.testARI.testLibs;

import java.text.SimpleDateFormat;
import java.util.Calendar;


//import org.testng.asserts.*;
import org.testng.asserts.SoftAssert;
import org.testng.asserts.Assertion;
import org.testng.Assert;

//import appcom.searspages.RepositoryElements;
public class Verification {

	// public static WebDriver driver ;
	// public static String baseBrowser;
	// public static String baseUrl;
	private Assertion hardAssert = new Assertion();
	private SoftAssert softAssert = new SoftAssert();
	/*
	 * Below are the verify method using TestNg Assertion / SoftAssert to verify
	 * the test cases
	 */

	public boolean verify(boolean actual, boolean expected) {

		boolean act = actual;
		boolean exp = expected;
		try {
			hardAssert.assertEquals(act, exp);
		} catch (Exception e) {
			Assert.fail();
		}
		return true;
	}

	public boolean verify(boolean actual, boolean expected, String message) {

		boolean act = actual;
		boolean exp = expected;
		String mes = message;
		try {
			hardAssert.assertEquals(act, exp, mes);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verify(byte[] actual, byte[] expected, String message) {

		// boolean act = actual ; boolean exp = expected; String mes = message;
		try {
			hardAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verify(byte actual, byte expected, String message) {

		try {
			hardAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verify(java.util.Collection actual,
			java.util.Collection expected, String message) {

		try {
			hardAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verify(double actual, double expected, double delta,
			String message) {

		try {
			hardAssert.assertEquals(actual, expected, delta, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verify(float actual, float expected, float delta,
			String message) {

		try {
			hardAssert.assertEquals(actual, expected, delta, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verify(int actual, int expected, String message) {

		try {
			hardAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verify(long actual, long expected, String message) {

		try {
			hardAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verify(java.lang.Object[] actual,
			java.lang.Object[] expected, String message) {

		try {
			hardAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verify(java.lang.Object actual, java.lang.Object expected,
			String message) {

		try {
			hardAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verify(String actual, String expected, String message) {

		try {
			hardAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verifyTrue(boolean condition, String message) {

		try {
			hardAssert.assertTrue(condition, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verifyNoTrue(boolean condition, String message) {

		try {
			hardAssert.assertFalse(condition, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public void verifyFail(String message) {

		try {
			hardAssert.fail(message);
		} catch (Exception e) {
			hardAssert.fail();
		}
	}

	public boolean verifySame(java.lang.Object actual,
			java.lang.Object expected, String message) {

		try {
			hardAssert.assertSame(actual, expected, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verifyNotSame(java.lang.Object actual,
			java.lang.Object expected, String message) {

		try {
			hardAssert.assertNotSame(actual, expected, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verifyNull(java.lang.Object object, String message) {

		try {
			hardAssert.assertNull(object, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	public boolean verifyNotNull(java.lang.Object object, String message) {

		try {
			hardAssert.assertNotNull(object, message);
		} catch (Exception e) {
			hardAssert.fail();
		}
		return true;
	}

	/*
	 * Below are Soft Assertion in order to continue run test case rest of steps
	 * even the
	 * 
	 * Assert exception happened
	 */

	public boolean softVerify(boolean actual, boolean expected) {

		boolean act = actual;
		boolean exp = expected;
		try {
			softAssert.assertEquals(act, exp);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(boolean actual, boolean expected, String message) {

		boolean act = actual;
		boolean exp = expected;
		String mes = message;
		try {
			softAssert.assertEquals(act, exp, mes);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(byte[] actual, byte[] expected, String message) {

		// boolean act = actual ; boolean exp = expected; String mes = message;
		try {
			softAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(byte actual, byte expected, String message) {

		try {
			softAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(java.util.Collection actual,
			java.util.Collection expected, String message) {

		try {
			softAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(double actual, double expected, double delta,
			String message) {

		try {
			softAssert.assertEquals(actual, expected, delta, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(float actual, float expected, float delta,
			String message) {

		try {
			softAssert.assertEquals(actual, expected, delta, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(int actual, int expected, String message) {

		try {
			softAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(long actual, long expected, String message) {

		try {
			softAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(java.lang.Object[] actual,
			java.lang.Object[] expected, String message) {

		try {
			softAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(java.lang.Object actual,
			java.lang.Object expected, String message) {

		try {
			softAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerify(String actual, String expected, String message) {

		try {
			softAssert.assertEquals(actual, expected, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerifyTrue(boolean condition, String message) {

		try {
			softAssert.assertTrue(condition, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerifyNotTrue(boolean condition, String message) {

		try {
			softAssert.assertFalse(condition, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVerifyFail(String message) {

		try {
			softAssert.fail(message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean softVverifySame(java.lang.Object actual,
			java.lang.Object expected, String message) {

		try {
			softAssert.assertSame(actual, expected, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean SoftVerifyNotSame(java.lang.Object actual,
			java.lang.Object expected, String message) {

		try {
			softAssert.assertNotSame(actual, expected, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean SoftVerifyNull(java.lang.Object object, String message) {

		try {
			softAssert.assertNull(object, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}

	public boolean SfotVerifyNotNull(java.lang.Object object, String message) {

		try {
			softAssert.assertNotNull(object, message);
		} catch (Exception e) {
			softAssert.fail();
		}
		return true;
	}
	
}
