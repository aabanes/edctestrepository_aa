package cloudpi.testARI.testLibs;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

//import org.apache.poi.xssf.usermodel.XSSFCell;
//import org.apache.poi.xssf.usermodel.XSSFRow;
//import org.apache.poi.xssf.usermodel.XSSFSheet;
//import org.apache.poi.hssf.usermodel.HSSFDateUtil;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class ExcelUtils {

	private Sheet wSheet;

	private Workbook wBook;
	
	private Row  wRow;

	private Cell wCell;

	// This method is to set the File path and to open the Excel file, Pass
	// Excel Path and Sheetname as Arguments to this method

	public void getExcelFile(String fileName, String SheetName)
			throws Exception {
		try {
			// Open the Excel file
			FileInputStream fileStream = new FileInputStream(fileName);
			 if(fileName.toLowerCase().endsWith("xlsx")){ 
				 try {  
				 wBook = new XSSFWorkbook(fileStream);  
				 } catch (IOException e) { 
			     e.printStackTrace();  
				 }
			 }else if(fileName.toLowerCase().endsWith("xls")){
				 try {  
				 wBook = new  HSSFWorkbook (fileStream);
				 } catch (IOException e) {  
					 e.printStackTrace();  
				 }
			 }else{
				 throw new IllegalArgumentException("UnSupport none ：xls/xlsx File Format!!");
			 }

			wSheet = wBook.getSheet(SheetName);
			
		} catch (Exception e) {

			throw (e);
		}
	}

	public Object[][] getTableArray(String FilePath, String SheetName)
			throws Exception {

		String[][] tabArray = null;

		String fileName = FilePath;
		String ssheet = SheetName;

		try {

			getExcelFile(fileName, ssheet);

			int startRow = 1;

			int startCol = 1;

			int ci, cj;

			int totalRows = wSheet.getLastRowNum();


			// you can write a function as well to get Column count

			int totalCols = 2;

			tabArray = new String[totalRows][totalCols];

			ci = 0;

			for (int i = startRow; i <= totalRows; i++, ci++) {

				cj = 0;

				for (int j = startCol; j <= totalCols; j++, cj++) {

					tabArray[ci][cj] = getCell(i, j);

					System.out.println(tabArray[ci][cj]);
				}
			}
		}
		catch (FileNotFoundException e) {

			System.out.println("Could not read the Excel sheet");

			e.printStackTrace();
		}

		catch (IOException e) {

			System.out.println("Could not read the Excel sheet");

			e.printStackTrace();
		}
		return (tabArray);

	}

	// This method is to read the test data from the Excel cell, in this we are
	// passing parameters as Row num and Col num
	public String getCellData(String fileName, String SheetName, int RowNum,
			int ColNum) throws Exception {

		try {

			FileInputStream ExcelFile = new FileInputStream(fileName);

			// Access the required test data sheet

			wBook = new XSSFWorkbook(ExcelFile);

			wSheet = wBook.getSheet(SheetName);
			
			wRow = wSheet.getRow(RowNum);
			
			wCell = wRow.getCell(ColNum);
			
			String dataCell = wCell.getStringCellValue();

			return dataCell;

		} catch (Exception e) {

			return "";
		}
	}

	public String getCell(int RowNum, int ColNum) throws Exception {

		try {

			wCell = wSheet.getRow(RowNum).getCell(ColNum);

			String CellData = wCell.getStringCellValue();

			return CellData;

		} catch (Exception e) {
		
			return "";
		}
	}

	public Object[][] getTableArray01(String FilePath, String SheetName,
			int iTestCaseRow) throws Exception

	{

		String[][] tabArray = null;

		try {

			// FileInputStream ExcelFile = new FileInputStream(FilePath);

			// Access the required test data sheet

			// ExcelWBook = new XSSFWorkbook(ExcelFile);
			//
			// ExcelWSheet = ExcelWBook.getSheet(SheetName);

			int startCol = 1;

			int ci = 0, cj = 0;

			int totalRows = 1;

			int totalCols = 2;

			tabArray = new String[totalRows][totalCols];

			for (int j = startCol; j <= totalCols; j++, cj++)

			{
				tabArray[ci][cj] = getCell(iTestCaseRow, j);
				System.out.println(tabArray[ci][cj]);
			}
		}

		catch (FileNotFoundException e)
		{
			System.out.println("Could not read the Excel sheet");

			e.printStackTrace();
		}

		catch (IOException e)
		{
			System.out.println("Could not read the Excel sheet");

			e.printStackTrace();
		}
		return (tabArray);
	}
	public static String getTestCaseName(String sTestCase) throws Exception {

		String value = sTestCase;
		try {
			int posi = value.indexOf("@");

			value = value.substring(0, posi);

			posi = value.lastIndexOf(".");

			value = value.substring(posi + 1);

			return value;

		} catch (Exception e) {

			throw (e);
		}

	}

	public int getRowContains(String sTestCaseName, int colNum)
			throws Exception {

		int i;

		try {

			int rowCount = getRowUsed();

			for (i = 0; i < rowCount; i++) {

				if (getCell(i, colNum).equalsIgnoreCase(sTestCaseName)) {

					break;

				}

			}

			return i;

		} catch (Exception e) {

			throw (e);

		}

	}

	public int getRowUsed() throws Exception {

		try {

			int RowCount = wSheet.getLastRowNum();

			return RowCount;

		} catch (Exception e) {

			System.out.println(e.getMessage());

			throw (e);
		}
	}

	// This method is to write in the Excel cell, Row num and Col num are the
	// parameters
	public void setCell(String Result, String outFile, int RowNum, int ColNum)
			throws Exception {

		try {

			wRow = wSheet.getRow(RowNum);

			wCell = wRow.getCell(ColNum, wRow.RETURN_BLANK_AS_NULL);
			
//			wCell = wRow.getCell(ColNum);

			if (wCell == null) {
				wCell = wRow.createCell(ColNum);
				wCell.setCellValue(Result);

			} else {
				wCell.setCellValue(Result);

			}

			// Constant variables Test Data path and Test Data file name

			FileOutputStream fileOut = new FileOutputStream(outFile);

			wBook.write(fileOut);

			fileOut.flush();

			fileOut.close();

		} catch (Exception e) {

			throw (e);
		}

	}
	public void setCell(String Result, int RowNum, int ColNum)
			throws Exception {

			wRow = wSheet.getRow(RowNum);

			wCell = wRow.getCell(ColNum, wRow.RETURN_BLANK_AS_NULL);

			if (wCell == null) {
				wCell = wRow.createCell(ColNum);
				wCell.setCellValue(Result);

			} else {
				wCell.setCellValue(Result);
			}
			// Constant variables Test Data path and Test Data file name
	}

	public void WriteExcel(String outFile)
			throws Exception {

		try {

		   // Constant variables Test Data path and Test Data file name

			FileOutputStream fileOut = new FileOutputStream(outFile);

			wBook.write(fileOut);

			fileOut.flush();

			fileOut.close();

		} catch (Exception e) {

			throw (e);
		}
	}
}
