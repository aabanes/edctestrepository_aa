package cloudpi.testARI.testcases;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import cloudpi.testARI.pages.RepositoryElements;
import cloudpi.testARI.utility.ExcelUtils;
import cloudpi.testbkof.pages.EDC_BackOffice;

public class UC27_Manage_Specific_Entity_Config extends cloudpi.testARI.testLibs.BaseTest {

	private EDC_BackOffice bkOffice = null;
	private String bkOfUsername, bkOfPassword, compName, usrLastName, usrFirstName, dateFormat, date;
	private ExcelUtils excel = null;
	private By newBy, newBy2, newBy3, newBy4;
	private RepositoryElements repEle = null;
	private Wait<WebDriver> wait = null;

	@Test(priority = 1, enabled = true)
	public void UC27_002_005() {
		bkOffice = new EDC_BackOffice(driver);
		excel = new ExcelUtils();

		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(1, 0);
			bkOfPassword = excel.getCellData(1, 1);
			usrFirstName = excel.getCellData(1, 2);
			usrLastName = excel.getCellData(1, 3);

			excel.getExcelFile("./testData/TestData.xlsx", "Sheet2");
			compName = excel.getCellData(3, 2);

			repEle = new RepositoryElements("../EDCTest_BackOffice/config/BackOffice_Repository.properties");
			newBy = repEle.byLocator("BKOF_Ppl&Org_InsuredList_FilterSearch_Results");
			newBy2 = repEle.byLocator("BKOF_Preferences_PreferencesWithProfileTab_SelectHomepage");
			newBy3 = repEle.byLocator("BKOF_Contact_Messages");
			newBy4 = repEle.byLocator("BKOF_PplOrg_UserProfile_EventsList");
		} catch (Exception e) {
			e.printStackTrace();
		}
		wait = new WebDriverWait(driver, 30);

		Reporter.log("Begin Test: UC27.002.005");
		bkOffice.BKOF_login(bkOfUsername, bkOfPassword);

		// Get User's preferred date format
		bkOffice.BKOF_MyProfileLink().click();
		Select profileDateFormat = new Select(bkOffice.BKOF_MyProfile_IDUsrAccessTab_DateFormat());
		dateFormat = profileDateFormat.getFirstSelectedOption().getText();

		Reporter.log(
				"Step 1 - Navigate to Insured List screen by selecting ADMINISTRATION > PEOPLE & ORGANIZATION > INSURED LIST");
		bkOffice.BKOF_MainMenu().click();
		bkOffice.BKOF_MainMenu_Administration().click();
		bkOffice.BKOF_MainMenu_Administration_PplOrg().click();
		bkOffice.BKOF_MainMenu_Administration_PplOrg_InsuredList().click();
		if (bkOffice.BKOF_PplOrg_InsuredList_FilterBtn().isDisplayed()) {
			Reporter.log("Step 1 Results - System displays S4 Insured List screen");
		} else {
			Assert.fail("Step 1 Results - Insured List is not present");
			Reporter.log("Step 1 Results - Insured List is not present");
		}
		
		//Search for Company
		bkOffice.BKOF_PplOrg_InsuredList_FilterBtn().click();
		bkOffice.BKOF_PplOrg_InsuredList_FilterSearch_Name().sendKeys(compName);
		bkOffice.BKOF_PplOrg_InsuredList_FilterSearch_SubmitBtn().click();
		
		Reporter.log("Step 2 - Click on the COMPANY NAME hyperlink");
		List<WebElement> resultsPaneEle2 = driver.findElements(newBy);
		List<WebElement> res2;
		for (int i = 0; i < resultsPaneEle2.size(); i++) {
			res2 = resultsPaneEle2.get(i).findElements(By.tagName("tr"));
			
			for (int j = 0; j < res2.size();) {
				if (res2.get(j).findElements(By.tagName("a")).size() > 0) {
					res2.get(j).findElement(By.tagName("a")).click();
					break;
				}
			}
		}

		if (bkOffice.BKOF_Menu_Header().isDisplayed()) {
			Reporter.log("Step 2 Results - System displays S6 Entity Folder screen");
		} else {
			softVerify(bkOffice.BKOF_Menu_Header().isDisplayed(), true, "Incorrect page.");
		}

		Reporter.log("Step 3 - Click on PREFERECNES > PREFERENCES WITH PROFILE tab");
		waitForPageToLoad(bkOffice.BKOF_Menu_Preferences());
		bkOffice.BKOF_Menu_Preferences().click();
		bkOffice.BKOF_Preferences_PreferencesWithProfileTab().click();
		Reporter.log("Step 3 Results - 'System displays S21 - Insured Preferences - Preferences' with profile screen");

		Reporter.log(
				"Step 4 - Modify the Default Homepage field, click on the  'Applies to all profiles' checkbox then click SUBMIT.");
		Select profSelect = new Select(bkOffice.BKOF_Preferences_PreferencesWithProfileTab_SelectHomepage());

		List<WebElement> elements = driver.findElements(newBy2);
		for (int i = 0; i < elements.size(); i++) {

			if (!elements.get(i).getText().contains(profSelect.getFirstSelectedOption().getText())) {
				profSelect.selectByIndex(i);
				break;
			}
		}
		bkOffice.BKOF_Preferences_PreferencesWithProfileTab_ApplyToAllProfiles().click();
		bkOffice.BKOF_Preferences_PreferencesWithProfileTab_SubmitBtn().click();
		setDate(dateFormat + " HH:mm:ss");

		List<WebElement> messageCol = driver.findElements(newBy3);
		List<WebElement> msgs;
		for (int i = 0; i < messageCol.size(); i++) {
			msgs = messageCol.get(i).findElements(By.tagName("div"));

			for (int j = 0; j < msgs.size(); j++) {
				if (msgs.get(j).getText().equals("Modification saved for all of the customer's profiles")) {
					Reporter.log(
							"Step 4 Results - Information message is displayed indicating that the modifications have been saved and have been applied to all profiles for PH; ('Modification saved for all of the customer's profiles.' appears on the screen.");
					break;
				}
			}

			if (i == messageCol.size()) {
				Reporter.log("Step 4 Results - Message not found");
			}
		}

		Reporter.log("Step 5 - Click on EVENTS tab");
		bkOffice.BKOF_Menu_Events().click();

		Reporter.log(
				"Step 5 Results - System displays S501 Entity Event Tab; \n Entity management event filter appears selected (i.e. checked) in left panel. \n List of entity management events are displayed in right panel");

		List<WebElement> eventList = driver.findElements(newBy4);
		List<WebElement> events;

		if (bkOffice.BKOF_Events_EntityyMgmt().isSelected()
				&& bkOffice.BKOF_PplOrg_UserProfile_EventsList().isDisplayed()) {
			Reporter.log(
					"Step 6 - Results - Events tab displays the following for the change: Event date: current date and time \n - Type: User Management \n - Origin: Contact user Last Name (name of person who performed the action) \n - Event:  User access granted to UserID");
		} else {
			Assert.fail("Step 6 Results - Checkbox not checked and/or events list is missing");
			Reporter.log("Step 6 Results - Checkbox not checked and/or events list is missing");
		}

		Reporter.log("Step 7 - Verify audit trail");
		for (int i = 0; i < eventList.size(); i++) {
			events = eventList.get(i).findElements(By.tagName("tr"));
			
			for (int j = 0; j < 2; j++) {
				if (events.get(j).getText().contains(getDate())
						&& events.get(j+1).getText().contains("Preferences modified")
						&& events.get(j+2).getText().contains(usrLastName + " " + usrFirstName)
						&& events.get(j+3).getText().contains("Preferences modified")) {
					Reporter.log(
							"Step 7 Results - An entry appears in the list of events corresponding to the modifications from previous step as follows: \n"
									+ "Event date - reflects the date and time the preferences were modified and appears in the user preferred date format; \n"
									+ "Type - set to 'Preferences modified'; \n Origin - reflects the last name and first name of the user who submitted the modification;"
									+ "Event - set to 'Preferences modified';");
					break;
				}
			}
		}

	}

	private void setDate(String dateFormat) {
		date = dateFormat.replace("DD", "dd");
		SimpleDateFormat format = new SimpleDateFormat(date);
		Calendar c = Calendar.getInstance();
		date = format.format(c.getTime());
	}

	private String getDate() {
		return date;
	}
	
	private void waitForPageToLoad(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
	}
}
