package cloudpi.testARI.testcases;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Reporter;
import org.testng.annotations.Test;

import cloudpi.testARI.pages.RepositoryElements;
import cloudpi.testARI.utility.EWSJavaAPI;
import cloudpi.testARI.utility.ExcelUtils;
import cloudpi.testbkof.pages.EDC_BackOffice;

public class UC26_Manage_Internal_Users extends cloudpi.testARI.testLibs.BaseTest {

	private EDC_BackOffice bkOffice = null;
	private ExcelUtils excel = null;
	private RepositoryElements repEle = null;
	private By newBy = null, newBy3 = null, newBy4 = null, newBy5 = null;
	private String bkOfUsername, bkOfPassword, firstName, lastName, email, usrFirstName, usrLastName, newUsrLogin,
			readLoginID, readPassword, readDomain, readEmailSubject, tempPass;
	private Wait<WebDriver> wait = null;
	
	@Test(priority = 1, enabled = true)
	public void UC25_001_003() {
		bkOffice = new EDC_BackOffice(driver);
		excel = new ExcelUtils();
		wait = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS).ignoring(StaleElementReferenceException.class);
		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(1, 0);
			bkOfPassword = excel.getCellData(1, 1);
			usrFirstName = excel.getCellData(1, 2);
			usrLastName = excel.getCellData(1, 3);

			excel.getExcelFile("./testData/TestData.xlsx", "Sheet3");
			firstName = excel.getCellData(2, 1);
			lastName = excel.getCellData(2, 2);
			email = excel.getCellData(2, 7);

			excel.getExcelFile("./testData/TestData.xlsx", "Sheet4");
			readLoginID = excel.getCellData(1, 0);
			readPassword = excel.getCellData(1, 1);
			readDomain = excel.getCellData(1, 2);
			readEmailSubject = excel.getCellData(1, 3);
			
			System.out.println(readLoginID + "\n" + readPassword + "\n" + readDomain + "\n" + readEmailSubject);

			repEle = new RepositoryElements("../EDCTest_BackOffice/config/BackOffice_Repository.properties");
			newBy = repEle.byLocator("BKOF_Contact_UserTable");
			newBy3 = repEle.byLocator("BKOF_Events_EventsTable");
			newBy4 = repEle.byLocator("BKOF_Contact_UserAccess_Active");
			newBy5 = repEle.byLocator("BKOF_Contact_Messages");
		} catch (Exception e) {
			e.printStackTrace();
		}

		Reporter.log(
				"Begin Test: UC25.001.003 - Platform Admin adds new contact to Operating Company - Grant user access");

		// Prerequisite - Creating new user for test.
		bkOffice.BKOF_login(bkOfUsername, bkOfPassword);
		bkOffice.BKOF_MainMenu().click();
		bkOffice.BKOF_MainMenu_Administration().click();

		bkOffice.BKOF_MainMenu_Administration_PplOrg().click();
		bkOffice.BKOF_MainMenu_Administration_PplOrg_OperatingComp().click();

		bkOffice.BKOF_Menu_Contact().click();
		bkOffice.BKOF_Contact_AddUserBtn().click();

		Select selectTitle = new Select(bkOffice.BKOF_Contact_IDCard_Title());
		selectTitle.selectByIndex(1);
		bkOffice.BKOF_Contact_IDCard_FirstName().sendKeys(firstName);
		bkOffice.BKOF_Contact_IDCard_LastName().sendKeys(lastName);
		bkOffice.BKOF_Contact_IDCard_Email().sendKeys(email);
		bkOffice.BKOF_Contact_IDCard_SubmitBtn().click();

		Reporter.log("Step 1 - Access Contact Record");
		Reporter.log("Step 2 - Click on Provide User Access");
		bkOffice.BKOF_Contact_IDCard_ProvideUserAccessBtn().click();
		if (bkOffice.BKOF_Contact_UserAccessTab().isDisplayed()) {
			Reporter.log("Step 2 Result - System displays User Access tab");
		} else {
			Assert.fail("Step 2 Result - Tab not present");
			Reporter.log("Step 2 Result - Tab not present");
		}

		bkOffice.BKOF_Contact_UserAccessTab().click();
		
		presenceOfElement(newBy4);
		Reporter.log("Step 3 - Click on the Active checkbox");
		bkOffice.BKOF_Contact_UserAccess_Active().click();
		
		if(bkOffice.BKOF_Contact_UserAccess_Active().isSelected()==false){
			bkOffice.BKOF_Contact_UserAccess_Active().click();
		}

		newUsrLogin = bkOffice.BKOF_Contact_UserAccess_Login().getAttribute("value");
		Reporter.log("Step 4 - Click on Add under the Profiles box");
		bkOffice.BKOF_Contact_UserAccess_AddProfileBtn().click();

		if (bkOffice.BKOF_Contact_UserAccess_AddProfile_ProfileDropDown().isDisplayed()) {
			Reporter.log("Step 4 Results - Profiles pop up opens");
		} else {
			Assert.fail("Step 4 Results - Popup not found");
			Reporter.log("Step 4 Results - Popup not found");
		}

		Reporter.log("Step 5 - Select a profile and submit");
		Select profileDrop = new Select(bkOffice.BKOF_Contact_UserAccess_AddProfile_ProfileDropDown());
		profileDrop.selectByVisibleText("CIS-All");

		bkOffice.BKOF_Contact_UserAccess_AddProfile_SubmitBtn().click();
		bkOffice.BKOF_Contact_UserAccess_SubmitBtn().click();

		Select usrProfileDrop = new Select(bkOffice.BKOF_Contact_UserAccessTab_ProfileTable());
		if (usrProfileDrop.getFirstSelectedOption().getText().contains("CIS-All")) {
			Reporter.log("Step 5 Results - Profile shows in Profile box");
		} else {
			Assert.fail("Step 5 Results - Profile does not show in Profile box");
			Reporter.log("Step 5 Results - Profile does not show in Profile box");
		}

		Reporter.log("Step 6 - Click Submit");
		bkOffice.BKOF_Contact_UserAccess_BeforeSubmitBtn().click();

		List<WebElement> messageCol = driver.findElements(newBy5);
		List<WebElement> msgs;
		for (int i = 0; i < messageCol.size(); i++) {
			msgs = messageCol.get(i).findElements(By.tagName("div"));

			for (int j = 0; j < msgs.size(); j++) {
				if (msgs.get(j).getText().equals("User access created")) {
					Reporter.log("Step 6 Results - 'User access created' message is present");
					break;
				}
			}
			
			if (i == messageCol.size())
			{
				Reporter.log("Step 6 Results - 'User access created' message not found");
			}
		}
		
		Reporter.log("Step 7 - View Events tab");
		bkOffice.BKOF_Menu_Events().click();

		List<WebElement> resultsPaneEle2 = driver.findElements(newBy3);
		List<WebElement> res2;
		for (int i = 0; i < resultsPaneEle2.size(); i++) {
			res2 = resultsPaneEle2.get(i).findElements(By.tagName("tr"));

			for (int j = 0; j < 2; j++) {
				if (res2.get(j).getText().contains("User management")
						&& res2.get(j).getText().contains(usrLastName + " " + usrFirstName)
						&& res2.get(j).getText().contains("User access granted to " + newUsrLogin)) {
					Reporter.log(
							"Step 7 - Results - Events tab displays the following for the change: Event date: current date and time \n - Type: User Management \n - Origin: Contact user Last Name ( name of person who performed the action \n - Event:  User access granted to UserID");
					break;
				}
			}
		}


		Reporter.log("Step 8 - Go back to Contact tab, select contact and go to User Access sub-tab");
		List<WebElement> contactCol= driver.findElements(newBy);
		List<WebElement> res;
		for (int i = 0; i < contactCol.size(); i++) {
			res = contactCol.get(i).findElements(By.tagName("td"));

			for (int j = 0; j < res.size(); j++) {
				if (res.get(j).getText().equals(newUsrLogin)) {
					res.get(j).click();
					break;
				}
			}
		}
		
		bkOffice.BKOF_Contact_UserAccessTab().click();
		Reporter.log("Step 9 - Click on New Password");
		bkOffice.BKOF_Menu_Contact().click();
		bkOffice.BKOF_Contact_UserAccess_NewPassword().click();
		Alert alert = driver.switchTo().alert();
		alert.accept();

		sleep(5);
		messageCol = driver.findElements(newBy5);
		for (int i = 0; i < messageCol.size(); i++) {
			msgs = messageCol.get(i).findElements(By.tagName("div"));

			for (int j = 0; j < msgs.size(); j++) {
				if (msgs.get(j).getText().equals("A new password has been sent to the user")) {
					Reporter.log("Step 9 Result - 'A new password has been sent to the user' message is present");
					break;
				}
			}
			
			if (i == messageCol.size())
			{
				Reporter.log("Step 9 Result - Message not present");
			}
		}
		
		try {
			EWSJavaAPI emailRead = new EWSJavaAPI();
			tempPass = emailRead.getTempPassword(readLoginID, readPassword, readDomain, email, readEmailSubject);
		} catch (Exception e) {
			Reporter.log("Error retrieving temporary password from mail. \n" + e.getMessage());
		}

		if (tempPass != "" || tempPass != null) {
			Reporter.log("Step 9 Result - Message appears that password sent. User receives email");
		} else {
			Reporter.log("Step 9 Result - Unable to retrieve temporary password mail");
			Reporter.log("Step 9 Result - Unable to retrieve temporary password mail");
		}

		bkOffice.BKOF_ExitBtn().click();
		
		alert = driver.switchTo().alert();
		alert.accept();

		bkOffice.BKOF_login(newUsrLogin, tempPass);
		Reporter.log("Step 10 - Log in to the front office using the credentials from the email");
		if (bkOffice.BKOF_ChangePassword_OldPassword().isDisplayed()) {
			Reporter.log("Step 10 Result- System prompts user to set new password");
		} else {
			Assert.fail("Step 10 Result - Change Password page not present");
			Reporter.log("Step 10 Result - Change Password page not present");
		}
		bkOffice.BKOF_ChangePassword_OldPassword().sendKeys(tempPass);
		bkOffice.BKOF_ChangePassword_NewPassword().sendKeys("1Test123");
		bkOffice.BKOF_ChangePassword_CnfmPassword().sendKeys("1Test123");
		bkOffice.BKOF_ChangePassword_SubmitBtn().click();

		Reporter.log("Step 11 - Enter new password and submit");
		if (bkOffice.BKOF_MainMenu().isDisplayed()) {
			Reporter.log("Step 11 Result - System displays homepage");
		} else {
			Assert.fail("Step 11 Result - Did not land on homepage");
			Reporter.log("Step 11 Result - Did not land on homepage");
		}
	}
	
	private void presenceOfElement(By eleBy){
		wait.until(ExpectedConditions.presenceOfElementLocated(eleBy));
	}
	
	private void sleep(int slp) {
		try {
			Thread.sleep(slp);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}