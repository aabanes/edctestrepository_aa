package cloudpi.testARI.testcases;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;
import cloudpi.testARI.pages.RepositoryElements;
import cloudpi.testARI.utility.ExcelUtils;
import cloudpi.testbkof.pages.EDC_BackOffice;
import junit.framework.Assert;

public class UC24_Manage_Profile_CreatMod extends cloudpi.testARI.testLibs.BaseTest {

	private EDC_BackOffice bkOffice = null;
	private String bkOfUsername, bkOfPassword, profileName;
	private ExcelUtils excel = null;
	private By newBy, newBy2, newBy3;
	private RepositoryElements repEle = null;
	private int i, j;

	@Test(priority = 1, enabled = true)
	public void UC01_000_001() {
		bkOffice = new EDC_BackOffice(driver);
		excel = new ExcelUtils();

		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(1, 0);
			bkOfPassword = excel.getCellData(1, 1);

			repEle = new RepositoryElements("../EDCTest_BackOffice/config/BackOffice_Repository.properties");
			newBy = repEle.byLocator("BKOF_PplOrg_UserProfile_Menu");
			newBy2 = repEle.byLocator("BKOF_PplOrg_UserProfile_ProfileList");
			newBy3 = repEle.byLocator("BKOF_PplOrg_UserProfile_EventsList");
		} catch (Exception e) {
			e.printStackTrace();
		}

		Reporter.log("Begin Test: UC24_001_001");
		bkOffice.BKOF_login(bkOfUsername, bkOfPassword);

		Reporter.log(
				"Step 1 - Navigate through the following path:  Administration > People & Organizaiton > User Profile.");
		bkOffice.BKOF_MainMenu().click();
		bkOffice.BKOF_MainMenu_Administration().click();
		bkOffice.BKOF_MainMenu_Administration_PplOrg().click();
		bkOffice.BKOF_MainMenu_Administration_PplOrg_UserProfile().click();

		List<WebElement> menuItems = driver.findElements(newBy);
		for (int i = 0; i < menuItems.size(); i++) {

			if (menuItems.get(i).findElement(By.tagName("li")).getAttribute("class").contains("state-active")
					&& menuItems.get(i).getText().contains("Front Office")) {
				Reporter.log("Step 1 Result - Front office tab is displayed by default.");
				break;
			}

			if (i == menuItems.size()) {
				Reporter.log("Step 1 Result - Tab is not selected by default.");
			}
		}

		if (bkOffice.BKOF_PplOrg_UserProfile_ProfileList().isDisplayed()) {
			Reporter.log("Step 1 Result - System displays a list of front office profiles");
		} else {
			Assert.fail("Step 1 Result - Profile List not present");
			Reporter.log("Step 1 Result - Profile List not present");
		}

		Reporter.log("Click on the back office tab");
		bkOffice.BKOF_PplOrg_UserProfile_Menu_BackOfficeTab().click();
		if (bkOffice.BKOF_PplOrg_UserProfile_ProfileList().isDisplayed()) {
			Reporter.log("Step 2 Result - System displays the list of profiles that are currently in the system");
		} else {
			Assert.fail("Step 2 Result - Profile List not present");
			Reporter.log("Step 2 Result - Profile List not present");
		}

		List<WebElement> profileListEle = driver.findElements(newBy2);
		List<WebElement> profile;
		for (i = 0; i < profileListEle.size(); i++) {
			profile = profileListEle.get(i).findElements(By.tagName("tr"));
			
			for (j = 0; j < profile.size(); j++) {
				if (profile.get(j).getText().contains("UC24_001_001_Test_Profile")) {
					profileName = profile.get(j).getText() + "1";
					break;
				}
			}

			if (j == profile.size()) {
				profileName = "UC24_001_001_Test_Profile";
			}
		}

		Reporter.log("Step 3 - Click on New profile");
		bkOffice.BKOF_PplOrg_UserProfile_NewProfileBtn().click();
		if (bkOffice.BKOF_PplOrg_UserProfile_NewProfilePopup_ProfileName().isDisplayed()) {
			Reporter.log("Step 3 Result - The system displays the New Profile creation pop-up");
		} else {
			Assert.fail("Step 3 Result - New Profile popup not displayed");
			Reporter.log("Step 3 Result - New Profile popup not displayed");
		}

		Reporter.log("Step 4 - Enter the profile name and clicks submit");
		bkOffice.BKOF_PplOrg_UserProfile_NewProfilePopup_ProfileName().sendKeys(profileName);
		bkOffice.BKOF_PplOrg_UserProfile_NewProfilePopup_SubmitBtn().click();

		Reporter.log("Step 5 - View Events tab");
		bkOffice.BKOF_PPlOrg_UserProfile_EventsTab().click();
		List<WebElement> resultsPaneEle2 = driver.findElements(newBy3);
		List<WebElement> res2;
		for (int i = 0; i < resultsPaneEle2.size(); i++) {
			res2 = resultsPaneEle2.get(i).findElements(By.tagName("tr"));

			for (int j = 0; j < 2; j++) {
				if (res2.get(j).getText().contains("Profile Created")) {
					Reporter.log(
							"Step 5 Result - The event tab displays that the 'Profile Creation' task was executed and displays the type and profile name");
					break;
				}
			}
			if (j == res2.size()) {
				Reporter.log("Step 5 Result - 'Profile Creation' task log not displayed");

			}
		}

		Reporter.log(
				"Step 6 - Return to the Administration > People & Organizaiton > User Profile tab. Click on the back office tab");
		bkOffice.BKOF_MainMenu().click();
		bkOffice.BKOF_MainMenu_Administration().click();
		bkOffice.BKOF_MainMenu_Administration_PplOrg().click();
		bkOffice.BKOF_MainMenu_Administration_PplOrg_UserProfile().click();
		bkOffice.BKOF_PplOrg_UserProfile_Menu_BackOfficeTab().click();
		
		profileListEle = driver.findElements(newBy2);
		for (i = 0; i < profileListEle.size(); i++) {
			profile = profileListEle.get(i).findElements(By.tagName("tr"));
			
			for (j = 0; j < profile.size(); j++) {
				if (profile.get(j).getText().contains(profileName)) {
					Reporter.log("Step 6 Result - New Profile is displayed in the list");
					break;
				}
			}

			if (j == profile.size()) {
				Assert.fail("Step 6 Result - New Profile is NOT displayed in the list");
				Reporter.log("Step 6 Result - New Profile is NOT displayed in the list");
			}
		}

	}
}