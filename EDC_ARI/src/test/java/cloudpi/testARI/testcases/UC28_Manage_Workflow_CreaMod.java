package cloudpi.testARI.testcases;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;

import cloudpi.testARI.pages.RepositoryElements;
import cloudpi.testARI.utility.ExcelUtils;
import cloudpi.testbkof.pages.EDC_BackOffice;
import junit.framework.Assert;

public class UC28_Manage_Workflow_CreaMod extends cloudpi.testARI.testLibs.BaseTest {

	private EDC_BackOffice bkOffice = null;
	private String bkOfUsername, bkOfPassword, usrLastName, usrFirstName, date, dateFormat, testCode;
	private ExcelUtils excel = null;
	private By newBy, newBy2, newBy3, newBy4, newBy5;
	private RepositoryElements repEle = null;
	private List<WebElement> workBasketCont, res, usrTable, selectedUsrList, eventList, events;
	private List<String> usrArr;
	private boolean resBool;
	private Random rmd;

	@Test(priority = 1, enabled = true)
	public void UC28_001_001() {
		bkOffice = new EDC_BackOffice(driver);
		excel = new ExcelUtils();
		resBool = true;
		usrArr = new ArrayList<String>();
		rmd = new Random();

		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(1, 0);
			bkOfPassword = excel.getCellData(1, 1);
			usrFirstName = excel.getCellData(1, 2);
			usrLastName = excel.getCellData(1, 3);

			usrArr.add(excel.getCellData(6, 2) + " " + excel.getCellData(6, 3));
			usrArr.add(excel.getCellData(7, 2) + " " + excel.getCellData(7, 3));
			usrArr.add(excel.getCellData(8, 2) + " " + excel.getCellData(8, 3));

			repEle = new RepositoryElements("../EDCTest_BackOffice/config/BackOffice_Repository.properties");
			newBy = repEle.byLocator("BKOF_Administration_WorkBasket_WorkBasketList");
			newBy2 = repEle.byLocator("BKOF_Administration_WorkBasket_WorkBasketDetailContent");
			newBy3 = repEle.byLocator("BKOF_Administration_WorkBasket_AddWkBst_UserTable");
			newBy4 = repEle.byLocator("BKOF_Administration_WorkBasket_AddWkBst_SelectedUsers");
			newBy5 = repEle.byLocator("BKOF_PplOrg_UserProfile_EventsList");

		} catch (Exception e) {
			e.printStackTrace();
		}
		Reporter.log("Begin Test: UC28.001.001");
		bkOffice.BKOF_login(bkOfUsername, bkOfPassword);

		// Get User's preferred date format
		bkOffice.BKOF_MyProfileLink().click();
		Select profileDateFormat = new Select(bkOffice.BKOF_MyProfile_IDUsrAccessTab_DateFormat());
		dateFormat = profileDateFormat.getFirstSelectedOption().getText();
		
		Reporter.log(
				"Step 1 - Navigate to the Workbasket Settings screen by selecting ADMINISTRATION > SETTINGS > WORK BASKET SETTINGS");
		bkOffice.BKOF_MainMenu().click();
		bkOffice.BKOF_MainMenu_Administration().click();
		bkOffice.BKOF_MainMenu_Administration_Settings().click();
		bkOffice.BKOF_MainMenu_Administration_Settings_WrkBsktSet().click();

		if (bkOffice.BKOF_Administration_WorkBasket_Header().getText().contains("WorkBasket settings")) {
			Reporter.log("Step 1 Result - System displays S143 Workbasket List screen");
		} else {
			Assert.fail("Step 1 Result - Landed on incorrect page");
			Reporter.log("Step 1 Result - Landed on incorrect page");
		}

		// Test visual layout manually. Only testing the following:
		// If workbaskets are displayed and if workbasket display code &
		// description for each workbasket in list
		// If add button appears enabled on screen
		// If right panel of page appears blank by default.
		Reporter.log("Step 2 - Verify S143 Workbasket List screen initial display.");

		workBasketCont = driver.findElements(newBy);
		for (int i = 0; i < workBasketCont.size(); i++) {
			res = workBasketCont.get(i).findElements(By.tagName("td"));

			for (int j = 0; j < res.size(); j++) {
				if (res.get(j).getText().isEmpty()) {
					Reporter.log("Step 2 Result - Value is empty in Workbasket. Please review Workbasket");
					break;
				}
				if (res.get(j).getText().equals("test")) {
					testCode = res.get(j).getText() + rmd.nextInt(99999);
					break;
				}
			}
			if (testCode.isEmpty()) {
				testCode = "test";
			}
		}

		if (bkOffice.BKOF_Administration_WorkBasket_AddBtn().isDisplayed()) {
			Reporter.log("Step 2 Result - Add button appears enabled on the screen");
		} else {
			Assert.fail("Step 2 Result - Add button not present on the screen");
			Reporter.log("Step 2 Result - Add button not present on the screen");
		}

		workBasketCont = driver.findElements(newBy2);
		res = driver.findElements(By.tagName("div"));

		if (res.size() == 0) {
			Reporter.log("Step 2 Result - Right panel of screen appears blank");
		} else {
			Reporter.log("Step 2 Result - Right panel of screen is populated");
		}

		Reporter.log("Step 3 - Click on ADD");
		bkOffice.BKOF_Administration_WorkBasket_AddBtn().click();

		if (bkOffice.BKOF_Administration_WorkBasket_AddWkBst_Code().isDisplayed()) {
			Reporter.log("Step 3 Result - System displays S144 Workbasket Details screen");
		} else {
			Assert.fail("Step 3 Result - System DOES NOT display S144 Workbasket Details screen");
			Reporter.log("Step 3 Result - System DOES NOT display S144 Workbasket Details screen");
		}

		Reporter.log(
				"Step 4 - Input a value in the Workbasket Code field that does not correspond to an existing Workbasket Code and enter a value in the Workbasket Description field; \n"
						+ "Select an Assignment Strategy from the Assignment Strategy dropdown list.");
		bkOffice.BKOF_Administration_WorkBasket_AddWkBst_Code().sendKeys(testCode);
		bkOffice.BKOF_Administration_WorkBasket_AddWkBst_Description().sendKeys("Testing Workbasket Creation");
		Select strat = new Select(bkOffice.BKOF_Administration_WorkBasket_AddWkBst_SelectStrategy());
		strat.selectByVisibleText("Random");
		bkOffice.BKOF_Administration_WorkBasket_AddWkBst_AuthorityLevel().click();

		Reporter.log(
				"Step 5 - Select a user from the Users List and click Right Arrow and repeat for 2 additional users.");
		Select usrTableSel = new Select(bkOffice.BKOF_Administration_WorkBasket_AddWkBst_UserTable());
		for (String string : usrArr) {
			try {
				usrTableSel.selectByVisibleText(string);
			} catch (NoSuchElementException e) {
				Reporter.log("User not found in user list. Check data sheet");
				break;
			}
		}

		bkOffice.BKOF_Administration_WorkBasket_AddWkBst_AddBtn().click();

		usrTable = driver.findElements(newBy3);
		Iterator<String> itr = usrArr.iterator();
		while (itr.hasNext()) {

			Object element = itr.next();
			for (int l = 0; l < usrTable.size(); l++) {
				res = usrTable.get(l).findElements(By.tagName("option"));
				for (int k = 0; k < res.size(); k++) {
					if (res.get(k).getText().equals(element)) {
						Reporter.log("Step 5 Result - User found in list. User" + element);
						break;
					}
				}
			}
			if (itr.hasNext() == false) {
				Reporter.log("Step 5 Result - Users not found selectable users list");
			}
			else
			{
				itr.next();
			}
		}

		selectedUsrList = driver.findElements(newBy4);
		
		while(itr.hasNext())
		for (int l = 0; l < selectedUsrList.size(); l++) {
			res = selectedUsrList.get(l).findElements(By.tagName("option"));
			for (int k = 0; k < res.size(); k++) {
				if (k == res.size()) {
					Reporter.log("User not found in list. User" + res.get(k).getText());
					break;
				}
			}
			if (itr.hasNext() == false) {
				Reporter.log("Step 5 Result - Users are found in selected users list");
			}
			else
			{
				itr.next();
			}
		}

		Reporter.log("Step 6 - Click SUBMIT");
		bkOffice.BKOF_Administration_WorkBasket_AddWkBst_SubmitBtn().click();
		setDate(dateFormat + " HH:mm:ss");
		System.out.println(getDate());
		workBasketCont = driver.findElements(newBy2);
		res = driver.findElements(By.tagName("div"));
		if (bkOffice.BKOF_Administration_WorkBasket_AddBtn().isDisplayed() && res.size() == 0) {
			Reporter.log("Step 6 Result - System displays the S143 Workbasket List screen \n"
					+ "- New Workbasket code and description appears in the list of workbaskets in the left panel \n"
					+ "- Right panel of screen appears blank");
		} else if (bkOffice.BKOF_Administration_WorkBasket_AddBtn().isDisplayed() == false) {
			Reporter.log("Step 6 Result - New Workbasket not present");
		} else if (res.size() == 0) {
			Reporter.log("Step 6 Result - Right panel not empty");
		}

		Reporter.log("Step 7 - Select the workbasket created in previous step.");
		workBasketCont = driver.findElements(newBy);
		for (int i = 0; i < workBasketCont.size(); i++) {
			res = workBasketCont.get(i).findElements(By.tagName("td"));

			for (int j = 0; j < res.size(); j++) {
				if (res.get(j).getText() == testCode) {
					res.get(j).click();
					break;
				}
			}
		}

		while(itr.hasNext())
		for (int l = 0; l < selectedUsrList.size(); l++) {
			res = selectedUsrList.get(l).findElements(By.tagName("option"));
			for (int k = 0; k < res.size(); k++) {
				if (k == res.size()) {
					resBool = false;
					break;
				}
			}
			if (itr.hasNext() == false) {
				resBool = true;
			}
			else
			{
				itr.next();
			}
		}
		
		for (int i = 0; i < workBasketCont.size(); i++) {
			res = workBasketCont.get(i).findElements(By.tagName("td"));

			for (int j = 0; j < res.size(); j++) {
				if (res.get(j).getText().equals(testCode)) {
					res.get(j).click();
					break;
				}
			}
			if (i == workBasketCont.size())
			{
				Assert.fail("Cannot find workbasket recently created.");
				Reporter.log("Cannot find workbasket recently created.");
			}
		}
		
		strat = new Select(bkOffice.BKOF_Administration_WorkBasket_AddWkBst_SelectStrategy());
		if (bkOffice.BKOF_Administration_WorkBasket_AddWkBst_Code().getAttribute("value").equals(testCode)
				&& bkOffice.BKOF_Administration_WorkBasket_AddWkBst_Description().getAttribute("value").equals("Testing Workbasket Creation")
				&& strat.getFirstSelectedOption().equals("Random")
				&& bkOffice.BKOF_Administration_WorkBasket_AddWkBst_AuthorityLevel().isSelected() && resBool == true) {
			Reporter.log("Step 7 Result - System displays S144: Workbasket Details screen \n"
					+ "- Workbasket appears selected in the left panel \n"
					+ "- Workbasket details appears in the right panel of screen and correctly reflects the values from previous steps.");
		} else {
			Reporter.log("Step 7 Result - Values in right pane mis-matched");
		}

		Reporter.log(
				"Step 8 - Navigate to the Workflow Audit Trial screen by selecting ADMINISTRATION > SETTINGS > WORKFLOW AUDIT TRAIL");
		bkOffice.BKOF_MainMenu_Administration_Settings().click();
		bkOffice.BKOF_MainMenu_Administration_Settings_WkflAudTrl().click();
		if (driver.findElement(newBy5).isDisplayed()) {
			Reporter.log("Step 8 Result - System displays S149 Workflow Audit Trail screen");
		} else {
			Assert.fail("Step 8 Result - Not on correct page");
			Reporter.log("Step 8 Result - Not on correct page");
		}

		Reporter.log("Step 9 - Verify workflow audit trail");
		eventList = driver.findElements(newBy5);
		for (int i = 0; i < eventList.size(); i++) {
			events = eventList.get(i).findElements(By.tagName("tr"));

			for (int j = 0; j < 2; j++) {
				if (events.get(j).getText().contains(getDate()) && events.get(j).getText().contains("Workbasket")
						&& events.get(j).getText().contains(usrLastName + " " + usrFirstName) && events.get(j)
								.getText().contains("Workbasket " + testCode + " created")) {
					Reporter.log(
							"Step 9 Result -An entry appears in the list corresponding to the workbasket added in previous step as follows: \n"
									+ "- Event date - reflects the date and time the workbasket was added and appears in the user preferred date format \n"
									+ "- Type - is set to 'Workbasket' \n"
									+ "- Origin - reflects the last name and first name of the user who submitted the request \n"
									+ "- Event - is set to 'Workbasket <code> created' where <code> represents the value entered in a previous step");
					break;
				}
			}
		}
	}

	private void setDate(String dateFormat) {
		date = dateFormat.replace("DD", "dd");
		SimpleDateFormat format = new SimpleDateFormat(date);
		Calendar c = Calendar.getInstance();
		date = format.format(c.getTime());
	}

	private String getDate() {
		return date;
	}

}
