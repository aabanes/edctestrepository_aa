package cloudpi.testARI.testcases;

import java.util.List;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;

import cloudpi.testARI.pages.RepositoryElements;
import cloudpi.testARI.utility.ExcelUtils;
import cloudpi.testbkof.pages.EDC_BackOffice;

public class UC03_Access_Quote_File extends cloudpi.testARI.testLibs.BaseTest {
	private EDC_BackOffice bkOffice = null;
	private String bkOfUsername, bkOfPassword;
	private ExcelUtils excel = null;
	private RepositoryElements repEle = null;
	private By newBy = null;

	@Test(priority = 1, enabled = true)
	public void UC3_001_003() {
		bkOffice = new EDC_BackOffice(driver);
		excel = new ExcelUtils();
		int i = 0, j = 0;
		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(5, 0);
			bkOfPassword = excel.getCellData(5, 1);

			repEle = new RepositoryElements("../EDCTest_BackOffice/config/BackOffice_Repository.properties");
			newBy = repEle.byLocator("BKOF_MainMenu_Commericial_Underwriting");
		} catch (Exception e) {
			e.printStackTrace();
		}

		Reporter.log("Begin Test: UC3.001.003 - No user access to quotes");
		bkOffice.BKOF_login(bkOfUsername, bkOfPassword);

		Reporter.log("Step 1 - Go to CIS Home page > Commercial > Underwriting");
		bkOffice.BKOF_MainMenu().click();
		bkOffice.BKOF_MainMenu_Commericial().click();
		bkOffice.BKOF_MainMenu_Commerical_UnderWriting().click();
		
		List<WebElement> menuItems = driver.findElements(newBy);
		List<WebElement> subMenuItems;
		for (i = 0; i < menuItems.size(); i++) {
			subMenuItems = menuItems.get(i).findElements(By.tagName("ul"));

			for (j = 0; j < subMenuItems.size(); j++) {
				if (subMenuItems.get(j).getText().equals("Quotes list")) {
					Assert.fail("Step 1 Results - Quotes List found");
					Reporter.log("Step 1 Results - Quotes List found");
					break;
				}
			}
			if (j == subMenuItems.size())
			{
				Reporter.log("Step 1 Results - Quotes list not an option in drop down");
			}
		}
	}
}
