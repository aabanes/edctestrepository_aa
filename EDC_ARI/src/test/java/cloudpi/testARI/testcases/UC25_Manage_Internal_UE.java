package cloudpi.testARI.testcases;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.testng.Reporter;
import org.testng.annotations.Test;

import cloudpi.testARI.utility.ExcelUtils;
import cloudpi.testARI.pages.RepositoryElements;
import cloudpi.testbkof.pages.EDC_BackOffice;

public class UC25_Manage_Internal_UE extends cloudpi.testARI.testLibs.BaseTest{
	private EDC_BackOffice bkOffice = null;
	private String bkOfUsername, bkOfPassword;
	private ExcelUtils excel = null;
	private RepositoryElements repEle = null;
	private By newBy = null;
	
	@Test(priority = 1, enabled = false)
	public void UC25_001_003() {
		bkOffice = new EDC_BackOffice(driver);
		excel = new ExcelUtils();
		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(1, 0);
			bkOfPassword = excel.getCellData(1, 1);
			repEle = new RepositoryElements("../EDCTest_BackOffice/config/BackOffice_Repository.properties");
			newBy = repEle.byLocator("BKOF_MyProfile_IDUsrAccessTab_Avaliability");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}

		Reporter.log("Begin Test: UC25.001.003 - Internal user modifies their availability to 100 %");
		bkOffice.BKOF_login(bkOfUsername, bkOfPassword);
		
		Reporter.log("Step 1 - Click on My Profile menu; available on the CIS Back office toolbar");
		bkOffice.BKOF_MyProfileLink().click();
		if (bkOffice.BKOF_MyProfile_IDUsrAccessTab().isDisplayed())
		{
			Reporter.log("Step 1 Result- System displays the My Profle popup, the tab displayed is the ID CARD and USER ACCESS tab");
		}
		else
		{
			Assert.fail("Step 1 Result - ID CARD and USER ACCESS tab not present");
			Reporter.log("Step 1 Result - ID CARD and USER ACCESS tab not present");
		}
		
		Reporter.log("Step 2 - Enter a 100 in the Availability field and hit submit");
		bkOffice.BKOF_MyProfile_IDUsrAccessTab().click();
		driver.findElement(newBy);
		bkOffice.BKOF_MyProfile_IDUsrAccessTab_Avaliability().clear();
		bkOffice.BKOF_MyProfile_IDUsrAccessTab_Avaliability().sendKeys("100");
		bkOffice.BKOF_MyProfile_IDUsrAccessTab_SubmitBtn().click();
		Alert alert = driver.switchTo().alert();
		alert.accept();
		Reporter.log("Step 2 Result- System save the entered value");
		
		bkOffice.BKOF_MyProfile_IDUsrAccessTab().click();
		driver.findElement(newBy);
		Reporter.log("Step 3 - Check availablity and it reflects 100");
		if (bkOffice.BKOF_MyProfile_IDUsrAccessTab_Avaliability().getAttribute("value") == "100")
		{
			Reporter.log("Step 3 Result- availablity value = 100");
		}
		else
		{
			softVerify(bkOffice.BKOF_MyProfile_IDUsrAccessTab_Avaliability().getAttribute("value"),"100","Step 3 Result - Avaliability value not equal to 100. Avaliability value is set to " + bkOffice.BKOF_MyProfile_IDUsrAccessTab_Avaliability().getAttribute("value"));
		}
	}
	
	@Test(priority = 1, enabled = true)
	public void UC25_003_001()
	{
		bkOffice = new EDC_BackOffice(driver);
		excel = new ExcelUtils();
		
		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(4, 0);
			bkOfPassword = excel.getCellData(4, 1);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Reporter.log("Begin Test: UC25.003.001 - Internal user modifies their password");
		bkOffice.BKOF_login(bkOfUsername, bkOfPassword);
		
		Reporter.log("Step 1 - User clicks on My Profile menu available on the CIS Back office toolbar");
		bkOffice.BKOF_MyProfileLink().click();
		
		if (bkOffice.BKOF_MyProfile_IDUsrAccessTab().isDisplayed() && bkOffice.BKOF_MyProfile_AuthoritiesTab().isDisplayed() && bkOffice.BKOF_MyProfile_CntryGradeTab().isDisplayed())
		{
			Reporter.log("Step 1 Result - System displays the My Profle popup,  the tab displayed is the ID CARD and USER ACCESS tab \n Screen is seperated into 3 parts: ID information, User Preferences, password management features");
		}
		else
		{
			softVerify(bkOffice.BKOF_MyProfile_IDUsrAccessTab().isDisplayed() && bkOffice.BKOF_MyProfile_AuthoritiesTab().isDisplayed() && bkOffice.BKOF_MyProfile_CntryGradeTab().isDisplayed(), true, "Step 1 Result - Tabs are missing");
		}
		
		Reporter.log("The user enters their old password and then enter and re-enters their password - with the correct criteria");
		bkOffice.BKOF_MyProfile_IDUsrAccessTab_OldPassText().sendKeys(bkOfPassword);
		bkOffice.BKOF_MyProfile_IDUsrAccessTab_NewPassText().sendKeys(bkOfPassword + "1");
		bkOffice.BKOF_MyProfile_IDUsrAccessTab_CnfmNewPassText().sendKeys(bkOfPassword + "1");
		bkOffice.BKOF_MyProfile_IDUsrAccessTab_SubmitBtn().click();
		Alert alert = driver.switchTo().alert();
		alert.accept();
		
		bkOffice.BKOF_MyProfile_CloseBtn().click();
		bkOffice.BKOF_ExitBtn().click();;
		alert = driver.switchTo().alert();
		alert.accept();
		Reporter.log("Step 4 - Logs out of the CIS system and re-login with their new password");
		bkOffice.BKOF_login(bkOfUsername, bkOfPassword + "1");
		if (bkOffice.BKOF_MyProfileLink().isDisplayed())
		{
			Reporter.log("Step 4 Result - User successfully logs on");
		}
		else
		{
			Assert.fail("Step 4 Result - Unable to log in");
			Reporter.log("Step 4 Result - User successfully logs on");
		}
		
		//Resetting password after test
		bkOffice.BKOF_MyProfileLink().click();
		bkOffice.BKOF_MyProfile_IDUsrAccessTab_OldPassText().sendKeys(bkOfPassword + "1");
		bkOffice.BKOF_MyProfile_IDUsrAccessTab_NewPassText().sendKeys("1Test123");
		bkOffice.BKOF_MyProfile_IDUsrAccessTab_CnfmNewPassText().sendKeys("1Test123");
		bkOffice.BKOF_MyProfile_IDUsrAccessTab_SubmitBtn().click();
		alert = driver.switchTo().alert();
		alert.accept();
		
		bkOffice.BKOF_MyProfile_CloseBtn().click();
		bkOffice.BKOF_ExitBtn().click();;
		alert = driver.switchTo().alert();
		alert.accept();
	}
	

}
