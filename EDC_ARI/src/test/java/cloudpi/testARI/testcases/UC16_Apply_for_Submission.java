package cloudpi.testARI.testcases;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.Test;

import cloudpi.testARI.utility.ExcelUtils;
import cloudpi.testbkof.pages.EDC_BackOffice;

public class UC16_Apply_for_Submission extends cloudpi.testARI.testLibs.BaseTest{
	
	private EDC_BackOffice bkOffice = null;
	private WebDriverWait wait;
	private String bkOfUsername, bkOfPassword;
	private ExcelUtils excel = null;
	
	@Test(priority = 1, enabled = true)
	public void UC16_005_001() {
		bkOffice = new EDC_BackOffice(driver);
		wait = new WebDriverWait(driver, 30);
		excel = new ExcelUtils();
		
		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(1, 0);
			bkOfPassword = excel.getCellData(1, 1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Reporter.log("Begin Test: UC16.005.001");

	}
}