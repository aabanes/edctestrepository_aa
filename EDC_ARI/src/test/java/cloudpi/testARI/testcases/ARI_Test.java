package cloudpi.testARI.testcases;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Reporter;
import org.testng.annotations.Test;

import cloudpi.testARI.pages.RepositoryElements;
import cloudpi.testARI.utility.ExcelUtils;
import cloudpi.testbkof.pages.EDC_BackOffice;
import junit.framework.Assert;

public class ARI_Test extends cloudpi.testARI.testLibs.BaseTest {
	private EDC_BackOffice bkOffice = null;
	private String bkOfUsername, bkOfPassword, testusr4, dateFormat, testCode;
	private ExcelUtils excel = null;
	private By newBy, newBy2, newBy3, newBy4;
	private RepositoryElements repEle = null;
	private List<WebElement> workBasketCont, res, usrTable, selectedUsrList;
	private List<String> usrArr;
	private int i;

	@Test
	public void f() {
		bkOffice = new EDC_BackOffice(driver);
		excel = new ExcelUtils();
		usrArr = new ArrayList<String>();
		i=0;

		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(1, 0);
			bkOfPassword = excel.getCellData(1, 1);
			usrArr.add(excel.getCellData(6, 2) + " " + excel.getCellData(6, 3));
			usrArr.add(excel.getCellData(7, 2) + " " + excel.getCellData(7, 3));
			usrArr.add(excel.getCellData(8, 2) + " " + excel.getCellData(8, 3));

			for (String string : usrArr) {
				System.out.println(string);
			}

			repEle = new RepositoryElements("../EDCTest_BackOffice/config/BackOffice_Repository.properties");
			newBy = repEle.byLocator("BKOF_Administration_WorkBasket_WorkBasketList");
			newBy2 = repEle.byLocator("BKOF_Administration_WorkBasket_WorkBasketDetailContent");
			newBy3 = repEle.byLocator("BKOF_Administration_WorkBasket_AddWkBst_UserTable");
			newBy4 = repEle.byLocator("BKOF_Administration_WorkBasket_AddWkBst_SelectedUsers");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Reporter.log("Begin Test: UC28.001.001");
		bkOffice.BKOF_login(bkOfUsername, bkOfPassword);

		// Get User's preferred date format
		bkOffice.BKOF_MyProfileLink().click();
		Select profileDateFormat = new Select(bkOffice.BKOF_MyProfile_IDUsrAccessTab_DateFormat());
		dateFormat = profileDateFormat.getFirstSelectedOption().getText();

		Reporter.log(
				"Step 1 - Navigate to the Workbasket Settings screen by selecting ADMINISTRATION > SETTINGS > WORK BASKET SETTINGS");
		bkOffice.BKOF_MainMenu().click();
		bkOffice.BKOF_MainMenu_Administration().click();
		bkOffice.BKOF_MainMenu_Administration_Settings().click();
		bkOffice.BKOF_MainMenu_Administration_Settings_WrkBsktSet().click();

		System.out.println(bkOffice.BKOF_Administration_WorkBasket_Header().getText());
		if (bkOffice.BKOF_Administration_WorkBasket_Header().getText().contains("WorkBasket settings")) {
			Reporter.log("Step 1 Result - System displays S143 Workbasket List screen");
		} else {
			Assert.fail("Step 1 Result - Landed on incorrect page");
			Reporter.log("Step 1 Result - Landed on incorrect page");
		}

		// Test visual layout manually. Only testing the following:
		// If workbaskets are displayed and if workbasket display code &
		// description for each workbasket in list
		// If add button appears enabled on screen
		// If right panel of page appears blank by default.
		Reporter.log("Step 2 - Verify S143 Workbasket List screen initial display.");

		if (bkOffice.BKOF_Administration_WorkBasket_AddBtn().isDisplayed()) {
			Reporter.log("Step 2 Result - Add button appears enabled on the screen");
		} else {
			Assert.fail("Step 2 Re1sult - Add button not present on the screen");
			Reporter.log("Step 2 Result - Add button not present on the screen");
		}

		workBasketCont = driver.findElements(newBy2);
		res = driver.findElements(By.tagName("div"));

		if (res.size() == 0) {
			Reporter.log("Step 2 Result - Right panel of screen appears blank");
		} else {
			Reporter.log("Step 2 Result - Right panel of screen is populated");
		}

		Reporter.log("Step 3 - Click on ADD");
		bkOffice.BKOF_Administration_WorkBasket_AddBtn().click();

		if (bkOffice.BKOF_Administration_WorkBasket_AddWkBst_Code().isDisplayed()) {
			Reporter.log("Step 3 Result - System displays S144 Workbasket Details screen");
		} else {
			Assert.fail("Step 3 Result - System DOES NOT display S144 Workbasket Details screen");
			Reporter.log("Step 3 Result - System DOES NOT display S144 Workbasket Details screen");
		}

		Reporter.log(
				"Step 4 - Input a value in the Workbasket Code field that does not correspond to an existing Workbasket Code and enter a value in the Workbasket Description field; \n"
						+ "Select an Assignment Strategy from the Assignment Strategy dropdown list.");
		bkOffice.BKOF_Administration_WorkBasket_AddWkBst_Code().sendKeys("test");
		bkOffice.BKOF_Administration_WorkBasket_AddWkBst_Description().sendKeys("Testing Workbasket Creation");
		Select strat = new Select(bkOffice.BKOF_Administration_WorkBasket_AddWkBst_SelectStrategy());
		strat.selectByVisibleText("Random");
		bkOffice.BKOF_Administration_WorkBasket_AddWkBst_AuthorityLevel().click();

		Reporter.log(
				"Step 5 - Select a user from the Users List and click Right Arrow and repeat for 2 additional users.");
		Select usrTableSel = new Select(bkOffice.BKOF_Administration_WorkBasket_AddWkBst_UserTable());
		for (String string : usrArr) {
			try {
				usrTableSel.selectByVisibleText(string);
			} catch (NoSuchElementException e) {

			}
		}
		bkOffice.BKOF_Administration_WorkBasket_AddWkBst_AddBtn().click();

		usrTable = driver.findElements(newBy3);
		for(String s : usrArr)
		{
			for(int l = 0; l < usrTable.size(); l++)
			{
				res = usrTable.get(l).findElements(By.tagName("option"));
				for(int k = 0; k < res.size(); k++)
				{
					if (res.get(k).getText().equals(s))
					{
						Assert.fail("User found in list. User" + s);
						Reporter.log("User found in list. User" + s);
						break;
					}
				}
			}
		}

		selectedUsrList = driver.findElements(newBy4);
		for (String string : usrArr) {
			for(int l = 0; l < selectedUsrList.size(); l++)
			{
				res = selectedUsrList.get(l).findElements(By.tagName("option"));
				for(int k = 0; k < res.size(); k++)
				{
					if (k == res.size())
					{
						Assert.fail("User not found in list. User" + string);
						Reporter.log("User not found in list. User" + string);
						break;
					}
				}
			}
		}
	}

}
