package cloudpi.testARI.testcases;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import cloudpi.testARI.utility.EWSJavaAPI;
import cloudpi.testARI.utility.ExcelUtils;
import cloudpi.testbkof.pages.EDC_BackOffice;
import junit.framework.Assert;
import cloudpi.testARI.pages.ARI_Login;
import cloudpi.testARI.pages.ARI_Menu;
import cloudpi.testARI.pages.ARI_PreSubmission;
import cloudpi.testARI.pages.RepositoryElements;

public class UC01_Process_Submission extends cloudpi.testARI.testLibs.BaseTest {

	private ARI_Login ariLogin;
	private ARI_Menu ariMenu;
	private EDC_BackOffice bkOffice;
	private Wait<WebDriver> wait;
	private String ariUsr, ariPassword, bkOfUsername, bkOfPassword, date, subID, email, readLoginID, readPassword,
			readDomain, readEmail, readEmailSubject, readCompareString, dateFormat, usrLastName, usrFirstName,
			lossesInYr = "", receivablesPastDate = "", creditDecisions = "", creditDecisionsReviewed = "",
			primaryProdServ = "", overallPercentage = "", typeOfBus = "", readilySold = "", foreignSales = "",
			affilatedComp = "", insReq = "", outCountry = "", outAnnualIns = "", outInsCurr = "", outPaymentTerms = "",
			inProv = "", inAnnualIns = "", inInsCurr = "", inPaymentTerms = "", brokerInvolved = "", brokerName = "",
			brokerAddress = "", brokerCity = "", brokerProv = "", brokerFName = "", brokerLName = "", brokerPhone = "",
			brokerEmail = "", claimBenefits = "", receivablesIns = "", coverageStart = "", policyLang = "",
			policyCurr = "", comments = "";;
	private ExcelUtils excel, excel2;
	private ARI_PreSubmission ariPre;
	private By newBy, newBy2, newBy3, newBy4, newBy5, newBy6, newBy7, newBy8;
	private RepositoryElements repEle;
	private EWSJavaAPI emailRead;

	@Test(priority = 1, enabled = false)
	public void UC01_000_001() {
		ariLogin = new ARI_Login(driver);
		ariMenu = new ARI_Menu(driver);
		ariPre = new ARI_PreSubmission(driver);
		bkOffice = new EDC_BackOffice(driver);
		wait = new WebDriverWait(driver, 30);
		excel = new ExcelUtils();
		excel2 = new ExcelUtils();
		emailRead = new EWSJavaAPI();

		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(1, 0);
			bkOfPassword = excel.getCellData(1, 1);
			ariUsr = excel.getCellData(11, 0);
			ariPassword = excel.getCellData(11, 1);
			excel.excelClose();

			excel.getExcelFile("./testData/TestData.xlsx", "Sheet4");
			readLoginID = excel.getCellData(2, 0);
			readPassword = excel.getCellData(2, 1);
			readDomain = excel.getCellData(2, 2);
			readEmail = excel.getCellData(2, 4);
			readEmailSubject = excel.getCellData(2, 3);
			readCompareString = excel.getCellData(2, 5);
			excel.excelClose();

			repEle = new RepositoryElements("../EDCTest_BackOffice/config/BackOffice_Repository.properties");
			newBy8 = repEle.byLocator("BKOF_Risk_Tasks_TasksTable");

		} catch (Exception e) {
			e.printStackTrace();
		}

		Reporter.log("Begin Test: UC01.000.001");

		preConUC16_005_001();
		bkOffice.BKOF_login(bkOfUsername, bkOfPassword);
		waitForPageToLoad(bkOffice.BKOF_SearchIcon());

		bkOffice.BKOF_MainMenu().click();
		bkOffice.BKOF_MainMenu_Risk().click();
		bkOffice.BKOF_MainMenu_Commerical_Workflow().click();
		bkOffice.BKOF_MainMenu_Commerical_Workflow_UnassignedTasks().click();

		bkOffice.BKOF_Risk_TaskMenu_Presubmission().click();
		Reporter.log("Step 1 - Clicks on 'Pre-Submission' File link");
		List<WebElement> taskTable = driver.findElements(newBy8);
		List<WebElement> tasks;
		for (int i = 0; i < taskTable.size(); i++) {
			tasks = taskTable.get(i).findElements(By.tagName("tr"));
			for (int j = 0; j < tasks.size(); j++) {
				if (tasks.get(j).getText().contains(subID)) {
					tasks.get(j).click();
				}
			}
		}
		Reporter.log(
				"Step 1 Result - Pre-Submission PDF opens (completed with Information submitted by Applicant from UC-BB-29 and UC-BB-16 and in template form T022 - Submission");

		if (bkOffice.BKOF_Task_New_Assignee().getText().equals(bkOfUsername) == false) {
			bkOffice.BKOF_Task_New_Assignee().clear();
			bkOffice.BKOF_Task_New_Assignee().sendKeys(bkOfUsername);
			bkOffice.BKOF_Task_AutoComplete().click();
			bkOffice.BKOF_Task_New_Assignee_SubmitBtn().click();
		} else {
			Assert.fail("New Assignee field missing!");
		}

		bkOffice.BKOF_MainMenu_Commerical_Workflow().click();
		bkOffice.BKOF_MainMenu_Commerical_Workflow_MyTasks().click();
		bkOffice.BKOF_Risk_TaskMenu_Presubmission().click();
		taskTable = driver.findElements(newBy8);
		for (int i = 0; i < taskTable.size(); i++) {
			tasks = taskTable.get(i).findElements(By.tagName("tr"));
			for (int j = 0; j < tasks.size(); j++) {
				if (tasks.get(j).getText().contains(subID)) {
					tasks.get(j).click();
				}
			}
		}
		Reporter.log("Step 2 - Check the display of the radio buttons on the right panel for the selected task");
		if (bkOffice.BKOF_Task_DeclinePreSubmissionLabel().getText().equals("Decline the pre-submission")
				&& bkOffice.BKOF_Task_CreateSubmissionLabel().getText().equals("Create submission")) {
			Reporter.log(
					"Step 2 Result - Two radio buttons should be displayed as:\n - Decline the pre-submission\n - Create submission\n None of the radio buttons should be selected by default");
		} else {
			Reporter.log("Step 2 Result - Incorrect labels.");
			Reporter.log("Expected: Decline the pre-submission \n Actual: "
					+ bkOffice.BKOF_Task_DeclinePreSubmissionLabel().getText());
			Reporter.log(
					"Expected: Create Submission \n Actual: " + bkOffice.BKOF_Task_CreateSubmissionLabel().getText());
		}

		Reporter.log(
				"Step 3 - Reviews PDF and decides to Decline the Submission and clicks 'Decline the Pre-Submission' radio button");
		bkOffice.BKOF_Task_DeclinePreSubmission().click();
		Reporter.log("Step 3 Result - Radio Button is selected");
		Reporter.log("Step 4 - Clicks 'Submit' button");
		bkOffice.BKOF_Task_DeclinePreSubmission_SubmitBtn().click();

		if (bkOffice.BKOF_Task_DeclinePreSubmissionPopup_ReasonSelect().isDisplayed()) {
			Reporter.log("Step 4 Result- S498 - Decline Submission Pop up is displayed..");
		} else {
			Assert.fail();
			Reporter.log("Step 4 Result- Popup not present.");
		}

		Reporter.log("Step 5 - Selects Reason Code from Dropdown.");
		Select selReasons = new Select(bkOffice.BKOF_Task_DeclinePreSubmissionPopup_ReasonSelect());
		String[] reasonArr = new String[] { "No Canadian Benefits", "Anti Corruption Issues", "Country/Political Risk",
				"Customer Risk", "Environmental Concern", "Failed Qualification Questions", "Obligor/Buyer Risk",
				"Security Issues/Terrorist" };
		List<WebElement> reasons = selReasons.getOptions();
		Iterator<WebElement> itr = reasons.iterator();
		for (int i = 0; i < reasonArr.length; i++) {
			for (WebElement s : reasons) {
				if (s.getText() == reasonArr[i]) {
					break;
				}
				if (!itr.hasNext()) {
					Reporter.log("Step 5 Result - Value not found in list. Value: " + reasonArr[i]);
				}
			}
		}

		selReasons.selectByVisibleText("No longer required");
		Reporter.log("Step 6 - Enters text comment in comment box");
		bkOffice.BKOF_Task_DeclinePreSubmissionPopup_Comment().sendKeys("Test Decline of Submission");
		Reporter.log("Step 6 Result - Comment populates field");
		bkOffice.BKOF_Task_DeclinePreSubmission_SubmitBtn().click();
		Reporter.log("Step 7 - Clicks Submit");

		Reporter.log("Step 8 - Verifies in Pre-Submission List (Underwriting > Pre-Submission List)");
		sleep(5);
		bkOffice.BKOF_MainMenu().click();
		bkOffice.BKOF_MainMenu_Commericial().click();
		bkOffice.BKOF_MainMenu_Commerical_UnderWriting().click();
		bkOffice.BKOF_MainMenu_Commerical_UnderWriting_PreSubmissionList().click();
		bkOffice.BKOF_Commercial_UnderWriting_PreSubmission_Filter_PreSubID().sendKeys(subID);
		bkOffice.BKOF_Commercial_UnderWriting_PreSubmission_Filter_SubmitBtn().click();
		taskTable = driver.findElements(newBy8);
		for (int i = 0; i < taskTable.size(); i++) {
			tasks = taskTable.get(i).findElements(By.tagName("tr"));
			for (int j = 0; j < tasks.size(); j++) {
				if (tasks.get(j).getText().contains(subID) && tasks.get(j).getText().contains("Declined")) {
					Reporter.log("Step 8 Result - Pre-Submission status has been changed to 'DECLINED'");
				}
				if (j == tasks.size()) {
					Reporter.log("Presubmission not found in Underwriting -> PreSubmission list");
				}
			}
		}

		// Unable to test step 9 - Integration test
		email = emailRead.getEmailBySubject(readLoginID, readPassword, readDomain, readEmail, readEmailSubject, 2);

		if (email.isEmpty() == false) {
			Reporter.log(
					"Step 10 Result - Has received 'Email  1 - Unable to Process Application'  in 'Policy Language'");
		} else {
			Reporter.log("Step 10 Result - No emails found");
		}

		Reporter.log("Step 11 - Verifies wording of email matches wording provided in template in proper language");

		if (email.contains(readCompareString)) {
			Reporter.log("Step 11 Result - Wording is as per template and in proper language");
		} else {
			Reporter.log("Step 11 Result - Wording mismatch. Email contents: \n\n" + email);
		}

		Reporter.log("Step 12 - Signs into CIS Portal and navigates to their Submission List");
		sleep(5);
		ariLogin.ARI_SignIn(ariUsr, ariPassword);
		ariMenu.ARI_Menu_PolicyHolder_Action().click();
		ariMenu.ARI_Menu_PolicyHolder_Action_PreSubmission().click();
		List<WebElement> subTbl = driver.findElements(newBy7);
		List<WebElement> res;
		setDate("dd/MM/YYYY", 0);

		subTbl = driver.findElements(newBy7);
		for (int i = 0; i < subTbl.size(); i++) {
			res = subTbl.get(i).findElements(By.tagName("tr"));
			for (int j = 0; j < res.size(); j++) {
				if (res.get(j).getText().contains("Declined") && res.get(j).getText().contains(getDate())
						&& res.get(j).getText().contains(subID)) {
					Reporter.log("Step 12 Result - Applicant can view the 'Declined' Submission.");
				}
				if (j == res.size()) {
					Reporter.log("Step 12 Result - Submission not found");
				}
			}
		}
		ariMenu.ARI_Menu_PolicyHolder_Exit().click();
	}

	@Test(priority = 1, enabled = false)
	public void UC01_001_002() {
		ariLogin = new ARI_Login(driver);
		ariMenu = new ARI_Menu(driver);
		ariPre = new ARI_PreSubmission(driver);
		bkOffice = new EDC_BackOffice(driver);
		wait = new WebDriverWait(driver, 30);
		excel = new ExcelUtils();
		excel2 = new ExcelUtils();
		emailRead = new EWSJavaAPI();

		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(1, 0);
			bkOfPassword = excel.getCellData(1, 1);
			ariUsr = excel.getCellData(11, 0);
			ariPassword = excel.getCellData(11, 1);
			usrFirstName = excel.getCellData(1, 2);
			usrLastName = excel.getCellData(1, 3);

			excel.getExcelFile("./testData/TestData.xlsx", "Sheet4");
			readLoginID = excel.getCellData(2, 0);
			readPassword = excel.getCellData(2, 1);
			readDomain = excel.getCellData(2, 2);
			readEmail = excel.getCellData(2, 4);
			readEmailSubject = excel.getCellData(2, 3);
			readCompareString = excel.getCellData(2, 5);
			excel.excelClose();

		} catch (Exception e) {
			e.printStackTrace();
		}

		Reporter.log("Begin Test: UC01.001.002");

		preConUC16_005_001();
		try {
			repEle = new RepositoryElements("../EDCTest_BackOffice/config/BackOffice_Repository.properties");
			newBy = repEle.byLocator("BKOF_Risk_Tasks_TasksTable");
			newBy2 = repEle.byLocator("BKOF_Contact_Messages");
			newBy3 = repEle.byLocator("BKOF_PplOrg_UserProfile_EventsList");
		} catch (IOException e) {
			e.printStackTrace();
		}

		bkOffice.BKOF_login(bkOfUsername, bkOfPassword);
		waitForPageToLoad(bkOffice.BKOF_SearchIcon());

		// Get User's preferred date format
		getDateFormat();

		// Search for presubmission with matching submission ID and create
		// submission
		createViewSubmission("Create");

		Select selInsurer = new Select(bkOffice.BKOF_Submission_GeneralInfo_PolicyAllocation_Insurer());
		Select selOffice = new Select(bkOffice.BKOF_Submission_GeneralInfo_PolicyAllocation_Office());
		Select selCommOwner = new Select(bkOffice.BKOF_Submission_GeneralInfo_PolicyAllocation_CommOwner());

		Reporter.log("Step 1 - Selects Insurer from dropdown menu (mandatory field)");
		if (selInsurer.getFirstSelectedOption().getText().equals("--None--")) {
			Reporter.log(
					"Step 1 Result - Field is pre-populated with 'Export Development Canada' - LOV is: Export Development Canada (For Phase 1- other registered Insurers for future Phases) and should be default value. ");
		}

		selInsurer.selectByIndex(1);
		Reporter.log("Step 1 Result - Able to select default value from field.");

		Reporter.log("Step 2 - Clicks on 'Office' dropdown and is able to select value from LOV");
		if (selOffice.getFirstSelectedOption().getText().equals("--None--")) {
			Reporter.log(
					"Step 2 Result - LOV is list of already registered Offices input by EDC. Default Value is None ");
			selOffice.selectByIndex(1);
			Reporter.log("Step 2 Result - Able to select a value from field.");
		}

		Reporter.log("Step 3 - Clicks on 'Commercial Owner' dropdown and is able to select value from LOV.");
		List<WebElement> selList = selCommOwner.getOptions();
		List<String> sortedList = new ArrayList<String>();
		List<String> nonSorted = new ArrayList<String>();

		for (WebElement ele : selList) {
			sortedList.add(ele.getText());
			nonSorted.add(ele.getText());
		}
		Collections.sort(sortedList);

		boolean res = sortedList.equals(nonSorted);
		if (res) {
			Reporter.log(
					"Step 3 Result - LOV is list of all operating users with Underwriting Permissions in alphabetical order (based on the last name).");
		} else {
			Reporter.log("Step 3 Result - Names not sorted alphabetically by last name");
		}
		selCommOwner.selectByIndex(2);
		if (selCommOwner.getFirstSelectedOption().getText().equals("--None--")) {
			Reporter.log("Step 3 Result - Default value of Commerical Office is set to --None--");
		} else {
			Reporter.log("Step 3 Result - Default value is not set to --None--");
		}
		bkOffice.BKOF_Submission_SaveBtn().click();
		setDate(dateFormat + " HH:mm:ss", 0);

		List<WebElement> messageCol = driver.findElements(newBy2);
		List<WebElement> msgs;
		for (int i = 0; i < messageCol.size(); i++) {
			msgs = messageCol.get(i).findElements(By.tagName("div"));

			for (int j = 0; j < msgs.size(); j++) {
				if (msgs.get(j).getText().equals("Modification saved")) {
					Reporter.log(
							"Step 4 Results - Information added to date is saved to Submission, Notice advising  'Modification Saved' is displayed");
					break;
				}
				if (j == msgs.size()) {
					Reporter.log("Step 4 Result - Message not found");
				}
			}
		}
		if (bkOffice.BKOF_Submission_Status().getText().contains("Draft")) {
			Reporter.log("Step 4 Result - Redirected back to submission page after update");
		} else {
			Assert.fail("Step 4 Result - Incorrect status on submission.");
		}
		bkOffice.BKOF_Menu_Submission_Events().click();
		List<WebElement> eventList = driver.findElements(newBy3);
		List<WebElement> events;

		for (int i = 0; i < eventList.size(); i++) {
			events = eventList.get(i).findElements(By.tagName("tr"));

			for (int j = 0; j < 2; j++) {
				if (events.get(j).getText().contains(getDate())
						&& events.get(j).getText().contains(usrLastName + " " + usrFirstName)
						&& events.get(j).getText().contains("Update of a submission draft")) {
					Reporter.log("Step 4 Results - Event is logged in Events tab as 'Update of a submission draft'.");
					break;
				}
			}
		}
	}

	@Test(priority = 1, enabled = false)
	public void UC01_001_003() {
		String readCntctTitle = "", readCntctFName = "", readCntctLastName = "", readCntctJobTitle = "",
				readCntctDepartment = "", readCntctPhoneNum = "", readCntctFaxNum = "", readCntctEmail = "",
				compName = "";
		ariLogin = new ARI_Login(driver);
		ariMenu = new ARI_Menu(driver);
		ariPre = new ARI_PreSubmission(driver);
		bkOffice = new EDC_BackOffice(driver);
		wait = new WebDriverWait(driver, 30);
		excel = new ExcelUtils();
		excel2 = new ExcelUtils();
		emailRead = new EWSJavaAPI();

		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(1, 0);
			bkOfPassword = excel.getCellData(1, 1);
			ariUsr = excel.getCellData(11, 0);
			ariPassword = excel.getCellData(11, 1);
			usrFirstName = excel.getCellData(1, 2);
			usrLastName = excel.getCellData(1, 3);

			excel.getExcelFile("./testData/TestData.xlsx", "Sheet3");
			readCntctTitle = excel.getCellData(5, 0);
			readCntctFName = excel.getCellData(5, 1);
			readCntctLastName = excel.getCellData(5, 2);
			readCntctJobTitle = excel.getCellData(5, 3);
			readCntctDepartment = excel.getCellData(5, 4);
			readCntctPhoneNum = excel.getCellData(5, 5);
			readCntctFaxNum = excel.getCellData(5, 6);
			readCntctEmail = excel.getCellData(5, 7);
			excel.excelClose();

		} catch (Exception e) {
			e.printStackTrace();
		}
		wait = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS)
				.ignoring(StaleElementReferenceException.class);
		Reporter.log("Begin Test: UC01.001.003");

		preConUC16_005_001();
		try {
			repEle = new RepositoryElements("../EDCTest_BackOffice/config/BackOffice_Repository.properties");
			newBy = repEle.byLocator("BKOF_Risk_Tasks_TasksTable");
			newBy2 = repEle.byLocator("BKOF_Ppl&Org_InsuredList_FilterSearch_Results");
			newBy3 = repEle.byLocator("BKOF_Contact_UserTable");
			newBy4 = repEle.byLocator("BKOF_Contact_Messages");
			newBy5 = repEle.byLocator("BKOF_PplOrg_UserProfile_EventsList");
			newBy6 = repEle.byLocator("BKOF_Submission_ApplicantTab_ContactDropdown");
		} catch (IOException e) {
			e.printStackTrace();
		}

		bkOffice.BKOF_login(bkOfUsername, bkOfPassword);
		waitForPageToLoad(bkOffice.BKOF_SearchIcon());

		// Get User's preferred date format
		getDateFormat();

		// Search for presubmission with matching submission ID and create
		// submission
		createViewSubmission("Create");

		compName = bkOffice.BKOF_Submission_CompanyName().getText();
		Reporter.log("Step 1 - Clicks on 'New' button to add a new contact");
		bkOffice.BKOF_Submission_ApplicantTab_NewBtn().click();
		if (bkOffice.BKOF_Submission_ApplicantTab_NewUsrPopup_FirstName().isDisplayed()) {
			Reporter.log("Step 1 Result - Displays S500 - New User window (OOTB)");
		} else {
			Assert.fail("Step 1 Result - New User popup not present.");
			Reporter.log("Step 1 Result - New User popup not present.");
		}

		Reporter.log(
				"Step 2 - Enters Contact details: 'Salutation', 'Last Name', 'First Name', 'Tel/Fax', 'Role/Dept', 'Language', 'Date Format', 'Email' as per SV01.SV.XXX and clicks 'Submit' button");
		Select selTitle = new Select(bkOffice.BKOF_Submission_ApplicantTab_NewUsrPopup_Title());
		selTitle.selectByVisibleText(readCntctTitle);
		bkOffice.BKOF_Submission_ApplicantTab_NewUsrPopup_LastName().sendKeys(readCntctLastName);
		bkOffice.BKOF_Submission_ApplicantTab_NewUsrPopup_FirstName().sendKeys(readCntctFName);
		bkOffice.BKOF_Submission_ApplicantTab_NewUsrPopup_Position().sendKeys(readCntctJobTitle);
		bkOffice.BKOF_Submission_ApplicantTab_NewUsrPopup_Department().sendKeys(readCntctDepartment);
		bkOffice.BKOF_Submission_ApplicantTab_NewUsrPopup_Phone().sendKeys(readCntctPhoneNum);
		bkOffice.BKOF_Submission_ApplicantTab_NewUsrPopup_Fax().sendKeys(readCntctFaxNum);
		bkOffice.BKOF_Submission_ApplicantTab_NewUsrPopup_Email().sendKeys(readCntctEmail);
		bkOffice.BKOF_Submission_ApplicantTab_NewUsrPopup_SubmitBtn().click();

		while (true) {
			try {
				retryingFindClick(
						By.xpath("//div[@id='zone-detail-applicant-content']//select[@id='select_csu_insPerId']"));
				Select selAppTabUsrList = new Select(bkOffice.BKOF_Submission_ApplicantTab_ContactDropdown());
				List<WebElement> selUsrList = selAppTabUsrList.getOptions();
				if (selAppTabUsrList.getFirstSelectedOption().getText().equals(readCntctLastName + " " + readCntctFName)
						&& bkOffice.BKOF_Submission_ApplicantTab_PhoneNum().getAttribute("value")
								.equals(readCntctPhoneNum)
						&& bkOffice.BKOF_Submission_ApplicantTab_FaxNum().getAttribute("value").equals(readCntctFaxNum)
						&& bkOffice.BKOF_Submission_ApplicantTab_Email().getAttribute("value").equals(readCntctEmail)) {
					Reporter.log(
							"Step 2 Result \n- Contact details populate Contact Section of Applicant Section (and replace previous contact information if had been populated by Applicant search).");
				} else {
					Reporter.log("Value mismatch. \n Contact Drop Down default value: "
							+ selAppTabUsrList.getFirstSelectedOption().getText()
							+ "Expected Contact Drop Down default value: " + readCntctFName + " " + readCntctFName
							+ "\n Contact Phone Number: "
							+ bkOffice.BKOF_Submission_ApplicantTab_PhoneNum().getAttribute("value")
							+ "Expected Phone Number: " + readCntctPhoneNum + "\n Contact Fax Number: "
							+ bkOffice.BKOF_Submission_ApplicantTab_FaxNum().getAttribute("value")
							+ "Expected Fax Number: " + readCntctFaxNum + "\n Contact Email Number: "
							+ bkOffice.BKOF_Submission_ApplicantTab_Email().getAttribute("value") + "Expected Email: "
							+ readCntctEmail);
				}

				retryingFindClick(
						By.xpath("//div[@id='zone-detail-applicant-content']//select[@id='select_csu_insPerId']"));
				selAppTabUsrList = new Select(bkOffice.BKOF_Submission_ApplicantTab_ContactDropdown());
				if (selUsrList.size() > 1) {
					selAppTabUsrList.selectByIndex(0);
					for (WebElement ele : selUsrList) {
						try {
							selAppTabUsrList.selectByVisibleText(ele.getText());
						} catch (NoSuchElementException e) {
							Reporter.log("Unable to select the following user from the list: User: " + ele.getText());
						}
					}
					Reporter.log(
							"Step 2 Result - If previous contact(s) existed, they are still available in the drop down menu and New Contact is added to dropdown menu.");
				}
				break;
			} catch (StaleElementReferenceException e) {
				retryingFindClick(
						By.xpath("//div[@id='zone-detail-applicant-content']//select[@id='select_csu_insPerId']"));
			}
		}

		Reporter.log(
				"Step 3 - Verifies that the Contact has been added at the Company Level by checking Insured - Clicks Administration > People & Organization > Insured List - Chooses Insured > Clicks on Contact");
		bkOffice.BKOF_MainMenu().click();
		bkOffice.BKOF_MainMenu_Administration().click();
		bkOffice.BKOF_MainMenu_Administration_PplOrg().click();
		bkOffice.BKOF_MainMenu_Administration_PplOrg_InsuredList().click();
		bkOffice.BKOF_PplOrg_InsuredList_FilterBtn().click();
		waitForPageToLoad(bkOffice.BKOF_PplOrg_InsuredList_FilterSearch_Name());
		bkOffice.BKOF_PplOrg_InsuredList_FilterSearch_Name().sendKeys(compName);
		bkOffice.BKOF_PplOrg_InsuredList_FilterSearch_SubmitBtn().click();

		List<WebElement> resultsPaneEle2 = driver.findElements(newBy2);
		List<WebElement> res2;
		for (int i = 0; i < resultsPaneEle2.size(); i++) {
			res2 = resultsPaneEle2.get(i).findElements(By.tagName("tr"));

			for (int j = 0; j < res2.size();) {
				if (res2.get(j).findElements(By.tagName("a")).size() > 0) {
					res2.get(j).findElement(By.tagName("a")).click();
					break;
				}
			}
		}

		waitForPageToLoad(bkOffice.BKOF_Identity_NameTable());
		bkOffice.BKOF_Menu_Contact().click();
		List<WebElement> contactCol = driver.findElements(newBy3);
		List<WebElement> res;
		for (int i = 0; i < contactCol.size(); i++) {
			res = contactCol.get(i).findElements(By.tagName("td"));

			for (int j = 0; j < res.size(); j++) {
				if (res.get(j).getText().equals(readCntctLastName + " " + readCntctFName)) {
					res.get(j).click();
					break;
				}
			}
		}

		selTitle = new Select(bkOffice.BKOF_Contact_IDCard_Title());
		if (selTitle.getFirstSelectedOption().getText().equals(readCntctTitle)) {
			Reporter.log("Title matches");
		} else {
			Reporter.log("Title mismatch");
		}
		if (bkOffice.BKOF_Contact_IDCard_LastName().getAttribute("value").equals(readCntctLastName)) {
			Reporter.log("Last Name matches");
		} else {
			Reporter.log("Last Name mismatch");
		}
		if (bkOffice.BKOF_Contact_IDCard_FirstName().getAttribute("value").equals(readCntctFName)) {
			Reporter.log("First Name matches");
		} else {
			Reporter.log("First Name mismatch");
		}
		if (bkOffice.BKOF_Contact_IDCard_JobTitle().getAttribute("value").equals(readCntctJobTitle)) {
			Reporter.log("Job Title matches");
		} else {
			Reporter.log("Job Title mismatch");
		}
		if (bkOffice.BKOF_Contact_IDCard_Department().getAttribute("value").equals(readCntctDepartment)) {
			Reporter.log("Department matches");
		} else {
			Reporter.log("Department mismatch");
		}
		if (bkOffice.BKOF_Contact_IDCard_PhoneNum().getAttribute("value").equals(readCntctPhoneNum)) {
			Reporter.log("Phone Number matches");
		} else {
			Reporter.log("Phone Number mismatch");
		}
		if (bkOffice.BKOF_Contact_IDCard_FaxNum().getAttribute("value").equals(readCntctFaxNum)) {
			Reporter.log("Fax Number matches");
		} else {
			Reporter.log("Fax Number mismatch");
		}
		if (bkOffice.BKOF_Contact_IDCard_Email().getAttribute("value").equals(readCntctEmail)) {
			Reporter.log("Email matches");
		} else {
			Reporter.log("Email mismatch");
		}
		Reporter.log(
				"Step 3 Results - Contact added appears in Contact List with appropriate details - Check log for results");
		createViewSubmission("View");

		waitForPageToLoad(bkOffice.BKOF_Submission_CompanyName());
		Reporter.log("Step 4 - Clicks on 'SAVE'");
		bkOffice.BKOF_Submission_SaveBtn().click();
		setDate(dateFormat + " HH:mm:ss", 0);

		List<WebElement> messageCol = driver.findElements(newBy4);
		List<WebElement> msgs;
		for (int i = 0; i < messageCol.size(); i++) {
			msgs = messageCol.get(i).findElements(By.tagName("div"));

			for (int j = 0; j < msgs.size(); j++) {
				if (msgs.get(j).getText().equals("Modification saved")) {
					Reporter.log(
							"Step 4 Results - Information added to date is saved to Submission, Notice advising  'Modification Saved' is displayed");
					break;
				}
				if (j == msgs.size()) {
					Reporter.log("Step 4 Result - Message not found");
				}
			}
		}
		waitForPageToLoad(bkOffice.BKOF_Submission_CompanyName());
		bkOffice.BKOF_Menu_Submission_Events().click();
		List<WebElement> eventList = driver.findElements(newBy5);
		List<WebElement> events;

		for (int i = 0; i < eventList.size(); i++) {
			events = eventList.get(i).findElements(By.tagName("tr"));

			for (int j = 0; j < eventList.size(); j++) {
				if (events.get(j).getText().contains(getDate())
						&& events.get(j).getText().contains(usrLastName + " " + usrFirstName)
						&& events.get(j).getText().contains("Update of a submission draft")) {
					Reporter.log("Step 4 Results - Event is logged in Events tab as 'Update of a submission draft'.");
					break;
				}
			}
		}
	}

	@Test(priority = 1, enabled = false)
	public void UC01_001_004() {
		ariLogin = new ARI_Login(driver);
		ariMenu = new ARI_Menu(driver);
		ariPre = new ARI_PreSubmission(driver);
		bkOffice = new EDC_BackOffice(driver);
		wait = new WebDriverWait(driver, 30);
		excel = new ExcelUtils();
		excel2 = new ExcelUtils();
		emailRead = new EWSJavaAPI();

		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(1, 0);
			bkOfPassword = excel.getCellData(1, 1);
			ariUsr = excel.getCellData(11, 0);
			ariPassword = excel.getCellData(11, 1);
			usrFirstName = excel.getCellData(1, 2);
			usrLastName = excel.getCellData(1, 3);

		} catch (Exception e) {
			e.printStackTrace();
		}
		wait = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS)
				.ignoring(StaleElementReferenceException.class);
		Reporter.log("Begin Test: UC01.001.004");

		preConUC16_005_001();
		try {
			repEle = new RepositoryElements("../EDCTest_BackOffice/config/BackOffice_Repository.properties");
			newBy = repEle.byLocator("BKOF_Risk_Tasks_TasksTable");
			newBy2 = repEle.byLocator("BKOF_Ppl&Org_InsuredList_FilterSearch_Results");
			newBy3 = repEle.byLocator("BKOF_Contact_UserTable");
			newBy4 = repEle.byLocator("BKOF_Contact_Messages");
			newBy5 = repEle.byLocator("BKOF_PplOrg_UserProfile_EventsList");
		} catch (IOException e) {
			e.printStackTrace();
		}

		bkOffice.BKOF_login(bkOfUsername, bkOfPassword);
		waitForPageToLoad(bkOffice.BKOF_SearchIcon());

		// Get User's preferred date format
		getDateFormat();

		// Search for presubmission with matching submission ID and create
		// submission
		createViewSubmission("Create");

		Reporter.log(
				"Step 1 - BO User changes Country Filter to 'All' and Selects Broker Company [Broker input by Applicant in BB-16 - Policy Administration Section (in PDF)] from 'Company name' Dropdown menu in Broker Section. ");
		Select selFilterCountry = new Select(bkOffice.BKOF_Submission_GeneralInfo_Broker_FilterPerCountry());
		selFilterCountry.selectByVisibleText("All");
		Select selCompany = new Select(bkOffice.BKOF_Submission_GeneralInfo_Broker_CompanyName());
		try {
			selCompany.selectByVisibleText(brokerName);
		} catch (NoSuchElementException e) {
			Reporter.log("Step 1 Result - Broker does not exist in list");
			Assert.fail("Step 1 Result - Broker does not exist in list");
		}

		bkOffice.BKOF_Submission_GeneralInfo_Broker_Address().getText().equals(brokerAddress);
		Select selBrokerUsr = new Select(bkOffice.BKOF_Submission_GeneralInfo_Broker_ContactName());
		try {
			if (selBrokerUsr.getFirstSelectedOption().getText().equals(brokerFName + " " + brokerLName)) {
				Reporter.log(
						"Step 1 Result - Broker User associated with broker company found and selected by default");
			} else {
				selBrokerUsr.selectByVisibleText(brokerFName + " " + brokerLName);
				Reporter.log(
						"Step 1 Result - Broker User associated with broker company found but not selected by default");
			}
		} catch (NoSuchElementException e) {
			Reporter.log("User does not exist in list");
			Assert.fail("User does not exist in list");
		}
		if (bkOffice.BKOF_Submission_ApplicantTab_NewBtn().isDisplayed()) {
			Reporter.log("Step 1 Result - A 'New' Button to add a contact appears once a Broker Company is chosen. ");
		} else {
			Reporter.log("Step 1 Result - New button not displayed. ");
		}

		if (bkOffice.BKOF_Submission_GeneralInfo_Broker_Address().getText().contains(brokerAddress)
				&& bkOffice.BKOF_Submission_GeneralInfo_Broker_Address().getText().contains(brokerCity)
				&& bkOffice.BKOF_Submission_GeneralInfo_Broker_Address().getText().contains(brokerProv)) {
			Reporter.log("Broker Address matches");
		} else {
			Reporter.log("Broker Address mismatches");
		}
		if (bkOffice.BKOF_Submission_GeneralInfo_Broker_PhoneNum().getText().equals(brokerPhone)) {
			Reporter.log("Broker Phone Number matches");
		} else {
			Reporter.log("Broker Phone Number mismatches");
		}
		if (bkOffice.BKOF_Submission_GeneralInfo_Broker_Email().getText().equals(brokerEmail)) {
			Reporter.log("Broker Email matches");
		} else {
			Reporter.log("Broker Email mismatches");
		}
		Reporter.log(
				"Step 1 Result - - Value selected is populated and all additional information associated with the registered Company is populated in additional fields, including contact if a contact is linked to the registered Broker. \n Check log for results");

		Reporter.log("Step 2 - Clicks on 'SAVE'");
		bkOffice.BKOF_Submission_SaveBtn().click();
		setDate(dateFormat + " HH:mm:ss", 0);

		List<WebElement> messageCol = driver.findElements(newBy4);
		List<WebElement> msgs;
		for (int i = 0; i < messageCol.size(); i++) {
			msgs = messageCol.get(i).findElements(By.tagName("div"));

			for (int j = 0; j < msgs.size(); j++) {
				if (msgs.get(j).getText().equals("Modification saved")) {
					Reporter.log(
							"Step 2 Results - Information added to date is saved to Submission, Notice advising  'Modification Saved' is displayed");
					break;
				}
				if (j == msgs.size()) {
					Reporter.log("Step 2 Result - Message not found");
				}
			}
		}
		waitForPageToLoad(bkOffice.BKOF_Submission_CompanyName());
		bkOffice.BKOF_Menu_Submission_Events().click();
		List<WebElement> eventList = driver.findElements(newBy5);
		List<WebElement> events;

		for (int i = 0; i < eventList.size(); i++) {
			events = eventList.get(i).findElements(By.tagName("tr"));

			for (int j = 0; j < eventList.size(); j++) {
				if (events.get(j).getText().contains(getDate())
						&& events.get(j).getText().contains(usrLastName + " " + usrFirstName)
						&& events.get(j).getText().contains("Update of a submission draft")) {
					Reporter.log("Step 2 Results - Event is logged in Events tab as 'Update of a submission draft'.");
					break;
				}
			}
		}
	}

	@Test(priority = 1, enabled = true)
	public void UC01_001_005() {
		ariLogin = new ARI_Login(driver);
		ariMenu = new ARI_Menu(driver);
		ariPre = new ARI_PreSubmission(driver);
		bkOffice = new EDC_BackOffice(driver);
		wait = new WebDriverWait(driver, 30);
		excel = new ExcelUtils();
		excel2 = new ExcelUtils();
		emailRead = new EWSJavaAPI();

		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(1, 0);
			bkOfPassword = excel.getCellData(1, 1);
			ariUsr = excel.getCellData(11, 0);
			ariPassword = excel.getCellData(11, 1);
			usrFirstName = excel.getCellData(1, 2);
			usrLastName = excel.getCellData(1, 3);

		} catch (Exception e) {
			e.printStackTrace();
		}
		wait = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS)
				.ignoring(StaleElementReferenceException.class);
		Reporter.log("Begin Test: UC01.001.005");

		preConUC16_005_001();
		try {
			repEle = new RepositoryElements("../EDCTest_BackOffice/config/BackOffice_Repository.properties");
			newBy = repEle.byLocator("BKOF_Risk_Tasks_TasksTable");
			newBy2 = repEle.byLocator("BKOF_Submission_GeneralInfo_Product_DatePicker_Days");
			newBy3 = repEle.byLocator("BKOF_Contact_Messages");
			newBy4 = repEle.byLocator("BKOF_PplOrg_UserProfile_EventsList");
		} catch (IOException e) {
			e.printStackTrace();
		}

		bkOffice.BKOF_login(bkOfUsername, bkOfPassword);
		waitForPageToLoad(bkOffice.BKOF_SearchIcon());

		// Get User's preferred date format
		getDateFormat();

		// Search for presubmission with matching submission ID and create
		// submission
		createViewSubmission("Create");

		Reporter.log("Step 1 - Chooses 'Next Renewal' of Date by clicking on radio button. ");
		bkOffice.BKOF_Submission_GeneralInfo_Product_RenewalDate().click();
		waitForPageToLoad(bkOffice.BKOF_Submission_GeneralInfo_Product_RenewalDatePickerBtn());
		if (bkOffice.BKOF_Submission_GeneralInfo_Product_RenewalDatePickerBtn().isDisplayed()) {
			Reporter.log("Step 1 Result - Dynamic Calendar appears.");
		} else {
			Reporter.log("Step 1 Result - Date picker for Renewal Date is not present.");
			Assert.fail("Step 1 Result - Date picker for Renewal Date is not present.");
		}
		bkOffice.BKOF_Submission_GeneralInfo_Product_RenewalDatePickerBtn().click();
		setDate(dateFormat + "HH:mm:ss", 30);

		try{
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("DD/MM/YYYY HH:mm:ss");
		LocalDate dtVal = LocalDate.parse(date, formatter);
		
		Select selMonth = new Select(bkOffice.BKOF_Submission_GeneralInfo_Product_DatePicker_Month());
		selMonth.selectByVisibleText(dtVal.getMonth().getDisplayName(TextStyle.SHORT, Locale.ENGLISH));
		Select selYear = new Select(bkOffice.BKOF_Submission_GeneralInfo_Product_DatePicker_Year());
		selYear.selectByVisibleText(String.valueOf(dtVal.getYear()));
		WebElement dateTable = driver.findElement(newBy2);
		List<WebElement> dates = dateTable.findElements(By.tagName("td"));

		for (WebElement ele : dates) {
			if (ele.getText().equals(String.valueOf(dtVal.getDayOfMonth()))) {
				ele.findElement(By.linkText(String.valueOf(dtVal.getDayOfMonth()))).click();
			}
		}
		
		}
		catch(Exception e)
		{
		}

		Reporter.log("Step 2 - Clicks on 'SAVE'");
		bkOffice.BKOF_Submission_SaveBtn().click();
		setDate(dateFormat + " HH:mm:ss", 0);

		List<WebElement> messageCol = driver.findElements(newBy3);
		List<WebElement> msgs;
		for (int i = 0; i < messageCol.size(); i++) {
			msgs = messageCol.get(i).findElements(By.tagName("div"));

			for (int j = 0; j < msgs.size(); j++) {
				if (msgs.get(j).getText().equals("Modification saved")) {
					Reporter.log(
							"Step 2 Results - Information added to date is saved to Submission, Notice advising  'Modification Saved' is displayed");
					break;
				}
				if (j == msgs.size()) {
					Reporter.log("Step 2 Result - Message not found");
				}
			}
		}
		waitForPageToLoad(bkOffice.BKOF_Submission_CompanyName());
		bkOffice.BKOF_Menu_Submission_Events().click();
		List<WebElement> eventList = driver.findElements(newBy4);
		List<WebElement> events;

		for (int i = 0; i < eventList.size(); i++) {
			events = eventList.get(i).findElements(By.tagName("tr"));

			for (int j = 0; j < eventList.size(); j++) {
				if (events.get(j).getText().contains(getDate())
						&& events.get(j).getText().contains(usrLastName + " " + usrFirstName)
						&& events.get(j).getText().contains("Update of a submission draft")) {
					Reporter.log("Step 2 Results - Event is logged in Events tab as 'Update of a submission draft'.");
					break;
				}
			}
		}

	}

	public void preConUC16_005_001() {

		List<String> totalSalesUS = null, totalSalesCAD = null, otherSales = null, badDebtUS = null, badDebtCAD = null,
				otherBadDebt = null, sourcesOfInfo = null, deliquentAcct = null;
		List<WebElement> subTbl = null;

		try {
			// Getting data for Financial Data tab.
			excel2.getExcelFile("./testData/PreSubmissionData.xlsx", "financial");
			String a[] = new String[] { excel2.getCellData(1, 1), excel2.getCellData(1, 2), excel2.getCellData(1, 3),
					excel2.getCellData(1, 4) };
			String b[] = new String[] { excel2.getCellData(2, 1), excel2.getCellData(2, 2), excel2.getCellData(2, 3),
					excel2.getCellData(2, 4) };
			String c[] = new String[] { excel2.getCellData(3, 1), excel2.getCellData(3, 2), excel2.getCellData(3, 3),
					excel2.getCellData(3, 4) };
			String d[] = new String[] { excel2.getCellData(4, 1), excel2.getCellData(4, 2), excel2.getCellData(4, 3),
					excel2.getCellData(4, 4) };
			String e[] = new String[] { excel2.getCellData(5, 1), excel2.getCellData(5, 2), excel2.getCellData(5, 3),
					excel2.getCellData(5, 4) };
			String f[] = new String[] { excel2.getCellData(6, 1), excel2.getCellData(6, 2), excel2.getCellData(6, 3),
					excel2.getCellData(6, 4) };

			totalSalesUS = Arrays.asList(a);
			totalSalesCAD = Arrays.asList(b);
			otherSales = Arrays.asList(c);
			badDebtUS = Arrays.asList(d);
			badDebtCAD = Arrays.asList(e);
			otherBadDebt = Arrays.asList(f);
			lossesInYr = excel2.getCellData(8, 1);

			// Getting data for Credit Management tab
			excel2.getExcelFile("./testData/PreSubmissionData.xlsx", "CreditManagement");
			receivablesPastDate = excel2.getCellData(0, 1);
			creditDecisions = excel2.getCellData(1, 1);
			sourcesOfInfo = Arrays.asList((excel2.getCellData(2, 1).split(",")));
			creditDecisionsReviewed = excel2.getCellData(3, 1);
			deliquentAcct = Arrays.asList((excel2.getCellData(4, 1).split(",")));

			// Getting data for Coverage Requested tab
			excel2.getExcelFile("./testData/PreSubmissionData.xlsx", "CoverageRequested");
			primaryProdServ = excel2.getCellData(0, 1);
			overallPercentage = excel2.getCellData(1, 1);
			typeOfBus = excel2.getCellData(2, 1);
			readilySold = excel2.getCellData(3, 1);
			foreignSales = excel2.getCellData(4, 1);
			affilatedComp = excel2.getCellData(5, 1);

			// Getting data for Country Coverage tab
			excel2.getExcelFile("./testData/PreSubmissionData.xlsx", "CountryCoverage");
			insReq = excel2.getCellData(0, 1);
			outCountry = excel2.getCellData(3, 1);
			outAnnualIns = excel2.getCellData(4, 1);
			outInsCurr = excel2.getCellData(5, 1);
			outPaymentTerms = excel2.getCellData(6, 1);
			// inCountry=excel2.getCellData(9, 1);
			inProv = excel2.getCellData(10, 1);
			inAnnualIns = excel2.getCellData(11, 1);
			inInsCurr = excel2.getCellData(12, 1);
			inPaymentTerms = excel2.getCellData(13, 1);

			// Getting data for Policy Administration
			excel2.getExcelFile("./testData/PreSubmissionData.xlsx", "PolicyAdmin");
			brokerInvolved = excel2.getCellData(0, 1);
			brokerName = excel2.getCellData(3, 1);
			brokerAddress = excel2.getCellData(4, 1);
			brokerCity = excel2.getCellData(5, 1);
			brokerProv = excel2.getCellData(6, 1);
			brokerFName = excel2.getCellData(8, 1);
			brokerLName = excel2.getCellData(9, 1);
			brokerPhone = excel2.getCellData(10, 1);
			brokerEmail = excel2.getCellData(11, 1);
			claimBenefits = excel2.getCellData(13, 1);
			receivablesIns = excel2.getCellData(14, 1);
			coverageStart = excel2.getCellData(15, 1);
			policyLang = excel2.getCellData(16, 1);
			policyCurr = excel2.getCellData(17, 1);

			// Getting data for Acceptance tab.
			excel2.getExcelFile("./testData/PreSubmissionData.xlsx", "Acceptance");
			comments = excel2.getCellData(0, 1);
			excel2.excelClose();

			repEle = new RepositoryElements("../EDC_ARI/config/ARI_PreSubmission_Repository.properties");
			newBy = repEle.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesUSRow");
			newBy2 = repEle.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesCADRow");
			newBy3 = repEle.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalSalesOtherRow");
			newBy4 = repEle.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsUSRow");
			newBy5 = repEle.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsCADRow");
			newBy6 = repEle.byLocator("ARI_PreSubmission_AppSubmission_FinancialData_TotalDebtsOtherRow");
			newBy7 = repEle.byLocator("ARI_PreSubmission_ApplicationSubmissionTable");

		} catch (Exception e1) {
			e1.printStackTrace();
		}

		wait = new WebDriverWait(driver, 30);
		ariLogin.ARI_SignIn(ariUsr, ariPassword);

		ariMenu.ARI_Menu_PolicyHolder_Action().click();
		ariMenu.ARI_Menu_PolicyHolder_Action_PreSubmission().click();
		submissionCheck();
		ariPre.ARI_PreSubmission_ApplyForSubmissionBtn().click();
		ariPre.ARI_PreSubmission_AppSubmission_ContinueBtn().click();

		// Populate Financial Data tab
		ariPre.ARI_PreSubmission_AppSubmission_FinancialDataTab().click();
		List<WebElement> financialDataTbl = driver.findElements(newBy);
		List<WebElement> res;
		for (int i = 0; i < financialDataTbl.size(); i++) {
			res = financialDataTbl.get(i).findElements(By.tagName("input"));
			for (int j = 0; j < res.size(); j++) {
				res.get(j).sendKeys(totalSalesUS.get(j));
			}
		}

		financialDataTbl = driver.findElements(newBy2);
		for (int i = 0; i < financialDataTbl.size(); i++) {
			res = financialDataTbl.get(i).findElements(By.tagName("input"));
			for (int j = 0; j < res.size(); j++) {
				res.get(j).sendKeys(totalSalesCAD.get(j));
			}
		}

		financialDataTbl = driver.findElements(newBy3);
		for (int i = 0; i < financialDataTbl.size(); i++) {
			res = financialDataTbl.get(i).findElements(By.tagName("input"));
			for (int j = 0; j < res.size(); j++) {
				res.get(j).sendKeys(otherSales.get(j));
			}
		}

		financialDataTbl = driver.findElements(newBy4);
		for (int i = 0; i < financialDataTbl.size(); i++) {
			res = financialDataTbl.get(i).findElements(By.tagName("input"));
			for (int j = 0; j < res.size(); j++) {
				res.get(j).sendKeys(badDebtUS.get(j));
			}
		}

		financialDataTbl = driver.findElements(newBy5);
		for (int i = 0; i < financialDataTbl.size(); i++) {
			res = financialDataTbl.get(i).findElements(By.tagName("input"));
			for (int j = 0; j < res.size(); j++) {
				res.get(j).sendKeys(badDebtCAD.get(j));
			}
		}

		financialDataTbl = driver.findElements(newBy6);
		for (int i = 0; i < financialDataTbl.size(); i++) {
			res = financialDataTbl.get(i).findElements(By.tagName("input"));
			for (int j = 0; j < res.size(); j++) {
				res.get(j).sendKeys(otherBadDebt.get(j));
			}
		}

		ariPre.ARI_PreSubmission_AppSubmission_FinancialData_LossesLastYr().sendKeys(lossesInYr);

		// Credit Management Tab
		ariPre.ARI_PreSubmission_AppSubmission_CreditManagementTab().click();
		ariPre.ARI_PreSubmission_AppSubmission_CreditManagement_PercentageOfReceivables().sendKeys(receivablesPastDate);

		Select creditDec = new Select(ariPre.ARI_PreSubmission_AppSubmission_CreditManagement_CreditDecisions());
		creditDec.selectByVisibleText(creditDecisions);

		Select sources = new Select(ariPre.ARI_PreSubmission_AppSubmission_CreditManagement_SourceInfo());
		for (String s : sourcesOfInfo) {
			try {
				sources.selectByVisibleText(s);
			} catch (NoSuchElementException e) {
				break;
			}
		}
		Select selCreditDecReviewed = new Select(
				ariPre.ARI_PreSubmission_AppSubmission_CreditManagement_FrequencyReview());
		selCreditDecReviewed.selectByVisibleText(creditDecisionsReviewed);

		Select selDeliquency = new Select(ariPre.ARI_PreSubmission_AppSubmission_CreditManagement_SelectDelinquency());
		for (String s : deliquentAcct) {
			try {
				selDeliquency.selectByVisibleText(s);
			} catch (NoSuchElementException e) {
				break;
			}
		}

		// Coverage requested Tab
		ariPre.ARI_PreSubmission_AppSubmission_CoverageRequestedTab().click();
		Select selProdServ = new Select(ariPre.ARI_PreSubmission_AppSubmission_CoverageRequested_PrimaryProduct());
		selProdServ.selectByVisibleText(primaryProdServ);
		ariPre.ARI_PreSubmission_AppSubmission_CoverageRequested_OverallBusiness().clear();
		ariPre.ARI_PreSubmission_AppSubmission_CoverageRequested_OverallBusiness().sendKeys(overallPercentage);
		Select selTypeBus = new Select(ariPre.ARI_PreSubmission_AppSubmission_CoverageRequested_TypeOfBusiness());
		selTypeBus.selectByVisibleText(typeOfBus);
		if (readilySold.contains("Yes")) {
			ariPre.ARI_PreSubmission_AppSubmission_CoverageRequested_ReadilySoldRdoBtnYes().click();
		} else {
			ariPre.ARI_PreSubmission_AppSubmission_CoverageRequested_ReadilySoldRdoBtnNo().click();
		}
		ariPre.ARI_PreSubmission_AppSubmission_CoverageRequested_PercentagedShipped().sendKeys(foreignSales);
		if (affilatedComp.contains("Yes")) {
			ariPre.ARI_PreSubmission_AppSubmission_CoverageRequested_AffiliateRdoBtnYes().click();
		} else {
			ariPre.ARI_PreSubmission_AppSubmission_CoverageRequested_AffiliateRdoBtnNo().click();
		}

		// Country Coverage tab
		ariPre.ARI_PreSubmission_AppSubmission_CountryCoverageTab().click();
		if (insReq.contains("Yes")) {
			ariPre.ARI_PreSubmission_AppSubmission_CountryCoverage_UltimateDestinationRdBtnYes().click();
		} else {
			ariPre.ARI_PreSubmission_AppSubmission_CountryCoverage_UltimateDestinationRdBtnNo().click();
		}
		ariPre.ARI_PreSubmission_AppSubmission_CountryCoverage_OutsideAddBtn().click();
		Select selOutCountry = new Select(ariPre.ARI_PreSubmission_AppSubmission_CountryCoverage_OutsideCan_Country());
		selOutCountry.selectByVisibleText(outCountry);
		ariPre.ARI_PreSubmission_AppSubmission_CountryCoverage_OutsideCan_AnticipatedInsSales().sendKeys(outAnnualIns);
		Select selOutInsCurr = new Select(
				ariPre.ARI_PreSubmission_AppSubmission_CountryCoverage_OutsideCan_AnticipatedInsSalesCurr());
		selOutInsCurr.selectByVisibleText(outInsCurr);
		Select selOutPaymentTerms = new Select(
				ariPre.ARI_PreSubmission_AppSubmission_CountryCoverage_OutsideCan_PaymentTerms());
		selOutPaymentTerms.selectByVisibleText(outPaymentTerms);

		ariPre.ARI_PreSubmission_AppSubmission_CountryCoverage_WithinAddBtn().click();
		Select selInProv = new Select(ariPre.ARI_PreSubmission_AppSubmission_CountryCoverage_WithinCan_Province());
		selInProv.selectByVisibleText(inProv);
		ariPre.ARI_PreSubmission_AppSubmission_CountryCoverage_WithinCan_AnticipatedInsSales().sendKeys(inAnnualIns);
		Select selInInsCurr = new Select(
				ariPre.ARI_PreSubmission_AppSubmission_CountryCoverage_WithinCan_AnticipatedInsSalesCurr());
		selInInsCurr.selectByVisibleText(inInsCurr);
		Select selInPaymentTerms = new Select(
				ariPre.ARI_PreSubmission_AppSubmission_CountryCoverage_WithinCan_PaymentTerms());
		selInPaymentTerms.selectByVisibleText(inPaymentTerms);

		// Policy Administration
		ariPre.ARI_PreSubmission_AppSubmission_PolicyAdministrationTab().click();
		if (brokerInvolved.contains("Yes")) {
			ariPre.ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerRdBtnYes().click();
			ariPre.ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerName().sendKeys(brokerName);
			ariPre.ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerAddress().sendKeys(brokerAddress);
			ariPre.ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerCity().sendKeys(brokerCity);
			Select selBrkProv = new Select(
					ariPre.ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerProvince());
			selBrkProv.selectByVisibleText(brokerProv);
			ariPre.ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerFirstName().sendKeys(brokerFName);
			ariPre.ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerLastName().sendKeys(brokerLName);
			ariPre.ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerWorkPhone().sendKeys(brokerPhone);
			ariPre.ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerEmail().sendKeys(brokerEmail);
		} else {
			ariPre.ARI_PreSubmission_AppSubmission_PolicyAdministration_BrokerRdBtnNo().click();
		}

		if (claimBenefits.contains("Yes")) {
			ariPre.ARI_PreSubmission_AppSubmission_PolicyAdministration_PolicyBeneficiaryRdBtnYes().click();
		} else {
			ariPre.ARI_PreSubmission_AppSubmission_PolicyAdministration_PolicyBeneficiaryRdBtnNo().click();
		}

		if (receivablesIns.contains("Yes")) {
			ariPre.ARI_PreSubmission_AppSubmission_PolicyAdministration_PolicyInsuredRdBtnYes().click();
		} else {
			ariPre.ARI_PreSubmission_AppSubmission_PolicyAdministration_PolicyInsuredRdBtnNo().click();
		}

		ariPre.ARI_PreSubmission_AppSubmission_PolicyAdministration_StartDate().sendKeys(coverageStart);
		Select selPolLang = new Select(ariPre.ARI_PreSubmission_AppSubmission_PolicyAdministration_PolicyLanguage());
		selPolLang.selectByVisibleText(policyLang);
		Select selPolCurr = new Select(ariPre.ARI_PreSubmission_AppSubmission_PolicyAdministration_PolicyCurrency());
		selPolCurr.selectByVisibleText(policyCurr);

		// Acceptance tab
		ariPre.ARI_PreSubmission_AppSubmission_AcceptanceTab().click();
		ariPre.ARI_PreSubmission_AppSubmission_Acceptance_Comment().sendKeys(comments);

		// Submit
		ariPre.ARI_PreSubmission_AppSubmission_Acceptance_IAgreeChkbx().click();
		ariPre.ARI_PreSubmission_AppSubmission_Acceptance_SubmitBtn().click();

		ariMenu.ARI_Menu_PolicyHolder_Action().click();
		ariMenu.ARI_Menu_PolicyHolder_Action_PreSubmission().click();

		subTbl = driver.findElements(newBy7);
		setDate("dd/MM/YYYY", 0);

		for (int i = 0; i < subTbl.size(); i++) {
			res = subTbl.get(i).findElements(By.tagName("tr"));
			for (int j = 0; j < res.size(); j++) {
				if (res.get(j).getText().contains("Submitted") && res.get(j).getText().contains(getDate())) {
					res = res.get(j).findElements(By.tagName("td"));
					for (int k = 0; k < res.size(); k++) {
						try {
							@SuppressWarnings("unused")
							int d = Integer.parseInt(res.get(k).getText());
							subID = res.get(k).getText();
							break;
						} catch (NumberFormatException e) {
						}
					}
				}
			}
		}
		ariMenu.ARI_Menu_PolicyHolder_Exit().click();

	}

	private void submissionCheck() {
		if (!ariPre.ARI_PreSubmission_ApplyForSubmissionBtn().isEnabled()) {
			Reporter.log(
					"'Apply For Submission' button is disabled. Check presubmission list for submissions that are in statuses other than 'Declined' or 'Cancelled'.");
			Assert.fail(
					"'Apply For Submission' button is disabled. Check presubmission list for submissions that are in statuses other than 'Declined' or 'Cancelled'.");
		}
	}

	private void createViewSubmission(String action) {

		if (action == "Create") {
			bkOffice.BKOF_MainMenu().click();
			bkOffice.BKOF_MainMenu_Risk().click();
			bkOffice.BKOF_MainMenu_Commerical_Workflow().click();
			bkOffice.BKOF_MainMenu_Commerical_Workflow_UnassignedTasks().click();
			bkOffice.BKOF_Risk_TaskMenu_Presubmission().click();

			waitForPageToLoad(driver.findElement(newBy));
			List<WebElement> taskTable = driver.findElements(newBy);
			List<WebElement> tasks;
			for (int i = 0; i < taskTable.size(); i++) {
				tasks = taskTable.get(i).findElements(By.tagName("tr"));
				for (int j = 0; j < tasks.size(); j++) {
					if (tasks.get(j).getText().contains(subID)) {
						tasks.get(j).click();
					}
				}
			}

			if (bkOffice.BKOF_Task_New_Assignee().getText().equals(bkOfUsername) == false) {
				bkOffice.BKOF_Task_New_Assignee().clear();
				bkOffice.BKOF_Task_New_Assignee().sendKeys(bkOfUsername);
				bkOffice.BKOF_Task_AutoComplete().click();
				bkOffice.BKOF_Task_New_Assignee_SubmitBtn().click();
			} else {
				Assert.fail("New Assignee field missing!");
			}

			bkOffice.BKOF_MainMenu_Commerical_Workflow().click();
			bkOffice.BKOF_MainMenu_Commerical_Workflow_MyTasks().click();
			bkOffice.BKOF_Risk_TaskMenu_Presubmission().click();
			taskTable = driver.findElements(newBy);
			for (int i = 0; i < taskTable.size(); i++) {
				tasks = taskTable.get(i).findElements(By.tagName("tr"));
				for (int j = 0; j < tasks.size(); j++) {
					if (tasks.get(j).getText().contains(subID)) {
						tasks.get(j).click();
					}
				}
			}
			bkOffice.BKOF_Task_CreateSubmission().click();
			bkOffice.BKOF_Task_CreateSubmission_SubmitBtn().click();

			if (bkOffice.BKOF_Submission_Status().getText().contains("Draft")) {
				Reporter.log("On submission with draft status");
			} else {
				Assert.fail("Incorrect status on submission.");
			}
		} else if (action == "View") {
			bkOffice.BKOF_MainMenu().click();
			bkOffice.BKOF_MainMenu_Risk().click();
			bkOffice.BKOF_MainMenu_Commerical_Workflow().click();
			bkOffice.BKOF_MainMenu_Commerical_Workflow_MyTasks().click();
			List<WebElement> taskTable = driver.findElements(newBy);
			List<WebElement> tasks;
			sleep(5);
			bkOffice.BKOF_Risk_TaskMenu_Presubmission().click();
			taskTable = driver.findElements(newBy);
			for (int i = 0; i < taskTable.size(); i++) {
				tasks = taskTable.get(i).findElements(By.tagName("tr"));
				for (int j = 0; j < tasks.size(); j++) {
					if (tasks.get(j).getText().contains(subID)) {
						tasks.get(j).click();
					}
				}
			}
			bkOffice.BKOF_Task_ViewSubmissionBtn().click();
		}
	}

	@AfterMethod
	private void cancelSubmission() {
		int attempt = 0;

		while (attempt < 2) {
			try {
				bkOffice.BKOF_Submission_CancelBtn().click();
				Select selReasons = new Select(bkOffice.BKOF_Task_DeclinePreSubmissionPopup_ReasonSelect());
				selReasons.selectByVisibleText("No longer required");
				bkOffice.BKOF_Task_DeclinePreSubmissionPopup_Comment().sendKeys("Canceling submission for next test");
				bkOffice.BKOF_Submission_Cancel_CnfmSubmitBtn().click();
				break;
			} catch (NoSuchElementException e) {
				// Attempts to navigate to submission page if submission was
				// already created to cancel submission
				createViewSubmission("View");
				attempt++;
			}
		}
	}

	private void getDateFormat() {
		// Get User's preferred date format
		bkOffice.BKOF_MyProfileLink().click();
		Select profileDateFormat = new Select(bkOffice.BKOF_MyProfile_IDUsrAccessTab_DateFormat());
		dateFormat = profileDateFormat.getFirstSelectedOption().getText();
	}

	private void setDate(String dateFormat, int addDays) {
		System.out.println(dateFormat);
		date = dateFormat.replace("DD", "dd");
		SimpleDateFormat format = new SimpleDateFormat(date);
		Calendar c = Calendar.getInstance();
		if (addDays != 0) {
			c.add(Calendar.DATE, addDays);
		}
		date = format.format(c.getTime());
	}

	private String getDate() {
		return date;
	}

	private void waitForPageToLoad(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	private void presenceOfElement(By eleBy) {
		wait.until(ExpectedConditions.presenceOfElementLocated(eleBy));
	}

	public boolean retryingFindClick(By by) {
		boolean result = false;
		int attempts = 0;
		while (attempts < 10) {
			try {
				driver.findElement(by).click();
				result = true;
				break;
			} catch (StaleElementReferenceException e) {
			}
			attempts++;
		}
		return result;
	}

	private void sleep(int slp) {
		try {
			Thread.sleep(slp);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
