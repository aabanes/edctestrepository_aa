package cloudpi.testARI.utility;

import microsoft.exchange.webservices.data.autodiscover.IAutodiscoverRedirectionUrl;

public class RedirectionUrlCallback implements IAutodiscoverRedirectionUrl {
    public boolean autodiscoverRedirectionUrlValidationCallback(
            String redirectionUrl) {
        return (redirectionUrl == "https://mail.edc.ca/autodiscover/autodiscover.xml");
    }
}