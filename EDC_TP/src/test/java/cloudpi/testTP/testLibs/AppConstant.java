package cloudpi.testTP.testLibs;


public class AppConstant {
	
	//This is the list of System Variables
	
	public static String BaseURL = "http://www.XXXXX.ca";
	public static final String FF = "Firefox";
	public static final String IE = "InternetExplore";
	public static final String CR = "InternetExplore";
	public static String projectPath = "C:\\Cloudpipe";
	public static int count = 0;
	public static int testcasesNumbers = 0;
	public static String homePageNameEDC = "Export Development Canada (EDC)";
	public static String exportingPageNameEDC = "Getting Started - Export Development Canada (EDC)";
	public static String solutionPageNameEDC = "Our Solutions - Export Development Canada (EDC)";
	public static String countryPageNameEDC = "Country Info - Export Development Canada (EDC)";
	public static String knowledgePageNameEDC = "Knowledge Centre - Export Development Canada (EDC)";
	public static String eventsPageNameEDC = "Upcoming Events";
	public static String aboutUsPageNameEDC = "About us - Export Development Canada (EDC)";
			
}
