package cloudpi.testTP.testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import cloudpi.testbkof.pages.EDC_BackOffice;
import cloudpi.testTP.pages.RepositoryElements;
import cloudpi.testTP.pages.TP_ApplyForApprovalPage;
import cloudpi.testTP.pages.TP_HomePage;
import cloudpi.testTP.pages.TP_LandingPage;
import cloudpi.testTP.pages.TP_Menu;
import cloudpi.testTP.pages.TP_PortfolioPage;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class TP_REG extends cloudpi.testTP.testLibs.BaseTest {

	//Testing changes and stuff
	private TP_LandingPage tpLand = null;
	private TP_Menu tpMenu = null;
	private TP_HomePage tpHome = null;
	private TP_ApplyForApprovalPage tpApproval = null;
	private TP_PortfolioPage tpPortfolio = null;
	private EDC_BackOffice bkOffice = null;
	private RepositoryElements repEle, repEle2, repEle3, repEle4 = null;
	private By newBy, newBy2, newBy3, newBy4 = null;
	private String policyNum, grossExposure, totalExposure, date, newDate, effectiveDate = "";
	private WebDriverWait wait;

	@Test
	public void TP_REG8_PH_requests_new_limit() {

		/*
		 * TP REG_8 requests new limit with Under Review (PH says yes to
		 * negative buyer info) and full approval Data setup: Buyer that exists
		 * in DNB Pre Setup: Take note of the total exposure before starting
		 * test
		 */

		tpLand = new TP_LandingPage(driver);
		tpMenu = new TP_Menu(driver);
		tpHome = new TP_HomePage(driver);
		tpApproval = new TP_ApplyForApprovalPage(driver);
		tpPortfolio = new TP_PortfolioPage(driver);
		bkOffice = new EDC_BackOffice(driver);
		wait = new WebDriverWait(driver, 30);

		try {
			repEle = new RepositoryElements("../EDC_TP/config/TP_LandingPage_Repository.properties");
			repEle2 = new RepositoryElements("../EDC_TP/config/TP_ApplyForApproval_Repository.properties");
			repEle3 = new RepositoryElements("../EDC_TP/config/TP_ViewMyPortfolio_Repository.properties");
			repEle4 = new RepositoryElements("../EDCTest_BackOffice/config/BackOffice_Repository.properties");
			newBy = repEle2.byLocator("TP_ApplyForApproval_stp1_SearchPane");
			newBy2 = repEle3.byLocator("TP_Portfolio_table");
			newBy3 = repEle4.byLocator("BKOF_Task_Table");
			newBy4 = repEle4.byLocator("BKOF_Decision_Area");
		} catch (IOException e) {
			e.printStackTrace();
		}

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		Reporter.log("Beginning test case TP_REG8_PH_requests_new_limit");
		Reporter.log("Step 1 - Logging into an existing TP account by entering Email and Password");

		TP_login("Stephen.Sum.ExternalConsultant@edc.ca", "1Test123");

//		Reporter.log("Step 1 - Expected Result - PH logged into a TP Account");
//		Reporter.log("Step 2 - On the Home screen, click on Apply button.");
//		tpHome.TP_Home_ApplyBtn().click();
//		if (verify(tpApproval.TP_ApplyForApproval_Step1Active().isDisplayed(), true)) {
//			Reporter.log("Step 2 - Your Customer screen is displayed.");
//		} else {
//			Assert.fail("Cannot find Step Active element");
//			Reporter.log("Step 2 - Step 1 Active element not found");
//		}
//
//		Reporter.log(
//				"Step 3 - On the Your Customer screen, search the buyer either by DUNS Number or Company Name with Country field.");
//		// tpApproval.TP_ApplyForApproval_stp1_dnsNumberTextfield().sendKeys("1306992");
//		tpApproval.TP_ApplyForApproval_stp1_nameTextfield().sendKeys("Wey");
//		// Put code in to check if Country is equal to UNITED STATES in file. If
//		// so, populate state field.
//		Select country = new Select(tpApproval.TP_ApplyForApproval_stp1_countryDropDown());
//		country.selectByVisibleText("UNITED STATES");
//		Select state = new Select(tpApproval.TP_ApplyForApproval_stp1_stateDropDown());
//		state.selectByVisibleText("WASHINGTON");
//		tpApproval.TP_ApplyForApproval_stp1_SearchButton().click();
//		waitForPageToLoad(tpApproval.TP_ApplyForApproval_stp1_SearchPane());
//
//		List<WebElement> resultsPaneEle = driver.findElements(newBy);
//		if (resultsPaneEle.size() == 0) {
//			Assert.fail("Step 3 - No search results returned");
//			Reporter.log("Step 3 - No search results returned.");
//		}
//
//		Reporter.log("Step 3 - System displays all the search results.");
//		Reporter.log("Step 4 - PH selects the buyer from the Search Results. ");
//		for (int i = 0; i < resultsPaneEle.size(); i++) {
//			if (resultsPaneEle.get(i).findElement(By.tagName("h3")).getText().equals("WEYERHAEUSER COMPANY")) {
//				resultsPaneEle.get(i).findElement(By.tagName("h3")).click();
//				break;
//			}
//		}
//
//		if (verify(tpApproval.TP_ApplyForApproval_Step2Active().isDisplayed(), true)) {
//			Reporter.log(
//					"Step 4 - Credit Limit Details screen is opened and the Buyer information is listed under Your Customer session.");
//			Reporter.log(
//					"Step 5 - On the Credit Limit Details screen, popoulate all the mandatory fields, and then clicks on NEXT button.");
//			tpApproval.TP_ApplyForApproval_stp2_creditLimitTextField().sendKeys("5000");
//			tpApproval.TP_ApplyForApproval_stp2_coveragePeriod90().click();
//			tpApproval.TP_ApplyForApproval_stp2_paymentTerms90().click();
//			tpApproval.TP_ApplyForApproval_stp2_nextButton().click();
//		} else {
//			Assert.fail("Step 4 - Company not found");
//			Reporter.log("Step 4 - Company not found");
//		}
//
//		if (verify(tpApproval.TP_ApplyForApproval_Step3Active().isDisplayed(), true)) {
//			Reporter.log("Step 5 - Your Application Details screen is displayed.");
//			Reporter.log(
//					"Step 6 - On the Application Details screen, answer all the questions,  ensure to answer yes for the question pertaining for negative information and then clicks on SUBMIT Button.");
//			tpApproval.TP_ApplyForApproval_stp3_goodsExportedYes().click();
//			tpApproval.TP_ApplyForApproval_stp3_negativeInfoYes().click();
//			tpApproval.TP_ApplyForApproval_stp3_negativeInfoTextarea().sendKeys("this is test text.");
//			tpApproval.TP_ApplyForApproval_stp3_sellingCustNo().click();
//			tpApproval.TP_ApplyForApproval_stp3_agreement().click();
//			tpApproval.TP_ApplyForApproval_stp3_submitButton().click();
//		} else {
//			Assert.fail("Step 5 - Not at correct page");
//			Reporter.log("Step 5 - Not at correct page");
//		}
//
//		if (verify(tpApproval.TP_ApplyForApproval_osOffer_header().isDisplayed(), true)) {
//			Reporter.log("Step 6 - The Application was submitted and the request falls under review.");
//		} else {
//			Assert.fail("Step 6 - Not at correct page");
//			Reporter.log("Step 6 - Not at correct page");
//		}
//
//		tpMenu.TP_Menu_ViewPortfolioButton().click();
//		tpPortfolio.TP_PortfolioNav_UnderReviewLabel().click();
//
//		List<WebElement> portfolioTableEle = driver.findElements(newBy2);
//		for (int i = 0; i < portfolioTableEle.size(); i++) {
//			if (portfolioTableEle.get(i).findElement(By.tagName("h3")).getText().contains("WEYERHAEUSER COMPANY")) {
//				String res = portfolioTableEle.get(i).findElement(By.tagName("h3")).getText();
//				policyNum = res.substring(res.indexOf("(") + 1, res.lastIndexOf(")"));
//				break;
//			}
//		}
//
//		if (policyNum == "" || policyNum == null) {
//			Assert.fail("Policy Number not found.");
//			Reporter.log("Policy Number not found");
//		}
//
//		Reporter.log("Step 7 - Login to the back office and navigate to limit request.");
//
//		BKOF_login("B.Grieve", "1Test123");
//		Reporter.log("Step 8 - Approve full requested amount.");
//		System.out.println(policyNum);
//		bkOffice.BKOF_Lookup().sendKeys(policyNum);
//		bkOffice.BKOF_Lookup().sendKeys(Keys.RETURN);
//		waitForPageToLoad(bkOffice.BKOF_Gross_Exposure());
//		grossExposure = addZeros(bkOffice.BKOF_Gross_Exposure().getText().replace("K", "").trim());
//		totalExposure = addZeros(bkOffice.BKOF_Total_Exposure().getText().replace("K", "").trim());
//
//		bkOffice.BKOF_Menu_Tasks().click();
//		waitForPageToLoad(bkOffice.BKOF_Task_Table());
//
//		List<WebElement> resultsPaneEle2 = driver.findElements(newBy3);
//		for (int i = 0; i < resultsPaneEle2.size(); i++) {
//			if (resultsPaneEle2.get(i).findElement(By.tagName("td")).getText().contains("Application")) {
//				resultsPaneEle2.get(i).findElement(By.tagName("td")).click();
//			}
//			if (bkOffice.BKOF_Demand_ReqAmt().getText() == "5 KUSD" && bkOffice.BKOF_Demand_Notes().isDisplayed()) {
//				break;
//			}
//			try {
//				sleep(3);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//
//		System.out.println(bkOffice.BKOF_Task_New_Assignee().getText());
//
//		if (bkOffice.BKOF_Task_New_Assignee().getText().equals("B.Grieve") == false) {
//			bkOffice.BKOF_Task_New_Assignee().clear();
//			bkOffice.BKOF_Task_New_Assignee().sendKeys("B.Grieve");
//			bkOffice.BKOF_Task_AutoComplete().click();
//			bkOffice.BKOF_Task_New_Assignee_SubmitBtn().click();
//		} else {
//			Assert.fail("New Assignee field missing!");
//		}
//
//		waitForPageToLoad(bkOffice.BKOF_Decision_Accept());
//		bkOffice.BKOF_Decision_Accept().click();
//		bkOffice.BKOF_Decision_SubmitBtn().click();
//		bkOffice.BKOF_Decision_CfrmPopup_CfrmSubmitBtn().click();
//		softVerify(totalExposure, bkOffice.BKOF_Total_Exposure().getText().replace("K", "").trim(),
//				"Total Exposure is not set to expected result. Total Exposure: "
//						+ bkOffice.BKOF_Total_Exposure().getText().replace("K", "").trim()
//						+ "\t Expected Total Exposure: " + totalExposure);
//		// Need to work on validating email functionality
//		Reporter.log(
//				"Step 8 - The Application was submitted and PH receives an offer which is  in Outstanding status. PH receives New EDC Approval email.");
//		driver.navigate().to("https://cis-qac.edc.ca/tp/main.do?body=login");
//		//TP_login("Stephen.Sum.ExternalConsultant@edc.ca", "1Test123");
//		tpMenu.TP_Menu_ViewPortfolioButton().click();
//
//		tpPortfolio.TP_PortfolioNav_OutstandingLabel().click();
//		
//		portfolioTableEle = driver.findElements(newBy2);
//		for (int i = 0; i < portfolioTableEle.size(); i++) {
//			if (portfolioTableEle.get(i).findElement(By.tagName("h3")).getText().contains("WEYERHAEUSER COMPANY")) {
//				portfolioTableEle.get(i).findElement(By.tagName("h3")).click();
//				break;
//			}
//		}
//
//		softVerify(tpApproval.TP_ApplyForApproval_osOffer_eDCApprvOffer_decision().getText(), "Full Approval",
//				"Decision is not set to expected result. Decision: "
//						+ tpApproval.TP_ApplyForApproval_osOffer_eDCApprvOffer_decision().getText()
//						+ "\t Expected Decision: Full Approval");
//		softVerify(tpApproval.TP_ApplyForApproval_osOffer_eDCApprvOffer_creditLimit().getText(), "USD 5,000.00",
//				"Credit Limit is not set to expected result. Credit Limit: "
//						+ tpApproval.TP_ApplyForApproval_osOffer_eDCApprvOffer_creditLimit().getText()
//						+ "\t Expected Credit Limit: USD 5,000.00");
//		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
//		Calendar c = Calendar.getInstance();
//		date = format.format(c.getTime());
//		if (tpApproval.TP_ApplyForApproval_osOffer_eDCApprvReq_coveragePeriod().getText().equals("90 days")) {
//			c.add(Calendar.DATE, 90);
//			newDate = format.format(c.getTime());
//		} else if (tpApproval.TP_ApplyForApproval_osOffer_eDCApprvReq_coveragePeriod().getText().equals("180 days")) {
//			c.add(Calendar.DATE, 180);
//			newDate = format.format(c.getTime());
//		}
//
//		c.add(Calendar.DATE, 15);
//		effectiveDate = format.format(c.getTime());
//		softVerify(tpApproval.TP_ApplyForApproval_osOffer_eDCApprvReq_coveragePeriod().getText(),
//				date + " to " + newDate,
//				"Coverage period does not reflect expected coverage date. Coverage Period: "
//						+ tpApproval.TP_ApplyForApproval_osOffer_eDCApprvReq_coveragePeriod().getText()
//						+ "\t Expected Coverage Period: " + date + " to " + newDate);
//		softVerify(tpApproval.TP_ApplyForApproval_osOffer_policyProfile_policyEffectiveDate().getText(), effectiveDate,
//				"Effective Policy Date does not reflect expected Effective Policy Date date. Effective Policy Date: "
//						+ tpApproval.TP_ApplyForApproval_osOffer_policyProfile_policyEffectiveDate().getText()
//						+ "\t Effective Policy Date: " + effectiveDate);
//		softVerify(tpApproval.TP_ApplyForApproval_osOffer_eDCApprvReq_requestedCreditLimit().getText(), "5,000.00",
//				"Gross Exposure does not reflect new value. Current Gross Exposure: "
//						+ tpApproval.TP_ApplyForApproval_osOffer_eDCApprvReq_requestedCreditLimit().getText() + "\t Expected Credit Limit: 5,000.00");
//		Reporter.log("Step 8 - Oustanding Offer validated. See report for results");
//
//		tpApproval.TP_ApplyForApproval_osOffer_agreement().click();
//		tpApproval.TP_ApplyForApproval_osOffer_acceptPayButton().click();
//
//		if (verify(tpApproval.TP_ApplyForApproval_osOffer_CCheader().getText().equals("Outstanding Offer Payment"),
//				true)) {
//			driver.switchTo().frame("frameMoneris");
//			tpApproval.TP_ApplyForApproval_osOffer_cardholder().sendKeys("Amor Abanes - Test");
//			tpApproval.TP_ApplyForApproval_osOffer_creditCardNum().sendKeys("4545454545454545");
//			tpApproval.TP_ApplyForApproval_osOffer_expiryDate().sendKeys("01/19");
//			tpApproval.TP_ApplyForApproval_osOffer_processTransButton().click();
//		} else {
//			Assert.fail("Not at Outstanding Offer Payment page!");
//		}
//		driver.switchTo().defaultContent();
//
//		Reporter.log(
//				"Step 15 - Navigate to the Buyer Profile in the Back Office. Ensure the total expsoure correctly reflects the new credit limit in USD");
//		BKOF_login("B.Grieve", "1Test123");
//		bkOffice.BKOF_Lookup().sendKeys(policyNum);
//		bkOffice.BKOF_Lookup().sendKeys(Keys.RETURN);
//		waitForPageToLoad(bkOffice.BKOF_Gross_Exposure());
//		softVerify(grossExposure, bkOffice.BKOF_Gross_Exposure().getText().replace("K", "").trim(),
//				"Gross Exposure is not set to expected result. Gross Exposure: "
//						+ bkOffice.BKOF_Gross_Exposure().getText().replace("K", "").trim()
//						+ "\t Expected Gross Exposure: " + grossExposure);
	}

	private void TP_login(String username, String password) {
		driver.navigate().to("https://cis-qac.edc.ca/tp/main.do?body=login");

		boolean elementPresent = false;
		try {
			tpLand.TP_Landing_Email().isDisplayed();
			elementPresent = true;

		} catch (NoSuchElementException e) {
			elementPresent = false;
		}

		if (elementPresent == true) {
			tpLand.TP_Landing_Email().sendKeys(username);
			tpLand.TP_Landing_Password().sendKeys(password);
			tpLand.TP_Landing_LoginBtn().click();

			if (verify(tpMenu.TP_MenuNav().isDisplayed(), true)) {
			}
			else{
				Assert.fail("Unable to log in");
				Reporter.log("Unsuccessfully logged in - Menu not found");
			}
		}

	}

	private void BKOF_login(String username, String password) {
		try {
			driver.navigate().to("https://cis-qac.edc.ca/grams/do");
			bkOffice.BKOF_Login().sendKeys(username);
			bkOffice.BKOF_Password().sendKeys(password);
			bkOffice.BKOF_LoginBtn().click();
		} catch (NoSuchElementException e) {
			Reporter.log(e.getMessage());
		}
	}

	private void waitForPageToLoad(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	private String addZeros(String val)
	{
		DecimalFormat formatter = new DecimalFormat("#,###,###.##");
		String res = formatter.format(String.format("%05x", val));
		return res;
	}
	

}
