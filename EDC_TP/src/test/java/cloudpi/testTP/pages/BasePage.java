package cloudpi.testTP.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import cloudpi.testTP.pages.RepositoryElements;
import cloudpi.testTP.pages.BasePage;

import java.io.IOException;

public abstract class BasePage {

	protected  WebDriver driver;
	protected static RepositoryElements repoElements = null;
	public  static  int need = 0;
	private static Logger log = Logger.getLogger(BasePage.class.getName());

	public BasePage(WebDriver driver)  {
		 this.driver = driver;
		if (need < 1 ) {
				//repoElements = new RepositoryElements("./config/Repository.properties");
//				System.out.println(" Repository load done ");
				log.info(" Repository load done ");
				need++ ;
				
		}
	}

}
