package cloudpi.testTP.pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TP_SignUpPage extends cloudpi.testTP.pages.MainPage{
	private RepositoryElements repoSignUpEle = null;
	public TP_SignUpPage(WebDriver driver) {
		super(driver);
		try {
			repoSignUpEle = new RepositoryElements("../EDC_TP/config/TP_SignUp_Repository.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static WebElement element = null;

	//## TP_SignUp - Steps 1-5 - Elements to verify that steps are active/inactive ##
	public WebElement TP_SignUp_Step1Active(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_Step1Active"));
		return element;
	}
	
	public WebElement TP_SignUp_Step2Active(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_Step2Active"));
		return element;
	}
	
	public WebElement TP_SignUp_Step3Active(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_Step3Active"));
		return element;
	}
	
	public WebElement TP_SignUp_Step4Active(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_Step4Active"));
		return element;
	}
	
	public WebElement TP_SignUp_stp1_CompanyName(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp1_CompanyName"));
		return element;
	}
	
	public WebElement TP_SignUp_stp1_BusinessNum(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp1_BusinessNum"));
		return element;
	}
	
	public WebElement TP_SignUp_stp1_Address(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp1_Address"));
		return element;
	}
	
	public WebElement TP_SignUp_stp1_City(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp1_City"));
		return element;
	}
	
	public WebElement TP_SignUp_stp1_PostalCode(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp1_PostalCode"));
		return element;
	}
	
	
	public WebElement TP_SignUp_stp1_Province(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp1_Province"));
		return element;
	}
	
	public WebElement TP_SignUp_stp1_PhoneNum(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp1_PhoneNum"));
		return element;
	}
	
	public WebElement TP_SignUp_stp1_resultsPane(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp1_resultsPane"));
		return element;
	}
	
	public WebElement TP_SignUp_stp1_manualIdentify(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp1_manualIdentify"));
		return element;
	}
	
	public WebElement TP_SignUp_stp1_additionalInfo(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp1_additionalInfo"));
		return element;
	}
	
	
	
	//Step 2
	public WebElement TP_SignUp_stp2_IndustrySector(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp2_IndustrySector"));
		return element;
	}
	
	public WebElement TP_SignUp_stp2_fiscalSalesTextfield(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp2_fiscalSalesTextfield"));
		return element;
	}
	
	public WebElement TP_SignUp_stp2_fiscalSalesType(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp2_fiscalSalesType"));
		return element;
	}
	
	public WebElement TP_SignUp_stp2_fiscalYearEnd(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp2_fiscalYearEnd"));
		return element;
	}
	
	public WebElement TP_SignUp_stp2_policyCurr(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp2_policyCurr"));
		return element;
	}
	
	public WebElement TP_SignUp_stp2_policyLang(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp2_policyLang"));
		return element;
	}
	
	//Step 3
	public WebElement TP_SignUp_stp3_Title(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp3_Title"));
		return element;
	}
	public WebElement TP_SignUp_stp3_FirstName(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp3_FirstName"));
		return element;
	}
	public WebElement TP_SignUp_stp3_LastName(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp3_LastName"));
		return element;
	}
	public WebElement TP_SignUp_stp3_JobTitle(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp3_JobTitle"));
		return element;
	}
	public WebElement TP_SignUp_stp3_Department(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp3_Department"));
		return element;
	}
	public WebElement TP_SignUp_stp3_PhoneNum(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp3_PhoneNum"));
		return element;
	}
	public WebElement TP_SignUp_stp3_FaxNum(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp3_FaxNum"));
		return element;
	}
	public WebElement TP_SignUp_stp3_Email(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp3_Email"));
		return element;
	}
	public WebElement TP_SignUp_stp3_CnFmEmail(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp3_CnFmEmail"));
		return element;
	}
	public WebElement TP_SignUp_stp3_Language(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp3_Language"));
		return element;
	}
	
	public WebElement TP_SignUp_stp3_DateFormat(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_stp3_DateFormat"));
		return element;
	}
	
	public WebElement TP_SignUp_Submit(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_Submit"));
		return element;
	}
	
	public WebElement TP_SignUp_Back(){
		element = driver.findElement(repoSignUpEle.byLocator("TP_SignUp_Back"));
		return element;
	}
	
}
