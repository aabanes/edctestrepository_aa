package cloudpi.testTP.pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import cloudpi.testTP.pages.RepositoryElements;

public class TP_Menu extends cloudpi.testTP.pages.MainPage{
	private RepositoryElements repoMenuElements = null;
	public TP_Menu(WebDriver driver) {
		super(driver);
		try {
			repoMenuElements = new RepositoryElements("../EDC_TP/config/TP_Menu_Repository.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static WebElement element = null;
	
	public WebElement TP_MenuNav(){
		element = driver.findElement(repoMenuElements.byLocator("TP_Menu_Nav"));
		return element;
	}
	
	public WebElement TP_Menu_HomeButton(){
		element = driver.findElement(repoMenuElements.byLocator("TP_Menu_HomeButton"));
		return element;
	}
	
	public WebElement TP_Menu_ApplyForApprovalButton(){
		element = driver.findElement(repoMenuElements.byLocator("TP_Menu_ApplyForApprovalButton"));
		return element;
	}
	
	public WebElement TP_Menu_ViewPortfolioButton(){
		element = driver.findElement(repoMenuElements.byLocator("TP_Menu_ViewPortfolioButton"));
		return element;
	}
	
	public WebElement TP_Menu_ManagePolicyButton(){
		element = driver.findElement(repoMenuElements.byLocator("TP_Menu_ManagePolicyButton"));
		return element;
	}
	
	public WebElement TP_Menu_ManageUsersButton(){
		element = driver.findElement(repoMenuElements.byLocator("TP_Menu_ManageUsersButton"));
		return element;
	}
	
	public WebElement TP_Menu_ShowAcount(){
		element = driver.findElement(repoMenuElements.byLocator("TP_Menu_ShowAcount"));
		return element;
	}
	
	public WebElement TP_Menu_SignOut(){
		element = driver.findElement(repoMenuElements.byLocator("TP_Menu_SignOut"));
		return element;
	}
}