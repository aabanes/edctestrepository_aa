package cloudpi.testTP.pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import cloudpi.testTP.pages.RepositoryElements;

public class TP_LandingPage extends cloudpi.testTP.pages.MainPage{
	private RepositoryElements repoLandingElements = null;
	public TP_LandingPage(WebDriver driver) {
		super(driver);
		try {
			repoLandingElements = new RepositoryElements("../EDC_TP/config/TP_LandingPage_Repository.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static WebElement element = null;
	
	public WebElement TP_Landing_SignUpButton(){
		element = driver.findElement(repoLandingElements.byLocator("TP_Landing_SignUpButton"));
		return element;
	}
	
	public WebElement TP_Landing_Email(){
		element = driver.findElement(repoLandingElements.byLocator("TP_Landing_Email"));
		return element;
	}
	
	public WebElement TP_Landing_Password(){
		element = driver.findElement(repoLandingElements.byLocator("TP_Landing_Password"));
		return element;
	}
	
	public WebElement TP_Landing_LoginBtn(){
		element = driver.findElement(repoLandingElements.byLocator("TP_Landing_LoginBtn"));
		return element;
	}
	
}
