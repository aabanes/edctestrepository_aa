package cloudpi.testTP.pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import cloudpi.testTP.pages.RepositoryElements;

public class TP_HomePage extends cloudpi.testTP.pages.MainPage{
	private RepositoryElements repoHomeElements = null;
	public TP_HomePage(WebDriver driver) {
		super(driver);
		try {
			repoHomeElements = new RepositoryElements("../EDC_TP/config/TP_HomePage_Repository.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static WebElement element = null;
	
	public WebElement TP_Home_ViewMyPortfolio_ActiveVal(){
		element = driver.findElement(repoHomeElements.byLocator("TP_Home_ViewMyPortfolio_ActiveVal"));
		return element;
	}
	
	public WebElement TP_Home_ViewMyPortfolio_Active(){
		element = driver.findElement(repoHomeElements.byLocator("TP_Home_ViewMyPortfolio_Active"));
		return element;
	}
	
	public WebElement TP_Home_ViewMyPortfolio_OutstandingVal(){
		element = driver.findElement(repoHomeElements.byLocator("TP_Home_ViewMyPortfolio_OutstandingVal"));
		return element;
	}
	
	public WebElement TP_Home_ViewMyPortfolio_Outstanding(){
		element = driver.findElement(repoHomeElements.byLocator("TP_Home_ViewMyPortfolio_Outstanding"));
		return element;
	}
	
	public WebElement TP_Home_ViewMyPortfolio_UnderReviewVal(){
		element = driver.findElement(repoHomeElements.byLocator("TP_Home_ViewMyPortfolio_UnderReviewVal"));
		return element;
	}
	
	public WebElement TP_Home_ViewMyPortfolio_UnderReview(){
		element = driver.findElement(repoHomeElements.byLocator("TP_Home_ViewMyPortfolio_UnderReview"));
		return element;
	}
	
	public WebElement TP_Home_ViewMyPortfolio_ClaimsVal(){
		element = driver.findElement(repoHomeElements.byLocator("TP_Home_ViewMyPortfolio_ClaimsVal"));
		return element;
	}
	
	public WebElement TP_Home_ViewMyPortfolio_Claims(){
		element = driver.findElement(repoHomeElements.byLocator("TP_Home_ViewMyPortfolio_Claims"));
		return element;
	}
	
	public WebElement TP_Home_ApplyBtn(){
		element = driver.findElement(repoHomeElements.byLocator("TP_Home_ApplyBtn"));
		return element;
	}
	
	public WebElement TP_Home_ApplyForClaimBtn(){
		element = driver.findElement(repoHomeElements.byLocator("TP_Home_ApplyForClaimBtn"));
		return element;
	}
	
	
}
