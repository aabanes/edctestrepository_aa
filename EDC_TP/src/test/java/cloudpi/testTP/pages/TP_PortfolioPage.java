package cloudpi.testTP.pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import cloudpi.testTP.pages.RepositoryElements;

public class TP_PortfolioPage extends cloudpi.testTP.pages.MainPage{
	private RepositoryElements repoPortfolioEle = null;
	public TP_PortfolioPage(WebDriver driver) {
		super(driver);
		try {
			repoPortfolioEle = new RepositoryElements("../EDC_TP/config/TP_PortfolioPage_Repository.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static WebElement element = null;
	
	public WebElement TP_PortfolioNav_ApprovalsHeader(){
		element = driver.findElement(repoPortfolioEle.byLocator("TP_PortfolioNav_ApprovalsHeader"));
		return element;
	}
	
	public WebElement TP_PortfolioNav_ActiveLabel(){
		element = driver.findElement(repoPortfolioEle.byLocator("TP_PortfolioNav_ActiveLabel"));
		return element;
	}
	
	public WebElement TP_PortfolioNav_ActiveAmt(){
		element = driver.findElement(repoPortfolioEle.byLocator("TP_PortfolioNav_ActiveAmt"));
		return element;
	}
	
	public WebElement TP_PortfolioNavExpiredLabel(){
		element = driver.findElement(repoPortfolioEle.byLocator("TP_PortfolioNavExpiredLabel"));
		return element;
	}
	
	public WebElement TP_PortfolioNavExpiredAmt(){
		element = driver.findElement(repoPortfolioEle.byLocator("TP_PortfolioNavExpiredAmt"));
		return element;
	}
	
	public WebElement TP_PortfolioNavOffersHeader(){
		element = driver.findElement(repoPortfolioEle.byLocator("TP_PortfolioNavOffersHeader"));
		return element;
	}
	
	public WebElement TP_PortfolioNav_UnderReviewLabel(){
		element = driver.findElement(repoPortfolioEle.byLocator("TP_PortfolioNav_UnderReviewLabel"));
		return element;
	}
	
	public WebElement TP_PortfolioNav_UnderReviewAmt(){
		element = driver.findElement(repoPortfolioEle.byLocator("TP_PortfolioNav_UnderReviewAmt"));
		return element;
	}
	
	public WebElement TP_PortfolioNav_OutstandingLabel(){
		element = driver.findElement(repoPortfolioEle.byLocator("TP_PortfolioNav_OutstandingLabel"));
		return element;
	}
	
	public WebElement TP_PortfolioNav_OutstandingAmt(){
		element = driver.findElement(repoPortfolioEle.byLocator("TP_PortfolioNav_OutstandingAmt"));
		return element;
	}
	
	public WebElement TP_PortfolioNav_ClosedLabel(){
		element = driver.findElement(repoPortfolioEle.byLocator("TP_PortfolioNav_ClosedLabel"));
		return element;
	}
	
	public WebElement TP_PortfolioNav_ClosedAmt(){
		element = driver.findElement(repoPortfolioEle.byLocator("TP_PortfolioNav_ClosedAmt"));
		return element;
	}
	
	public WebElement TP_PortfolioNav_ClaimsLabel(){
		element = driver.findElement(repoPortfolioEle.byLocator("TP_PortfolioNav_ClaimsLabel"));
		return element;
	}
	
	public WebElement TP_PortfolioNav_ClaimsAmt(){
		element = driver.findElement(repoPortfolioEle.byLocator("TP_PortfolioNav_ClaimsAmt"));
		return element;
	}
	
	public WebElement TP_Portfolio_table(){
		element = driver.findElement(repoPortfolioEle.byLocator("TP_Portfolio_table"));
		return element;
	}
	
	public WebElement TP_Portfolio_export(){
		element = driver.findElement(repoPortfolioEle.byLocator("TP_Portfolio_export"));
		return element;
	}
}