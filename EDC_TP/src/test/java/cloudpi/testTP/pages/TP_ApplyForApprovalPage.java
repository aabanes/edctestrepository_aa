
package cloudpi.testTP.pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import cloudpi.testTP.pages.RepositoryElements;

public class TP_ApplyForApprovalPage extends cloudpi.testTP.pages.MainPage{
	private RepositoryElements repoApprovalElements = null;
	
	public TP_ApplyForApprovalPage(WebDriver driver) {
		super(driver);
		try {
			repoApprovalElements = new RepositoryElements("../EDC_TP/config/TP_ApplyForApproval_Repository.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static WebElement element = null;
	
	
	//TP_Apply_For_An_Approval - Steps 1-5 - Elements to verify that steps are active/inactive
	public WebElement TP_ApplyForApproval_Step1Active(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_Step1Active"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_Step2Active(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_Step2Active"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_Step3Active(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_Step3Active"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_Step4Active(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_Step4Active"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_Step5Active(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_Step5Active"));
		return element;
	}
	
	
	//TP_Apply_For_An_Approval - Step 1 Elements
	public WebElement TP_ApplyForApproval_stp1_nameTextfield(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp1_nameTextfield"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp1_addressTextfield(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp1_addressTextfield"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp1_cityTextField(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp1_cityTextField"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp1_countryDropDown(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp1_countryDropDown"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp1_stateDropDown(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp1_stateDropDown"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp1_postalCodeTextfield(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp1_postalCodeTextfield"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp1_dnsNumberTextfield(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp1_dnsNumberTextfield"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp1_SearchButton(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp1_SearchButton"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp1_SearchPane(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp1_SearchPane"));
		return element;
	}
	
	
	//TP_Apply_For_An_Approval - Step 2 Element
	public WebElement TP_ApplyForApproval_stp2_creditLimitTextField(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp2_creditLimitTextField"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp2_coveragePeriod90(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp2_coveragePeriod90"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp2_coveragePeriod180(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp2_coveragePeriod180"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp2_paymentTerms90(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp2_paymentTerms90"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp2_paymentTerms90to180(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp2_paymentTerms90to180"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp2_paymentTermsOther(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp2_paymentTermsOther"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp2_nextButton(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp2_nextButton"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp2_backButton(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp2_backButton"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp2_modifyButton(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp2_modifyButton"));
		return element;
	}
	
	
	//TP_Apply_For_An_Approval - Step 3 Elements
	public WebElement TP_ApplyForApproval_stp3_goodsExportedYes(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp3_goodsExportedYes"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp3_goodsExportedNo(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp3_goodsExportedNo"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp3_negativeInfoYes(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp3_negativeInfoYes"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp3_negativeInfoNo(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp3_negativeInfoNo"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp3_negativeInfoTextarea(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp3_negativeInfoTextarea"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp3_sellingCustYes(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp3_sellingCustYes"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp3_sellingCustNo(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp3_sellingCustNo"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp3_agreement(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp3_agreement"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_stp3_submitButton(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_stp3_submitButton"));
		return element;
	}
	
	
	//TP_Apply_For_An_Approval - Outstanding Offer Elements (all of them...hopefully)
	public WebElement TP_ApplyForApproval_osOffer_header(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_header"));
		return element;
	}	
	
	public WebElement TP_ApplyForApproval_osOffer_eDCApprvReq_dateOfRequest(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_eDCApprvReq_dateOfRequest"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_eDCApprvReq_customer(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_eDCApprvReq_customer"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_eDCApprvReq_requestedCreditLimit(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_eDCApprvReq_requestedCreditLimit"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_eDCApprvReq_coveragePeriod(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_eDCApprvReq_coveragePeriod"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_eDCApprvReq_negInfo(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_eDCApprvReq_negInfo"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_eDCApprvOffer_decision(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_eDCApprvOffer_decision"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_eDCApprvOffer_refNum(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_eDCApprvOffer_refNum"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_eDCApprvOffer_creditLimit(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_eDCApprvOffer_creditLimit"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_eDCApprvOffer_coveragePeriod(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_eDCApprvOffer_coveragePeriod"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_eDCApprvOffer_insPercentage(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_eDCApprvOffer_insPercentage"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_eDCApprvOffer_premiumAmt(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_eDCApprvOffer_premiumAmt"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_eDCApprvOffer_underwriterCom(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_eDCApprvOffer_underwriterCom"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_eDCApprvOffer_offerExpDate(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_eDCApprvOffer_offerExpDate"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_policyProfile_company(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_policyProfile_company"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_policyProfile_policyNum(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_policyProfile_policyNum"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_policyProfile_mainContactName(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_policyProfile_mainContactName"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_policyProfile_policyLang(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_policyProfile_policyLang"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_policyProfile_policyCurr(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_policyProfile_policyCurr"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_policyProfile_policyEffectiveDate(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_policyProfile_policyEffectiveDate"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_declaration(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_declaration"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_backButton(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_backButton"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_acceptPayButton(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_acceptPayButton"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_agreement(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_agreement"));
		return element;
	}
	
	
	
	
	// Outstanding Offer Payment
	public WebElement TP_ApplyForApproval_CCosOffer_mainContent(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_CCosOffer_mainContent"));
		return element;
	}	
	
	public WebElement TP_ApplyForApproval_osOffer_CCheader(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_CCosOffer_header"));
		return element;
	}	
	
	public WebElement TP_ApplyForApproval_osOffer_cardholder(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_cardholder"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_creditCardNum(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_creditCardNum"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_expiryDate(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_expiryDate"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_processTransButton(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_processTransButton"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_cancelTransButton(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_cancelTransButton"));
		return element;
	}
	
	
	//These elements appear after Outstanding Offer cancellation and Declining the Oustanding Order
	public WebElement TP_ApplyForApproval_osOffer_showMeOffer(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_showMeOffer"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_cancelMsg(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_cancelMsg"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_declineOffer(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_declineOffer"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_declineOfferInPopup(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_declineOfferInPopup"));
		return element;
	}
	
	public WebElement TP_ApplyForApproval_osOffer_declineOfferCnfm(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_declineOfferCnfm"));
		return element;
	}	
	
	public WebElement TP_ApplyForApproval_osOffer_declineMsg(){
		element = driver.findElement(repoApprovalElements.byLocator("TP_ApplyForApproval_osOffer_declineMsg"));
		return element;
	}	
	
}
