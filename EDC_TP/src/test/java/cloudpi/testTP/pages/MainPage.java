package cloudpi.testTP.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.apache.log4j.Logger;

public  class MainPage extends cloudpi.testTP.pages.BasePage  {
	
	public  static  WebElement element = null;
	
	public MainPage(WebDriver driver) {
		super(driver);
	}

	static Logger log = Logger.getLogger(MainPage.class.getName());
	
	public  String getPageTitle(){
		return driver.getTitle();
	}
}
