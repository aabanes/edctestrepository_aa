package cloudpi.testui.utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import cloudpi.testARI.utility.RedirectionUrlCallback;
import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.PropertySet;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.enumeration.property.BodyType;
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.enumeration.search.LogicalOperator;
import microsoft.exchange.webservices.data.core.enumeration.service.ConflictResolutionMode;
import microsoft.exchange.webservices.data.core.exception.service.local.ServiceLocalException;
import microsoft.exchange.webservices.data.core.exception.service.remote.ServiceResponseException;
import microsoft.exchange.webservices.data.core.service.folder.Folder;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.core.service.schema.EmailMessageSchema;
import microsoft.exchange.webservices.data.core.service.schema.ItemSchema;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.search.FindItemsResults;
import microsoft.exchange.webservices.data.search.ItemView;
import microsoft.exchange.webservices.data.search.filter.SearchFilter;
import microsoft.exchange.webservices.data.search.filter.SearchFilter.SearchFilterCollection;

public class EWSJavaAPI {

	// The following code only retrieves mail by subject. 
	// You will need to add the following dependencies to your POM file:
	
	String username, password, domain, email, search, tempPass = "";
	int viewItemNum = 0;
	FindItemsResults<Item> findResults = null;
	ExchangeService service = null;

	public String getTempPassword(String loginID, String passwrd, String sysDomain, String mail, String emailSubject) {
		return getTempPassword(loginID, passwrd, sysDomain, mail, emailSubject, 1);
	}

	public String getTempPassword(String loginID, String passwrd, String sysDomain, String mail, String emailSubject,
			int numOfItems) {
		username = loginID;
		password = passwrd;
		domain = sysDomain;
		email = mail;
		search = emailSubject;
		viewItemNum = numOfItems;

		getMailContentsFromInbox();
		tempPass();
		return tempPass;
	}
	
	public String getEmailBySubject(String loginID, String passwrd, String sysDomain, String mail, String emailSubject) {
		return getEmailBySubject(loginID, passwrd, sysDomain, mail, emailSubject, 1);
	}

	public String getEmailBySubject(String loginID, String passwrd, String sysDomain, String mail, String emailSubject,
			int numOfItems) {
		username = loginID;
		password = passwrd;
		domain = sysDomain;
		email = mail;
		search = emailSubject;
		viewItemNum = numOfItems;

		getMailContentsFromInbox();
		return emailBySubject();
	}


	private void getMailContentsFromInbox() {
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
		ExchangeCredentials credentials = new WebCredentials(username, password, domain);
		try {
			service.setCredentials(credentials);
			service.autodiscoverUrl(email, new RedirectionUrlCallback());
			Folder inbox = Folder.bind(service, WellKnownFolderName.Inbox);

			ItemView view = new ItemView(viewItemNum);
			SearchFilterCollection filterCol = new SearchFilterCollection(LogicalOperator.And);
			SearchFilter filter;
			filterCol.add(filter = new SearchFilter.IsEqualTo(EmailMessageSchema.Subject, search));
			filterCol.add(filter = new SearchFilter.ContainsSubstring(EmailMessageSchema.DateTimeReceived, dateFormat.format(date)));
			filterCol.add(filter = new SearchFilter.IsEqualTo(EmailMessageSchema.IsRead, false));
			findResults = service.findItems(inbox.getId(), filter, view);

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	private void tempPass(){
		String val = "";
		PropertySet props = new PropertySet(ItemSchema.Body);
		props.setRequestedBodyType(BodyType.Text);
		try {
			if(!findResults.equals(null))
			{
				service.loadPropertiesForItems(findResults, props);
				
			}
		} catch (Exception e) {
			tempPass = "";
			System.out.println("No Emails in user inbox match the following criteria: \n - Created Today \n - Unread \n - Matches the specified subject");			
			e.printStackTrace();
		}
		for (Item item : findResults.getItems()) {
			try {
				val = item.getBody().toString();
				tempPass = val.substring(val.lastIndexOf(":") + 1, val.lastIndexOf(":") + 10).trim();
				EmailMessage em = (EmailMessage) item;
				em.setIsRead(true);
				em.update(ConflictResolutionMode.AlwaysOverwrite);
			} catch (ServiceLocalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServiceResponseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		service.close();
	}
	
	private String emailBySubject(){
		String val = "";
		PropertySet props = new PropertySet(ItemSchema.Body);
		props.setRequestedBodyType(BodyType.Text);
		try {
			if(!findResults.equals(null))
			{
			service.loadPropertiesForItems(findResults, props);
			}
		} catch (Exception e) {
			System.out.println("No Emails in user inbox match the following criteria: \n - Created Today \n - Unread \n - Matches the specified subject");	
			e.printStackTrace();
		}
		for (Item item : findResults.getItems()) {
			try {
				val = item.getBody().toString();
				EmailMessage em = (EmailMessage) item;
				em.setIsRead(true);
				em.update(ConflictResolutionMode.AlwaysOverwrite);
			} catch (ServiceLocalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		service.close();
		return val;
	}
}
