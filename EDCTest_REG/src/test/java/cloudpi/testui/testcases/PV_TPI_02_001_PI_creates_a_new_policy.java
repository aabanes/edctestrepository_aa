package cloudpi.testui.testcases;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;

import cloudpi.testTPI.pages.LoginPage;
import cloudpi.testTPI.pages.TPI_ManageAddBuyerPage;
import cloudpi.testTPI.pages.TPI_PolicyListPage;
import cloudpi.testTPI.pages.TPI_PortfolioDetailPage;
import cloudpi.testTPI.pages.TPI_RepositoryElements;
import cloudpi.testTPI.utility.ExcelUtils;
import junit.framework.Assert;

public class PV_TPI_02_001_PI_creates_a_new_policy extends cloudpi.testui.testLibs.BaseTest {
	private TPI_PortfolioDetailPage tpiPortfolioDetail = null;
	private LoginPage tpiLogin = null;
	private TPI_ManageAddBuyerPage tpiManageAddBuyer = null;
	private TPI_PolicyListPage tpiPolicyList = null;
	private String compName, tpiUsername, tpiPassword, dunsNum, province, country;
	private ExcelUtils excel = null;
	private TPI_RepositoryElements repEle = null;
	private By newBy = null;
	private Integer preResults = 0;

	@Test(priority = 1, enabled = true)
	public void PV_TPI_02_001_PI_creates_a_new_policy_ph_identified_via_DNB() {
		tpiLogin = new LoginPage(driver);
		try {
			tpiPolicyList = new TPI_PolicyListPage(driver);
			tpiManageAddBuyer = new TPI_ManageAddBuyerPage(driver);
			tpiPortfolioDetail = new TPI_PortfolioDetailPage(driver);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		excel = new ExcelUtils();
		
		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			tpiUsername = excel.getCellData(2, 0);
			tpiPassword = excel.getCellData(2, 1);

			excel.getExcelFile("./testData/TestData.xlsx", "Sheet2");
			country = excel.getCellData(4, 4);
			compName = excel.getCellData(4, 2);
			province = excel.getCellData(4, 9);
			dunsNum = excel.getCellData(4, 1);
		
			repEle = new TPI_RepositoryElements("../CloudpiEDCtestTPI/config/TPI_PolicyViewListRepository.properties");
			newBy = repEle.byLocator("PolicyList_SearchResults");

		} catch (Exception e) {
			e.printStackTrace();
		}

		Reporter.log(
				"Begin Test: PV TPI-02-001_PI creates a new policy - ph identified via DNB (1)");

		Reporter.log("Step 1 - Login to front portal");
		try {
			tpiLogin.signAccount(tpiUsername, tpiPassword);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Reporter.log("Step 2 - Click View, Policy List");
		tpiPolicyList.PolicyListMenu_View().click();
		tpiPolicyList.PolicyListMenu_PolicyList().click();
		
		List<WebElement> policyListResults = driver.findElements(newBy);
		if (policyListResults.size() > 0)
		{
			Reporter.log("Step 2 Result - List of all active policies for the PI is displayed");
		}
		else
		{
			Reporter.log("Step 2 Result - No Results returned");
		}
		
		Select selPolicySearchType = new Select(tpiPolicyList.PolicyList_PolicyTypeSearch());
		if (selPolicySearchType.getFirstSelectedOption().getText().equals("Policyholder name") == false)
		{
			selPolicySearchType.selectByValue("Policyholder name");
		}
		tpiPolicyList.PolicyList_PolicySearchCriteria().sendKeys(compName);
		tpiPolicyList.PolicyList_PolicySearchBtn().click();
		preResults = policyListResults.size();
		
		Reporter.log("Step 3 - Click Add a New Policy");
		tpiPolicyList.PolicyList_AddBtn().click();
		Reporter.log("Step 3 Result- Policyholder Identification screen is displayed");
		
		Reporter.log("Step 4 - Search for a company that exists in DNB");
		Select selProvince = new Select(tpiPolicyList.PolicyList_AddNewPolicy_Province());
		selProvince.selectByVisibleText(getProvinces(province));
		tpiPolicyList.PolicyList_AddNewPolicy_CompanyName().sendKeys(compName);
		tpiPolicyList.PolicyList_AddNewPolicy_DunsNum().sendKeys(dunsNum);
		tpiPolicyList.PolicyList_AddNewPolicy_SubmitBtn().click();
		
		Reporter.log("Step 5 - Click select");
		tpiPolicyList.PolicyList_AddNewPolicy_Result().click();
		
		if (tpiPolicyList.PolicyList_RegisterPolicy_InsPolNum().isDisplayed())
		{
			Reporter.log("Step 4 Result - Company is identified");
			Reporter.log("Step 5 - Register Policy page present.");
		}
		else
		{
			Assert.fail("Step 5 - Register Policy page not present.");
			Reporter.log("Step 5 - Register Policy page not present.");
		}
		
		Reporter.log("Step 6 - Enter required information. Submit");
		Random rmd = new Random();
		tpiPolicyList.PolicyList_RegisterPolicy_InsPolNum().sendKeys(String.valueOf(rmd.nextInt(99999)));
		tpiPolicyList.PolicyList_RegisterPolicy_CreditIns().click();
		tpiPolicyList.PolicyList_RegisterPolicy_SocialResponsibility().click();
		tpiPolicyList.PolicyList_RegisterPolicy_SubmitBtn().click();
		Reporter.log("Step 6 Results - Policy is created successfully");
		
		Reporter.log("Step 7 - Validate that new policy is now listed in policy list");
		policyListResults = driver.findElements(newBy);
		if (preResults < policyListResults.size())
		{
			Reporter.log("Step 7 Result - Policy is in policy list");
		}
		else
		{
			Reporter.log("Step 7 Result - Policy not in list");
		}
		
		try {
			tpiManageAddBuyer.openAddBuyer();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		Reporter.log("Step 8 - Navigate to any buyer profile and bring up credit limit application screen");
		Select selCountry = new Select(tpiManageAddBuyer.countrySelectDropBox());
		selCountry.selectByVisibleText(country);
		
		if (selCountry.getFirstSelectedOption().getText().equals("CANADA"))
		{
			Select selProv = new Select(tpiManageAddBuyer.stateSelectDropBox());
			selProv.selectByVisibleText(getProvinces(province));
		}
		tpiManageAddBuyer.DUNSNumberTextBox().sendKeys(dunsNum);
		tpiManageAddBuyer.buyerSubmitButton().click();
		tpiManageAddBuyer.SelectBtnOneResult().click();
		
		tpiPortfolioDetail.serviceButton().click();

		Select selPolicyHolder = new Select(tpiPortfolioDetail.coverageSelect());
		if (selPolicyHolder.getFirstSelectedOption().getText().equals("")) {
			Reporter.log(
					"Step 2 - Naviage to buyer as per data setup, click services, ensure that the PH field defaults to blank");
		} else {
			Reporter.log("Step 2 Result - Value exists in Policy Holder dropdown. Value: "
					+ selPolicyHolder.getFirstSelectedOption().getText());
		}

		List<WebElement> policyHolderOptions = selPolicyHolder.getOptions();
		for (WebElement ele : policyHolderOptions) {
			if (ele.getText().contains(compName.toUpperCase())) {
				selPolicyHolder.selectByVisibleText(ele.getText());
				Reporter.log("Step 9 - Ensure that new policy is in the policyholder list and you are able to select it for limit application");
				break;
			}
			
			if (policyHolderOptions.iterator().hasNext() == false)
			{
				Assert.fail("Step 9 - Unable to select policyholder from list.");
				Reporter.log("Step 9 - Unable to select policyholder from list.");
			}
		}
	}
	
	private String getProvinces(String province)
	{
		String result = "";
		Map<String, String> provinceMap = new HashMap<String, String>();
		provinceMap.put("AB", "ALBERTA");
		provinceMap.put("BC", "BRITISH COLUMBIA");
		provinceMap.put("MB", "MANITOBA");
		provinceMap.put("NB", "NEW BRUNSWICK");
		provinceMap.put("NL", "NEWFOUNDLAND AND LABRADOR");
		provinceMap.put("NS", "NOVA SCOTIA");
		provinceMap.put("NT", "NORTHWEST TERRITORIES");
		provinceMap.put("NU", "NUNAVUT");
		provinceMap.put("ON", "ONTARIO");
		provinceMap.put("PE", "PRINCE EDWARD ISLAND");
		provinceMap.put("QC", "QUEBEC");
		provinceMap.put("SK", "SASKATCHEWAN");
		provinceMap.put("YT", "YUKON");
		for (@SuppressWarnings("unused") String key : provinceMap.keySet())
		{
			if (provinceMap.containsKey(province))
			{
				result = provinceMap.get(province);
				break;
			}
		}
		return result;
	}
}
