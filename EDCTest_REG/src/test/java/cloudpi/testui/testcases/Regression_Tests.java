package cloudpi.testui.testcases;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import cloudpi.testARI.utility.EWSJavaAPI;
import cloudpi.testARI.utility.ExcelUtils;
import cloudpi.testTP.pages.RepositoryElements;
import cloudpi.testTP.pages.TP_ApplyForApprovalPage;
import cloudpi.testTP.pages.TP_LandingPage;
import cloudpi.testTP.pages.TP_Menu;
import cloudpi.testTP.pages.TP_SignUpPage;
import cloudpi.testTPI.pages.LoginPage;
import cloudpi.testTPI.pages.ManageAddBuyerPage;
import cloudpi.testbkof.pages.EDC_BackOffice;

public class Regression_Tests extends cloudpi.testARI.testLibs.BaseTest {

	private EDC_BackOffice bkOffice = null;
	private TP_LandingPage tpLand = null;
	private TP_Menu tpMenu = null;
	private TP_ApplyForApprovalPage tpApproval = null;
	private LoginPage TPILogin = null;
	private ManageAddBuyerPage TPIManageAddBuyer = null;
	private TP_SignUpPage tpSignUp = null;
	private ExcelUtils excel = null;
	private String bkOfUsername, bkOfPassword, tpiUsername, tpiPassword, tpUsername, tpPassword, readLoginID,
			readPassword, readDomain, readEmailSubject, readEmailBody, duns, compName, country, province, state,
			selectVal, title, firstName, lastName, jobTitle, department, phoneNum, faxNumber, email, language,
			dateFormat, compInfoName, compInfoBusNum, compInfoAddress, compInfoCity, compInfoFaxNum, compInfoProv,
			compInfoPostalCode;
	private By newBy, newBy2, newBy3 = null;
	private RepositoryElements repEle, repEle2 = null;
	private String[] provArr = { "ALBERTA", "BRITISH COLUMBIA", "MANITOBA", "NEW BRUNSWICK",
			"NEWFOUNDLAND AND LABRADOR", "NORTHWEST TERRITORIES", "NOVA SCOTIA", "NUNAVUT", "ONTARIO",
			"PRINCE EDWARD ISLAND", "QUEBEC", "SASKATCHEWAN", "YUKON TERRITORY" };
	private String[] provAbrv = { "AB", "BC", "MB", "NB", "NL", "NT", "NS", "NU", "ON", "PE", "QC", "SK", "YT" };
	private static String[] stateArrAbrv = { "ALABAMA,AL", "ALASKA,AK", "ARIZONA,AZ", "ARKANSAS,AR", "CALIFORNIA,CO",
			"COLORADO,CO", "CONNECTICUT,CT", "DELAWARE,DE", "FLORIDA,FL", "GEORGIA,GA", "HAWAII,HI", "IDAHO,ID",
			"ILLINOIS,IL", "INDIANA,IN", "IOWA,IA", "KANSAS,KA", "KENTUCKY,KY", "LOUISIANA,LA", "MAINE,ME",
			"MARYLAND,MD", "MASSACHUSETTS,MA", "MICHIGAN,MI", "MINNESOTA,MN", "MISSISSIPPI,MS", "MISSOURI,MO",
			"MONTANA,MT", "NEBRASKA,NE", "NEW HAMPSHIRE,NH", "NEW JERSEY,NJ", "NEW MEXICO,NM", "NEW YORK,NY",
			"NORTH CAROLINA,NC", "NORTH DAKOTA,ND", "OHIO,OH", "OKLAHOMA,OK", "OREGON,OR", "PENNSYLVANIA,PA",
			"RHODE ISLAND,RI", "SOUTH CAROLINA,SC", "SOUTH DAKOTA,SD", "TENNESSEE,TN", "TEXAS,TX", "UTAH,UT",
			"VERMONT,VT", "VIRGINA,VA", "WASHINGTON,WA", "WEST VIRGINA,WV", "WISCONSIN,WI", "WYOMING,WY" };
	private WebDriverWait wait;
	private EWSJavaAPI ewsJava = null;

	@Test(priority = 1, enabled = false)
	public void CIS_Core_Test_Script_7() {
		bkOffice = new EDC_BackOffice(driver);
		TPILogin = new LoginPage(driver);
		TPIManageAddBuyer = new ManageAddBuyerPage(driver);
		excel = new ExcelUtils();

		try {
			repEle = new RepositoryElements("../EDC_TP/config/TP_ApplyForApproval_Repository.properties");
			repEle2 = new RepositoryElements("../CloudpiEDCtestTPI/config/RepositoryTPI.properties");
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(1, 0);
			bkOfPassword = excel.getCellData(1, 1);
			tpiUsername = excel.getCellData(2, 0);
			tpiPassword = excel.getCellData(2, 1);
			tpUsername = excel.getCellData(3, 0);
			tpPassword = excel.getCellData(3, 1);

			excel.getExcelFile("./testData/TestData.xlsx", "Sheet2");
			duns = excel.getCellData(1, 1);
			compName = excel.getCellData(1, 2);
			country = excel.getCellData(1, 4);
			state = excel.getCellData(1, 9);

			newBy = repEle.byLocator("TP_ApplyForApproval_stp1_SearchPane");
			newBy2 = repEle2.byLocator("TPI_AddBuyer_SearchResults");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 30);

		Reporter.log("Step 1: Log into Grams");
		BKOF_login(bkOfUsername, bkOfPassword);
		waitForPageToLoad(bkOffice.BKOF_SearchIcon());

		Reporter.log("Step 2: Click on magnifying glass");
		bkOffice.BKOF_SearchIcon().click();

		if (bkOffice.BKOF_SearchWindow().isDisplayed()) {
			Reporter.log("Step 2: Search Window Opens");
		} else {
			Assert.fail("Search window not open");
			Reporter.log("Search window not open");
		}

		Reporter.log("Step 3: Select Direct Search");
		bkOffice.BKOF_SearchWindow_DirectSearchLink().click();
		Reporter.log("Step 4: Enter a DUNS number");
		bkOffice.BKOF_SearchWindow_DirectSearch_Ref().sendKeys(duns);
		Reporter.log("Step 5: Click Submit");
		bkOffice.BKOF_SearchWindow_SubmitBtn().click();

		if (bkOffice.BKOF_Menu_Header().equals(duns + " - " + compName)) {
			Reporter.log("Step 5: Buyer File Opens");
		} else {
			softVerify(bkOffice.BKOF_Menu_Header().getText(), duns + " - " + compName,
					"Incorrect page. Header is: " + duns + " - " + compName);
		}

		Reporter.log("Step 6: Log into TPI portal");
		driver.navigate().to("https://cis-qac.edc.ca/herakles/do");
		try {
			TPILogin.signAccount(tpiUsername, tpiPassword);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Reporter.log("Step 7: Go to Manage > Add a Buyer");
		TPIManageAddBuyer.Manage_Menulink().click();
		TPIManageAddBuyer.manage_AddBuyer().click();
		Reporter.log("Step 8: Search for a company");
		TPIManageAddBuyer.TPI_AddBuyer_DUNS().sendKeys(duns);
		Reporter.log("Step 9: Click Submit");
		TPIManageAddBuyer.buyerSubmitButton().click();

		Reporter.log("Step 9: Search results show");
		List<WebElement> resultsPaneEle2 = driver.findElements(newBy2);
		if (resultsPaneEle2.size() == 0) {
			Assert.fail("No search results returned");
			Reporter.log("No search results returned.");
		}
		sleep(3);
		Reporter.log("Step 10: Select record");
		for (int i = 0; i < resultsPaneEle2.size(); i++) {
			if (resultsPaneEle2.get(i).findElement(By.tagName("p")).getText().contains(compName)) {
				resultsPaneEle2.get(i)
						.findElement(By.className("ui-corner-all tss-button ui-state-highlight ui-state-default"))
						.click();
				break;
			}
		}
		sleep(3);
		if (TPIManageAddBuyer.detailNameId().getText().contains(duns + " " + compName)) {
			Reporter.log("Step 10: Buyer File Opens");
		} else {
			softVerify(TPIManageAddBuyer.detailNameId().getText(), duns + " - " + compName,
					"Incorrect page. Header is: " + duns + " - " + compName);
		}

		Reporter.log("Step 11: Log into TP portal");
		TP_login(tpUsername, tpPassword);
		tpMenu = new TP_Menu(driver);
		Reporter.log("Step 12: Select Apply for an Approval");
		tpMenu.TP_Menu_ApplyForApprovalButton().click();
		;
		Reporter.log("Step 13: Search for a company");

		tpApproval = new TP_ApplyForApprovalPage(driver);
		if (tpApproval.TP_ApplyForApproval_Step1Active().isDisplayed()) {
			Select countrySelect = new Select(tpApproval.TP_ApplyForApproval_stp1_countryDropDown());
			countrySelect.selectByVisibleText(country);

			if (country.equals("UNITED STATES")) {
				Select stateSelect = new Select(tpApproval.TP_ApplyForApproval_stp1_stateDropDown());
				for (int i = 0; i < stateArrAbrv.length; i++) {
					if (stateArrAbrv[i].substring(stateArrAbrv[i].indexOf(",") + 1, stateArrAbrv[i].length())
							.equals(state)) {
						stateSelect.selectByVisibleText(stateArrAbrv[i].substring(0, stateArrAbrv[i].indexOf(",")));
						break;
					}
				}
			}

			tpApproval.TP_ApplyForApproval_stp1_dnsNumberTextfield().sendKeys(duns);
		} else {
			Assert.fail("Not on Step 1 - Submit an Approval page");
			Reporter.log("Not on Step 1 - Submit an Approval page");
		}
		Reporter.log("Step 14: Click Search");
		tpApproval.TP_ApplyForApproval_stp1_SearchButton().click();

		List<WebElement> resultsPaneEle = driver.findElements(newBy);
		if (resultsPaneEle.size() == 0) {
			Assert.fail("No search results returned");
			Reporter.log("No search results returned.");
		}

		for (int i = 0; i < resultsPaneEle.size(); i++) {
			if (resultsPaneEle.get(i).findElement(By.tagName("h3")).getText().equals(compName)) {
				Reporter.log("Step 14: Search Results show");
				break;
			}
		}
	}

	@Test(priority = 1, enabled = true)
	public void Policyholder_signs_up_a_new_Policy() {
		bkOffice = new EDC_BackOffice(driver);
		tpApproval = new TP_ApplyForApprovalPage(driver);
		tpSignUp = new TP_SignUpPage(driver);

		excel = new ExcelUtils();

		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(2, 1);
			bkOfPassword = excel.getCellData(2, 2);
			tpUsername = excel.getCellData(4, 1);
			tpPassword = excel.getCellData(4, 2);

			excel.getExcelFile("./testData/TestData.xlsx", "Sheet2");
			compName = excel.getCellData(3, 3);
			province = excel.getCellData(3, 10);

			excel.getExcelFile("./testData/TestData.xlsx", "Sheet3");
			title = excel.getCellData(2, 1);
			firstName = excel.getCellData(2, 2);
			lastName = excel.getCellData(2, 3);
			jobTitle = excel.getCellData(2, 4);
			department = excel.getCellData(2, 5);
			phoneNum = excel.getCellData(2, 6);
			faxNumber = excel.getCellData(2, 7);
			email = excel.getCellData(2, 8);
			language = excel.getCellData(2, 9);
			dateFormat = excel.getCellData(2, 10);

			excel.getExcelFile("./testData/TestData.xlsx", "Sheet4");
			readLoginID = excel.getCellData(2, 1);
			readPassword = excel.getCellData(2, 2);
			readDomain = excel.getCellData(2, 3);
			readEmailSubject = excel.getCellData(2, 4);
			readEmailBody = excel.getCellData(2, 5);

			repEle = new RepositoryElements("../EDC_TP/config/SignUp_Repository.properties");
			newBy = repEle.byLocator("TP_SignUp_stp1_resultsPane");

			repEle2 = new RepositoryElements("../EDCTest_BackOffice/config/BackOffice_Repository.properties");
			newBy2 = repEle2.byLocator("BKOF_PplOrg_InsuredList_FilterSearch_Results");
			newBy3 = repEle2.byLocator("BKOF_Contact_UserTable");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String tempPass = "";
		ewsJava = new EWSJavaAPI();

		Reporter.log("Step 1 - Navigate to TP Front End Portal");
		Reporter.log("Step 1 - TP Front End Portal is opened.");

		Reporter.log("Step 2 - Clicks up SIGN UP button.");
		tpLand.TP_Landing_SignUpButton().click();
		if (tpSignUp.TP_SignUp_Step1Active().isDisplayed()) {
			Reporter.log("Step 2 - Your Company screen is displayed.");
		} else {
			Assert.fail("Not on correct page");
			Reporter.log("Not on correct page");
		}

		Reporter.log(
				"Step 3 -  On the Your Company screen, search the company by Company Name with Country and Province fields");
		tpSignUp.TP_SignUp_stp1_CompanyName().sendKeys(compName);
		for (int i = 0; i < provAbrv.length; i++) {
			if (provAbrv[i].equals(province)) {
				selectVal = provArr[i];
				break;
			}
		}
		Select country = new Select(tpSignUp.TP_SignUp_stp1_Province());
		country.selectByVisibleText(selectVal);
		tpSignUp.TP_SignUp_Submit().click();

		List<WebElement> resultsPaneEle = driver.findElements(newBy);
		if (resultsPaneEle.size() < 0) {
			Assert.fail("No Results Returned in Sign up - Results pane");
			Reporter.log("No Results Returned in Sign up - Results pane");
		} else {
			Reporter.log("Step 3 - System displays all the search results.");
		}

		for (int i = 0; i < resultsPaneEle.size(); i++) {
			if (resultsPaneEle.get(i).findElement(By.tagName("h3")).getText().contains(compName)) {
				resultsPaneEle.get(i).findElement(By.tagName("h3")).click();
				Reporter.log("Step 4 - PH selects the company from the Search Results.");
				break;
			}
		}

		if (tpSignUp.TP_SignUp_Step2Active().isDisplayed()) {
			Reporter.log("Step 4 - Additional Information screen is opened.");
		} else {
			Assert.fail("Not on correct page");
			Reporter.log("Not on correct page");
		}

		Reporter.log(
				"Step 5 - On the Additional Informations screen, popoulate all the mandatory fields, and then clicks on NEXT button.");
		Select industry = new Select(tpSignUp.TP_SignUp_stp2_IndustrySector());
		Select fiscalType = new Select(tpSignUp.TP_SignUp_stp2_fiscalSalesType());
		Select polCurr = new Select(tpSignUp.TP_SignUp_stp2_policyCurr());
		Select lang = new Select(tpSignUp.TP_SignUp_stp2_policyLang());

		industry.selectByIndex(1);
		tpSignUp.TP_SignUp_stp2_fiscalSalesTextfield().sendKeys("10000");
		fiscalType.selectByIndex(1);
		tpSignUp.TP_SignUp_stp2_fiscalYearEnd().sendKeys(setDate());
		polCurr.selectByIndex(1);
		lang.selectByIndex(1);
		tpSignUp.TP_SignUp_Submit().click();

		if (tpSignUp.TP_SignUp_Step3Active().isDisplayed()) {
			Reporter.log("Step 5 - Your Accout screen is displayed.");
		} else {
			Assert.fail("Not on correct page");
			Reporter.log("Not on correct page");
		}

		Reporter.log(
				"Step 6 - On the Your Account screen, adding a contact by populating the mandatory fields, and then clicks on NEXT Button.");
		Select titleSelect = new Select(tpSignUp.TP_SignUp_stp3_JobTitle());
		titleSelect.selectByVisibleText(title);
		tpSignUp.TP_SignUp_stp3_FirstName().sendKeys(firstName);
		tpSignUp.TP_SignUp_stp3_LastName().sendKeys(lastName);
		tpSignUp.TP_SignUp_stp3_JobTitle().sendKeys(jobTitle);
		tpSignUp.TP_SignUp_stp3_Department().sendKeys(department);
		tpSignUp.TP_SignUp_stp3_PhoneNum().sendKeys(phoneNum);
		tpSignUp.TP_SignUp_stp3_FaxNum().sendKeys(faxNumber);
		tpSignUp.TP_SignUp_stp3_Email().sendKeys(email);
		tpSignUp.TP_SignUp_stp3_CnFmEmail().sendKeys(email);
		Select signup_Lang = new Select(tpSignUp.TP_SignUp_stp3_Language());
		signup_Lang.selectByVisibleText(language);
		Select signup_DateFormat = new Select(tpSignUp.TP_SignUp_stp3_DateFormat());
		signup_DateFormat.selectByVisibleText(dateFormat);
		tpSignUp.TP_SignUp_Submit().click();

		if (ewsJava.getEmailBySubject(readLoginID, readPassword, readDomain, email, readEmailSubject)
				.contains(readEmailBody)) {
			Reporter.log(
					"Step 6 - Policyholder get Confirmation message that the account has been created successfully. PH receives a Login email with login and password.");
		} else {
			softVerify(ewsJava.getEmailBySubject(readLoginID, readPassword, readDomain, email, readEmailSubject),
					readEmailBody, "Email does not contain expected body.");
		}

		Reporter.log("Step 7 - Log into the back office");
		BKOF_login(bkOfUsername, bkOfPassword);
		Reporter.log("Step 7 - UW logged into the back office");

		Reporter.log("Step 8 - Navigate to Administration -> People & Organization -> Insured List");
		bkOffice.BKOF_MainMenu().click();
		bkOffice.BKOF_MainMenu_Administration().click();

		bkOffice.BKOF_MainMenu_Administration_PplOrg().click();
		bkOffice.BKOF_MainMenu_Administration_PplOrg_InsuredList().click();

		Reporter.log("Step 8 - All the insured companies are listed under the Insured list.");

		Reporter.log(
				"Step 9 - Find the company that PH used to create the TP account, then clicks on the hyperlink of the insured company.");
		bkOffice.BKOF_PplOrg_InsuredList_FilterBtn().click();
		bkOffice.BKOF_PplOrg_InsuredList_FilterSearch_Email().sendKeys(email);
		bkOffice.BKOF_PplOrg_InsuredList_FilterSearch_SubmitBtn().click();

		List<WebElement> resultsPaneEle2 = driver.findElements(newBy2);
		List<WebElement> res2;
		for (int i = 0; i < resultsPaneEle2.size(); i++) {
			res2 = resultsPaneEle2.get(i).findElements(By.tagName("td"));

			for (int j = 0; j < res2.size(); j++) {
				if (res2.get(j).getText().contains(compName)) {
					res2.get(j).findElement(By.tagName("a")).click();
					break;
				}
			}
		}

		if (bkOffice.BKOF_Menu_Header().equals(duns + " - " + compName)) {
			Reporter.log("Step 9 - Insured company information is displayed with the correct name and address.");
		} else {
			softVerify(bkOffice.BKOF_Menu_Header().getText(), duns + " - " + compName,
					"Incorrect page. Header is: " + duns + " - " + compName);
		}

		Reporter.log("Step 10 - Navigate to Contact tab, verifying the contact has been created.");
		bkOffice.BKOF_Menu_Contact().click();

		sleep(10);

		driver.findElement(newBy3);
		List<WebElement> resultsPaneEle3 = driver.findElements(newBy3);
		List<WebElement> res3;
		for (int i = 0; i < resultsPaneEle3.size(); i++) {
			res3 = resultsPaneEle3.get(i).findElements(By.tagName("td"));

			for (int j = 0; j < res3.size(); j++) {
				if (res3.get(j).getText().equals(email)) {
					if (res3.get(j + 1).findElement(By.tagName("img")).isDisplayed()) {
						res3.get(j).click();
						break;
					}
				}
			}

		}

		Select selectTitle = new Select(bkOffice.BKOF_Contact_IDCard_Title());
		Select selectLang = new Select(bkOffice.BKOF_Contact_IDCard_Language());
		Select selectDateFormat = new Select(bkOffice.BKOF_Contact_IDCard_DateFormat());

		softVerify(selectTitle.getFirstSelectedOption().getText(), title,
				"Incorrect Title value. Value in back office: " + selectTitle.getFirstSelectedOption().getText()
						+ "\n Expected Value: " + title);
		softVerify(bkOffice.BKOF_Contact_IDCard_FirstName().getAttribute("value"), firstName,
				"Incorrect Title value. Value in back office: "
						+ bkOffice.BKOF_Contact_IDCard_FirstName().getAttribute("value") + "\n Expected Value: "
						+ firstName);
		softVerify(bkOffice.BKOF_Contact_IDCard_LastName().getAttribute("value"), lastName,
				"Incorrect Title value. Value in back office: "
						+ bkOffice.BKOF_Contact_IDCard_LastName().getAttribute("value") + "\n Expected Value: "
						+ lastName);
		softVerify(bkOffice.BKOF_Contact_IDCard_JobTitle().getAttribute("value"), jobTitle,
				"Incorrect Title value. Value in back office: "
						+ bkOffice.BKOF_Contact_IDCard_JobTitle().getAttribute("value") + "\n Expected Value: "
						+ jobTitle);
		softVerify(bkOffice.BKOF_Contact_IDCard_Department().getAttribute("value"), department,
				"Incorrect Title value. Value in back office: "
						+ bkOffice.BKOF_Contact_IDCard_Department().getAttribute("value") + "\n Expected Value: "
						+ department);
		softVerify(bkOffice.BKOF_Contact_IDCard_PhoneNum().getAttribute("value"), phoneNum,
				"Incorrect Title value. Value in back office: "
						+ bkOffice.BKOF_Contact_IDCard_PhoneNum().getAttribute("value") + "\n Expected Value: "
						+ phoneNum);
		softVerify(bkOffice.BKOF_Contact_IDCard_FaxNum().getAttribute("value"), faxNumber,
				"Incorrect Title value. Value in back office: "
						+ bkOffice.BKOF_Contact_IDCard_FaxNum().getAttribute("value") + "\n Expected Value: "
						+ faxNumber);
		softVerify(bkOffice.BKOF_Contact_IDCard_Email().getAttribute("value"), email,
				"Incorrect Title value. Value in back office: "
						+ bkOffice.BKOF_Contact_IDCard_Email().getAttribute("value") + "\n Expected Value: " + email);
		softVerify(selectLang.getFirstSelectedOption().getText(), language,
				"Incorrect Title value. Value in back office: " + selectLang.getFirstSelectedOption().getText()
						+ "\n Expected Value: " + language);
		softVerify(selectDateFormat.getFirstSelectedOption().getText(), dateFormat,
				"Incorrect Title value. Value in back office: " + selectDateFormat.getFirstSelectedOption().getText()
						+ "\n Expected Value: " + dateFormat);
		Reporter.log("Step 10 - Ensure the contact information is correct and the contact is Active. ");

		Reporter.log("Step 11 - Navigate to Policy tab, verifying there is TP policy was created.");
		bkOffice.BKOF_Menu_Policies().click();
		Reporter.log("Step 11 - Ensure the Policy was created with Active Status and Start Date is corrct.");

		Reporter.log("Step 12 - Clicks on the hyperlink of the TP policy");
		Reporter.log("Step 12 - PH logged into the account.");

		Reporter.log("Step 13 - Log into the TP account by populating the email and password.");
		tempPass = ewsJava.getTempPassword("sumxst", "Canada123", "EDC-NT", "Stephen.Sum.ExternalConsultant@edc.ca",
				"Login Information");
		Reporter.log("Step 13 - Change Password screen is displayed.");

		Reporter.log("Step 14 - Terms of Use screen is displayed, checks on 'I accept' and clicks on CONFIRM button.");
		Reporter.log("Step 14 - The Password is changed to personal password.");

		Reporter.log(
				"Step 15 - On the Change Password screen, enters the Current password, New Password and Confirm Password, than clicks on CHANGE PASSWORD button.");
		Reporter.log("Step 15 - PH can apply the first limit");
	}

	@Test(priority = 1, enabled = false)
	public void REG2_UW_Manually_Create_Policy() {

		bkOffice = new EDC_BackOffice(driver);
		tpLand = new TP_LandingPage(driver);
		tpMenu = new TP_Menu(driver);
		tpApproval = new TP_ApplyForApprovalPage(driver);
		tpSignUp = new TP_SignUpPage(driver);
		Actions act = new Actions(driver);
		excel = new ExcelUtils();

		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(2, 1);
			bkOfPassword = excel.getCellData(2, 2);
			tpUsername = excel.getCellData(4, 1);
			tpPassword = excel.getCellData(4, 2);

			excel.getExcelFile("./testData/TestData.xlsx", "Sheet2");
			compName = excel.getCellData(3, 3);
			province = excel.getCellData(3, 10);

			excel.getExcelFile("./testData/TestData.xlsx", "Sheet3");
			compInfoName = excel.getCellData(5, 12);
			compInfoBusNum = excel.getCellData(5, 6);
			compInfoAddress = excel.getCellData(5, 13);
			compInfoCity = excel.getCellData(5, 13);
			compInfoProv = excel.getCellData(5, 14);
			compInfoPostalCode = excel.getCellData(5, 15);

			title = excel.getCellData(5, 1);
			firstName = excel.getCellData(5, 2);
			lastName = excel.getCellData(5, 3);
			jobTitle = excel.getCellData(5, 4);
			department = excel.getCellData(4, 5);
			phoneNum = excel.getCellData(5, 6);
			faxNumber = excel.getCellData(5, 7);
			email = excel.getCellData(5, 8);
			language = excel.getCellData(5, 9);
			dateFormat = excel.getCellData(5, 10);

			repEle = new RepositoryElements("../EDC_TP/config/SignUp_Repository.properties");
			newBy = repEle.byLocator("TP_SignUp_stp1_resultsPane");
			repEle2 = new RepositoryElements("../EDCTest_BackOffice/config/BackOffice_Repository.properties");
			newBy2 = repEle2.byLocator("BKOF_Risk_Portfolio_PolicyList_CreatePolicyFromTeplatePopup_NewInsuredRdoBtn");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Reporter.log("Step 1 - Go to TP front portal");
		Reporter.log("Step 2 - Clicks Sign up button.");
		tpLand.TP_Landing_SignUpButton().click();
		if (tpSignUp.TP_SignUp_Step1Active().isDisplayed()) {
			Reporter.log("Step 2 - Your Company screen is displayed.");
		} else {
			Assert.fail("Not on correct page");
			Reporter.log("Not on correct page");
		}

		Reporter.log("Step 3 - Enter values into the company name and province");
		tpSignUp.TP_SignUp_stp1_CompanyName().sendKeys(compName);
		for (int i = 0; i < provAbrv.length; i++) {
			if (provAbrv[i].equals(province)) {
				selectVal = provArr[i];
				break;
			}
		}
		Select country = new Select(tpSignUp.TP_SignUp_stp1_Province());
		country.selectByVisibleText(selectVal);

		Reporter.log("Step 4 - Click submit on the search screen to see the manual identification open");
		tpSignUp.TP_SignUp_Submit().click();

		if (tpSignUp.TP_SignUp_stp1_manualIdentify().isDisplayed()) {
			Reporter.log("Step 4 - Text box for manual identification option appears below search");
		} else {
			Assert.fail("Manual Identification link not present");
			Reporter.log("Manual Identification link not present");
		}

		Reporter.log("Step 5 - Click 'Here' on the manual identification message");
		tpSignUp.TP_SignUp_stp1_manualIdentify().click();
		Reporter.log("Step 5 - Manual identification screen displayed");

		Reporter.log("Step 6 - Enter required information to create your new policy. Click Next");
		tpSignUp.TP_SignUp_stp1_CompanyName().sendKeys(compInfoName);
		tpSignUp.TP_SignUp_stp1_BusinessNum().sendKeys(compInfoBusNum);
		tpSignUp.TP_SignUp_stp1_Address().sendKeys(compInfoAddress);
		tpSignUp.TP_SignUp_stp1_City().sendKeys(compInfoCity);
		tpSignUp.TP_SignUp_stp1_PhoneNum().sendKeys(compInfoBusNum);

		for (int i = 0; i < provAbrv.length; i++) {
			if (provAbrv[i].equals(province)) {
				selectVal = provArr[i];
				break;
			}
		}
		tpSignUp.TP_SignUp_stp1_BusinessNum().sendKeys(selectVal);
		tpSignUp.TP_SignUp_stp1_PostalCode().sendKeys(compInfoPostalCode);
		tpSignUp.TP_SignUp_Submit();
		
		if (tpSignUp.TP_SignUp_Step2Active().isDisplayed())
		{
			Reporter.log("Step 6 - Additional Information screen is displayed. Creation of Company Profile email is sent");
		}
		else
		{
			Assert.fail("Not on correct page");
			Reporter.log("Not on correct page");
		}
		
		Reporter.log(
				"Step 7 - On the Additional Information screen, popoulate all the mandatory fields, and then clicks on NEXT button.");
		Select industrySelect = new Select(tpSignUp.TP_SignUp_stp2_IndustrySector());
		Select currSelect = new Select(tpSignUp.TP_SignUp_stp2_fiscalSalesType());
		Select policyLang = new Select(tpSignUp.TP_SignUp_stp2_policyCurr());
		Select policyDocLang = new Select(tpSignUp.TP_SignUp_stp2_policyLang());
		
		industrySelect.selectByIndex(1);
		tpSignUp.TP_SignUp_stp2_fiscalSalesTextfield().sendKeys("5000");
		currSelect.selectByIndex(1);
		tpSignUp.TP_SignUp_stp2_fiscalYearEnd().sendKeys(setDate());
		policyLang.selectByIndex(1);
		policyDocLang.selectByIndex(1);
		tpSignUp.TP_SignUp_Submit();
		
		if (tpSignUp.TP_SignUp_Step3Active().isDisplayed())
		{
			Reporter.log("Step 8 - Fill in the contact information required and click next");
		}
		else
		{
			Assert.fail("Not on correct page");
			Reporter.log("Not on correct page");
		}
		
		Select titleSelect = new Select(tpSignUp.TP_SignUp_stp3_Title());
		titleSelect.selectByVisibleText(title);
		
		
		
		Reporter.log("Step 8 - Confirmation screen is displayed.");
		
		Reporter.log("Step 9 - Login to back office, navigate to risk module. Click portfolio. Click insured list");
		BKOF_login(bkOfUsername, bkOfPassword);
		bkOffice.BKOF_MainMenu().click();
		bkOffice.BKOF_MainMenu_Risk().click();

		bkOffice.BKOF_MainMenu_Administration_PplOrg().click();
		bkOffice.BKOF_MainMenu_Administration_PplOrg_InsuredList().click();

		bkOffice.BKOF_Risk_Portfolio().click();
		bkOffice.BKOF_Risk_Portfolio_InsuredList();
		if (bkOffice.BKOF_Risk_Portfolio_InsuredList_SearchResults().isDisplayed()) {
			Reporter.log("Step 9 - Insured list is displayed");
		} else {
			Assert.fail("Incorrect Page");
		}
		Reporter.log("Step 10 - Click Create an insured");
		bkOffice.BKOF_Risk_Portfolio_InsuredList_CreateInsurerBtn().click();

		if (bkOffice.BKOF_Risk_Portfolio_InsuredList_CreateInsurer_header().isDisplayed()) {
			Reporter.log("Step 10 - Create an insured screen is diplayed");
		} else {
			Assert.fail("Incorrect Page");
		}
		Reporter.log("Step 11 - Search for company");
		bkOffice.BKOF_Risk_Portfolio_InsuredList_CreateInsurer_SubmitBtn().click();
		if (bkOffice.BKOF_Risk_Portfolio_InsuredList_CreateInsurer_ManualCreateLink().isDisplayed()) {
			Reporter.log("Step 11 - Company not found, manual identification option is displayed");
		} else {
			Assert.fail("Link not present");
		}

		Reporter.log("Step 12 - Choose manual identification, and enter information as per request. Click submit");
		bkOffice.BKOF_Risk_Portfolio_InsuredList_CreateInsurer_CompanyName().sendKeys(compName + "123");
		Select actCode = new Select(bkOffice.BKOF_Risk_Portfolio_InsuredList_CreateInsurer_ActivityCode());
		actCode.selectByIndex(1);
		bkOffice.BKOF_Risk_Portfolio_InsuredList_CreateInsurer_Address().sendKeys(compInfoAddress + " 123");
		bkOffice.BKOF_Risk_Portfolio_InsuredList_CreateInsurer_zipCode().sendKeys(compInfoPostalCode);
		bkOffice.BKOF_Risk_Portfolio_InsuredList_CreateInsurer_city().sendKeys("Neo " + compInfoCity);
		bkOffice.BKOF_Risk_Portfolio_InsuredList_CreateInsurer_phoneNum().sendKeys(compInfoBusNum);
		act.sendKeys(Keys.ENTER);

		if (bkOffice.BKOF_Menu_Header().equals(duns + " - " + compName)) {
			Reporter.log("Step 12 - Insured has been added, user is navigated new insured company page");
		} else {
			softVerify(bkOffice.BKOF_Menu_Header().getText(), duns + " - " + compName,
					"Incorrect page. Header is: " + duns + " - " + compName);
		}

		Reporter.log(
				"Step 13 - Navigate to the contact tab, and add yourself as a contact, provide access and send password");
		bkOffice.BKOF_Menu_Contact().click();
		bkOffice.BKOF_Contact_AddUserBtn().click();

		Select selectTitle = new Select(bkOffice.BKOF_Contact_IDCard_Title());

		selectTitle.selectByIndex(1);
		bkOffice.BKOF_Contact_IDCard_FirstName().sendKeys(firstName);
		bkOffice.BKOF_Contact_IDCard_LastName().sendKeys(lastName);
		bkOffice.BKOF_Contact_IDCard_JobTitle().sendKeys(jobTitle);
		bkOffice.BKOF_Contact_IDCard_Department().sendKeys(department);
		bkOffice.BKOF_Contact_IDCard_PhoneNum().sendKeys(phoneNum);
		bkOffice.BKOF_Contact_IDCard_FaxNum().sendKeys(faxNumber);
		bkOffice.BKOF_Contact_IDCard_Email().sendKeys(email);
		bkOffice.BKOF_Contact_IDCard_SubmitBtn().click();

		Reporter.log(
				"Step 14 - Navigate to the perimeter and check off the following fields\n - In the Portal access section, click the 1.p - Access to TP portal check box.\n - In the EDC TP section, click the EDC_TP20 – Users Management check box.\n\n Click Submit.");
		bkOffice.BKOF_Menu_Perimeter().click();
		bkOffice.BKOF_Perimeter_PortalAccess_AccessToTPChbx().click();
		bkOffice.BKOF_Perimeter_EDCTP_EDCTP20Chbx().click();
		bkOffice.BKOF_Perimeter_SubmitBtn().click();

		if (bkOffice.BKOF_Perimeter_PortalAccess_AccessToTPChbx().isSelected()
				&& bkOffice.BKOF_Perimeter_EDCTP_EDCTP20Chbx().isSelected()) {
			Reporter.log("Step 14 - Modification is saved");
		} else {
			softVerify(bkOffice.BKOF_Perimeter_PortalAccess_AccessToTPChbx().isSelected(), true,
					"Portal Access element not enabled.");
			softVerify(bkOffice.BKOF_Perimeter_EDCTP_EDCTP20Chbx().isSelected(), true, "EDC TP element not enabled.");
		}

		Reporter.log("Step 15 - Click portfolio, policy list");
		bkOffice.BKOF_Risk_Portfolio().click();
		bkOffice.BKOF_Risk_Portfolio_PolicyList().click();

		if (bkOffice.BKOF_Risk_Portfolio_PolicyList_Header().getText().trim().equals("Policy list")) {
			Reporter.log("Step 15 - List of policies is displayed");
		} else {
			Assert.fail("Incorrect page");
		}

		Reporter.log("Step 16 - Click Create policy from template");
		bkOffice.BKOF_Risk_Portfolio_PolicyList_CreatePolicyFromTemplateBtn().click();
		if (bkOffice.BKOF_Risk_Portfolio_PolicyList_CreatePolicFromTeplatePopupTitle().isDisplayed()) {
			Reporter.log("Step 16 - Create policy from template popup is displayed");
		}
		Reporter.log("Step 17 - Fill in template");
		Select modelSelect = new Select(bkOffice.BKOF_Risk_Portfolio_PolicyList_CreatePolicyFromTeplatePopup_ModelSelect());
		if (modelSelect.getFirstSelectedOption().getText() == "TP")
		{
			modelSelect.selectByVisibleText("TP");
		}
		List<WebElement> newInsuredRdo = driver.findElements(newBy2);
		if (newInsuredRdo.get(1).isSelected())
		{
			newInsuredRdo.get(1).click();
		}
		Select insuredDropDown = new Select(bkOffice.BKOF_Risk_Portfolio_PolicyList_CreatePolicyFromTeplatePopup_InsuredDropDown());
		insuredDropDown.selectByVisibleText(compName);
		Select defaultCurr = new Select(bkOffice.BKOF_Risk_Portfolio_PolicyList_CreatePolicyFromTeplatePopup_DefaultCurrDropDown());
		if (defaultCurr.getFirstSelectedOption().getText() != "CAD")
		{
			defaultCurr.selectByVisibleText("CAD");
		}
		Reporter.log(
				"Step 17 - Template: TP\n Create new insured? NO\n Insured: select your newly created insured from the list\n Default currency: Choose desired currency");
		Reporter.log("Step 18 - Click submit");
		bkOffice.BKOF_Risk_Portfolio_PolicyList_CreatePolicyFromTeplatePopup_SubmitBtn().click();
		act.sendKeys(Keys.ENTER);
		
		if (bkOffice.BKOF_Policy_PolicyTitle().getText().contains(compName + " - Policy Id: "))
		{
			Reporter.log("Step 18 - User is navigated to policy page");
		}
		else
		{
			Assert.fail("Landed on incorrect page");
		}
		
		Reporter.log("Step 19 - Add yourself as the contact for the main insured");
		bkOffice.BKOF_PolicyPage_Contact_Email().sendKeys(email);
		bkOffice.BKOF_PolicyPage_Contact_EmailSubmitBtn().click();
		
		Reporter.log("Step 20 - Log into the TP account by populating the email and password from email");
		//TP_login(email, tempPass);
		
		Reporter.log("Step 20 - PH logged into the account.");
		Reporter.log("Step 21 - Terms of Use screen is displayed, checks on 'I accept.' and clicks on CONFIRM button.");
		Reporter.log("Step 21 - Change Password screen is displayed.\n The Password is changed to personal password.");
		Reporter.log(
				"Step 22 - On the Change Password screen, enters the Current password, New Password and Confirm Password, than clicks on CHANGE PASSWORD button.");
		Reporter.log("Step 22 - PH can apply the first limit.");
	}


	private void BKOF_login(String username, String password) {
		try {
			driver.navigate().to("https://cis-qac.edc.ca/grams/do");
			bkOffice.BKOF_Login().sendKeys(username);
			bkOffice.BKOF_Password().sendKeys(password);
			bkOffice.BKOF_LoginBtn().click();
		} catch (NoSuchElementException e) {
			Reporter.log(e.getMessage());
		}
	}

	private void TP_login(String username, String password) {
		driver.get("https://cis-qac.edc.ca/tp/main.do");
		tpLand = new TP_LandingPage(driver);
		tpLand.TP_Landing_Email().sendKeys(username);
		tpLand.TP_Landing_Password().sendKeys(password);
		tpLand.TP_Landing_LoginBtn().click();
		tpMenu = new TP_Menu(driver);
		tpApproval = new TP_ApplyForApprovalPage(driver);
	}

	private String setDate() {
		String date = "";
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 30);
		date = format.format(c.getTime());
		return date;
	}

	private void waitForPageToLoad(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	private void presenceOfElement(By eleBy){
		wait.until(ExpectedConditions.presenceOfElementLocated(eleBy));
	}
	

	private void sleep(int slp) {
		try {
			Thread.sleep(slp);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
