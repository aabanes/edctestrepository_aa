package cloudpi.testui.testcases;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.Test;

import cloudpi.testTPI.pages.LoginPage;
import cloudpi.testTPI.pages.TPI_PortfolioDetailPage;
import cloudpi.testTPI.pages.TPI_RepositoryElements;
import cloudpi.testTPI.pages.TPI_CreditApprovalRequestPage;
import cloudpi.testTPI.pages.TPI_ManageAddBuyerPage;
import cloudpi.testTPI.utility.ExcelUtils;
import cloudpi.testbkof.pages.EDC_BackOffice;
import cloudpi.testui.utility.EWSJavaAPI;

public class Apply_for_a_new_credit_limit extends cloudpi.testui.testLibs.BaseTest {
	private LoginPage tpiLogin = null;
	private TPI_PortfolioDetailPage tpiPortfolioDetail = null;
	private TPI_CreditApprovalRequestPage tpiLimitReq = null;
	private TPI_ManageAddBuyerPage tpiManageAddBuyer = null;
	private EDC_BackOffice bkOffice = null;
	private String bkOfUsername, bkOfPassword, compName, tpiUsername, tpiPassword, dunsNum, preTotalExposure, postTotalExposure, insCreditLimit, reqCreditAppr, reqCreditApprCurr, maxPayTerms, coveragePeriod,
			buyerCredRating, coInsRatio, comments, province, country, lookup, date, readMailUsr, readMailPass, readMailDomain, readMailEmail, readMailSubject, readMailSubject2;
	private ExcelUtils excel = null;
	private TPI_RepositoryElements repEle = null;
	private By newBy;
	private Wait<WebDriver> wait;
	private EWSJavaAPI readMail = null;

	@Test(priority = 1, enabled = true)
	public void apply_for_new_credit_limit() {

		bkOffice = new EDC_BackOffice(driver);
		tpiLogin = new LoginPage(driver);
		try {
			tpiPortfolioDetail = new TPI_PortfolioDetailPage(driver);
			tpiManageAddBuyer = new TPI_ManageAddBuyerPage(driver);
			tpiLimitReq = new TPI_CreditApprovalRequestPage(driver);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		wait = new WebDriverWait(driver, 60);
		excel = new ExcelUtils();
		readMail = new EWSJavaAPI();
		
		setDate("dd/MM/YYYY", 0);
		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(1, 0);
			bkOfPassword = excel.getCellData(1, 1);
			tpiUsername = excel.getCellData(2, 0);
			tpiPassword = excel.getCellData(2, 1);

			excel.getExcelFile("./testData/TestData.xlsx", "Sheet2");
			country = excel.getCellData(2, 4);
			compName = excel.getCellData(2, 2);
			province = excel.getCellData(2, 9);
			dunsNum = excel.getCellData(2, 1);
			
			excel.getExcelFile("./testData/TestData.xlsx", "Sheet4");
			readMailUsr = excel.getCellData(3, 0);
			readMailPass = excel.getCellData(3, 1);
			readMailDomain = excel.getCellData(3, 2);
			readMailEmail = excel.getCellData(3, 3);
			readMailSubject = excel.getCellData(3, 4);
			readMailSubject2 = excel.getCellData(4, 4);
			
			System.out.println(readMailUsr + " " + readMailPass + " " + readMailDomain + " " + readMailEmail + " " + readMailSubject + " " + readMailSubject2);

			excel.getExcelFile("./testData/TestData.xlsx", "Sheet5");
			insCreditLimit = excel.getCellData(0, 1);
			reqCreditAppr = excel.getCellData(1, 1);
			reqCreditApprCurr = excel.getCellData(2, 1);
			maxPayTerms = excel.getCellData(3, 1);
			coveragePeriod = excel.getCellData(4, 1);
			buyerCredRating = excel.getCellData(5, 1);
			coInsRatio = excel.getCellData(6, 1);
			comments = excel.getCellData(10, 1);
		
			repEle = new TPI_RepositoryElements("../EDCTest_BackOffice/config/BackOffice_Repository.properties");
			newBy = repEle.byLocator("BKOF_Risk_Tasks_TasksTable");

		} catch (Exception e) {
			e.printStackTrace();
		}

		Reporter.log(
				"Begin Test: Apply for a new credit limit - buyer is Canadian company that exists in DNB, and it has buyer rating (1)");

		Reporter.log("Step 1 - Login to front portal");
		try {
			tpiLogin.signAccount(tpiUsername, tpiPassword);
			tpiManageAddBuyer.openAddBuyer();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Select selCountry = new Select(tpiManageAddBuyer.countrySelectDropBox());
		selCountry.selectByVisibleText(country);
		Select selProv = new Select(tpiManageAddBuyer.stateSelectDropBox());
		selProv.selectByVisibleText(getProvinces(province));
		tpiManageAddBuyer.DUNSNumberTextBox().sendKeys(dunsNum);
		tpiManageAddBuyer.buyerSubmitButton().click();
		tpiManageAddBuyer.SelectBtnOneResult().click();
		
		lookup = tpiPortfolioDetail.getCID();
		
		tpiPortfolioDetail.serviceButton().click();

		Select selPolicyHolder = new Select(tpiPortfolioDetail.coverageSelect());
		if (selPolicyHolder.getFirstSelectedOption().getText().equals("")) {
			Reporter.log(
					"Step 2 - Naviage to buyer as per data setup, click services, ensure that the PH field defaults to blank");
		} else {
			Reporter.log("Step 2 Result - Value exists in Policy Holder dropdown. Value: "
					+ selPolicyHolder.getFirstSelectedOption().getText());
		}

		List<WebElement> policyHolderOptions = selPolicyHolder.getOptions();
		for (WebElement ele : policyHolderOptions) {
			if (ele.getText().contains(compName.toUpperCase())) {
				selPolicyHolder.selectByVisibleText(ele.getText());
				break;
			}
		}

		tpiLimitReq.CreditLimitInc_CreditApprovalRequestBtn().click();
		tpiLimitReq.CreditLimitInc_InsCreditLimit().sendKeys(insCreditLimit);
		tpiLimitReq.CreditLimitInc_ReqCreditApprv().sendKeys(reqCreditAppr);

		Select selReqCreditCurr = new Select(tpiLimitReq.CreditLimitInc_ReqCreditApprvCurr());
		selReqCreditCurr.selectByVisibleText(reqCreditApprCurr);

		tpiLimitReq.CreditLimitInc_MaxPaymentTerms().sendKeys(maxPayTerms);

		Select selCoveragePeriod = new Select(tpiLimitReq.CreditLimitInc_CoveragePeriod());
		selCoveragePeriod.selectByVisibleText(coveragePeriod);

		Select selCoverageRating = new Select(tpiLimitReq.CreditLimitInc_BuyerCreditRating());
		selCoverageRating.selectByVisibleText(buyerCredRating);

		tpiLimitReq.CreditLimitInc_CoInsRatio().sendKeys(coInsRatio);
		tpiLimitReq.CreditLimitInc_Comments().sendKeys(comments);
		tpiLimitReq.CreditLimitInc_SubmitBtn().click();
		tpiLogin.logoutBtn().click();

		
		Reporter.log("Step 3 - Login to back office");
		bkOffice.BKOF_login(bkOfUsername, bkOfPassword);
		bkOffice.BKOF_Lookup().sendKeys(lookup);
		bkOffice.BKOF_Lookup().sendKeys(Keys.ENTER);

		retryingFindClick(By.id("zion-page-title"));
		preTotalExposure = bkOffice.BKOF_Total_Exposure().getText();
		
		Reporter.log("Step 4 - Navigate to limit request");
		bkOffice.BKOF_Menu_Tasks().click();
		List<WebElement> taskTable = driver.findElements(newBy);
		List<WebElement> tasks;
		for (int i = 0; i < taskTable.size(); i++) {
			tasks = taskTable.get(i).findElements(By.tagName("tr"));
			if (tasks.get(i).getText().contains("0 h")) {
				tasks.get(i).click();
			}
		}

		if (bkOffice.BKOF_Task_New_Assignee().getText().equals(bkOfUsername) == false) {
			bkOffice.BKOF_Task_New_Assignee().clear();
			bkOffice.BKOF_Task_New_Assignee().sendKeys(bkOfUsername);
			bkOffice.BKOF_Task_AutoComplete().click();
			bkOffice.BKOF_Task_New_Assignee_SubmitBtn().click();
		} else {
			Assert.fail("New Assignee field missing!");
		}
		
		Reporter.log("Step 5 - Approve the request in full");
		bkOffice.BKOF_Decision_Accept().click();
		bkOffice.BKOF_Decision_SubmitBtn().click();
		bkOffice.BKOF_Decision_CfrmPopup_CfrmSubmitBtn().click();
	
		if (readMail.getEmailBySubject(readMailUsr, readMailPass, readMailDomain, readMailEmail, readMailSubject).contains("Quote"))
		{
			Reporter.log("Step 5 Result - Quote email sent");
		}
		else
		{
			Reporter.log("Step 5 Result - Quote email not sent");
		}
		

		try {
			tpiLogin.signAccount(tpiUsername, tpiPassword);
			tpiManageAddBuyer.openAddBuyer();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Reporter.log("Step 6 - On the portal, navigate to the buyer profile and click services, accept quote using current date");
		retryingFindClick(By.id("select_state"));
		selProv = new Select(tpiManageAddBuyer.stateSelectDropBox());
		selProv.selectByVisibleText(getProvinces(province));
		tpiManageAddBuyer.DUNSNumberTextBox().sendKeys(dunsNum);
		tpiManageAddBuyer.buyerSubmitButton().click();
		tpiManageAddBuyer.SelectBtnOneResult().click();
		tpiPortfolioDetail.serviceButton().click();
		
		selPolicyHolder = new Select(tpiPortfolioDetail.coverageSelect());
		policyHolderOptions = selPolicyHolder.getOptions();
		for (WebElement ele : policyHolderOptions) {
			if (ele.getText().contains(compName.toUpperCase())) {
				selPolicyHolder.selectByVisibleText(ele.getText());
				break;
			}
		}
		
		tpiLimitReq.CreditLimitAcceptQuote().click();
		tpiLimitReq.CreditLimitEffectiveDate().clear();
		tpiLimitReq.CreditLimitEffectiveDate().sendKeys(date);
		tpiLimitReq.CreditLimitSubmitBtn().click();
		retryingFindClick(By.xpath("//button[@title='Close']"));
		
		String reinsEmail = readMail.getEmailBySubject(readMailUsr, readMailPass, readMailDomain, readMailEmail, readMailSubject2).replaceAll("\\s+\\s","");
		DecimalFormat dblFormatter = new DecimalFormat("#,###");
		double credLimit = Double.valueOf(insCreditLimit);
		double credApproval = Double.valueOf(reqCreditAppr);
		String credLimitInString = reqCreditApprCurr + " " + dblFormatter.format(credLimit);
		String credApprovalInString = reqCreditApprCurr + " " + dblFormatter.format(credApproval);
		
		if (reinsEmail.contains("Insurer Credit Limit:" + credLimitInString))
		{
			Reporter.log("Step 6 - Result - Reinsurance certification email reflects correct Insurer Credit Limit");
		}
		else
		{
			Reporter.log("Step 6 - Result - Reinsurance certification email does not reflect correct Insurer Credit Limit");
		}
		if(reinsEmail.contains("EDC Credit Limit:" + credApprovalInString))
		{
			Reporter.log("Step 7 - Result - Reinsurance certification email reflects correct EDC Credit Limit");
		} 
		else
		{
			Reporter.log("Step 7 - Result - Reinsurance certification email does not reflect correct EDC Credit Limit");
		}
		
		tpiPortfolioDetail.detail_CoverageTab().click();
		Select selPolHolder = new Select(tpiPortfolioDetail.CoverageTabSelect());
		policyHolderOptions = selPolHolder.getOptions();
		for (WebElement ele : policyHolderOptions) {
			if (ele.getText().contains(compName.toUpperCase())) {
				selPolHolder.selectByVisibleText(ele.getText());
				break;
			}
		}
		
		if (tpiPortfolioDetail.CoveragePolicyAmt().getText() == dblFormatter.format(credApproval) + " " + reqCreditApprCurr)
		{
			Reporter.log("Step 8 - Result - Coverage properly reflected.");
		}
		else
		{
			Reporter.log("Step 8 - Result - Coverage not properly reflected. Coverage is: " + tpiPortfolioDetail.CoveragePolicyAmt().getText());
		}
		
		bkOffice.BKOF_login(bkOfUsername, bkOfPassword);
		bkOffice.BKOF_Lookup().sendKeys(lookup);
		bkOffice.BKOF_Lookup().sendKeys(Keys.ENTER);
		retryingFindClick(By.id("zion-page-title"));
		postTotalExposure = bkOffice.BKOF_Total_Exposure().getText();
		
		int preTExp, postTExp, creditAmt;
		preTExp = Integer.valueOf(preTotalExposure.replace("K", "").trim());
		postTExp = Integer.valueOf(postTotalExposure.replace("K", "").trim());
		
		creditAmt = Integer.valueOf(reqCreditAppr.substring(0, 2).trim());
		if (postTExp == preTExp + creditAmt)
		{
			Reporter.log("Step 9 - Total Exposure reflects new limit");
		}
		else
		{
			Reporter.log("Step 9 Result - Total Exposure not properly reflected. \n Expected Exposure: " + preTExp + creditAmt + "\n Actual Exposure: " + postTExp);
		}
	}

	private void waitForPageToLoad(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	private String getProvinces(String province)
	{
		String result = "";
		Map<String, String> provinceMap = new HashMap<String, String>();
		provinceMap.put("AB", "ALBERTA");
		provinceMap.put("BC", "BRITISH COLUMBIA");
		provinceMap.put("MB", "MANITOBA");
		provinceMap.put("NB", "NEW BRUNSWICK");
		provinceMap.put("NL", "NEWFOUNDLAND AND LABRADOR");
		provinceMap.put("NS", "NOVA SCOTIA");
		provinceMap.put("NT", "NORTHWEST TERRITORIES");
		provinceMap.put("NU", "NUNAVUT");
		provinceMap.put("ON", "ONTARIO");
		provinceMap.put("PE", "PRINCE EDWARD ISLAND");
		provinceMap.put("QC", "QUEBEC");
		provinceMap.put("SK", "SASKATCHEWAN");
		provinceMap.put("YT", "YUKON");
		for (@SuppressWarnings("unused") String key : provinceMap.keySet())
		{
			if (provinceMap.containsKey(province))
			{
				result = provinceMap.get(province);
				break;
			}
		}
		return result;
	}
	
	private void setDate(String dateFormat, int addDays) {
		System.out.println(dateFormat);
		date = dateFormat.replace("DD", "dd");
		SimpleDateFormat format = new SimpleDateFormat(date);
		Calendar c = Calendar.getInstance();
		if (addDays != 0) {
			c.add(Calendar.DATE, addDays);
		}
		date = format.format(c.getTime());
	}
	
	public boolean retryingFindClick(By by) {
		boolean result = false;
		int attempts = 0;
		while (attempts < 10) {
			try {
				driver.findElement(by).click();
				result = true;
				break;
			} catch (StaleElementReferenceException e) {
			}
			attempts++;
		}
		return result;
	}
}