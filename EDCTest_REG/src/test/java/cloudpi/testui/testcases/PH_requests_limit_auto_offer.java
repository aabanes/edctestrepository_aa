package cloudpi.testui.testcases;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.thoughtworks.selenium.webdriven.commands.WaitForPageToLoad;

import cloudpi.testbkof.pages.EDC_BackOffice;
import cloudpi.testTP.pages.RepositoryElements;
import cloudpi.testTP.pages.TP_ApplyForApprovalPage;
import cloudpi.testTP.pages.TP_LandingPage;
import cloudpi.testTP.pages.TP_Menu;
import cloudpi.testTP.pages.TP_PortfolioPage;
import cloudpi.testTPI.pages.TPI_RepositoryElements;
import cloudpi.testTPI.utility.ExcelUtils;

public class PH_requests_limit_auto_offer extends cloudpi.testui.testLibs.BaseTest {

	private TP_LandingPage tpLand = null;
	private TP_Menu tpMenu = null;
	private TP_PortfolioPage tpPortfolio = null;
	private TP_ApplyForApprovalPage tpApproval = null;
	private String policyNum, bkOfUsername, bkOfPassword, tpUsername, tpPassword, compName, address, city, countryName, stateProvName, postalCode, dunsNum;
	private RepositoryElements repEle = null;
	private By newBy = null;
	private By newBy2 = null;
	private EDC_BackOffice bkOffice = null;
	private ExcelUtils excel = null;
	private Wait<WebDriver> wait;

	@Test(priority = 1, enabled = true)
	public void PH_requests_a_new_limit_with_auto_offer() {

		tpLand = new TP_LandingPage(driver);
		tpMenu = new TP_Menu(driver);
		tpApproval = new TP_ApplyForApprovalPage(driver);
		excel = new ExcelUtils();
		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(2, 0);
			bkOfPassword = excel.getCellData(2, 1);
			tpUsername = excel.getCellData(3, 0);
			tpPassword = excel.getCellData(3, 1);

			excel.getExcelFile("./testData/TestData.xlsx", "Sheet2");
			compName = excel.getCellData(1,2);
			address = excel.getCellData(1,7);
			city = excel.getCellData(1,8);
			countryName = excel.getCellData(1,4);
			stateProvName = excel.getCellData(1,9);
			
			repEle = new RepositoryElements("../EDC_TP/config/TP_ApplyForApproval_Repository.properties");
			newBy = repEle.byLocator("TP_ApplyForApproval_stp1_SearchPane");
			newBy2 = repEle.byLocator("TP_ApplyForApproval_osOffer_agreement");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 60);
		String date = new SimpleDateFormat("dd/MM/yyyy").format(new Date());

		Reporter.log("Beginning test case - PH requests a new limit with auto-offer and pay the premium (Happy Flow) (1)");
		driver.navigate().to("https://cis-qac.edc.ca/tp");
		tpLand.TP_Landing_Email().sendKeys(tpUsername);
		tpLand.TP_Landing_Password().sendKeys(tpPassword);
		tpLand.TP_Landing_LoginBtn().click();

		if (verify(tpMenu.TP_MenuNav().isDisplayed(), true)) {
			Reporter.log("Successfully logged in");
		} else {
			Reporter.log("Unsuccessfully logged in - Menu not found");
			Assert.fail("Unsuccessfully logged in - Menu not found");
		}

		tpMenu.TP_Menu_ApplyForApprovalButton().click();

		// Checking to see if we're at step 1 and, if so, populate fields.
		if (verify(tpApproval.TP_ApplyForApproval_Step1Active().isDisplayed(), true)) {
			Reporter.log("@ Apply For A Approval - Step 1");


			tpApproval.TP_ApplyForApproval_stp1_nameTextfield().sendKeys(compName);
			tpApproval.TP_ApplyForApproval_stp1_addressTextfield().sendKeys(address);
			tpApproval.TP_ApplyForApproval_stp1_cityTextField().sendKeys(city);

			Select country = new Select(tpApproval.TP_ApplyForApproval_stp1_countryDropDown());
			country.selectByVisibleText(countryName);
			
			if (countryName.contains("UNITED STATES") == true)
			{
				waitForPageToLoad(tpApproval.TP_ApplyForApproval_stp1_stateDropDown());
				Select state = new Select(tpApproval.TP_ApplyForApproval_stp1_stateDropDown());
				state.selectByVisibleText(stateProv(stateProvName).toUpperCase());
			}

			tpApproval.TP_ApplyForApproval_stp1_SearchButton().click();
			
			List<WebElement> searchPane = driver.findElements(newBy);
			for(int i=0; i < searchPane.size(); i++)
			{
				System.out.println(searchPane.get(i).findElement(By.tagName("h3")).getText().equals(compName.toUpperCase()));
				if (searchPane.get(i).findElement(By.tagName("h3")).getText().equals(compName.toUpperCase()))
				{
					searchPane.get(i).findElement(By.tagName("h3")).click();
					break;
				}
			}

		} else {
			Assert.fail("Not at correct step!");
		}

		// Checking to see if we're at step 2 and, if so, populate fields.
		if (verify(tpApproval.TP_ApplyForApproval_Step2Active().isDisplayed(), true)) {
			Reporter.log("@ Apply For A Approval - Step 2");

			// Ideally this would be from a file and not hardcoded
			tpApproval.TP_ApplyForApproval_stp2_creditLimitTextField().sendKeys("5000");
			tpApproval.TP_ApplyForApproval_stp2_coveragePeriod90().click();
			tpApproval.TP_ApplyForApproval_stp2_paymentTerms90().click();
			tpApproval.TP_ApplyForApproval_stp2_nextButton().click();
		} else {
			Assert.fail("Not at correct step!");
		}

		// Checking to see if we're at step 3 and, if so, populate fields.
		if (verify(tpApproval.TP_ApplyForApproval_Step3Active().isDisplayed(), true)) {
			Reporter.log("@ Apply For A Approval - Step 3");
			tpApproval.TP_ApplyForApproval_stp3_goodsExportedYes().click();
			tpApproval.TP_ApplyForApproval_stp3_negativeInfoNo().click();
			tpApproval.TP_ApplyForApproval_stp3_sellingCustNo().click();
			tpApproval.TP_ApplyForApproval_stp3_agreement().click();
			tpApproval.TP_ApplyForApproval_stp3_submitButton().click();
		} else {
			Assert.fail("Not at correct step!");
		}

		// Checking to see if we're at step 4 and, if so, populate fields.
		if (verify(tpApproval.TP_ApplyForApproval_Step4Active().isDisplayed(), true)) {
			Reporter.log("@ Apply For A Approval - Step 4");
			try {
				sleep(10);
			} catch (InterruptedException e) {
				Reporter.getCurrentTestResult().setStatus(ITestResult.FAILURE);
				Reporter.log(e.getMessage());
				e.printStackTrace();
			}
		} else {
			Assert.fail("Not at correct step!");
		}

//		// Outstanding Order field. Verify values are as expected. Again, should
//		// be from a file.
		waitForPageToLoad(tpApproval.TP_ApplyForApproval_osOffer_agreement());
		if (verify(tpApproval.TP_ApplyForApproval_osOffer_header().getText().equals("Outstanding Offer"), true)) {
			Reporter.log("@ On Outstanding Offers page - Validating Offer");
			
			validateItems(
					tpApproval.TP_ApplyForApproval_osOffer_eDCApprvOffer_decision().getText().equals("Full Approval"),
					tpApproval.TP_ApplyForApproval_osOffer_eDCApprvOffer_decision().getText());
			validateItems(
					tpApproval.TP_ApplyForApproval_osOffer_eDCApprvOffer_creditLimit().getText().equals("USD 5,000.00"),
					tpApproval.TP_ApplyForApproval_osOffer_eDCApprvOffer_creditLimit().getText());
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			Calendar c = Calendar.getInstance();
			c.set(Calendar.DATE, 90);
			String newDate = format.format(c.getTime());
			validateItems(tpApproval.TP_ApplyForApproval_osOffer_eDCApprvOffer_coveragePeriod().equals(newDate), tpApproval.TP_ApplyForApproval_osOffer_eDCApprvOffer_coveragePeriod().getText());
			
			c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
			format = new SimpleDateFormat("yyyy/MM/dd");
			newDate = format.format(c.getTime());
			validateItems(
					tpApproval.TP_ApplyForApproval_osOffer_policyProfile_policyEffectiveDate().getText()
							.equals(newDate),
					tpApproval.TP_ApplyForApproval_osOffer_policyProfile_policyEffectiveDate().getText());
			
			retryingFindClick(newBy2);
			tpApproval.TP_ApplyForApproval_osOffer_acceptPayButton().click();
			
		} else {
			Assert.fail("Not at Outstanding Offer page!");
		}

		if (verify(tpApproval.TP_ApplyForApproval_osOffer_CCheader().getText().equals("Outstanding Offer Payment"),
				true)) {
			driver.switchTo().frame("frameMoneris");
			tpApproval.TP_ApplyForApproval_osOffer_cardholder().sendKeys("Test");
			tpApproval.TP_ApplyForApproval_osOffer_creditCardNum().sendKeys("454545454545454545");
			tpApproval.TP_ApplyForApproval_osOffer_expiryDate().sendKeys("10/2016");
			tpApproval.TP_ApplyForApproval_osOffer_acceptPayButton().click();
		} else {
			Assert.fail("Not at Outstanding Offer Payment page!");
		}

		driver.switchTo().defaultContent();


	}

	private boolean isNumeric(String num) {
		Reporter.log("Validating Reference Number (if contains only numeric characters)");
		try {
			double d = Double.parseDouble(num);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	private void validateItems(boolean result, String text) {
		if (result == false) {
			Reporter.getCurrentTestResult().setStatus(ITestResult.FAILURE);
			Reporter.log("Element that failed validation: " + text);
		} else {
			Reporter.getCurrentTestResult().setStatus(ITestResult.SUCCESS);
			Reporter.log("Element that passed validation: " + text);
		}
	}
	
	private String stateProv (String stateProvince){
		String res = "";
		Map<String, String> states = new HashMap<String, String>();
		states.put("AL", "Alabama");
		states.put("AK", "Alaska");
		states.put("AB", "Alberta");
		states.put("AS", "American Samoa");
		states.put("AZ", "Arizona");
		states.put("AR", "Arkansas");
		states.put("AE", "Armed Forces (AE)");
		states.put("AA", "Armed Forces Americas");
		states.put("AP", "Armed Forces Pacific");
		states.put("BC", "British Columbia");
		states.put("CA", "California");
		states.put("CO", "Colorado");
		states.put("CT", "Connecticut");
		states.put("DE", "Delaware");
		states.put("DC", "District Of Columbia");
		states.put("FL", "Florida");
		states.put("GA", "Georgia");
		states.put("GU", "Guam");
		states.put("HI", "Hawaii");
		states.put("ID", "Idaho");
		states.put("IL", "Illinois");
		states.put("IN", "Indiana");
		states.put("IA", "Iowa");
		states.put("KS", "Kansas");
		states.put("KY", "Kentucky");
		states.put("LA", "Louisiana");
		states.put("ME", "Maine");
		states.put("MB", "Manitoba");
		states.put("MD", "Maryland");
		states.put("MA", "Massachusetts");
		states.put("MI", "Michigan");
		states.put("MN", "Minnesota");
		states.put("MS", "Mississippi");
		states.put("MO", "Missouri");
		states.put("MT", "Montana");
		states.put("NE", "Nebraska");
		states.put("NV", "Nevada");
		states.put("NB", "New Brunswick");
		states.put("NH", "New Hampshire");
		states.put("NJ", "New Jersey");
		states.put("NM", "New Mexico");
		states.put("NY", "New York");
		states.put("NF", "Newfoundland");
		states.put("NC", "North Carolina");
		states.put("ND", "North Dakota");
		states.put("NT", "Northwest Territories");
		states.put("NS", "Nova Scotia");
		states.put("NU", "Nunavut");
		states.put("OH", "Ohio");
		states.put("OK", "Oklahoma");
		states.put("ON", "Ontario");
		states.put("OR", "Oregon");
		states.put("PA", "Pennsylvania");
		states.put("PE", "Prince Edward Island");
		states.put("PR", "Puerto Rico");
		states.put("PQ", "Quebec");
		states.put("RI", "Rhode Island");
		states.put("SK", "Saskatchewan");
		states.put("SC", "South Carolina");
		states.put("SD", "South Dakota");
		states.put("TN", "Tennessee");
		states.put("TX", "Texas");
		states.put("UT", "Utah");
		states.put("VT", "Vermont");
		states.put("VI", "Virgin Islands");
		states.put("VA", "Virginia");
		states.put("WA", "Washington");
		states.put("WV", "West Virginia");
		states.put("WI", "Wisconsin");
		states.put("WY", "Wyoming");
		states.put("YT", "Yukon Territory");
		for (String key : states.keySet())
		{
			System.out.println(states);
			if (states.containsKey(stateProvince))
			{
				res = states.get(stateProvince);
				break;
			}
		}
		return res;
	}
	
	private void waitForPageToLoad(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	public boolean retryingFindClick(By by) {
		boolean result = false;
		int attempts = 0;
		while (attempts < 10) {
			try {
				driver.findElement(by).click();
				result = true;
				break;
			} catch (StaleElementReferenceException e) {
			}
			attempts++;
		}
		return result;
	}
}
