package cloudpi.testui.testcases;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Reporter;
import org.testng.annotations.Test;
import cloudpi.testTPI.pages.TPI_PortfolioPage;
import cloudpi.testARI.pages.RepositoryElements;
import cloudpi.testARI.utility.EWSJavaAPI;
import cloudpi.testARI.utility.ExcelUtils;
import cloudpi.testTPI.pages.LoginPage;
import cloudpi.testbkof.pages.EDC_BackOffice;

public class PV_TPI_51_006_PI extends cloudpi.testARI.testLibs.BaseTest {
	private LoginPage tpiLogin = null;
	private EDC_BackOffice bkOffice = null;
	private TPI_PortfolioPage tpiPortfolio = null;
	private ExcelUtils excel = null;
	private RepositoryElements repEle = null;
	private By newBy = null, newBy2 = null, newBy3 = null, newBy4 = null;
	private String bkOfUsername, bkOfPassword, firstName, lastName, email, newUsrLogin, compName,
			readLoginID, readPassword, readDomain, readEmailSubject, tempPass;
	private Wait<WebDriver> wait = null;
	
	@Test(priority = 1, enabled = true)
	public void PV_TPI_51_006_PI_Admin_creates_a_new_user_account() {
		tpiLogin = new LoginPage(driver);
		bkOffice = new EDC_BackOffice(driver);
		try {
			tpiPortfolio = new TPI_PortfolioPage(driver);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		excel = new ExcelUtils();
		
		wait = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS).ignoring(StaleElementReferenceException.class);
		try {
			excel.getExcelFile("./testData/TestData.xlsx", "datasheet01");
			bkOfUsername = excel.getCellData(1, 0);
			bkOfPassword = excel.getCellData(1, 1);

			excel.getExcelFile("./testData/TestData.xlsx", "Sheet2");
			compName = excel.getCellData(3, 2);
			
			excel.getExcelFile("./testData/TestData.xlsx", "Sheet3");
			firstName = excel.getCellData(2, 1);
			lastName = excel.getCellData(2, 2);
			email = excel.getCellData(2, 7);

			excel.getExcelFile("./testData/TestData.xlsx", "Sheet4");
			readLoginID = excel.getCellData(1, 0);
			readPassword = excel.getCellData(1, 1);
			readDomain = excel.getCellData(1, 2);
			readEmailSubject = excel.getCellData(1, 4);

			repEle = new RepositoryElements("../EDCTest_BackOffice/config/BackOffice_Repository.properties");
			newBy = repEle.byLocator("BKOF_Ppl&Org_InsuredList_FilterSearch_Results");
			newBy2 = repEle.byLocator("BKOF_Contact_Messages");
			newBy3 = repEle.byLocator("BKOF_Menu_Contact");
			newBy4 = repEle.byLocator("BKOF_Contact_UserAccess_Active");
		} catch (Exception e) {
			e.printStackTrace();
		}

		Reporter.log(
				"Begin Test: PV TPI-51-006_PI Admin creates a new user account - administrator (3)");
		Reporter.log("Step 1 - Login to back office, navigate to administration module");
		bkOffice.BKOF_login(bkOfUsername, bkOfPassword);
		bkOffice.BKOF_MainMenu().click();
		bkOffice.BKOF_MainMenu_Administration().click();
		
		Reporter.log("Step 2 - Click People & Organisation, click Insured List");
		bkOffice.BKOF_MainMenu_Administration_PplOrg().click();
		bkOffice.BKOF_MainMenu_Administration_PplOrg_InsuredList().click();
		if (bkOffice.BKOF_PplOrg_InsuredList_FilterBtn().isDisplayed()) {
			Reporter.log("Step 2 Results - User is navigated to insured list");
		} else {
			Assert.fail("Step 2 Results - Insured List is not present");
			Reporter.log("Step 2 Results - Insured List is not present");
		}
		
		//Search for Company
		Reporter.log("Step 3 - Find the Private insurer of your choice, naviage to PI page");
		bkOffice.BKOF_PplOrg_InsuredList_FilterBtn().click();
		bkOffice.BKOF_PplOrg_InsuredList_FilterSearch_Name().sendKeys(compName);
		bkOffice.BKOF_PplOrg_InsuredList_FilterSearch_SubmitBtn().click();
		
		List<WebElement> resultsPaneEle2 = driver.findElements(newBy);
		List<WebElement> res2;
		for (int i = 0; i < resultsPaneEle2.size(); i++) {
			res2 = resultsPaneEle2.get(i).findElements(By.tagName("tr"));
			
			for (int j = 0; j < res2.size();) {
				if (res2.get(j).findElements(By.tagName("a")).size() > 0) {
					res2.get(j).findElement(By.tagName("a")).click();
					break;
				}
			}
		}
		
		sleep(5);
		Reporter.log("Step 4 - Navigate to the contact tab");
		retryingFindClick(newBy3);
		
		Reporter.log("Step 5 - Click Add");
		bkOffice.BKOF_Contact_AddUserBtn().click();

		if (bkOffice.BKOF_Contact_IDCard_Title().isDisplayed()){
			Reporter.log("Step 5 Result - ID Card is displayed on right side of page");
		}
		else
		{
			Assert.fail("Step 5 Result - ID Card fields not displayed");
			Reporter.log("Step 5 Result - ID Card fields not displayed");
		}
		
		Reporter.log("Step 6 - Enter the last name, first name and email of the new user. Click submit");
		Select selectTitle = new Select(bkOffice.BKOF_Contact_IDCard_Title());
		selectTitle.selectByIndex(1);
		bkOffice.BKOF_Contact_IDCard_FirstName().sendKeys(firstName);
		bkOffice.BKOF_Contact_IDCard_LastName().sendKeys(lastName);
		bkOffice.BKOF_Contact_IDCard_Email().sendKeys(email);
		bkOffice.BKOF_Contact_IDCard_SubmitBtn().click();

		List<WebElement> messageCol = driver.findElements(newBy2);
		List<WebElement> msgs;
		for (int i = 0; i < messageCol.size(); i++) {
			msgs = messageCol.get(i).findElements(By.tagName("div"));

			for (int j = 0; j < msgs.size(); j++) {
				if (msgs.get(j).getText().equals("Contact has been created")) {
					Reporter.log("Step 5 Results - 'Contact has been created' popup appears at top right corner of screen");
					break;
				}
			}
			
			if (i == messageCol.size())
			{
				Reporter.log("Step 5 Results - 'Contact has been created' message not found");
			}
		}	
		
		Reporter.log("Step 7 - Click provide user access");
		bkOffice.BKOF_Contact_IDCard_ProvideUserAccessBtn().click();
		if (bkOffice.BKOF_Contact_UserAccessTab().isDisplayed()) {
			Reporter.log("Step 7 Result - User access tab is displayed on right side of page");
		} else {
			Assert.fail("Step 7 Result - Tab not present");
			Reporter.log("Step 7 Result - Tab not present");
		}

		sleep(5);
		Reporter.log("Step 8 - Check the box next to active, select user language, click add under the profiles box, on popup select TPI user, click submit to close popup.");
		bkOffice.BKOF_Contact_UserAccessTab().click();
		retryingFindClick(newBy4);
		
		if(bkOffice.BKOF_Contact_UserAccess_Active().isSelected()==false){
			bkOffice.BKOF_Contact_UserAccess_Active().click();
		}

		bkOffice.BKOF_Contact_UserAccess_AddProfileBtn().click();
		Select profileDrop = new Select(bkOffice.BKOF_Contact_UserAccess_AddProfile_ProfileDropDown());
		profileDrop.selectByVisibleText("TPI USER");

		bkOffice.BKOF_Contact_UserAccess_AddProfile_SubmitBtn().click();
		bkOffice.BKOF_Contact_UserAccess_SubmitBtn().click();
		
		retryingFindClick(newBy2);
		messageCol = driver.findElements(newBy2);
		for (int i = 0; i < messageCol.size(); i++) {
			msgs = messageCol.get(i).findElements(By.tagName("div"));

			for (int j = 0; j < msgs.size(); j++) {
				if (msgs.get(j).getText().equals("Modification saved")) {
					Reporter.log("Step 8 Results - 'Modification saved' popup appears at top right corner of screen");
					break;
				}
			}
			
			if (i == messageCol.size())
			{
				Reporter.log("Step 8 Results - 'Modification has been saved' popup does not appear");
			}
		}

		Reporter.log("Step 9 - Click Submit");
		bkOffice.BKOF_Contact_UserAccess_BeforeSubmitBtn().click();
		newUsrLogin = bkOffice.BKOF_Contact_UserAccess_Login().getAttribute("value");
		
		retryingFindClick(newBy2);
		messageCol = driver.findElements(newBy2);
		for (int i = 0; i < messageCol.size(); i++) {
			msgs = messageCol.get(i).findElements(By.tagName("div"));

			for (int j = 0; j < msgs.size(); j++) {
				if (msgs.get(j).getText().equals("User access created")) {
					Reporter.log("Step 9 Results - 'User access created' popup appears at top right corner of screen");
					break;
				}
			}
			
			if (i == messageCol.size())
			{
				Reporter.log("Step 9 Results - 'User access created' popup does not appear");
			}
		}
		
		Reporter.log("Step 10 - Click New password");
		bkOffice.BKOF_Contact_UserAccess_NewPassword().click();
		Alert alert = driver.switchTo().alert();
		alert.accept();

		retryingFindClick(newBy2);
		messageCol = driver.findElements(newBy2);
		for (int i = 0; i < messageCol.size(); i++) {
			msgs = messageCol.get(i).findElements(By.tagName("div"));

			for (int j = 0; j < msgs.size(); j++) {
				if (msgs.get(j).getText().equals("A new password has been sent to the user")) {
					Reporter.log("Step 10 Result - 'New password has been sent to the user' popup appears at the top right corner of screen. New user receives email with login information");
					break;
				}
			}
			
			if (i == messageCol.size())
			{
				Reporter.log("Step 10 Result - Message not present");
			}
		}
		
		try {
			EWSJavaAPI emailRead = new EWSJavaAPI();
			tempPass = emailRead.getTempPassword(readLoginID, readPassword, readDomain, email, readEmailSubject);
		} catch (Exception e) {
			Reporter.log("Error retrieving temporary password from mail. \n" + e.getMessage());
		}
		
		bkOffice.BKOF_ExitBtn().click();
		alert = driver.switchTo().alert();
		alert.accept();
		
		Reporter.log("Create new password, click submit");
		try {
			Reporter.log("Step 11 - Navigate to TPI front portal");
			tpiLogin.pageOpen();
			
			Reporter.log("Step 12 - Login using login information from email");
			System.out.println(newUsrLogin + " " + tempPass);
			tpiLogin.signAccount(newUsrLogin, tempPass);
			Reporter.log("Step 12 Result - User is logged in, change password screen is displayed");
			Reporter.log("Step 13 - Create new password, click submit");
			tpiLogin.signInWithTempPass(tempPass, "1Test123", "1Test123");
			
			if (tpiPortfolio.buyerSearchName().isDisplayed())
			{
				Reporter.log("Step 13 Result - User is navigated to buyer portfolio");
			}
			else
			{
				Assert.fail("Step 13 Result - User not navigated to buyer portfolio");
				Reporter.log("Step 13 Result - User not navigated to buyer portfolio");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void presenceOfElement(By eleBy){
		wait.until(ExpectedConditions.presenceOfElementLocated(eleBy));
	}
	
	private void sleep(int slp) {
		try {
			Thread.sleep(slp);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public boolean retryingFindClick(By by) {
		boolean result = false;
		int attempts = 0;
		while (attempts < 10) {
			try {
				driver.findElement(by).click();
				result = true;
				break;
			} catch (StaleElementReferenceException e) {
			}
			attempts++;
		}
		return result;
	}
}